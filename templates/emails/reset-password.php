<?php
/**
 * This template is sent to customer when password reset is requested in admin area
 *
 * @since 2019.6
 *
 * @var $object_id
 */
$store    = new Eso_Store();
$customer = new Eso_Customer( $object_id );

eso_email_template_header( __( "Požadavek na změnu hesla", "eso" ) ); ?>
    <h1><?php _e( "Požadavek na změnu hesla", "eso" ) ?></h1>
    <p>
		<?php echo __( "Byl zadán požadavek na změnu hesla k Vašemu účtu na e-shopu", "eso" ) . " <a href='" . get_home_url() . "'>" . $store->get_name() . "</a>" ?>
    </p>
    <a class="button"
       href="<?php echo $customer->get_password_reset_url() ?>"><?php _e( "Nastavit nové heslo", "eso" ) ?></a>
    <p>
        <?php _e("Pokud jste změnu hesla nevyžádali, jednoduše tento email ignorujte. Stávající heslo se nezmění.", "eso") ?>
    </p>
<?php eso_email_template_footer(); ?>