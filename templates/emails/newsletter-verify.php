<?php
/**
 * This template is sent to customer after registering for newsletter
 *
 * @var $object_id
 */
$store    = new Eso_Store();
$subscriber = new Eso_Newsletter_Subscriber( $object_id );

eso_email_template_header( $store->get_name() . " - " . __("Přihlášení k odběru novinek", "eso" ) ); ?>

	<h1><?php _e( "Děkujeme Vám za přihlášení k odběru novinek na našem e-shopu ". $store->get_name(), "eso" ) ?></h1>
	<br>
	<p>
		<?php echo __( "Klikněte prosím na odkaz níže pro potvrzení odběru novinek pro adresu ", "eso" ) ?><strong><?php echo $subscriber->get_email() ?></strong>
	</p>
	<a class="button"
	   href="<?php echo $subscriber->get_verify_url() ?>"><?php _e( "Potvrdit odběr", "eso" ) ?>
	</a>
    <p> </p>
	<p>
		<?php _e("Přejeme hezký den", "eso") ?>
	</p>
	<p>
		<?php _e("Tým " . $store->get_name(), "eso") ?>
	</p>
<?php eso_email_template_footer(); ?>