<?php
/**
 * Render related products of product.
 *
 * @since 2020.2.17
 *
 * @var $products array
 * @var $product Eso_Product
 */

if ( ! empty( $products ) ) {
	foreach ( $products as $product_id ) {
		$product = new Eso_Product( $product_id );
		echo $product->get_list_item_template();
	}
}
