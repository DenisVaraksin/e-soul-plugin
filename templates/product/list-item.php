<?php
/**
 * @since 2019.7
 *
 * @var $this Eso_Product
 */
?>
<div class="col-sm-6 col-md-4 col-lg-3 mb-3">
    <a href="<?php the_permalink( $this->get_id() ); ?>" class="card__wrap product-item">
        <div class="product-item__image">
			<?php if ( $this->get_featured_image_id() ) {
				$this->the_featured_image();
			} else {
				echo eso_empty_thumbnail();
			}
			?>
        </div>
		<?php
        if($this->get_tag_id()) {
	        $product_tag = new Eso_Product_Tag( $this->get_tag_id() );
	        $product_tag->render("product-item");
        }
		?>
        <h3 class="product-item__title"><?php echo $this->get_name(); ?></h3>
        <span class="font-weight-bold stock-status--<?php echo $this->get_stock_status_slug() ?>"><?php echo $this->get_stock_status_name(); ?></span>
        <span class="d-block"><?php echo $this->get_category_list(); ?></span>
        <div class="price price--list">
			<?php if ( $this->is_discounted( eso_get_active_currency() ) ) : ?>
                <span class="price-before-discount"><?php echo $this->get_price_before_discount( eso_get_active_currency(), true ) ?></span>
			<?php endif; ?>
            <span class="price__item"><?php echo $this->get_price( eso_get_active_currency(), true, true ); ?></span>
        </div>
    </a>
</div>