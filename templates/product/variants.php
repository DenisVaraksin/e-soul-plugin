<?php
/**
 * Render default version of Eso_Product_Variant for template using Bootstrap classes
 *
 * @since 2020.2.17
 * @var $variants array
 * @var $variant Eso_Product_Variant
 */
foreach ( $variants as $variant_slug => $variant_data ) {
	$variant = new Eso_Product_Variant( $variant_slug, $this );
	?>
    <div class="form-group">
        <label for="<?php echo $variant->get_slug() ?>">
			<?php echo $variant->get_name() ?>
        </label>
        <select name="<?php echo $variant->get_slug() ?>" class="form-control" id="<?php echo $variant->get_slug() ?>">
			<?php foreach ( $variant->get_attributes() as $attribute ) : ?>
                <option value="<?php echo $attribute["slug"] ?>"><?php echo $attribute["slug"] ?></option>
			<?php endforeach; ?>
        </select>
    </div>
<?php }
?>