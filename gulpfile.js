var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var terser = require('gulp-terser');
var rename = require("gulp-rename");

gulp.task('sass', function (cb) {
    gulp
        .src(['assets/css/**/*.scss', 'admin/assets/css/**/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: "compressed"
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(
            gulp.dest(function (f) {
                return f.base;
            })
        );
    cb();
});

gulp.task('js', function () {
    return gulp.src(['assets/js/**/*.js', 'admin/assets/js/**/*.js', '!assets/js/**/*.min.js', '!admin/assets/js/**/*.min.js'])
        .pipe(terser({
            "ecma": 6
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(function(file) {
            return file.base;
        }))
});

gulp.task(
    'default',
    gulp.series('sass', 'js', function (cb) {
        gulp.watch(['assets/css/**/*.scss', 'admin/assets/css/**/*.scss', 'admin/assets/js/**/*.js', 'assets/js/**/*.js', '!assets/js/**/*.min.js', '!admin/assets/js/**/*.min.js'], gulp.series('sass', 'js'));
        cb();
    })
);