<?php
/*
Plugin Name: E-soul
Plugin URI: https://esoul-docs.marketsoul.cz
Description: Kompletní e-commerce řešení pro české eshopy. Pokud je k dispozici nová verze, aktualizujte tento plugin a získávejte tak důležitá vylepšení.
Version: 2020.5.1
Author: Marketsoul s. r. o.
Author URI: https://esoul-docs.marketsoul.cz
Text Domain: eso
Requires at least: 5.0
Tested up to: 5.2.3
Requires PHP: 5.6
Domain Path: /languages/
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $wpdb;

$plugin_data = get_file_data(__FILE__, array('Version' => 'Version'), false);
$plugin_version = $plugin_data['Version'];

defined( 'ESO_VERSION' ) || define( 'ESO_VERSION', $plugin_version );
defined( 'ESO_DIR' ) || define( 'ESO_DIR', dirname( __FILE__ ) );
defined( 'ESO_FILE' ) || define( 'ESO_FILE', __FILE__ );
defined( 'ESO_BASENAME' ) || define( 'ESO_BASENAME', plugin_basename( __FILE__ ) );
defined( 'ESO_NONCE' ) || define( 'ESO_NONCE', 'e-soul-nonce' );
defined( 'ESO_SESSIONS_TABLE' ) || define( 'ESO_SESSIONS_TABLE', $wpdb->prefix . 'eso_sessions' );
defined( 'ESO_CART_TABLE' ) || define( 'ESO_CART_TABLE', $wpdb->prefix . 'eso_cart' );
defined( 'ESO_SHIPPING_ZONE_TABLE' ) || define( 'ESO_SHIPPING_ZONE_TABLE', $wpdb->prefix . 'eso_shipping_zone' );
defined( 'ESO_SHIPPING_METHOD_TABLE' ) || define( 'ESO_SHIPPING_METHOD_TABLE', $wpdb->prefix . 'eso_shipping_method' );
defined( 'ESO_PAYMENT_METHOD_TABLE' ) || define( 'ESO_PAYMENT_METHOD_TABLE', $wpdb->prefix . 'eso_payment_method' );
defined( 'ESO_MODULE_TABLE' ) || define( 'ESO_MODULE_TABLE', $wpdb->prefix . 'eso_module' );
defined( 'ESO_MODULE_DATA_TABLE' ) || define( 'ESO_MODULE_DATA_TABLE', $wpdb->prefix . 'eso_module_data' );
defined( 'ESO_MODULE_CAT_TABLE' ) || define( 'ESO_MODULE_CAT_TABLE', $wpdb->prefix . 'eso_module_category' );
defined( 'ESO_COUPON_TABLE' ) || define( 'ESO_COUPON_TABLE', $wpdb->prefix . 'eso_coupon' );
defined( 'ESO_NEWSLETTER_TABLE' ) || define( 'ESO_NEWSLETTER_TABLE', $wpdb->prefix . 'eso_newsletter' );
defined( 'ESO_URL' ) || define( 'ESO_URL', plugin_dir_url( __FILE__ ) );

require_once( ESO_DIR . '/includes/common.php' );

require_once( ESO_DIR . '/includes/entities/entities.php' );

require_once( ESO_DIR . '/includes/functions.php' );

require_once( ESO_DIR . '/includes/modules/modules.php' );

require_once( ESO_DIR . '/addons/addons.php' );

include( plugin_dir_path( __FILE__ ) . 'vendor/autoload.php' );

if(is_admin()) {
	require_once( ESO_DIR . '/admin/admin.php' );
}

require_once( ESO_DIR . '/includes/autoupdater.php' );

register_activation_hook( __FILE__, 'my_activation_func' ); function my_activation_func() {
	file_put_contents(__DIR__.'/activation_log.txt', ob_get_contents());
}

function eso_plugin_load_plugin_textdomain() {
	load_plugin_textdomain( 'eso', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'eso_plugin_load_plugin_textdomain' );
