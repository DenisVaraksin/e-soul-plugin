(function ($) {
    $(function () {
        //Prevent submit with enter
        $(document).on("keydown", ":input:not(textarea):not(:submit)", function(event) {
            return event.key !== "Enter";
        });

        $(".pill-move-next").click(function (e) {
            if ($(this).attr("id") === "checkout-move-to-summary") {
                let form = document.getElementById("eso-checkout");
                if (form.checkValidity() === false || $("#eso-checkout input.is-invalid").length) {

                    $("#eso-checkout").find("input[required]").each(function () {
                        if ($(this).val() === "") {
                            validateInput($(this), false)
                            console.log($(this).attr("name"));
                        }
                    });

                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#eso-checkout").offset().top
                    }, 1000);

                    e.preventDefault();
                } else {
                    $(".nav-item").removeClass("disabled");

                    refreshCheckoutSummary();
                    moveToNextCheckoutPage();
                }
            } else {
                moveToNextCheckoutPage();
            }
        });

        /**
         * @since 2019.7
         */
        function moveToNextCheckoutPage() {
            $("#cart-nav").find("a.active").closest("li").next("li").find("a").trigger("click");

            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#cart").offset().top - 40
                }, 500);
            }, 300);

        }

        if (window.location.hash) {
            const cart_link = $("#cart-nav").find("a[href='" + window.location.hash + "']");

            if (window.location.hash === "#cart-summary") {
                $(".nav-item").removeClass("disabled");
            } else if (window.location.hash === "#cart-customer") {
                cart_link.parent().removeClass("disabled");
            }

            cart_link.trigger("click");
        }

        $("#cart-nav a").click(function () {
            window.location.hash = $(this).attr("href");
        });

        $(document).on("change", "#cart-customer input, #cart-customer select, #cart-customer textarea", function () {
            if ($(this).data("key")) {
                updateCustomerMeta($(this).data("key"), $(this).val());
            } else if ($(this).attr("type") === "checkbox") {
                if ($(this).is(":checked")) {
                    updateCustomerMeta($(this).attr("id"), $(this).val());
                } else {
                    updateCustomerMeta($(this).attr("id"), 0);
                }
            } else {
                if (!$(this).hasClass("novalidate") && !$(this).is("select")) {
                    validateInput($(this), true);
                }
            }

            if ($(this).hasClass("check-with-billing")) {
                const target = $(this).data("check");
                const target_el = $("#" + target);

                if (target_el.val() === "") {
                    target_el.val($(this).val());
                    updateCustomerMeta(target, $(this).val());
                }
            }
        });

        $(window).on('load', function () {
            $("#cart-customer input[type='text'], #cart-customer input[type='tel'], #cart-customer input[type='email'], #cart-customer select").each(function () {
                if ($(this).val() !== "" && !$(this).hasClass("novalidate") && !$(this).is("select")) {
                    validateInput($(this), false, false);
                }
            });
        });

        /**
         * @since 2019.7
         */
        $("#coupon button").click(function () {
            const thisForm = $("#coupon");
            const code = $("#coupon-code").val();
            const thisButton = $(this);

            updateCustomerMeta("coupon-code", code);

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: {
                    "action": "eso_ajax",
                    "eso_action": "apply_coupon",
                    "code": code
                },
                beforeSend: function () {
                    makeLoading(thisButton);
                    $("#coupon-result").remove();
                },
                success: function (result) {
                    removeLoading(thisButton);
                    thisForm.after("<p id='coupon-result'>" + result + "</p>");
                    refreshCartTotal();
                }
            });
        });

        const checkoutForm = $("#eso-checkout");
        const checkoutSubmitButton = $("#eso-cart-submit");
        const checkoutSubmitButtonValue = checkoutSubmitButton.val();

        $("#agree-terms").change(function() {
           if($(this).is(":checked")) {
               $(this).removeClass("is-invalid");
           }
        });

        /**
         * @since 2020.2.10 required checkbox for terms
         */
        checkoutForm.submit(function (e) {
            e.preventDefault();

            let agreeTerms = $("#agree-terms");
            if(!agreeTerms.is(":checked")) {
                agreeTerms.addClass("is-invalid");
                return false;
            }

            $.ajax({
                url: php_vars["ajaxurl"],
                method: 'POST',
                data: checkoutForm.serialize(),
                beforeSend: function () {
                    makeLoading(checkoutSubmitButton);
                },
                complete: function () {
                },
                error: function () {
                    checkoutSubmitButton.html(checkoutSubmitButtonValue);
                    checkoutSubmitButton.append(php_vars["_error"]);
                },
                success: function (result) {
                    removeLoading(checkoutSubmitButton);
                    window.location.replace(result);
                }
            })
        });

    });
})(jQuery);

/**
 * @since 2019.6
 */
function refreshCartTotal() {
    const summary = jQuery(".order_summary");

    jQuery.ajax({
        url: php_vars["ajaxurl"],
        method: 'POST',
        data: {
            "action": "eso_ajax",
            "eso_action": "render_cart_totals"
        },
        beforeSend: function () {
            summary.addClass("processing");
        },
        error: function (result) {
            console.log(result);
        },
        success: function (result) {
            jQuery(".cart-totals").html(result);
            summary.removeClass("processing");
        }
    });
}

/**
 * @param field_key
 * @param field_value
 */
function updateSummary(field_key, field_value) {
    jQuery("#cart-summary").find("#summary_" + field_key).html(field_value);
}

/**
 * @since 2019.6
 */
function updatePaymentMethods() {
    const checkoutPayment = jQuery("#checkout-payment");
    jQuery.ajax({
        url: php_vars["ajaxurl"],
        method: "POST",
        data: {
            "action": "eso_ajax",
            "eso_action": "update_payment_methods",
            "selected_country": jQuery("#shipping_country").val()
        },
        beforeSend: function () {
            checkoutPayment.addClass("processing");
        },
        success: function (result) {
            checkoutPayment.html(result);
            checkoutPayment.removeClass("processing");

            refreshCheckoutSummary();
            refreshCartTotal();
        }
    });
}

/**
 * @since 2019.6
 */
function updateShippingMethods() {
    const checkoutShipment = jQuery("#checkout-shipment");

    jQuery.ajax({
        url: php_vars["ajaxurl"],
        method: "POST",
        data: {
            "action": "eso_ajax",
            "eso_action": "update_shipping_methods",
            "selected_country": jQuery("#shipping_country").val()
        },
        beforeSend: function () {
            checkoutShipment.addClass("processing");
        },
        success: function (result) {
            checkoutShipment.html(result);
            checkoutShipment.removeClass("processing");
            updatePaymentMethods();
        }
    });
}

/**
 * @since 2019.4
 *
 * @param field_key
 * @param field_value
 */
function updateCustomerMeta(field_key, field_value) {
    jQuery.ajax({
        url: php_vars["ajaxurl"],
        method: "POST",
        data: {
            "action": "eso_ajax",
            "eso_action": "update_customer_meta",
            "field_key": field_key,
            "field_value": field_value,
        },
        success: function (result) {
            updateSummary(field_key, field_value);

            if (field_key === "shipping_country") {
                updateShippingMethods();
                validateInput(jQuery("#shipping_postcode"), false);
            }

            if (field_key === "shipping_method") {
                updatePaymentMethods();
            }

            if (field_key === "shipping_method" || field_key === "payment_method") {
                refreshCartTotal();
            } else {
                refreshCheckoutSummary();
            }

            if (field_key === "billing_country") {
                validateInput(jQuery("#billing_postcode"), false);
            }
        }
    });
}

function refreshCheckoutSummary() {
    jQuery.ajax({
        url: php_vars["ajaxurl"],
        method: "POST",
        data: {
            "action": "eso_ajax",
            "eso_action": "render_checkout_summary",
        },
        success: function (result) {
            jQuery("#checkout-summary").html(result);
        }
    })
}

/**
 * @since 2019.6
 */
function validateInput(thisInput, updateMetaOnSuccess, showWhenSuccess = true) {
    jQuery.ajax({
        url: php_vars["ajaxurl"],
        method: "POST",
        data: {
            "action": "eso_ajax",
            "eso_action": "validate_customer_meta",
            "key": thisInput.attr("id"),
            "value": thisInput.val(),
        },
        error: function (result) {
            console.log(result);
        },
        success: function (result) {
            if (result.length) {

                displayValidationError(thisInput, result);
            } else {
                hideValidationError(thisInput, showWhenSuccess);

                if (updateMetaOnSuccess === true) {
                    updateCustomerMeta(thisInput.attr("id"), thisInput.val());
                }
            }
        }
    });
}

/**
 * @since 2019.6
 *
 * @param thisInput
 * @param result
 */
function displayValidationError(thisInput, result) {
    thisInput.removeClass("is-valid").addClass("is-invalid");
    let form_group = thisInput.closest(".form-group");
    form_group.find(".invalid-feedback").remove();
    form_group.append("<div class='invalid-feedback'>" + result + "</div>");
}

function hideValidationError(thisInput, showWhenSuccess) {
    thisInput.removeClass("is-invalid")
    if (showWhenSuccess) {
        thisInput.addClass("is-valid");

        setTimeout(function () {
            thisInput.removeClass("is-valid");
        }, 3000)
    }
}