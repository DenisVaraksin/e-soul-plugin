(function ($) {
    $(function () {

        'use strict';

        $(".ajax--onready").each(function () {
            processAjax($(this));
        });

        $(window).on('load', function () {
            $(".ajax--onload").each(function () {
                processAjax($(this));
            });
        });

        $(".ajax-form").submit(function (e) {
            e.preventDefault();

            processAjax($(this));
        });

        $(".ajax-form--onchange").change(function (e) {
            processAjax($(this));
        });

        function processAjax(thisForm) {
            const ajaxResult = thisForm.find(".ajax-result");

            const submitButton = thisForm.find("button[type='submit']");
            const successRedirect = thisForm.find("input[name='eso_success_redirect']");
            const successCallback = thisForm.find("input[name='eso_success_callback']");

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: thisForm.serialize(),
                beforeSend: function () {
                    makeLoading(submitButton);
                    if (ajaxResult.length) {
                        makeLoading(ajaxResult);
                    }
                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    removeLoading(submitButton);

                    if (ajaxResult.length) {
                        removeLoading(ajaxResult);
                        ajaxResult.html(result);
                    }

                    if(successCallback.length) {
                        processAjaxFormSuccessCallback(thisForm);
                    }

                    if(successRedirect.length) {
                        window.location.replace(successRedirect.val());
                    }
                }
            });
        }

        function processAjaxFormSuccessCallback(thisForm) {
            const formData = thisForm.serialize() + "&do_success_callback";
            const callbackTarget = $(thisForm.find("input[name='eso_callback_target']").val());

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: formData,
                beforeSend: function() {
                    makeLoading(callbackTarget);
                },
                error: function(result) {
                    console.log(result);
                },
                success: function(result) {
                    callbackTarget.html(result);
                    removeLoading(callbackTarget);
                }
            });
        }

    });

})(jQuery);
