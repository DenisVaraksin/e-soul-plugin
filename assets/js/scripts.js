(function ($) {
    $(function () {

        'use strict';

        $(".currency-switch").click(function () {
            const currency = $(this).data("currency");

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: {
                    "action": "eso_ajax",
                    "eso_action": "currency_switch",
                    "eso-currency-code": currency,
                },
                error: function () {
                    $(".currency-switcher-list").append(php_vars["_error"]);
                },
                success: function (result) {
                    location.reload();
                }
            })
        });

        /**
         * @since 2019.8
         */
        $("input[type=number]").on("change", function () {
            validateNumber($(this));
        });

        /**
         * @since 2019.8
         *
         * @param thisInput
         */
        function validateNumber(thisInput) {
            const min = parseInt(thisInput.attr("min"));
            const max = parseInt(thisInput.attr("max"));
            const value = parseInt(thisInput.val());

            if (min) {
                if (value < min) {
                    thisInput.val(min);
                }
            }

            if (max) {
                if (value > max) {
                    thisInput.val(max);
                }

                if (thisInput.hasClass("eso-product-quantity")
                    || thisInput.hasClass("eso-cart-item-quantity-change")) {

                    if (value >= max) {
                        $.ajax({
                            url: php_vars["ajaxurl"],
                            method: "POST",
                            data: {
                                "action": "eso_ajax",
                                "eso_action": "the_out_of_stock_alert",
                                "product_name": thisInput.data("product-name"),
                                "max": max
                            },
                            success: function (result) {
                                $("body").append(result);
                            }
                        })
                    }

                }
            }
        }
    });

})(jQuery);

function makeLoading(element) {
    element.prop("disabled", true);
    element.prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
}

function removeLoading(element) {
    element.prop("disabled", false);
    element.find(".spinner-border").remove();
}
