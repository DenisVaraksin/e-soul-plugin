(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        var registerForm = $("#eso-register-form");
        var registerStatus = $("#eso-register-form-status");

        registerForm.submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: registerForm.serialize(),
                beforeSend: function() {
                    registerStatus.html(php_vars["wait_please"]).addClass("text-info");
                },
                error: function(result) {
                    console.log(result);
                    registerStatus.html(result).removeClass("text-info").addClass("text-danger");
                },
                success: function(result) {
                    console.log(result);
                    registerStatus.html(result).removeClass("text-danger text-info").addClass("text-success");
                }
            });
        });

        const loginForm = $("#eso-login-form");
        const loginStatus = $("#eso-login-form-status");

        loginForm.submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: loginForm.serialize(),
                beforeSend: function() {
                    makeLoading(loginForm.find("button[type='submit']"));
                },
                error: function(result) {
                    loginStatus.html(result).removeClass("text-info").addClass("text-danger");
                },
                success: function(result) {
                    console.log(result);

                    if(result.success) {
                        window.location.replace(loginForm.find("input[name='_wp_http_referer']").attr("value"));
                    } else {
                        loginStatus.html(result).removeClass("text-info").addClass("text-danger");
                        removeLoading(loginForm.find("button[type='submit']"));
                    }
                }
            });
        });

        $(document).on("change", "#account input, #account select, #account textarea", function () {

                if(!$(this).hasClass("novalidate") || $(this).val() === '') {
                    validateInput($(this), true);
                }
        });

    });

    $('#shipping_phone').text(function(i, text) {
        return text.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
    });

})(jQuery, this);



/**
 * @since 2019.12
 */
function validateInput(thisInput, updateMetaOnSuccess) {
    console.log(thisInput.attr("id"));
    console.log(thisInput.val());
    jQuery.ajax({
        url: php_vars["ajaxurl"],
        method: "POST",
        data: {
            "action": "eso_ajax",
            "eso_action": "validate_customer_meta",
            "key": thisInput.attr("id"),
            "value": thisInput.val(),
        },
        error: function (result) {
            console.log(result);
        },
        success: function (result) {
            console.log(result);
            if (result.length) {
                if (thisInput.val() === '' ){
                    console.log("error");
                    displayValidationError(thisInput, result);
                }
                console.log("error");
                displayValidationError(thisInput, result);
            } else {
                console.log("correct");
                hideValidationError(thisInput);
            }
        }
    });
}

/**
 * @since 2019.12
 *
 * @param thisInput
 * @param result
 */
function displayValidationError(thisInput, result) {
    thisInput.removeClass("is-valid").addClass("is-invalid");
    let form_group = thisInput.closest(".form-group");
    form_group.find(".invalid-feedback").remove();
    form_group.append("<div class='invalid-feedback'>" + result + "</div>");
}

function hideValidationError(thisInput) {
    thisInput.removeClass("is-invalid").addClass("is-valid");
}

