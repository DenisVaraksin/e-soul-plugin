(function ($) {

    $(function () {

        'use strict';

        /**
         * @since 2019.7
         */
        $("#single-esoul_order-pay").click(function (e) {
            e.preventDefault();

            const thisButton = $(this);
            const order_id = thisButton.data("order");
            const method = thisButton.data("method");

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: {
                    "action": "eso_ajax",
                    "eso_action": "pay_order_after",
                    "order_id": order_id,
                    "payment_method": method
                },
                beforeSend: function () {
                    makeLoading(thisButton);
                },
                success: function (result) {
                    window.location.replace(result);
                    removeLoading(thisButton);
                }
            });
        });

        function makeLoading(element) {
            element.prop("disabled", true);
            element.prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        }

        function removeLoading(element) {
            element.prop("disabled", false);
            element.find(".spinner-border").remove();
        }
    });

})
(jQuery);
