(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        $("#show-debug-bar").click(function() {
            $("#eso-debug-bar").toggleClass("active");

            $.ajax( {
                url: php_vars["ajaxurl"],
                method: "POST",
                data: {
                    "eso_action": "frontend_debug_bar",
                    "action": "eso_admin_ajax"
                },
                error: function(result) {
                    $("#eso-debug-bar").html(php_vars["_error"]);
                },
                success: function(result) {
                    $("#eso-debug-bar").html(result);
                }
            });
        });

        $("#show-frontend-dashboard").click(function() {
           $("#eso-frontend-dashboard").toggleClass("active");

           $.ajax({
               url: php_vars["ajaxurl"],
               method: "POST",
               data: {
                   "action": "eso_admin_ajax",
                   "eso_action": "show_frontend_dashboard",
               },
               error: function(result) {
                   console.log(result);
               },
               success: function(result) {
                   $("#eso-frontend-dashboard").html(result);
               }
           });
        });
    });

})(jQuery, this);
