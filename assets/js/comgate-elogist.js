(function ($, root, undefined) {

    $(function () {

        'use strict';

        // DOM ready, take it away

        const shippingMethodChecked = $("input[name='checkout[options][shipping_method][code]']:checked");

        $(document).on("change", "input[name='checkout[options][shipping_method][code]']", function() {
            checkBranches($(this));
        });

        checkBranches(shippingMethodChecked);

        function checkBranches(thisInput) {
            const thisParent = thisInput.parent();

            if (thisInput.val() === "zasilkovna") {
                $.ajax({
                    url: php_vars["ajaxurl"],
                    method: "POST",
                    data: {
                        "action": "eso_ajax",
                        "eso_action": "show_zasilkovna_branches",
                    },
                    beforeSend: function () {
                        makeLoading(thisParent);
                    },
                    error: function (result) {
                        console.log(result);
                    },
                    success: function (result) {
                        thisParent.append(result);
                        removeLoading(thisParent);
                        validateInput($("#zasilkovna_branch"), false);
                    }
                });
            } else {
                $("#zasilkovna_branch").remove();
            }
        }

    });

})(jQuery, this);