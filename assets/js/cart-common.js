(function ($) {
    $(function () {

        const nonceValue = $("#_wpnonce").val();

        function renderQuickLook() {
            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: {
                    "action": "eso_ajax",
                    "eso_action": "render_cart_quick_look"
                },
                success: function(result) {
                    $("#cart-quick-look").remove();
                    $(".menu-cart").append(result);
                }
            });
        }

        renderQuickLook();

        const addToCartForm = $(".add-to-cart-form");

        addToCartForm.submit(function (e) {
            e.preventDefault();

            const addToCartButton = $(this).find(".add-to-cart");
            const nonceValue = $(this).find("#_wpnonce").attr("value");

            const product_id = $(this).data("id");
            const quantity = $(this).find(".eso-product-quantity").attr("value");

            $.ajax({
                url: php_vars["ajaxurl"],
                method: 'POST',
                data: {
                    "_wpnonce": nonceValue,
                    "action": "eso_ajax",
                    "eso_action": "add_to_cart",
                    "eso_product_id": product_id,
                    "eso_product_quantity": quantity
                },
                beforeSend: function () {
                    makeLoading(addToCartButton);
                },
                error: function (result) {
                    console.log(result);
                    addToCartButton.html(php_vars["_error"])
                },
                success: function (result) {
                    removeLoading(addToCartButton);
                    refreshCartValue();
                    $("#added-to-cart").remove();
                    $("body").append(result);
                    $("#added-to-cart").modal();
                }
            })
        });

        const cartItemQuantityChange = $(".eso-cart-item-quantity-change");

        cartItemQuantityChange.change(function () {
            const cartItem = $(this).parents("tr");
            const cartItemId = $(this).data("id");
            const cartItemQuantity = $(this).attr("value");

            $.ajax({
                url: php_vars["ajaxurl"],
                method: 'POST',
                data: {
                    "_wpnonce": nonceValue,
                    "action": "eso_ajax",
                    "eso_action": "cart_item_update_quantity",
                    "eso_product_quantity": cartItemQuantity,
                    "eso_product_id": cartItemId
                },
                beforeSend: function () {
                    cartItem.addClass("processing");
                },
                error: function () {
                    cartItemQuantityChange.append(php_vars["_error"]);
                },
                success: function (result) {
                    refreshCartItemValue(cartItemId, result);
                    refreshCartValue();
                    refreshCartSum();

                    cartItem.removeClass("processing");
                }
            });
        });

        const removeItemButton = $(".eso-cart-item-remove");

        removeItemButton.click(function (e) {
            e.preventDefault();

            const cartItem = $(this).parents("tr");
            const cartItemId = $(this).data("id");

            $.ajax({
                url: php_vars["ajaxurl"],
                method: 'POST',
                data: {
                    "_wpnonce": nonceValue,
                    "eso-product-id": cartItemId,
                    "action": "eso_ajax",
                    "eso_action": "cart_remove_item",
                },
                beforeSend: function () {
                    cartItem.addClass("processing");
                },
                success: function () {
                    cartItem.remove();
                    refreshEmptyCartMessage();
                    refreshCartValue();
                    refreshCartSum();

                }
            });
        });

        /**
         * @since 2019.3
         * @since 2019.8 hide #cart-sum-table when cart is empty
         * @since 2020.2.11
         */
        function refreshEmptyCartMessage() {
            const emptyCartMessage = $(".empty-cart-message");

            if ($("#cart-content-table > tbody tr.product-row").length > 0) {
                emptyCartMessage.removeClass("active");
            } else {
                $("#cart-content-table").hide();
                $(".cart-actions").hide();
                emptyCartMessage.addClass("active");
                $("#cart-sum-table").hide();
                $("a[href=#cart-customer]").parent().addClass("disabled");
            }
        }

        /**
         * @since 2019.4
         * @since 2020.3.1
         * If you want to display sum of prices in header, use
             <span id="cart-sum">
                <?php $cart = new Eso_Cart( eso_session_token() );
                echo $cart->get_sum();
                ?>
             </span>
         * if you want to display number of items in cart,
         * use
             <span id="cart-count">
                <?php $cart = new Eso_Cart( eso_session_token() );
                echo $cart->get_items_count();
                ?>
             </span>
         */
        function refreshCartValue() {
            const cart_sum = $("#cart-sum");
            const cart_count = $("#cart-count");

            let value_target;
            let eso_action;

            if(cart_sum.length) {
                value_target = cart_sum;
                eso_action = "get_cart_sum_with_currency";
            } else if(cart_count.length) {
                value_target = cart_count;
                eso_action = "get_cart_items_count";
            }

            if(!value_target || !eso_action) {
                return;
            }

            $.ajax({
                url: php_vars["ajaxurl"],
                method: 'POST',
                data: {
                    "action": "eso_ajax",
                    "eso_action": eso_action
                },
                error: function () {
                    value_target.html(php_vars["_error"])
                },
                success: function (result) {
                    value_target.html(result);
                }
            });

            renderQuickLook();
        }

        /**
         * @since 2019.8 also refresh cart totals
         */
        function refreshCartSum() {
            const cart_sum_table = $("#cart-sum-table"); //#checkout-summary

            $.ajax({
                url: php_vars["ajaxurl"],
                method: 'POST',
                data: {
                    "action": "eso_ajax",
                    "eso_action": "render_cart_sum"
                },
                beforeSend: function() {
                    cart_sum_table.addClass("processing");
                },
                success: function (result) {
                    cart_sum_table.removeClass("processing");
                    cart_sum_table.html(result);
                }
            });

            const summary = $(".order_summary");

            $.ajax({
                url: php_vars["ajaxurl"],
                method: 'POST',
                data: {
                    "action": "eso_ajax",
                    "eso_action": "render_cart_totals"
                },
                beforeSend: function () {
                    summary.addClass("processing");
                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    $(".cart-totals").html(result);
                    summary.removeClass("processing");
                }
            });
        }

        /**
         * @since 2019.4
         *
         * @param cartItem
         * @param cartItemValue
         */
        function refreshCartItemValue(cartItem, cartItemValue) {
            $(".eso-cart-item-value-" + cartItem).html(cartItemValue)
        }

    });

})(jQuery);