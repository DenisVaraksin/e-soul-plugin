=== E-soul ===
Contributors: felixprojekt
Requires at least: 5.0
Tested up to: 5.2.5
Requires PHP: 5.6

== Description ==
Kompletní e-commerce řešení pro české eshopy. Pokud je k dispozici nová verze, aktualizujte tento plugin a získávejte tak důležitá vylepšení.

== Changelog ==
= 2020.2.5 =
* possibility to switch between stable and beta branches

= 2020.1.1 =
* support for gulp

= 2019.12.4 =
* add Frusack Theme to list of supported themes
* add .idea to .gitignore