<?php
function eso_stock_status_terms() {
	$terms = get_terms( [ 'taxonomy' => 'stock_status', 'hide_empty' => false ] );

	return array_reverse( $terms );
}

/**
 * @return array
 * @since 2019.7
 *
 */
function eso_get_stock_statuses() {
	$terms = get_terms( [ 'taxonomy' => 'stock_status', 'hide_empty' => false ] );

	$statuses = [];

	foreach ( $terms as $term ) {
		$status     = new Eso_Stock_Status( $term->term_id );
		$statuses[] = $status;
	}

	return $statuses;
}

/**
 * @return array|false|WP_Term
 *
 * @since 2019.5
 */
function eso_get_default_stock_status() {
	$term = get_term_by( 'slug', 'in-stock', 'stock_status' );

	return $term;
}

/**
 * @param null $parent
 * @param array $exclude
 * @param bool $hide_empty
 *
 * @param bool $childless
 *
 * @return array
 * @since 2019.7
 */
function eso_get_product_categories( $parent = null, $exclude = [], $hide_empty = false, $childless = false ) {
	$args = [
		'taxonomy'   => 'product_category',
		'hide_empty' => $hide_empty,
	];

	if ( $parent ) {
		$args['parent'] = $parent;
	}

	if(!empty($exclude)) {
		$args['exclude'] = $exclude;
	}

	if($childless) {
		$args['childless'] = true;
	}

	$terms = get_terms( $args );

	return $terms;
}

/**
 * Returns all product_tag taxonomy items
 *
 * @return array
 * @since 2019.7
 *
 */
function eso_get_product_tags() {
	$terms = get_terms( [ 'taxonomy' => 'product_tag', 'hide_empty' => false ] );

	$tags = [];

	foreach ( $terms as $term ) {
		$tag    = new Eso_Product_Tag( $term->term_id );
		$tags[] = $tag;
	}

	return $tags;
}

/**
 * @since 2019.6
 */
function eso_empty_thumbnail() {
	return "<img src='" . get_stylesheet_directory_uri() . "/img/no-image.png'/>";
}

/**
 * @param $price_with_tax
 * @param Eso_Tax_Rate $tax_rate
 *
 * @return float|int
 * @since 2019.7
 *
 */
function eso_get_price_without_tax( $price_with_tax, Eso_Tax_Rate $tax_rate, $comma_delimiter = false ) {
	$percentage = $tax_rate->get_value();

	$price_without_tax = $price_with_tax * ( ( 100 - $percentage ) / 100 );

	$price_without_tax = round( $price_without_tax, 2 );

	if ( $comma_delimiter ) {
		$price_without_tax = str_replace( ".", ",", $price_without_tax );
	}

	return $price_without_tax;
}

/**
 * @param $price
 * @param $discount_type
 * @param $value
 * @param bool $comma_delimiter
 *
 * @return float|int|mixed
 * @since 2019.7
 *
 */
function eso_get_price_after_discount( $price, $discount_type, $value, $comma_delimiter = false ) {

	$price = (float) $price;
	$value = (float) $value;

	if ( $discount_type == "%" ) {
		$after_discount = $price * ( ( 100 - $value ) / 100 );
	} else if ( $discount_type == "-" ) {
		$after_discount = $price - $value;
	}

	if ( ! isset( $after_discount ) ) {
		$after_discount = $price;
	}

	if ( $comma_delimiter ) {
		$after_discount = str_replace( ".", ",", $after_discount );
	}

	return $after_discount;
}

add_action( 'pre_get_posts', 'advanced_search_query' );
function advanced_search_query( $query ) {
    if ( isset( $_REQUEST['search'] ) && $query->is_search && $query->is_main_query() ) {

        $query->set( 'post_type', 'esoul_product' );

        $_model = $_GET['ean'] != '' ? $_GET['ean'] : '';

        $meta_query = array(
            array(
                'key'     => 'ean_code', // assumed your meta_key is 'car_model'
                'value'   => $_model,
                'compare' => 'LIKE', // finds models that matches 'model' from the select field
            )
        );

        $query->set( 'meta_query', $meta_query );

    }
}