<?php
if ( ! class_exists( 'Eso_Comgate_Payment', false ) ) {
	class Eso_Comgate_Payment {

		private $order;

		/* @var $order Eso_Order */
		public function __construct( $order ) {

			$this->order = $order;

			require_once ESO_DIR . '/addons/agmo/AgmoPaymentsSimpleDatabase.php';
			require_once ESO_DIR . '/addons/agmo/AgmoPaymentsSimpleProtocol.php';

			$module = new Eso_Module( "comgate_payments" );

			$this->config = array(
				'paymentsUrl' => 'https://payments.comgate.cz/v1.0/create',
				'merchant'    => $module->get_data_value( "store_id" ),
				'secret'      => $module->get_data_value( "secure_key" )
			);

			if ( $module->get_data_value( "production" ) == 1 ) {
				$this->config["test"] = false;
			} else {
				$this->config["test"] = true;
			}

			// initialize payments data object
			$this->paymentsDatabase = new AgmoPaymentsSimpleDatabase(
				ESO_DIR . '/temp',
				$this->config['merchant'],
				$this->config['test']
			);

			// initialize payments protocol object
			$this->paymentsProtocol = new AgmoPaymentsSimpleProtocol(
				$this->config['paymentsUrl'],
				$this->config['merchant'],
				$this->config['test'],
				$this->config['secret']
			);
		}

		public function pay() {
			try {

				// prepare payment parameters
				$refId    = $this->paymentsDatabase->createNextRefId();
				$price    = $this->order->get_total();
				$currency = eso_get_active_currency_code();

				// create new payment transaction
				$this->paymentsProtocol->createTransaction(
					"CZ", // country
					$price,             // price
					$currency,          // currency
					$this->order->get_title(),     // label
					$refId,             // refId
					null,               // payerId
					'STANDARD',         // vatPL
					'PHYSICAL',         // category
					'ALL'    // method
				);
				$transId = $this->paymentsProtocol->getTransactionId();

				update_post_meta( $this->order->get_id(), "comgate_payment_id", $refId );

				$this->order->add_log( "ID Comgate platby: " . $refId );

				// save transaction data
				$this->paymentsDatabase->saveTransaction(
					$transId,       // transId
					$refId,         // refId
					$price,         // price
					$currency,      // currency
					'PENDING'       // status
				);

				// redirect to agmo payments system
				echo $this->paymentsProtocol->getRedirectUrl();

				return true;
			} catch ( Exception $e ) {
				write_log( "Eso_Comgate_Payment->pay() ended with error: " . $e->getMessage() );

				return false;
			}
		}

		/**
		 * @since 2019.8
		 */
		public function get_status() {
			if ( ! empty( $_GET["status"] ) ) {
				$status = sanitize_text_field( $_GET["status"] );
			}

			if ( $status ) {
				if ( $status == "cancelled" ) {
					$status = "waiting";
				}

				return $status;
			}

			return null;
		}

	}
}