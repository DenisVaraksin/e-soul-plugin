<?php

if ( ! class_exists( 'Eso_Zasilkovna', false ) ) {
	class Eso_Zasilkovna {

		public function __construct() {
			$module = new Eso_Module( "zasilkovna" );

			$this->module = $module;
		}

		/**
		 * @return string
		 */
		private function get_api_key() {
			return $this->module->get_data_value("api_key");
		}

		public function send( $order_id ) {
			$order = new Eso_Order($order_id);

			$gw          = new SoapClient( "http://www.zasilkovna.cz/api/soap.wsdl" );
			$apiPassword = $this->get_api_key();

			try {
				$packet = $gw->createPacket( $apiPassword, array(
					'number'    => $order->get_id(),
					'name'      => $order->get_shipping_first_name(),
					'surname'   => $order->get_shipping_last_name(),
					'email'     => $order->get_shipping_email(),
					'phone'     => $order->get_shipping_phone(),
					'addressId' => 85,
					'cod'       => $order->get_integer_total(),
					'value'     => $order->get_integer_total(),
					'eshop'     => get_site_url()
				) );
			} catch ( SoapFault $e ) {
				write_log("Eso_Zasilkovna->send() ended with error " . $e->getMessage());
			}
		}
	}
}