<?php
/**
 * @since 2019.6
 */
if ( ! class_exists( 'Eso_Instagram_Feed', false ) ) {

	class Eso_Instagram_Feed {
		public function __construct( ) {
			add_shortcode( 'eso_instagram_feed', array( $this, 'render_shortcode' ) );
		}

		private function fetch_url($account) {
			$insta_source = file_get_contents('http://instagram.com/'.$account);
			$shards = explode('window._sharedData = ', $insta_source);
			$insta_json = explode(';</script>', $shards[1]);
			$insta_array = json_decode($insta_json[0], TRUE);
			return $insta_array;
		}

		public function update_data() {
			$module = new Eso_Module("instagram_feed");
			$account = $module->get_data_value("account");

			$new_data = $this->fetch_url($account);

			$posts_limit = $module->get_data_value("posts_limit");

			if($posts_limit > 12) {
				$posts_limit = 12;
			}

			$followers = number_format($new_data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_followed_by']['count'], 0, ".", ".");

			if($followers != 0) {
				$posts = [];

				for($cnt = 0; $cnt < $posts_limit; $cnt++)
				{
					$latest_array = $new_data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'][$cnt];

					if(isset($latest_array['node'])) {

						$posts[$latest_array['node']['shortcode']] = $latest_array['node']['thumbnail_resources'][2]['src'];

					}
				}

				update_option("eso_instagram_feed_posts", $posts);

			} else {
				write_log("Instagram Feed is private or has 0 followers");
			}
		}

		/**
		 * This is fired if set_enabled is set to true on module activation
		 */
		public function activate() {
			$this->update_data();
		}

		public function render_shortcode() {
			$posts = get_option("eso_instagram_feed_posts");
			$out = "";

			if(!empty($posts)) {
				$out .= "<div class='esf-posts-wrapper esf-posts-ig'>";

				foreach($posts as $id => $url) {
					$out  .= '<a href="http://instagram.com/p/'. $id .'" target="_blank" class="esf-item esf-item-ig"><div class="esf-ig-thumbnail"><img src="'. $url .'"></div></a>';

				}
			} else {
				$out .= __("Zatím žádné příspěvky", "eso");
			}

			//Fetch new posts in the background

			$out .= '<form class="ajax--onload">
                <input type="hidden" name="action" value="eso_ajax"/>
                <input type="hidden" name="eso_action" value="update_instagram_feed"/>
            </form>';

			return $out;
		}
	}
}

if(function_exists('eso_is_module_active')) {
	if(eso_is_module_active("instagram_feed")) {
		return new Eso_Instagram_Feed();
	}
}
