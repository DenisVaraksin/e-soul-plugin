<?php
/**
 * @since 2019.5 $wpdb moved to getters in hope it will be faster to execute
 */
if ( ! class_exists( 'Eso_Module', false ) ) {
	class Eso_Module {

		private $code;

		private $name;

		private $enabled;

		private $category;

		public function __construct( $code ) {
			$this->code = $code;
		}

		public function get_code() {
			return sanitize_text_field( $this->code );
		}

		/**
		 * @return mixed
		 * @since 2019.5
		 */
		public function get_name() {
			global $wpdb;
			$row = $wpdb->get_row( "SELECT name FROM " . ESO_MODULE_TABLE . " WHERE code = '" . $this->get_code() . "'" );

			return $row->name;
		}

		/**
		 * @return mixed
		 * @since 2019.5
		 */
		public function get_category() {
			global $wpdb;
			$row = $wpdb->get_row( "SELECT category FROM " . ESO_MODULE_TABLE . " WHERE code = '" . $this->get_code() . "'" );

			return $row->category;
		}

		/**
		 * @return array|null|object
		 * @since 2019.5
		 */
		public function get_data() {
			global $wpdb;
			$row = $wpdb->get_results("SELECT code, name, type, value, required, atts FROM " . ESO_MODULE_DATA_TABLE . " WHERE module = '" . $this->get_code() . "'");

			return $row;
		}

		/**
		 * @return string|null
		 *
		 * @since 2019.5
		 */
		public function get_data_value($field_code) {
			global $wpdb;

			$row = $wpdb->get_row("SELECT value FROM " . ESO_MODULE_DATA_TABLE . " WHERE module = '" . $this->get_code() . "' AND code = '" . $field_code . "'");

			if(isset($row->value)) {
				return $row->value;
			}

			return null;

		}

		/**
		 * @param $field_key
		 * @param $field_value
		 *
		 * @since 2019.5
		 */
		public function set_data_field($field_key, $field_value) {
			global $wpdb;

			$data = [
				"value" => $field_value,
			];

			$where = [
				"module" => $this->get_code(),
				"code" => $field_key
			];

			$wpdb->update(ESO_MODULE_DATA_TABLE, $data, $where);
		}

		public function is_enabled() {
			global $wpdb;
			$row = $wpdb->get_row( "SELECT enabled FROM " . ESO_MODULE_TABLE . " WHERE code = '" . $this->get_code() . "'" );

			if(isset($row->enabled)) {
				return $row->enabled;
			}

			return false;
		}

		/**
		 * @param bool $value
		 *
		 * @return false|int
		 * @since 2019.5
		 */
		public function set_enabled( $value = true ) {
			global $wpdb;

			$data = [
				"enabled" => $value,
			];

			$where = [
				"code" => $this->get_code(),
			];

			$result = $wpdb->update( ESO_MODULE_TABLE, $data, $where );

			if($value == true) {
				$this->activate();
			} else {
				$this->deactivate();
			}

			return $result;
		}

		/**
		 * This function is called when set_enabled is called with true
		 *
		 * @since 2019.5
		 */
		public function activate() {
			$code = $this->get_code();
			global $wpdb;

			switch($code):
				case "comgate_payments":
					$data = [
						"code" => "comgate_payments",
						"name" => "Comgate Platební brána",
						"message" => "Zaplaťte v prostředí Comgate",
						"enabled" => 1
					];
					$wpdb->replace(ESO_PAYMENT_METHOD_TABLE, $data);
					break;
				case "gopay":
					$data = [
						"code" => "gopay",
						"name" => "GoPay Platební brána",
						"message" => "Zaplaťte v prostředí GoPay",
						"enabled" => 1
					];
					$wpdb->replace(ESO_PAYMENT_METHOD_TABLE, $data);
					break;
				case "gpwebpay":
					$data = [
						"code" => "gpwebpay",
						"name" => "GP WebPay Platební brána",
						"message" => "Zaplaťte v prostředí GP WebPay",
						"enabled" => 1
					];
					$wpdb->replace(ESO_PAYMENT_METHOD_TABLE, $data);
					break;
//				case "zasilkovna":
//					$data = [
//						"code" => "zasilkovna",
//						"name" => "Zásilkovna",
//						"enabled" => 1
//					];
//					$wpdb->replace(ESO_SHIPPING_METHOD_TABLE, $data);
//					break;
				case "comgate_logistika_sk":
				case "comgate_logistika":
					$comgate_elogist = new Eso_Comgate_Elogist();
					$comgate_elogist->activate();
					break;
				case "instagram_feed":
					$instagram_feed = new Eso_Instagram_Feed();
					$instagram_feed->activate();
					break;

			endswitch;
		}

		/**
		 * This function is called when set_enabled is called with false
		 *
		 * @since 2019.5
		 */
		public function deactivate() {
			$code = $this->get_code();
			global $wpdb;

			switch($code):
				case "comgate_payments":
					$data = [
						"enabled" => 0
					];
					$where = [
						"code" => "comgate_payments",
					];
					$wpdb->update(ESO_PAYMENT_METHOD_TABLE, $data, $where);
					break;
				case "gopay":
					$data = [
						"enabled" => 0
					];
					$where = [
						"code" => "gopay",
					];
					$wpdb->update(ESO_PAYMENT_METHOD_TABLE, $data, $where);
					break;
				case "zasilkovna":
					$data = [
						"enabled" => 0
					];
					$where = [
						"code" => "zasilkovna",
					];
					$wpdb->update(ESO_SHIPPING_METHOD_TABLE, $data, $where);
					break;
				case "comgate_logistika":
				case "comgate_logistika_sk":
					$comgate_elogist = new Eso_Comgate_Elogist();
					$comgate_elogist->deactivate();
					break;
			endswitch;
		}

		/**
		 * @since 2019.5
		 */
		public function render_item() {
			require( ESO_DIR . '/admin/views/module/item.php' );
		}

		/**
		 * @since 2019.5
		 */
		public function render_settings() {
			require( ESO_DIR . '/admin/views/module/settings.php' );
		}

		/**
		 * @param $field
		 *
		 * @since 2019.5
		 */
		public function render_field($field) {
			$fields = new Eso_Admin_Fields();
			
			if($field->type == "checkbox") {
				$fields->checkbox($field->name, $field->name, $field->value);
			} else if($field->type == "select") {
				require( ESO_DIR . '/admin/views/module/fields/select.php' );
			} else if($field->type == "number") {
				require( ESO_DIR . '/admin/views/module/fields/number.php' );
			} else if($field->type == "file") {
				require( ESO_DIR . '/admin/views/module/fields/file.php' );
			} else if($field->type == "password") {
				require( ESO_DIR . '/admin/views/module/fields/password.php' );
			} else {
				require( ESO_DIR . '/admin/views/module/fields/text.php' );
			}
		}

		/**
		 * @param $field_key
		 * @param $field_value
		 *
		 * @return null
		 * @since 2020.1.5
		 */
		public function handle_file_upload($field_key, $field_value) {

			if($this->get_code() == "gpwebpay" && $field_key == "private_key") {
				move_uploaded_file($field_value,ESO_DIR . '/certificates/gpwebpay-pvk.key');
			}

			if($this->get_code() == "gpwebpay" && $field_key == "public_cert_test") {
				move_uploaded_file($field_value,ESO_DIR . '/certificates/gpe.signing_test.pem');
			}

			if($this->get_code() == "gpwebpay" && $field_key == "public_cert_prod") {
				move_uploaded_file($field_value,ESO_DIR . '/certificates/gpe.signing_prod.pem');
			}

			return null;
		}
	}
}