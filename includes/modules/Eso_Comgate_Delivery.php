<?php
if ( ! class_exists( 'Eso_Comgate_Delivery', false ) ) {
	/**
	 * Class Eso_Comgate_Delivery
	 *
	 * @since 2019.5
	 */
	class Eso_Comgate_Delivery {

		private $order;

		public function __construct( Eso_Order $order ) {

			$this->order   = $order;
			$this->country = $order->get_shipping_country();

			$this->comgate_elogist = new Eso_Comgate_Elogist();

			if ( ! in_array( $this->country, [ "CZ", "SK" ] ) ) {
				write_log( "Called Eso_Comgate_Logist with invalid country" );
			}

			if ( $this->country == "SK" ) {
				$this->module = new Eso_Module( "comgate_logistika_sk" );
			} else {
				$this->module = new Eso_Module( "comgate_logistika" );
			}
		}

		public function delivery_order() {
			$param = new StdClass();

			$param->projectId = $this->module->get_data_value( "project" );

			$elogist_order_id = $this->order->get_id();

			$param->orderId         = $elogist_order_id;
			$param->orderDateTime   = date( 'c', strtotime( $this->order->get_date_and_time_created() ) );
			$param->customerOrderId = $this->order->get_id();
			$param->paymentId       = $elogist_order_id;

			$param->sender        = new StdClass();
			$param->sender->label = get_site_option( "store-name" );

			$address           = new StdClass();
			$address->street   = $this->order->get_shipping_street();
			$address->city     = $this->order->get_shipping_city();
			$address->postcode = $this->order->get_shipping_postcode();
			$address->country  = $this->order->get_shipping_country();

			$param->recipient          = new StdClass();
			$param->recipient->name    = $this->order->get_shipping_name();
			$param->recipient->address = $address;
			$param->recipient->phone   = str_replace( " ", "", $this->order->get_shipping_phone() );
			$param->recipient->email   = $this->order->get_shipping_email();

			$param->shipping            = new StdClass();
			$carrierId                  = $this->get_carrier_code();
			$param->shipping->carrierId = $carrierId;

			if ( $carrierId === "ZASILKOVNA" && $this->order->get_zasilkovna_branch_id() ) {
				$param->shipping->branchId = $this->order->get_zasilkovna_branch_id();
			}

			if ( in_array( $this->order->get_currency()->get_code(), [ "CZK", "EUR" ] ) ) {
				$currency = $this->order->get_currency();
			} else {
				$currency = new Eso_Currency( "CZK" );
			}

			if ( $this->order->get_payment_method()->get_code() == "cod" ) {
				$param->shipping->cod           = new StdClass();
				$param->shipping->cod->_        = round( $this->order->get_total( false, $currency ) );
				$param->shipping->cod->currency = $currency->get_code();
			}

			$param->shipping->attempts = 3;
			$param->shipping->comment  = $this->order->get_note();

			$param->shipping->insurance           = new StdClass();
			$param->shipping->insurance->_        = $this->order->get_total( false, $currency );
			$param->shipping->insurance->currency = $currency->get_code();

			$param->orderItems = new StdClass();

			$oCounter = 0;
			/* @var $oItem Eso_Order_Item */
			foreach ( $this->order->get_items() as $oItem ) {

				$param->orderItems->orderItem[ $oCounter ]                             = new StdClass();
				$param->orderItems->orderItem[ $oCounter ]->productSheet               = new StdClass();
				$param->orderItems->orderItem[ $oCounter ]->productSheet->productId    = $oItem->get_product_id();
				$param->orderItems->orderItem[ $oCounter ]->productSheet->name         = $oItem->get_name();
				$param->orderItems->orderItem[ $oCounter ]->productSheet->description  = $oItem->get_name();
				$param->orderItems->orderItem[ $oCounter ]->productSheet->quantityUnit = 'PC';
				$param->orderItems->orderItem[ $oCounter ]->quantity                   = $oItem->get_quantity();

				$oCounter ++;
			}

			try {
				$soap_client = $this->comgate_elogist->client( $this->country );

				$response = $soap_client->DeliveryOrder( $param );

				if ( isset( $response->result ) ) {
					if ( $response->result->code === 1000 ) {

						update_post_meta( $this->order->get_id(), '_order_sent_to_comgate', true );
						update_post_meta( $this->order->get_id(), 'elogist_order_id', $elogist_order_id );

						$this->order->add_log( "Označeno jako zaplacené a odeslané do Comgate." );
					} else {
						// var_dump($response->result->description);

						// return $response->result->description;

						$this->order->add_log( "Chyba odeslání do Comgate: " . $response->result->code . " " . $response->result->description );
					}
				}
			} catch ( SoapFault $e ) {
				$this->order->add_log( "Chyba odeslání do Comgate: " . $e->faultcode . " " . $e->faultstring );
			} catch ( Exception $e ) {
				$this->order->add_log( "Chyba odeslání do Comgate: " . $e->getMessage() );
			}

			return;
		}

		public function get_carrier_code() {
			$method      = $this->order->get_shipping_method();
			$method_code = $method->get_code();
			$country     = $this->order->get_shipping_country();

			if ( $method_code == "czech_post" ) {
				return "CPOST";
			} else if ( $method_code == "geis" ) {
				return "GEIS";
			} else if ( $method_code == "zasilkovna" ) {
				return "ZASILKOVNA";
			} else if ( $country == "SK" && ( $method_code == "dhl" || $method_code == "dhl_express" ) ) {
				return "DHL-PARCEL-SK";
			} else if ( $method_code == "dhl" || $method_code == "dhl_express" ) {
				return "DHL";
			} else if ( $method_code == "gls" ) {
				return "GLS";
			} else if ( $method_code == "personally" ) {
				return "OSOBNE";
			} else {
				return "OSTATNI";
			}
		}

		/**
		 * @return bool|null|string
		 */
		public function get_order_status() {
			$soap_client = $this->comgate_elogist->client( $this->country );
			$param       = new StdClass();

			$param->projectId = $this->module->get_data_value( "project" );

			$orderIdMeta = $this->order->get_elogist_order_id();

			if ( empty( $orderIdMeta ) ) {
				return null;
			}

			$param->orderId = $orderIdMeta;

			try {
				$response = $soap_client->DeliveryOrderStatusGet( $param );
				if ( isset( $response->result ) ) {
					if ( $response->result->code === 1000 ) {
						return $response->deliveryOrderStatus;
					} else {
						return $response->result->description;
					}
				}
			} catch ( SoapFault $e ) {
				write_log( $e->faultcode . $e->faultstring );
			} catch ( Exception $e ) {
				write_log( $e->getMessage() );
			}

			return false;
		}

		/**
		 * @return string|null
		 */
		public function get_order_status_name() {
			$order_status = $this->get_order_status();

			if ( $order_status && isset( $order_status->status ) ) {
				return $order_status->status;
			}

			return null;
		}

		/**
		 * @since 2019.7
		 * @return null
		 */
		public function set_order_status_from_comgate() {

			$log = "Stav objednávky zaktualizován z Comgate na ";

			switch ( $this->get_order_status_name() ) {
				case "NEW":
					$this->order->set_status_by_slug( "for-completion", $log );
					break;
				case "CANCELLED":
					$this->order->set_status_by_slug( "cancelled", $log );
					break;
				case "COMPLETABLE":
					$this->order->set_status_by_slug( "for-completion", $log );
					break;
				case "ADMINISTERED":
					$this->order->set_status_by_slug( "for-completion", $log );
					break;
				case "COMPLETING":
					$this->order->set_status_by_slug( "on-the-way", $log );
					break;
				case "READY":
					$this->order->set_status_by_slug( "on-the-way", $log );
					break;
				case "SHIPPED":
					$this->order->set_status_by_slug( "on-the-way", $log );
					break;
				case "DELIVERED":
					$this->order->set_status_by_slug( "delivered", $log );
					break;
				case "ABANDONED":
					$this->order->set_status_by_slug( "failed", $log );
					break;
				case "PAID":
					$this->order->set_status_by_slug( "delivered", $log );
					break;
				case "RESTORED":
					$this->order->set_status_by_slug( "cancelled", $log );
					break;
				default:
					break;
			}

			$this->order->set_status_updated();

			return null;
		}

	}
}