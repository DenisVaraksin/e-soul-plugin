<?php
/**
 * @since 0.0.23
 */
function eso_get_all_modules() {
	global $wpdb;

	$rows = $wpdb->get_results( "SELECT code, name, data, enabled, category FROM " . ESO_MODULE_TABLE . "" );

	return $rows;
}

/**
 * @since 0.0.23
 */
function eso_get_all_module_categories() {
	global $wpdb;

	$rows = $wpdb->get_results( "SELECT id, name FROM " . ESO_MODULE_CAT_TABLE );

	return $rows;
}

/**
 * @since 2019.5
 */
function eso_google_analytics() {
	$module = new Eso_Module( "google_analytics" );

	$measurement_code = $module->get_data_value( "service_id" );

	if ( $measurement_code && $module->is_enabled() ) {
		echo '
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=' . $measurement_code . '"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag("js", new Date());
		
		  gtag("config", "' . $measurement_code . '");
		</script>';
	}
}

/**
 * @param $module_name
 *
 * @return bool
 * @since 2019.5
 */
function eso_is_module_active( $module_name ) {
	$module = new Eso_Module( $module_name );

	if ( $module->is_enabled() ) {
		return true;
	}

	return false;
}

/**
 * @return bool
 *
 * @since 2019.5
 */
function eso_is_comgate_elogist_active() {
	if ( eso_is_module_active( "comgate_logistika" ) || eso_is_module_active( "comgate_logistika_sk" ) ) {
		return true;
	}

	return false;
}