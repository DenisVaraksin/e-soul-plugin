<?php
if ( ! class_exists( 'Eso_Module_Category', false ) ) {
	class Eso_Module_Category {

		private $id;

		private $name;

		public function __construct( $id ) {
			$this->id = $id;

			global $wpdb;
			$row = $wpdb->get_row("SELECT name FROM " . ESO_MODULE_CAT_TABLE . " WHERE id = " . $this->get_id());

			$this->name = $row->name;
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->id;
		}

		public function get_name() {
			return $this->name;
		}

		public function has_modules() {
			if(empty($this->get_modules())) {
				return false;
			}

			return true;
		}

		public function get_modules() {
			global $wpdb;

			$modules = [];
			$rows = $wpdb->get_results("SELECT code, name, enabled, category FROM " . ESO_MODULE_TABLE . " WHERE category = " . $this->get_id());

			foreach($rows as $row) {
				$modules[$row->code] = new Eso_Module($row->code);
			}

			return $modules;
		}

	}
}