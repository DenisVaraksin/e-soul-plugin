<?php

if ( ! class_exists( 'Eso_GpWebPay', false ) ) {
	/**
	 * Class Eso_GpWebPay
	 * @since 2020.2.4
	 */
	class Eso_GpWebPay {

		private $order;
		private $module;

		/* @var $order Eso_Order */
		public function __construct( $order ) {
			$this->order = $order;

			$module       = new Eso_Module( "gpwebpay" );
			$this->module = $module;
		}

		private function request_params() {
			$params                    = [];
			$params['MERCHANTNUMBER']  = $this->module->get_data_value( "merchant_number" );
			$params['OPERATION']       = 'CREATE_ORDER';
			$params['ORDERNUMBER']     = $this->order->get_id();
			$params['AMOUNT']          = $this->order->get_integer_total();
			$params['CURRENCY']        = $this->get_iso_currency();
			$params['DEPOSITFLAG']     = 1;
			$params['URL']             = eso_get_page_link( "thankyou" ) . '?gpwebpay=response';
			$params["EMAIL"]           = $this->order->get_shipping_email();
			$params['REFERENCENUMBER'] = $this->order->get_id();

			return $params;
		}

		public function pay() {
			$params     = $this->request_params();
			$digestText = implode( '|', $params );

			$signature        = $this->sign( $digestText );
			$params['DIGEST'] = $signature;

			if ( $this->module->get_data_value( "production" ) == 1 ) {
				$redirect_url = 'https://3dsecure.gpwebpay.com/pgw/order.do';
			} else {
				$redirect_url = 'https://test.3dsecure.gpwebpay.com/pgw/order.do';
			}
			$url = $redirect_url . '?' . http_build_query( $params );

			$this->order->add_log( "Zákazník přesměrován na platební bránu GP WebPay" );

			echo $url;

			return null;
		}

		private function sign( $text ) {
			$privateKeyFile = ESO_DIR . '/certificates/gpwebpay-pvk.key';
			$password       = $this->module->get_data_value( "private_key_pass" );

			$fp  = fopen( $privateKeyFile, "r" );
			$key = fread( $fp, filesize( $privateKeyFile ) );
			fclose( $fp );
			if ( ! ( $privateKey = openssl_pkey_get_private( $key, $password ) ) ) {
				write_log( "GP WebPay key or password not valid", true );

				return false;
			}

			$pkey = openssl_get_privatekey( $privateKey, $password );

			openssl_sign( $text, $signature, $pkey );

			$signature = base64_encode( $signature );
			openssl_free_key( $pkey );

			return $signature;
		}

		/**
		 * @return bool|string
		 */
		public function get_result() {
			$digest  = $_GET["DIGEST"];
			$digest1 = $_GET["DIGEST1"];

			$response_params = $this->response_params();
			if ( ! $response_params ) {
				$this->order->add_log( "Formát odpovědi z GP WebPay byl neplatný" );

				return false;
			}

			$data                    = $response_params;
			$data1                   = $data;
			$data1["MERCHANTNUMBER"] = $this->module->get_data_value( "merchant_number" );

			if ( ! $this->verify( $data, $digest ) || ! $this->verify( $data1, $digest1 ) ) {
				$this->order->add_log( "Nepodařilo se ověřit podpis GP WebPay platby", true );

				return false;
			};

			$this->order->add_log( "GP WebPay odpověď: " . sanitize_text_field( $_GET["RESULTTEXT"] ) );

			$status = $_GET["PRCODE"];

			switch ( $status ) {
				case 0:
					return "thankyou";
					break;
				case 20:
					return "waiting";
					break;
				default:
					return "failed";
					break;
			}
		}

		private function response_params() {
			$param_names = [ "OPERATION", "ORDERNUMBER", "PRCODE", "SRCODE", "RESULTTEXT" ];
			$params      = [];

			foreach ( $param_names as $param_name ) {
				if ( ! isset( $_GET[ $param_name ] ) ) {
					return false;
				}

				$params[ $param_name ] = $_GET[ $param_name ];
			}

			return $params;
		}

		private function verify( array $params, $digest ) {
			test_log("---");
			$data = implode("|", $params);

			if ( $this->module->get_data_value( "production" ) == 1 ) {
				$certificate = ESO_DIR . '/certificates/gpe.signing_prod.pem';
			} else {
				$certificate = ESO_DIR . '/certificates/gpe.signing_test.pem';
			}

			$pubkeyid = openssl_get_publickey( file_get_contents($certificate));

			$digest   = base64_decode( $digest );
			if ( ! openssl_verify( $data, $digest, $pubkeyid ) ) {
				openssl_free_key( $pubkeyid );
				return false;
			}

			openssl_free_key( $pubkeyid );
			return true;
		}

		private function get_iso_currency() {
			$order_currency = $this->order->get_currency_code();

			if ( $order_currency == 'USD' ) {
				return '840';
			} elseif ( $order_currency == 'EUR' ) {
				return '978';
			} elseif ( $order_currency == 'GBP' ) {
				return '826';
			}

			return '203'; //CZK
		}
	}
}