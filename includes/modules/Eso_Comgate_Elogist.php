<?php
if ( ! class_exists( 'Eso_Comgate_Elogist', false ) ) {
	/**
	 * Class Eso_Comgate_Elogist
	 * Called for building up Comgate logistics modules
	 *
	 * @see Eso_Module
	 * @since 2019.5
	 */
	class Eso_Comgate_Elogist {
		public function get_shipping_methods() {
			$shipping_methods = [
				"zasilkovna" => [
					"name"            => "Zásilkovna",
					"prices"          => [
						"CZK" => 79,
						"EUR" => 3,
						"USD" => 4,
						"GBP" => 3
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					],
				],
				"dhl"        => [
					"name"            => "DHL",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"USD" => 5,
						"GBP" => 4
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					],
				],
				"geis"       => [
					"name"            => "GEIS",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"USD" => 5,
						"GBP" => 4
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					],
				],
				"gls"        => [
					"name"            => "GLS",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"USD" => 5,
						"GBP" => 4
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					],
				]
			];

			return $shipping_methods;
		}

		public function activate() {
			$shipping_methods = $this->get_shipping_methods();

			$this->add_shipping_method( "zasilkovna", $shipping_methods["zasilkovna"]["name"], $shipping_methods["zasilkovna"]["prices"] );
			$this->add_shipping_method( "dhl", $shipping_methods["dhl"]["name"], $shipping_methods["dhl"]["prices"] );
			$this->add_shipping_method( "geis", $shipping_methods["geis"]["name"], $shipping_methods["geis"]["prices"] );
			$this->add_shipping_method( "gls", $shipping_methods["gls"]["name"], $shipping_methods["gls"]["prices"] );
			$this->add_shipping_method( "czech_post" );
			$this->add_shipping_method( "personally" );

			$this->update_products();

			$this->update_personal_collection_address();
		}

		public function deactivate() {
			$this->disable_shipping_method( "zasilkovna" );
			$this->disable_shipping_method( "dhl" );
			$this->disable_shipping_method( "geis" );
			$this->disable_shipping_method( "gls" );
			$this->disable_shipping_method( "czech_post", true );
			$this->disable_shipping_method( "personally", true );
		}

		/**
		 * @param $country
		 *
		 * @return SoapClient
		 */
		public function client( $country ) {

			if ( $country == "SK" ) {
				$module = new Eso_Module( "comgate_logistika_sk" );
			} else {
				$module = new Eso_Module( "comgate_logistika" );
			}

			$service_uri = $this->get_service_uri( $module->get_data_value( "production" ) );
			$login       = $module->get_data_value( "login" );
			$pass        = $module->get_data_value( "pass" );

			$options = array(
				'location'           => $service_uri,
				'soap_version'       => SOAP_1_2,
				'login'              => $login,
				'password'           => $pass,
				'encoding'           => 'UTF-8',
				'trace'              => true,
				'exceptions'         => true,
				'cache_wsdl'         => WSDL_CACHE_NONE,
				'connection_timeout' => 120,
			);

			try {
				return new SoapClient( ESO_DIR . "/addons/wsdl/ComGateELogistService_v1.13.wsdl", $options );
			} catch ( SoapFault $e ) {
			    write_log($e->getMessage());
			}
		}

		/**
		 * @since 2019.8.25
		 */
		public function zasilkovna_branches() {
			$param            = new StdClass();
			$param->carrierId = "ZASILKOVNA";

			try {
				$response = $this->client( "CZ" )->BranchListGet( $param );

				if ( isset( $response->result ) ) {
					if ( $response->result->code == 1000 ) { ?>
                        <select name="checkout[options][zasilkovna_branch]"
                                id="zasilkovna_branch" required="required"
                                class="form-control" title="<?php _e( "Vyberte", "eso" ); ?>">
                            <option disabled selected
                                    value><?php echo esc_html__( "Vyberte", "eso" ); ?></option>
							<?php foreach ( $response->branch as $branch ) {
								?>
                                <option value="<?php echo $branch->branchId; ?>:<?php echo $branch->name; ?>"><?php echo $branch->name; ?></option>
							<?php } ?>
                        </select>
					<?php } else {
						write_log( "error on zasilkovna_branches with " );
					}
				}
			} catch ( SoapFault $e ) {
				write_log( $e->faultcode . $e->faultstring );
			} catch ( Exception $e ) {
				write_log( $e->getMessage() );
			}
		}

		private function add_shipping_method( $code, $name = null, $default_prices = null ) {
			global $wpdb;

			$data = [
				"code" => $code,
				"icon" => "comgate"
			];

			if ( $name ) {
				$data["name"] = $name;
			}

			if ( $default_prices ) {
				$data["default_prices"] = maybe_serialize( $default_prices );
			}

			if ( $wpdb->get_row( "SELECT * FROM " . ESO_SHIPPING_METHOD_TABLE . " WHERE code = '" . $code . "'" ) ) {
				$wpdb->update( ESO_SHIPPING_METHOD_TABLE, $data, [ "code" => $code ] );
			} else {
				$wpdb->insert( ESO_SHIPPING_METHOD_TABLE, $data );
			}
		}

		/**
		 * @since 2019.8 set icon to ""
		 *
		 * @param $code
		 */
		private function disable_shipping_method( $code, $enabled = false ) {
			global $wpdb;

			$data = [
				"enabled" => $enabled
			];

			if ( $code !== "zasilkovna" ) {
				$data["icon"] = "";
			}

			$where = [
				"code" => $code
			];

			$wpdb->update( ESO_SHIPPING_METHOD_TABLE, $data, $where );
		}

		private function update_products() {
			$products = new WP_Query( [
				"posts_per_page" => - 1,
				"post_type"      => "esoul_product",
			] );

			if ( $products->have_posts() ) {
				foreach ( $products->get_posts() as $post ) {
					$this->update_product( $post->ID );
				}
			}
		}

		/**
		 * @since 2019.8.25
		 */
		private function update_personal_collection_address() {
			$store = new Eso_Store();

			if ( empty( $store->get_personal_collection_address() ) ) {
				$param            = new StdClass();
				$param->carrierId = "OSOBNE";

				try {
					$response = $this->client( "CZ" )->BranchListGet( $param );

					if ( isset( $response->result ) ) {
						if ( $response->result->code == 1000 ) {
						    $address = $response->branch;
						    update_option("eso_personal_collection_address", $address->name . "\n" . $address->street . "\n" . $address->city );
						} else {
							write_log( "error on update_personal_collection_address" );
						}
					}
				} catch ( SoapFault $e ) {
					write_log( $e->faultcode . $e->faultstring );
				} catch ( Exception $e ) {
					write_log( $e->getMessage() );
				}
			}
		}

		public function update_product( $product_id ) {
			$product = new Eso_Product( $product_id );

			foreach ( $this->get_enabled_countries() as $country ) {
				if ( $country == "SK" ) {
					$module = new Eso_Module( "comgate_logistika_sk" );
				} else {
					$module = new Eso_Module( "comgate_logistika" );
				}

				$params               = new stdClass();
				$params->projectId    = $module->get_data_value( "project" );
				$params->productId    = $product_id;
				$params->name         = $product->get_name();
				$params->quantityUnit = "PC";

				try {
					$response = $this->client( $country )->ProductUpdate( $params );

					if ( isset( $response->result ) ) {
						if ( ! $response->result->code == 1000 ) {
							write_log( $response->result );
						}
					}
				} catch ( SoapFault $e ) {
					write_log( $e->faultcode . $e->faultstring );
				} catch ( Exception $e ) {
					write_log( $e->getMessage() );
				}
			}
		}

		public function get_service_uri( $production ) {
			if ( $production === true ) {
				return "https://elogist.comgate.cz/api/soap";
			} else {
				return "https://elogist-demo.comgate.cz/api/soap";
			}
		}

		public function get_enabled_countries() {
			$countries = [];

			if ( eso_is_module_active( "comgate_logistika" ) ) {
				$countries[] = "CZ";
			}

			if ( eso_is_module_active( "comgate_logistika_sk" ) ) {
				$countries[] = "SK";
			}

			return $countries;
		}

		public function render_enabled_countries() {
			$countries = $this->get_enabled_countries();

			if ( ! empty( $countries ) ) : ?>
                <select name="storageorder-country" title="<?php _e( "skladu", "eso" ) ?>" class="form-control">
					<?php foreach ( $countries as $country ) : ?>
                        <option value="<?php echo $country ?>"><?php echo $country ?></option>
					<?php endforeach; ?>
                </select>
			<?php endif;
		}

		/**
		 * @param $country
		 * @param $order_id
		 * @param $supplier
		 * @param $items
		 * @param $note
		 *
		 * @return string
		 *
		 * @since 2019.5
		 */
		public function storage_order( $country, $order_id, $supplier, $items, $note = "" ) {
			$soap_client = $this->client( $country );

			if ( $country == "SK" ) {
				$module = new Eso_Module( "comgate_logistika_sk" );
			} else if ( $country == "CZ" ) {
				$module = new Eso_Module( "comgate_logistika" );
			} else {
				return false;
			}

			$param            = new StdClass();
			$param->projectId = $module->get_data_value( "project" );
			$param->orderId   = $order_id;
			$param->supplier  = $supplier;
			$param->note      = $note;

			$param->orderItems = new StdClass();

			for ( $i = 0; $i < count( $items ); $i ++ ) {
				if ( $items[ $i ]['quantity'] > 0 ) {
					$param->orderItems->orderItem[ $i ]            = new StdClass();
					$param->orderItems->orderItem[ $i ]->productId = $items[ $i ]['productId'];
					$param->orderItems->orderItem[ $i ]->unitValue = $items[ $i ]['unitValue'];
					$param->orderItems->orderItem[ $i ]->quantity  = $items[ $i ]['quantity'];
				}
			}

			try {
				$response = $soap_client->StorageOrder( $param );
				if ( isset( $response->result ) ) {
					if ( $response->result->code === 1000 ) {
						return $response;
					} else {
						write_log( $response->result->description );

						return $response->result->description;
					}
				}
			} catch ( SoapFault $e ) {
				write_log( $e->faultcode . $e->faultstring );
			} catch ( Exception $e ) {
				write_log( $e->getMessage() );
			}

			return true;
		}
	}
}