<?php

if ( ! class_exists( 'Eso_Gopay', false ) ) {
	class Eso_Gopay {

		private $order;
		private $module;
		private $gopay;

		/* @var $order Eso_Order */
		public function __construct( $order ) {
			$this->order = $order;

			$module = new Eso_Module( "gopay" );

			$this->module = $module;

			if ( $module->get_data_value( "production" ) == 1 ) {
				$isProductionMode = true;
			} else {
				$isProductionMode = false;
			}

			$this->gopay = GoPay\Api::payments( [
				'goid'             => $module->get_data_value( "go_id" ),
				'clientId'         => $module->get_data_value( "client_id" ),
				'clientSecret'     => $module->get_data_value( "client_secret" ),
				'isProductionMode' => $isProductionMode,
				'scope'            => 'payment-all',
				'language'         => GoPay\Definition\Language::CZECH,
				'timeout'          => 30
			] );
		}

		public function pay() {
			$currency_code = $this->order->get_currency_code();

			$total = $this->order->get_integer_total();

			try {
				$response = $this->gopay->createPayment( [
//					'payer' => [
//						'default_payment_instrument' => GoPay\Definition\Payment\PaymentInstrument::BANK_ACCOUNT,
//						'allowed_payment_instruments' => [GoPay\Definition\Payment\PaymentInstrument::BANK_ACCOUNT],
//						'default_swift' => BankSwiftCode::FIO_BANKA,
//						'allowed_swifts' => [BankSwiftCode::FIO_BANKA, BankSwiftCode::MBANK],
//						'contact' => ['first_name' => 'Zbynek',
//						              'last_name' => 'Zak',
//						              'email' => 'test@test.cz',
//						              'phone_number' => '+420777456123',
//						              'city' => 'C.Budejovice',
//						              'street' => 'Plana 67',
//						              'postal_code' => '373 01',
//						              'country_code' => 'CZE'
//						]
//					],
					'amount'       => $total,
					'currency'     => $currency_code,
					'order_number' => $this->order->get_id(),
					'items'        => [
						[
							'type'        => 'ITEM',
							'name'        => __( "Objednávka", "eso" ),
							'product_url' => home_url(),
							'ean'         => 1,
							'amount'      => $total,
							'count'       => 1,
							'vat_rate'    => GoPay\Definition\Payment\VatRate::RATE_4
						],
					],
					'callback'     => [
						'return_url'       => home_url() . "/thankyou",
						'notification_url' => home_url() . "/thankyou",
					],
					'lang'         => GoPay\Definition\Language::CZECH
				] );

				if ( $response->hasSucceed() ) {
					$url = $response->json['gw_url'];

					update_post_meta( $this->order->get_id(), "gopay_payment_id", $response->json['id'] );

					$this->order->add_log( "Přiřazeno GoPay ID: " . $response->json['id'] );

					echo $url;
				} else {
					write_log( "Eso_Gopay_Payment->pay() returned with " . $response );

					$this->order->add_log( "Platební metoda GoPay, stav " . $response->json['state'] . ", ID GoPay platby: " . $response->json['id'] );

					echo eso_get_page_link( "thankyou" );
				}

				return true;

			} catch ( Exception $e ) {
				write_log( "Eso_Gopay_Payment->pay() ended with error: " . $e->getMessage() );

				$this->order->add_log( "Platební metoda GoPay skončila systémovou chybou." );

				return false;
			}
		}

		/**
		 * @param $payment_id
		 *
		 * @return mixed
		 *
		 * @since 2019.5
		 */
		public function getStatus( $payment_id ) {
			$response = $this->gopay->getStatus( $payment_id );

			if ( $response->hasSucceed() ) {
				return $response->json['state'];
			}

			return "failed";
		}

		/**
		 * @since 2019.8 return waiting on CANCELLED
		 *
		 * @param $payment_id
		 *
		 * @return string
		 */
		public function get_result( $payment_id ) {
			$status = $this->getStatus( $payment_id );

			switch ( $status ) {
				case "PAID":
					return "thankyou";
					break;
				case "PAYMENT_METHOD_CHOSEN":
				case "AUTHORIZED":
				case "CANCELED":
				case "CREATED":
					return "waiting";
					break;
				default:
					return "failed";
					break;
			}
		}

	}
}