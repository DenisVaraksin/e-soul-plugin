<?php
/**
 * @since 2019.6
 * @deprecated will be probably removed 2020.3
 */
if ( ! class_exists( 'Eso_Facebook_Feed', false ) ) {

	class Eso_Facebook_Feed {
		public function __construct( ) {
			add_shortcode( 'eso_facebook_feed', array( $this, 'render_shortcode' ) );
		}

		private function fetch_url($url) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);

			$feedData = curl_exec($ch);
			curl_close($ch);

			return $feedData;
		}

		private function get_status_time($created_time) {
			$months = ["ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince"];

			return date("j", strtotime($created_time)) . ". " . $months[date("m", strtotime($created_time)) - 1];
		}

		public function render_shortcode() {

			$module = new Eso_Module("facebook_feed");

			$page_id = $module->get_data_value("page_id");
			$postsLimit = $module->get_data_value("posts_limit");
			$app_id = "257603041382403";
			$app_secret = "552a1f64d70bb2856c99da33508fd28b";

			//Retrieve auth token - do we need that?
			$authToken = $this->fetch_url("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");

			$json_object = $this->fetch_url("https://graph.facebook.com/$page_id/posts?access_token=EAAQ26aZCZBFg0BAJJhZBTL8v6I8z1IljirEaUA9iO8czWliQKZBUa9sKZBL2rqtPpBVcg3s9aFZBoay89wxEYQdozzGHZAS6TISOThSgYkYg8r0aLhpIwoD0fweh25BRzcAbfLXLLyaqWQQjyt06JEFI6l2VqYHnuvgjWFCyMvUEAZDZD&fields=message,full_picture,created_time,link&limit=$postsLimit");
			$fcb_data = json_decode($json_object);

			$out = '';

			if(!empty($fcb_data)) {
				$out .= '<div class="esf-posts-wrapper esf-posts-fb">';

				if(isset($fcb->data)) {
					foreach($fcb_data->data as $status):
						$statusTime = $this->get_status_time($status->created_time);

						$out .= '
					<a href="https://facebook.com/' . $status->id . '" target="_blank" class="esf-item esf-item-fb">';

						if (isset($status->full_picture)) {
							$out .= "<div class='esf-thumbnail-wrapper'><img src='{$status->full_picture}' class='esf-thumbnail'/></div>";
						}
						$out .= '<div class="esf-date">' . $statusTime . '</div>';

						if(isset($status->message)) {
							$out .= '<p class="esf-post-text">' . $status->message . '</p>';
						}

						$out .= '</a>';
					endforeach;
				}

				$out .= '
			</div>
		';
			} else {
				$out .= '<p class="text-danger">' . __("Zatím žádné příspěvky.", "eso") . '</p>';
			}

			return $out;
		}
	}
}

if(function_exists('eso_is_module_active')) {
	if(eso_is_module_active("facebook_feed")) {
		return new Eso_Facebook_Feed();
	}
}
