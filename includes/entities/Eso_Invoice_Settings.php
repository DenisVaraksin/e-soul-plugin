<?php
if ( ! class_exists( 'Eso_Invoice_Settings', false ) ) {
	/**
	 * Class Eso_Invoice_Settings
	 *
	 * @since 2019.6
	 */
	class Eso_Invoice_Settings {

		public function __construct( ) {

		}

		public function get_issued_by() {
			return get_option("eso_invoice_issuedby");
		}

		public function get_street() {
			return get_option("eso_billing_street");
		}

		public function get_city() {
			return get_option("eso_billing_city");
		}

		public function get_country() {
			return get_option("eso_billing_country");
		}

		public function get_ico() {
			return get_option("eso_ico");
		}

		public function get_dic() {
			return get_option("eso_dic");
		}

		public function get_file_number() {
			return get_option("eso_billing_file_number");
		}

		public function get_before_note() {
			return get_option("eso_billing_before_note");
		}

		public function get_after_note() {
			return get_option("eso_billing_after_note");
		}

		private function get_logo_id() {
			return get_option("eso_store_logo");
		}

		public function get_logo() {
			return scaled_image_path($this->get_logo_id(), 'medium');
		}

		/**
		 * @return bool
		 */
		public function is_vat_payer() {
			$meta = get_option("eso_billing_vat_payer");

			if($meta == 1) {
				return true;
			}

			return false;
		}

		public function get_prefix() {
			return get_option("eso_invoice_prefix");
		}

		/**
		 * @return bool
		 */
		public function has_prefix_year() {
			$meta = get_option("eso_invoice_prefix_year");

			if($meta == 1) {
				return true;
			}

			return false;
		}

		public function get_number_length() {
			return (int) get_option("eso_invoice_number_length");
		}

		public function get_due_days() {
			return get_option("eso_invoice_due_days");
		}

		public function get_constant_symbol() {
			return get_option("eso_constant_symbol");
		}

		public function get_specific_symbol() {
			return get_option("eso_specific_symbol");
		}

		public function get_bank_account() {
			return get_option("eso_store_account_number");
		}

		public function get_iban() {
			return get_option("eso_store_iban");
		}

		public function get_bic() {
			return get_option("eso_store_bic");
		}

		public function get_contact_name() {
			return get_option("eso_invoice_contact_name");
		}

		public function get_contact_email() {
			return get_option("eso_invoice_contact_email");
		}

		public function get_contact_phone() {
			return get_option("eso_invoice_contact_phone");
		}

	}
}