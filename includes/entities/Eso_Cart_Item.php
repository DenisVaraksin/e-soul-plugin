<?php
if ( ! class_exists( 'Eso_Cart_Item', false ) ) {
	class Eso_Cart_Item {

		private $product_id;

		public function __construct( $product_id ) {
			$this->product_id = $product_id;
		}

		/**
		 * Returns the item id
		 *
		 * @return int
		 */
		public function get_id() {
			return (int) $this->product_id;
		}

		/**
		 * Prints out the item id
		 */
		public function the_id() {
			echo $this->product_id;
		}

		/**
		 * @since 2019.6
		 *
		 * @return Eso_Product
		 */
		public function get_product() {
			return new Eso_Product( $this->get_id() );
		}

		public function get_quantity() {
			global $wpdb;

			$result = $wpdb->get_row( "SELECT quantity FROM " . ESO_CART_TABLE . " WHERE token = '" . eso_session_token() . "' AND product_id = " . $this->get_id(), ARRAY_A );

			if ( $result ) {
				return (int) $result["quantity"];
			}

			return 0;
		}

		/**
		 * @since 2019.8 update stock status when quantity changes
		 *
		 * @param $quantity
		 *
		 * @return false|int
		 */
		public function set_quantity( $quantity ) {
			$old_quantity        = $this->get_quantity();
			$quantity_difference = $quantity - $old_quantity;

			if ( $quantity_difference > 0 ) {
				$this->get_product()->reduce_stock_amount( $quantity_difference );
			} else if ( $quantity_difference < 0 ) {
				$this->get_product()->add_stock_amount( $quantity_difference );
			}

			global $wpdb;

			$data = [
				"quantity" => $quantity
			];

			$where = [
				"token"      => eso_session_token(),
				"product_id" => $this->get_id()
			];

			$result = $wpdb->update( ESO_CART_TABLE, $data, $where );

			if ( $result == 0 ) {
				$data = [
					"token"      => eso_session_token(),
					"product_id" => $this->get_id(),
					"quantity"   => $quantity
				];

				$insert = $wpdb->insert( ESO_CART_TABLE, $data );

				return $insert;
			}

			return $result;
		}

		public function add_quantity( $quantity = 1 ) {
			$cart = new Eso_Cart( eso_session_token() );

			$new_quantity = $this->get_quantity() + $quantity;
			$this->set_quantity( $new_quantity );

			return $cart->update_item( $this );
		}

		public function get_total( Eso_Currency $currency, $comma_delimiter = false ) {
			$quantity = $this->get_quantity();

			$total = $this->get_product()->get_price( $currency ) * $quantity;

			if ( $comma_delimiter ) {
				return str_replace( ".", ",", $total );
			}

			return $total;
		}

		/**
		 * @since 2019.6
		 *
		 * @param Eso_Currency $currency
		 *
		 * @return float|int
		 */
		public function get_total_without_tax( Eso_Currency $currency ) {
			$total = $this->get_product()->get_price_without_tax( $currency ) * $this->get_quantity();

			return $total;
		}

		/**
		 * @param bool $comma_delimiter
		 *
		 * @return string
		 */
		public function get_total_with_currency( $comma_delimiter = false ) {
			/* @var $currency Eso_Currency */
			$currency = eso_get_active_currency();

			return $this->get_total( $currency, $comma_delimiter ) . " " . $currency->get_symbol();
		}

	}
}