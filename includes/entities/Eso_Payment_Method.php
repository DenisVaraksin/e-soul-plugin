<?php
if ( ! class_exists( 'Eso_Payment_Method', false ) ) {
	class Eso_Payment_Method {

		private $code;

		private $name;

		private $message;

		/**
		 * Eso_Payment_Method constructor.
		 *
		 * @param $code
		 */
		public function __construct( $code ) {
			$this->code = $code;

			global $wpdb;

			$row = $wpdb->get_row( "SELECT name, code, message, enabled FROM " . ESO_PAYMENT_METHOD_TABLE . " WHERE code = '" . $this->get_code() . "'" );

			if ( ! empty( $row->name ) ) {
				$this->name = $row->name;
			}
			if ( ! empty( $row->message ) ) {
				$this->message = $row->message;
			}
			if ( ! empty( $row->enabled ) ) {
				$this->enabled = $row->enabled;
			}
		}

		public function get_code() {
			return $this->code;
		}

		public function get_name() {
			return $this->name;
		}

		public function get_message() {
			return $this->message;
		}

		public function set_name( $name ) {
			$this->name = $name;

			global $wpdb;

			$data = [
				"name" => $name
			];

			$where = [
				"code" => $this->get_code()
			];

			$wpdb->update( ESO_PAYMENT_METHOD_TABLE, $data, $where );
		}

		public function is_enabled() {
			return $this->enabled;
		}

		public function set_enabled( $enabled ) {
			global $wpdb;
			$data = [
				"enabled" => $enabled
			];

			$where = [
				"code" => $this->get_code()
			];
			$wpdb->update( ESO_PAYMENT_METHOD_TABLE, $data, $where );
		}

	}
}