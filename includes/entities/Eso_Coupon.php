<?php
if ( ! class_exists( 'Eso_Coupon', false ) ) {
	/**
	 * @since 2019.7
	 *
	 * Class Eso_Coupon
	 */
	class Eso_Coupon {

		public function __construct( $id ) {
			$this->id = (int) $id;

			global $wpdb;
			$row = $wpdb->get_row( "SELECT id, name, code, type, value, free_shipping, registered_only, enabled, date_start, date_end FROM " . ESO_COUPON_TABLE . " WHERE id = " . $this->id );

			if ( isset( $row->name ) ) {
				$this->name = $row->name;
			}
			if ( isset( $row->code ) ) {
				$this->code = $row->code;
			}
			if ( isset( $row->type ) ) {
				$this->type = $row->type;
			}
			if ( isset( $row->value ) ) {
				$this->value = $row->value;
			}
			if ( isset( $row->free_shipping ) ) {
				$this->free_shipping = $row->free_shipping;
			}
			if ( isset( $row->registered_only ) ) {
				$this->registered_only = $row->registered_only;
			}
			if ( isset( $row->enabled ) ) {
				$this->enabled = $row->enabled;
			}

			if ( ! empty( $row->date_start ) ) {
				$this->date_start = $row->date_start;
			}
			if ( ! empty( $row->date_end ) ) {
				$this->date_end = $row->date_end;
			}
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return $this->id;
		}

		/**
		 * @return string|null
		 */
		public function get_name() {
			if ( isset( $this->name ) ) {
				return $this->name;
			}

			return null;
		}

		/**
		 * @return string|null
		 */
		public function get_code() {
			if ( isset( $this->code ) ) {
				return $this->code;
			}

			return null;
		}

		/**
		 * @return string|null
		 */
		public function get_type() {
			if ( isset( $this->type ) ) {
				return $this->type;
			}

			return null;
		}

		/**
		 * @return null|string
		 */
		public function get_pretty_discount() {
			if ( $this->get_type() && $this->get_value() ) {

				if ( $this->get_type() == "%" ) {
					$discount = "- " . $this->get_value();
					$discount .= " %";
				} else {
					$values = str_replace( " ", "", $this->value );
					$values = explode( ",", $values );

					$discount = $values[0] . " Kč, ";
					$discount .= $values[1] . " $, ";
					$discount .= $values[2] . " €, ";
					$discount .= $values[3] . " £";
				}

				return $discount;
			}

			return null;
		}

		/**
		 * @return array|string|null
		 */
		public function get_value( Eso_Currency $currency = null ) {
			if ( isset( $this->value ) ) {
				if ( $currency ) {
					if ( $this->get_type() == "-" ) {
						$values = str_replace( " ", "", $this->value );
						$values = explode( ",", $values );

						switch ( $currency->get_code() ) {
							case "CZK":
								return (float) $values[0];
								break;
							case "USD":
								return (float) $values[1];
							case "EUR":
								return (float) $values[2];
							case "GBP";
								return (float) $values[3];
								break;
						}

						return (float) $this->value;
					}

					return (float) $this->value;
				}

				return (float) $this->value;
			}

			return null;
		}

		public function get_date_start( $format = "string" ) {
			if ( isset( $this->date_start ) ) {
				$date = new DateTime( $this->date_start );
				if ( $format == "datetime-local" ) {
					return $date->format( "Y-m-d\TH:i" );
				} else if ( $format == "DateTime" ) {
					return $date;
				}

				return $date->format( "j. n. Y H:i" );
			}

			return null;
		}

		public function get_date_end( $format = "string" ) {
			if ( isset( $this->date_end ) ) {
				$date = new DateTime( $this->date_end );
				if ( $format == "datetime-local" ) {
					return $date->format( "Y-m-d\TH:i" );
				} else if ( $format == "DateTime" ) {
					return $date;
				}

				return $date->format( "j. n. Y H:i" );
			}

			return null;
		}

		public function is_free_shipping() {
			if ( isset( $this->free_shipping ) ) {
				return $this->free_shipping;
			}

			return null;
		}

		public function is_for_registered_only() {
			if ( isset( $this->registered_only ) ) {
				return $this->registered_only;
			}

			return null;
		}

		public function is_enabled() {
			if ( isset( $this->enabled ) ) {
				return $this->enabled;
			}

			return null;
		}

		/**
		 * @internal This doesn't check if the customer is logged in or not
		 *
		 * @return bool
		 */
		public function is_active() {
			if ( ! $this->is_enabled() ) {
				return false;
			}

			$timestamp = ( new DateTime() )->getTimestamp();

			if ( $this->get_date_start() ) {
				$date_start = ( $this->get_date_start( "DateTime" ) )->getTimestamp();

				if ( $timestamp < $date_start ) {
					return false;
				}
			}

			if ( $this->get_date_end() ) {
				$date_end = ( $this->get_date_end( "DateTime" ) )->getTimestamp();
				if ( $timestamp > $date_end ) {
					return false;
				}
			}

			//if ( $this->) {

			//}

			return true;
		}

		/**
		 * @return string
		 */
		public function get_edit_url() {
			return admin_url( "admin.php?page=eso-coupon-edit&id=" . $this->get_id() );
		}

		public function the_edit_url() {
			echo $this->get_edit_url();
		}

		public function update( $data ) {
			global $wpdb;

			if ( ! isset( $data["enabled"] ) ) {
				$data["enabled"] = null;
			}

			if ( ! isset( $data["free_shipping"] ) ) {
				$data["free_shipping"] = null;
			}

			if ( ! isset( $data["registered_only"] ) ) {
				$data["registered_only"] = null;
			}

			$where = [
				"id" => (int) $this->get_id()
			];

			$update = $wpdb->update( ESO_COUPON_TABLE, $data, $where );

			return $update;
		}

		public function delete() {
			global $wpdb;

			$where = [
				"id" => $this->get_id()
			];

			$wpdb->delete( ESO_COUPON_TABLE, $where );
		}
	}
}