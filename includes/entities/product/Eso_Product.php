<?php
if ( ! class_exists( 'Eso_Product', false ) ) {
	class Eso_Product {

		private $product_id;

		public function __construct( $product_id ) {
			$this->product_id = $product_id;
		}

		public function get_id() {
			return $this->product_id;
		}

		public function get_name() {
			$meta = get_the_title( $this->get_id() );

			return $meta;
		}

		public function get_slug() {
			$post = get_post( $this->get_id() );
			$meta = $post->post_name;

			return $meta;
		}

		public function get_perex() {
			$meta = get_post_meta( $this->get_id(), 'perex', true );

			return esc_textarea( $meta );
		}

		/**
		 * @return string
         * @deprecated will be removed 2020.3
		 */
		public function get_content() {
			$meta = get_post_meta( $this->get_id(), 'content', true );

			return esc_textarea( $meta );
		}

		/**
		 * @since 2019.6
		 */
		public function get_visibility() {
			return get_post_status( $this->get_id() );
		}

		/**
		 * @return string
		 *
		 * @since 2019.5
		 */
		public function get_edit_url() {
			return admin_url( "admin.php?page=single-product&id=" . $this->get_id() );
		}

		/**
		 * @since 2019.5
		 */
		public function the_edit_url() {
			echo $this->get_edit_url();
		}

		/**
		 * @return string
		 *
		 * @since 2020.1.16
		 */
		public function get_block_editor_url() {
			return get_edit_post_link($this->get_id());
		}

		/**
		 * @since 2020.1.16
		 */
		public function the_block_editor_url() {
			echo $this->get_block_editor_url();
		}

		/**
		 * @return false|string
		 *
		 * @since 2019.6
		 */
		public function get_view_url() {
			return get_permalink( $this->get_id() );
		}

		/**
		 * @return array|false|WP_Term
		 */
		public function get_stock_status() {
			$terms = get_the_terms( $this->get_id(), 'stock_status' );

			if ( ! $terms ) {
				return eso_get_default_stock_status();
			}

			return $terms[0];
		}

		/**
		 * @since 2019.6
		 */
		public function delete() {
			if ( current_user_can( "install_plugins" ) ) {
				wp_delete_post( $this->get_id() );
			}
		}

		/**
		 * @return int
		 */
		public function get_stock_status_id() {
			$stock_status = $this->get_stock_status();

			return $stock_status->term_id;
		}

		/**
		 * @return string
		 */
		public function get_stock_status_name() {
			$stock_status = $this->get_stock_status();

			return $stock_status->name;
		}


		/**
		 * @since 2019.6
		 */
		public function render_stock_status_name() {
			$name = $this->get_stock_status_name();

			echo "<span class='stock-status stock-status--" . $this->get_stock_status_slug() . "'>" . $name . "</span>";
		}

		public function get_stock_status_slug() {
			$stock_status = $this->get_stock_status();

			return $stock_status->slug;
		}

		/**
		 * @param $status_id
		 *
		 * @since 2019.5
		 */
		public function set_stock_status_by_id( $status_id ) {
			$status = get_term_by( 'id', $status_id, 'stock_status' );

			wp_set_post_terms( $this->get_id(), $status->term_id, 'stock_status' );
		}

		/**
		 * @param $status_slug
		 *
		 * @since 2019.5
		 */
		public function set_stock_status_by_slug( $status_slug ) {
			$status = get_term_by( 'slug', $status_slug, 'stock_status' );

			wp_set_post_terms( $this->get_id(), $status->term_id, 'stock_status' );
		}

		public function get_date_created() {
			return get_the_date( "", $this->get_id() );
		}

		/**
		 * @return bool
		 *
		 * @since 2019.5
		 */
		public function is_purchasable() {
			$non_purchasable_statuses = [ "sold-out", "not-available", "on-request" ];

			$stock_status = $this->get_stock_status_slug();

			if ( in_array( $stock_status, $non_purchasable_statuses ) ) {
				return false;
			}

			return true;
		}

		public function is_discounted( Eso_Currency $currency ) {
			if ( $this->get_discount( $currency ) == 0 ) {
				return false;
			}

			return true;
		}

		/**
		 * @param Eso_Currency $currency
		 * @param bool $include_currency
		 *
		 * @return int|string
		 * @since 2019.6 We are saving prices as price_CZK and so on and not as an array
		 */
		public function get_price_before_discount( Eso_Currency $currency, $include_currency = false ) {
			$price = get_post_meta( $this->get_id(), 'price_' . $currency->get_code(), true );

			/**
			 * @internal fallback to array prices, can be deleted 2019.7
			 */
			if ( empty( $price ) ) {
				$meta = get_post_meta( $this->get_id(), 'price', true );
				if ( $meta && isset( $meta[ $currency->get_code() ] ) ) {
					$price = $meta[ $currency->get_code() ];
				}
			}

			if ( ! empty( $price ) ) {
				$price_before_discount = sanitize_text_field( str_replace( ",", ".", $price ) );
			}
//			@2019.6 Do we need this default currency fallback?
//			else if ( isset( $meta[ eso_get_default_currency_name() ] ) ) {
//				$price_before_discount = sanitize_text_field( str_replace( ",", ".", $meta[ eso_get_default_currency_name() ] ) );
//			}

			if ( isset( $price_before_discount ) && $include_currency ) {
				return $price_before_discount . " " . $currency->get_symbol();
			} else if ( isset( $price_before_discount ) ) {
				return intval( $price_before_discount );
			}

			return 0;
		}

		/**
		 * @param Eso_Currency $currency
		 * @param bool $comma_delimiter
		 * @param bool $include_currency
		 *
		 * @return float|int|mixed|string
		 */
		public function get_price( Eso_Currency $currency, $comma_delimiter = false, $include_currency = false ) {
			$price = eso_get_price_after_discount(
				$this->get_price_before_discount( $currency ),
				$this->get_discount_type( $currency ),
				$this->get_discount( $currency ),
				$comma_delimiter );

			if ( $include_currency ) {
				return $price . " " . $currency->get_symbol();
			}

			return $price;
		}

		/**
         * @since 2019.6
         * @since 2020.2.9 $include_currency param
         *
		 * @param Eso_Currency $currency
		 * @param bool $include_currency
         *
         * @return float|int
         * @see eso_get_active_currency() for returning in user's active currency
		 */
		public function get_price_without_tax( Eso_Currency $currency, $include_currency = false ) {
			$percentage     = $this->get_tax_rate( $currency )->get_value();
			$price_with_tax = $this->get_price( $currency );

			$price_without_tax = round($price_with_tax - ($price_with_tax * ($percentage / (100 + $percentage))), 2);

			if($include_currency) {
			    return $price_without_tax . " " . $currency->get_symbol();
            }

			return $price_without_tax;
		}

		/**
		 * @since 2019.6
         * @since 2020.2.9 $include_currency param
         *
		 * @param bool $include_currency
         * @param Eso_Currency $currency
		 *
		 * @return float|int
		 */
		public function get_price_before_discount_without_tax( Eso_Currency $currency, $include_currency = false ) {
			$price_with_tax = $this->get_price_before_discount( $currency );
			$tax_rate       = $this->get_tax_rate( $currency );
			$percentage     = $tax_rate->get_value();

			$price = round($price_with_tax - ($price_with_tax * ($percentage / (100 + $percentage))), 2);

			if($include_currency) {
			    return $price . " " . $currency->get_symbol();
            }

			return $price;
		}

		public function get_discount_type( Eso_Currency $currency ) {
			$meta = get_post_meta( $this->product_id, 'discount_type', true );

			if ( isset( $meta[ $currency->get_code() ] ) ) {
				return $meta[ $currency->get_code() ];
			}

			return "%";
		}

		public function get_discount( Eso_Currency $currency ) {
			$meta = get_post_meta( $this->product_id, 'discount', true );

			if ( isset( $meta[ $currency->get_code() ] ) ) {
				return intval( ( $meta[ $currency->get_code() ] ) );
			}

			return 0;
		}

		/**
		 * @param Eso_Currency $currency
		 *
		 * @return Eso_Tax_Rate
		 */
		public function get_tax_rate( Eso_Currency $currency ) {
			$meta = get_post_meta( $this->product_id, 'tax', true );

			if ( isset( $meta[ $currency->get_code() ] ) ) {
				$tax_rate_id = intval( $meta[ $currency->get_code() ] );
			} else if ( isset( $meta[ eso_get_default_currency_name() ] ) ) {
				$tax_rate_id = intval( $meta[ eso_get_default_currency_name() ] );
			} else {
				$tax_rate_id = 1;
			}

			$tax_rate = new Eso_Tax_Rate( $tax_rate_id );

			return $tax_rate;
		}

		public function get_tax_rate_id( Eso_Currency $currency ) {
			$meta = get_post_meta( $this->product_id, 'tax', true );

			if ( isset( $meta[ $currency->get_code() ] ) ) {
				return intval( $meta[ $currency->get_code() ] );
			} else if ( isset( $meta[ eso_get_default_currency_name() ] ) ) {
				return intval( $meta[ eso_get_default_currency_name() ] );
			}

			return 1;
		}

		public function get_images( $include_featured = true ) {
			$meta = get_post_meta( $this->product_id, 'images', true );

			if ( ! $include_featured ) {
				if ( isset( $meta[0] ) ) {
					unset( $meta[0] );
				}

				return $meta;
			}

			return $meta;
		}

		/**
		 * @return int|null
		 */
		public function get_featured_image_id() {
			$images = $this->get_images();

			if ( isset( $images[0] ) ) {
				return intval( $images[0] );
			}

			return null;
		}

		/**
		 * @since 2019.5
		 *
		 * @param string $size
		 *
		 * @return null|string
		 */
		public function get_featured_image( $size = 'small' ) {
			if ( $this->get_featured_image_id() ) {
				return wp_get_attachment_image( $this->get_featured_image_id(), $size );
			}

			return null;
		}

		/**
		 * @param string $size
		 *
		 * @since 2019.5
		 */
		public function the_featured_image( $size = 'small' ) {
			if ( $this->get_featured_image() ) {
				echo $this->get_featured_image( $size );
			}
		}

		/**
		 * @since 2019.5
		 *
		 * @return mixed
		 */
		public function get_stock_amount() {
			$meta = get_post_meta( $this->get_id(), "stock_amount", true );

			return intval( $meta );
		}

		/**
		 * @since 2019.5
		 * @since 2019.8 $amount is absint()
		 *
		 * @param int $amount
		 */
		public function add_stock_amount( $amount = 1, $log = false ) {
			$amount = absint( $amount );

			$current_amount = $this->get_stock_amount();
			$new_amount     = $current_amount + $amount;

			if ( $new_amount > 0 && $this->get_stock_status_slug() == "sold-out" ) {
				$this->set_stock_status_by_slug( "in-stock" );
			}

			update_post_meta( $this->get_id(), "stock_amount", $new_amount );

			if($log) {
				$this->add_log( $current_amount, $amount );
            }
		}

		/**
		 * @param int $amount
		 *
		 * @since 2019.5
		 */
		public function reduce_stock_amount( $amount = 1, $log = false ) {
			$amount = absint( $amount );

			$current_amount = $this->get_stock_amount();
			$new_amount     = $current_amount - $amount;

			if ( $new_amount == 1 ) {
				$this->set_stock_status_by_slug( "last-item" );
			} else if ( $new_amount <= 0 ) {
				$this->set_stock_status_by_slug( "sold-out" );
				$new_amount = 0;
			}

			update_post_meta( $this->get_id(), "stock_amount", $new_amount );

			if ( $log ) {
				$this->add_log( $current_amount, "-" . $amount );
			}
		}

		/**
		 * @return mixed
		 *
		 * @since 2019.5
		 */
		public function get_log() {
			$meta = get_post_meta( $this->get_id(), "log" );

			return array_reverse( $meta );
		}

		/**
		 * @since 2019.5
		 * @since 2019.7 replace message with amount
		 */
		public function add_log( $old_amount, $storage_diff ) {
			if ( is_user_logged_in() ) {
				$current_user      = wp_get_current_user();
				$current_user_name = $current_user->display_name;
			} else {
				$current_user_name = "nepřihlášen";
			}

			$log = [];

			$log["time"]         = eso_db_now();
			$log["user"]         = $current_user_name;
			$log["old_amount"]   = $old_amount;
			$log["storage_diff"] = $storage_diff;

			add_post_meta( $this->get_id(), "log", $log );
		}

		/**
		 * @since 2019.5
		 */
		public function admin_render_storage_log() {
			require_once( ESO_DIR . "/admin/views/product/templates/storage-log.php" );
		}

		/**
		 * @param Eso_Currency $currency
		 * @param bool $include_currency
		 *
		 * @return mixed
		 * @since 2019.7 $include_currency introduced
		 *
		 * @since 2019.6
		 */
		public function get_turnover( Eso_Currency $currency = null, $include_currency = false ) {
			if ( ! isset( $currency ) ) {
				$currency = eso_get_active_currency();
			}

			$meta = get_post_meta( $this->get_id(), 'turnover_' . $currency->get_code(), true );

			if ( empty( $meta ) ) {
				return $this->set_turnover( $currency, $include_currency );
			}

			if ( $include_currency ) {
				return $meta . " " . $currency->get_symbol();
			}

			return $meta;
		}

		/**
		 * @since 2019.5
		 * @since 2019.7 $include_currency introduced
		 *
		 * @return float|int
		 *
		 * @internal This can be VERY slow - use it through Eso_Admin_Ajax or Eso_Ajax to execute asynchronously
		 */
		public function set_turnover( Eso_Currency $currency = null, $include_currency = false ) {
			if ( ! isset( $currency ) ) {
				$currency = eso_get_active_currency();
			}

			$turnover = 0;

			$orders = new WP_Query( [
				"post_type"     => "esoul_order",
				"post_per_page" => - 1,
				"post_status"   => "publish",
			] );

			/* @var $order Eso_Order */
			if ( $orders->have_posts() ) {
				while ( $orders->have_posts() ) {
					$orders->the_post();

					$order = new Eso_Order( get_the_ID() );

					if ( $order->has_item( $this->get_id() ) ) {
						$order_item = $order->get_item( $this->get_id() );

						$turnover += $order_item->get_total( $currency );
					}
				}
			}

			update_post_meta( $this->get_id(), "turnover_" . $currency->get_code(), $turnover );

			if ( $include_currency ) {
				$turnover .= " " . $currency->get_symbol();
			}

			return $turnover;
		}

		/**
		 * @since 2019.7
		 *
		 * @param DateTime $from
		 * @param DateTime $till
		 *
		 * @return float|int
		 */
		public function get_sales_development( DateTime $from, DateTime $till ) {
			$reports = new Eso_Reports();

			$turnover = 0;

			$orders = $reports->get_orders_between( $from, $till );

			/* @var $order Eso_Order */
			if ( $orders->have_posts() ) {
				while ( $orders->have_posts() ) {
					$orders->the_post();

					$order = new Eso_Order( get_the_ID() );

					if ( $order->has_item( $this->get_id() ) ) {
						$order_item = $order->get_item( $this->get_id() );

						$turnover += $order_item->get_total( eso_get_active_currency() );
					}
				}
			}

			return $turnover;
		}


		/**
		 * @param DateTime $from
		 * @param DateTime $till
		 *
		 * @return array
		 * @throws Exception
		 * @since 2019.7
		 *
		 * @internal this is sort of duplicate of Eso_Dashboard->get_overview_data() and should be re-thought
		 */
		public function get_overview_data( DateTime $from, DateTime $till ) {
			$days_diff = $till->diff( $from )->days;

			if ( $days_diff < 32 ) {
				$interval = DateInterval::createFromDateString( '1 day' );
				$period   = new DatePeriod( $from, $interval, $till->modify( "+1 day" ) );

				/* @var $day DateTime */
				foreach ( $period as $day ) {
					$day->setTime( 0, 0 );
					$labels[] = $day->format( "j. n." );

					$this_day = new DateTime( $day->format( "Y-m-d" ) );
					$next_day = new DateTime( $day->modify( "+1 day" )->format( "Y-m-d" ) );

					$data[] = $this->get_sales_development( $this_day, $next_day );

				}

			} else {
				$interval = new DateInterval( 'P1M' );
				$period   = new DatePeriod( $from, $interval, $till->modify( "+1 day" ) );

				/* @var $day DateTime */
				foreach ( $period as $day ) {

					$this_day = new DateTime( $day->format( "Y-m-d" ) );
					$next_day = new DateTime( $day->modify( "last day of this month" )->format( "Y-m-d" ) );

					$data[] = $this->get_sales_development( $this_day, $next_day );
				}

				$labels = [ "1.", "2.", "3.", "4.", "5.", "6.", "7.", "8.", "9.", "10.", "11.", "12." ];
			}

			$overview = [
				"labels"   => $labels,
				"datasets" => [
					"label" => __( "Tržby", "eso" ),
					"data"  => $data
				]
			];

			return $overview;
		}

		/**
		 * @since 2019.6
		 *
		 * @return null|WP_Term
         *
         * @deprecated use get_categories()
		 */
		private function get_category() {
			$terms = get_the_terms( $this->get_id(), "product_category" );

			if ( $terms && ! is_wp_error( $terms ) ) {
				return $terms[0];
			}

			return null;
		}

		/**
		 * @since 2019.6
		 *
		 * @return int|null
         *
         * @deprecated use get_categories()
		 */
		public function get_category_id() {
			$category = $this->get_category();

			if ( $category ) {
				return $category->term_id;
			}

			return null;
		}

		/**
		 * @since 2019.6
		 *
		 * @return null|string
         * @deprecated use get_categories()
		 */
		public function get_category_name() {
			$category = $this->get_category();

			if ( $category ) {
				return $category->name;
			}

			return null;
		}

		/**
         * Returns array of category terms
         *
		 * @since 2020.3.5
		 */
		public function get_categories() {
			$terms = get_the_terms( $this->get_id(), 'product_category' );

			if ( $terms && ! is_wp_error( $terms ) ) {
				return $terms;
			}

			return null;
        }

		/**
		 * Outputs comma separated list of product categories
		 *
		 * @param bool $with_tags if true, categories will be clickable
		 *
		 * @return false|string|WP_Error
		 * @since 2020.3.5
		 */
		public function get_category_list($with_tags = false) {
		    $list = get_the_term_list($this->get_id(), 'product_category', '', ', ');

		    if($with_tags) {
		        return $list;
            }

			return strip_tags($list);
		}

		/**
		 * Renders all product categories with their respective HTML
		 *
		 * @param string $category_class
		 *
		 * @since 2020.3.2
		 */
		public function render_categories($category_class = "product-item") {
			if ( ! empty( $this->get_categories() ) ) {
				foreach($this->get_categories() as $term) {
					$product_category = new Eso_Product_Category( $term->term_id );
					$product_category->render( $category_class );
				}
			}
		}

		/**
		 * @since 2019.7
		 *
		 * @return null|WP_Term
         * @deprecated will be removed 2020.10, use get_tags()
		 */
		private function get_tag() {
			$terms = get_the_terms( $this->get_id(), "product_tag" );

			if ( $terms && ! is_wp_error( $terms ) ) {
				return $terms[0];
			}

			return null;
		}

		/**
		 * @since 2019.7
		 *
		 * @return int|null
         * @deprecated will be removed 2020.10, use get_tags()
		 */
		public function get_tag_id() {
			$tag = $this->get_tag();

			if ( $tag ) {
				return $tag->term_id;
			}

			return null;
		}

		/**
		 * @since 2019.7
		 *
		 * @return null|string
         * @deprecated will be removed 2020.10, use get_tags()
		 */
		public function get_tag_name() {
			$tag = $this->get_tag();

			if ( $tag ) {
				return $tag->name;
			}

			return null;
		}

		/**
		 * @since 2019.7
		 *
		 * @return null|string
         * @deprecated will be removed 2020.10
		 */
		public function get_tag_slug() {
			$tag = $this->get_tag();

			if ( $tag ) {
				return $tag->slug;
			}

			return null;
		}


		/**
		 * @since 2019.7
		 */
		public function get_tags() {
			$terms = get_the_terms( $this->get_id(), "product_tag" );

			if ( $terms && ! is_wp_error( $terms ) ) {
				return $terms;
			}

			return null;
		}

		/**
		 * Outputs comma separated list of product tags
		 *
		 * @param bool $with_tags If true, tags will be clickable
		 *
		 * @return string
		 * @since 2020.3.2
		 */
		public function get_tag_list($with_tags = false) {
		    $list = get_the_term_list($this->get_id(), 'product_tag', '', ', ');

		    if($with_tags) {
			    return $list;
            }

			return strip_tags($list);
        }

		/**
		 * Renders all product tags with their respective HTML
		 *
		 * @param string $tag_class
		 *
		 * @since 2020.3.2
		 */
        public function render_tags($tag_class = "product-item") {
	        if ( ! empty( $this->get_tags() ) ) {
		        foreach($this->get_tags() as $term) {
			        $product_tag = new Eso_Product_Tag( $term->term_id );
			        $product_tag->render( $tag_class );
		        }
	        }
        }

		/**
		 * @since 2019.6
		 *
		 */
		public function get_list_item_template() {
		    $template_url = eso_get_template_url("product/list-item");

			ob_start();
			require( $template_url );
			$html = ob_get_contents();
			ob_get_clean();

			return $html;
		}

        /**
         * @since 2020.5
         *
         */
        public function get_list_item_search_template() {
            $template_url = eso_get_template_url("product/list-item-search");

            ob_start();
            require( $template_url );
            $html = ob_get_contents();
            ob_get_clean();

            return $html;
        }

		/**
		 * @since 2019.7
		 *
		 * @param $parent_id
		 */
		public function admin_render_related_product_info( $parent_id ) {
			require( ESO_DIR . '/admin/views/product/templates/related-product-info.php' );
		}

		/**
		 * @since 2019.7
		 *
		 * @see eso_render_available_related_products()
		 */
		public function admin_render_row_item() {
			require( ESO_DIR . '/admin/views/product/templates/row-item.php' );
		}

		/**
		 * @since 2019.7
		 *
		 * @param $product_id
		 */
		public function add_related_product( $product_id ) {
			$meta = $this->get_related_products_array();

			if ( ! is_array( $meta ) ) {
				$meta = [];
			}

			$meta[ $product_id ] = $product_id;

			update_post_meta( $this->get_id(), "related_products", $meta );
		}

		/**
		 * @since 2019.7
		 *
		 * @param $product_id
		 */
		public function remove_related_product( $product_id ) {
			$meta = $this->get_related_products_array();

			if ( is_array( $meta ) && isset( $meta[ $product_id ] ) ) {
				unset( $meta[ $product_id ] );
			}

			update_post_meta( $this->get_id(), "related_products", $meta );
		}

		/**
		 * @since 2019.7
		 *
		 * @return array
		 */
		public function get_related_products_array() {
			$meta = get_post_meta( $this->get_id(), "related_products", true );

			if ( empty( $meta ) ) {
				return [];
			}

			return $meta;
		}

		/**
		 * @since 2019.7
		 *
		 * @return array
		 */
		public function get_related_products() {
			$products = [];

			foreach ( $this->get_related_products_array() as $product_id ) {
				$products[ $product_id ] = new Eso_Product( $product_id );
			}

			return $products;
		}

		/**
		 * Renders all related products.
		 * Defaults to plugin template, but you can copy it from /templates/product/related-products.php
		 * put it inside /templates/product/related-products.php and modify it to your liking.
		 *
		 * @since 2020.2.17
		 */
		public function render_related_products() {
			$products = $this->get_related_products_array();

			$template_url = eso_get_template_url("product/related-products");

			if(!empty($template_url)) {
				require($template_url);
			}

			return;
		}

		/**
		 * @since 2019.7
		 *
		 * @return array
		 */
		public function get_variants_meta() {
			$meta = get_post_meta( $this->get_id(), "variants", true );

			if ( empty( $meta ) ) {
				return [];
			}

			return $meta;
		}

		/**
		 * @since 2019.7
		 *
		 * @return array
		 */
		public function get_variants() {
			$variants = [];

			foreach ( $this->get_variants_meta() as $variant_slug => $variant_data ) {
				$variants[ $variant_slug ] = new Eso_Product_Variant( $variant_slug, $this );
			}

			return $variants;
		}

		/**
		 * @since 2019.7
		 *
		 * @param $slug
		 *
		 * @see Eso_Product_Variant
		 *
		 * @return array|null
		 */
		public function get_variant( $slug ) {
			$variants = $this->get_variants_meta();

			if ( isset( $variants[ $slug ] ) ) {
				return $variants[ $slug ];
			}

			return null;
		}

		/**
         * Renders all variants and their values as select inputs.
         * Defaults to plugin template, but you can copy it from /templates/product/variant.php
         * put it inside /templates/product/variant.php and modify it to your liking.
         *
		 * @since 2020.2.17
		 */
		public function render_variants() {
            $variants = $this->get_variants_meta();

            require(eso_get_template_url("product/variants"));

            return;
        }

		public function admin_render_new_variant_form() {
			$fields = new Eso_Fields();
			?>
            <div class="col-xl-4 col-lg-6 mb-default add-variant">
                <div class="whitebox uibox">
                    <div id="add-variant-form">
						<?php $fields->form_group_input( "variant_name", "Název vlastnosti" ); ?>
						<?php $fields->form_group_input( "variant_attributes", "Hodnoty (oddělené čárkami)" ); ?>
                        <button type="button"
                                class="btn btn-light btn-plus"><?php _e( "Přidat vlastnost", "eso" ) ?></button>
                    </div>
                </div>
            </div>
			<?php
		}

		/**
		 * @since 2020.2.1
		 */
		public function get_color() {
			$meta = get_post_meta( $this->get_id(), "color", true );

			return $meta;
        }

		/**
		 * @since 2020.2.1
		 */
        public function get_symbol_image_id() {
	        $meta = get_post_meta( $this->get_id(), "symbol_image_id", true );

	        return $meta;
        }

		/**
		 * @param array $size
		 *
		 * @return false|string
         * @since 2020.2.1
		 */
        public function get_symbol_image_url($size = [50, 50]) {
	        $symbol_image_id = $this->get_symbol_image_id();

	        if($symbol_image_id) {
	            return wp_get_attachment_image_url($symbol_image_id, $size);
            }

	        return null;
        }

		/**
		 * @param array $size
		 *
		 * @return string|null
		 * @since 2020.2.1
		 */
        public function get_symbol_image($size = [50, 50]) {
            $symbol_image_id = $this->get_symbol_image_id();

            if($symbol_image_id) {
                return wp_get_attachment_image($symbol_image_id, $size);
            }

            return null;
        }

        /**
         * @since 2020.4
         *
         * @return mixed
         */
        public function get_ean_code() {
            $meta = get_post_meta( $this->get_id(), "ean_code", true );
            if($meta) {
                return htmlspecialchars( $meta );
            }
            return null;
        }
	}
}