<?php
if ( ! class_exists( 'Eso_Product_Variant', false ) ) {
	/**
	 * @since 2019.7
	 *
	 * Class Eso_Product_Variant
	 */
	class Eso_Product_Variant {

		private $slug;
		private $product;

		public function __construct( $slug, Eso_Product $product ) {
			$this->slug    = $slug;
			$this->product = $product;
		}

		/**
		 * @param $name
		 * @param $attributes - comma separated string
		 */
		public function add( $name, $attributes ) {
			$meta = $this->get_product()->get_variants_meta();

			if ( ! is_array( $meta ) ) {
				$meta = [];
			}

			$attributes = explode( ",", $attributes );

			$attributes_array = [];

			foreach ( $attributes as $attribute ) {
				$attribute_name                                        = trim( $attribute );
				$attributes_array[ sanitize_title( $attribute_name ) ] = [
					"name" => $attribute_name,
					"slug" => sanitize_title( $attribute_name )
				];
			}

			$meta[ $this->get_slug() ]["name"]       = $name;
			$meta[ $this->get_slug() ]["attributes"] = $attributes_array;

			update_post_meta( $this->get_product()->get_id(), "variants", $meta );
		}

		/**
		 * @return string
		 */
		public function get_slug() {
			return sanitize_text_field( $this->slug );
		}

		/**
		 * @return Eso_Product
		 */
		public function get_product() {
			return $this->product;
		}

		/**
		 * @return array|null
		 */
		public function get_data_array() {
			return $this->get_product()->get_variant( $this->get_slug() );
		}

		/**
		 * @return string|null
		 */
		public function get_name() {
			if ( isset( $this->get_data_array()["name"] ) ) {
				return $this->get_data_array()["name"];
			}

			return null;
		}

		/**
		 * @return array
		 */
		public function get_attributes() {
			if ( isset( $this->get_data_array()["attributes"] ) ) {
				return $this->get_data_array()["attributes"];
			}

			return [];
		}

		/**
		 * @param $attribute_slug
		 *
		 * @return array|null
		 */
		public function get_attribute( $attribute_slug ) {
			if ( isset( $this->get_attributes()[ $attribute_slug ] ) ) {
				return $this->get_attributes()[ $attribute_slug ];
			}

			return null;
		}

		/**
		 * @param $attribute_name
		 */
		public function add_attribute( $attribute_name ) {
			$attribute_name = trim( $attribute_name );


			$meta = $this->get_product()->get_variants_meta();

			if ( ! is_array( $meta ) ) {
				$meta = [];
			}

			$meta[ $this->get_slug() ]["attributes"][ sanitize_title( $attribute_name ) ] = [
				"name" => $attribute_name,
				"slug" => sanitize_title( $attribute_name )
			];

			update_post_meta( $this->get_product()->get_id(), "variants", $meta );
		}

		public function remove_attribute( $attribute_slug ) {
			$meta = $this->get_product()->get_variants_meta();

			if ( isset( $meta[ $this->get_slug() ]["attributes"][ $attribute_slug ] ) ) {
				unset( $meta[ $this->get_slug() ]["attributes"][ $attribute_slug ] );
			}

			update_post_meta( $this->get_product()->get_id(), "variants", $meta );
		}

		public function render_edit_form() {
			require( ESO_DIR . "/admin/views/product/templates/edit-variant.php" );
		}

		public function render_editable_attribute( $attribute_slug ) {
			$attribute = $this->get_attribute( $attribute_slug );
			?>
            <div data-slug="<?php echo $attribute_slug ?>" class="btn btn-light editable-tag">
                <span class="tag-name"><?php echo $attribute["name"]; ?></span>
                <span class="remove-tag">&times;</span>
            </div>
		<?php }

		public function remove() {
			$meta = $this->get_product()->get_variants_meta();

			if ( is_array( $meta ) && isset( $meta[ $this->get_slug() ] ) ) {
				unset( $meta[ $this->get_slug() ] );
			}

			update_post_meta( $this->get_product()->get_id(), "variants", $meta );
		}


	}
}