<?php
if ( ! class_exists( 'Eso_Stock_Status', false ) ) {
	/**
	 * @since 2019.7
	 *
	 * Class Eso_Stock_Status
	 */
	class Eso_Stock_Status {
		const TAXONOMY = "stock_status";

		private $term_id;

		public function __construct( $term_id ) {
			$this->term_id = $term_id;

			$this->term = get_term( $this->term_id, self::TAXONOMY );
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->term_id;
		}

		public function set_name( $name ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "name" => $name ] );
		}

		public function get_name() {
			return $this->term->name;
		}

		/**
		 * @param $slug
		 *
		 * @returns null|WP_Error
		 */
		public function set_slug( $slug ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "slug" => $slug ] );
		}

		public function get_slug() {
			return $this->term->slug;
		}

		public function get_count() {
			return $this->term->count;
		}

		/**
		 * @return bool
		 */
		public function can_be_deleted() {
			if(!in_array($this->get_slug(), ["in-one-week", "until-christmas", "on-request", "last-item", "sold-out", "not-available", "in-stock"])) {
				return true;
			}

			return false;
		}
	}
}
