<?php
if ( ! class_exists( 'Eso_Product_Tag', false ) ) {
	/**
	 * @since 2019.7
	 *
	 * Class Eso_Product_Tag
	 */
	class Eso_Product_Tag {
		const TAXONOMY = "product_tag";

		private $term_id;

		public function __construct( $term_id ) {
			$this->term_id = $term_id;
			$this->term    = get_term( $this->term_id, self::TAXONOMY );
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->term_id;
		}

		public function set_name( $name ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "name" => $name ] );
		}

		public function get_name() {
			return $this->term->name;
		}

		/**
		 * @param $slug
		 *
		 * @returns null|WP_Error
		 */
		public function set_slug( $slug ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "slug" => $slug ] );
		}

		public function get_slug() {
			return $this->term->slug;
		}

		public function get_count() {
			return $this->term->count;
		}

		/**
		 * @param string $class
		 */
		public function render( $class = "product-item" ) { ?>
            <div class="<?php echo $class ?>__tag product-tag tag--<?php echo $this->get_slug() ?>"><?php echo $this->get_name() ?></div>
		<?php }
	}
}