<?php
if ( ! class_exists( 'Eso_Product_Category', false ) ) {
	/**
	 * @since 2019.7
	 *
	 * Class Eso_Product_Category
	 */
	class Eso_Product_Category {
		const TAXONOMY = "product_category";

		private $category_id;

		public function __construct( $category_id ) {
			$this->category_id = $category_id;
			$this->term        = get_term( $this->category_id, self::TAXONOMY );
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->category_id;
		}

		/**
		 * @param $name
		 */
		public function set_name( $name ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "name" => $name ] );
		}

		/**
		 * @return string
		 */
		public function get_name() {
			return $this->term->name;
		}

		/**
		 * @param $slug
		 *
		 * @returns null|WP_Error
		 */
		public function set_slug( $slug ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "slug" => $slug ] );
		}

		/**
		 * @return string
		 */
		public function get_slug() {
			return $this->term->slug;
		}

		/**
		 * @param $parent_id
		 */
		public function set_parent( $parent_id ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "parent" => $parent_id ] );
		}

		/**
		 * @return int
		 */
		public function get_parent_id() {
			return $this->term->parent;
		}

		/**
		 * @return int
		 */
		public function get_count() {
			return $this->term->count;
		}

		/**
		 * @param string $class
		 * @since 2020.3.5
		 */
		public function render( $class = "product-item" ) { ?>
			<div class="<?php echo $class ?>__category product-category category--<?php echo $this->get_slug() ?>"><?php echo $this->get_name() ?></div>
		<?php }
	}
}