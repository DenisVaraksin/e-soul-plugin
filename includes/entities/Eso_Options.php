<?php

if ( ! class_exists( 'Eso_Options', false ) ) {
	class Eso_Options {
		public function reset_to_factory_defaults() {
			if ( in_array( 'administrator', wp_get_current_user()->roles ) ) {
				global $wpdb;

				$tables = [
					ESO_MODULE_DATA_TABLE,
					ESO_MODULE_TABLE,
					ESO_CART_TABLE,
					ESO_MODULE_CAT_TABLE,
					ESO_PAYMENT_METHOD_TABLE,
					ESO_SHIPPING_METHOD_TABLE,
					ESO_SESSIONS_TABLE,
					ESO_SHIPPING_ZONE_TABLE
				];

				foreach($tables as $table) {
					$sql    = "DROP TABLE IF EXISTS " . $table;
					$wpdb->query( $sql );
				}
			}
		}
	}
}
