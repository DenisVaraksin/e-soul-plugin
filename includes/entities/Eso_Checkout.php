<?php
if ( ! class_exists( 'Eso_Checkout', false ) ) {
	/**
	 * Class Eso_Checkout
	 *
	 * @since 2019.5
	 * @since 2019.6    shortcodes removed and render functions moved to Eso_Checkout_Fields
	 */
	class Eso_Checkout {

		public function __construct() {

		}

		/**
		 * @param $customer_data
		 *
		 * @return int|WP_Error
		 * @since 2019.5
		 */
		public function handle_unregistered_customer( $customer_data ) {

			if ( email_exists( $customer_data["shipping_email"] ) ) {
				$user = get_user_by( "email", $customer_data["shipping_email"] );

				return $user->ID;
			}

			$userdata = [
				"user_login" => $customer_data["shipping_email"],
				"user_email" => $customer_data["shipping_email"],
				"first_name" => $customer_data["shipping_first_name"],
				"last_name"  => $customer_data["shipping_last_name"],
				"role"       => "eso_unregistered"
			];

			$user_id = wp_insert_user( $userdata );

			update_user_meta( $user_id, "shipping_phone", $customer_data["shipping_phone"] );

			return $user_id;
		}

		/**
		 * @param $post_data
		 *
		 * @throws Exception
		 * @since 2019.6
		 */
		public function do_checkout( $post_data ) {
			$order_id = $this->fill_order( $post_data );
			$this->process_payment( $order_id );
			$this->reduce_stock( $order_id );
			$this->empty_cart();
		}

		/**
		 * @param $post_data $_POST array containing data from HTML form
		 *
		 * @return int|WP_Error $order_id
		 *
		 * @throws Exception
		 * @since 2019.6 options reformatted to contain payment and shipment method price
		 * @since 2019.7 generate access token, coupon_id
		 * @since 2019.5
		 */
		public function fill_order( $post_data ) {

			$post_author_id = $this->handle_unregistered_customer( $post_data["checkout"]["customer"] );

			$postarr = [
				"post_title"   => __( "Objednávka selhala", "eso" ),
				"post_content" => "",
				"post_type"    => "esoul_order",
				"post_status"  => "publish",
				"post_author"  => $post_author_id
			];

			$order_id = wp_insert_post( $postarr );

			$title = __( "Objednávka #", "eso" ) . $order_id;

			$orderarr = [
				"ID"         => $order_id,
				"post_title" => $title,
				"post_name"  => sanitize_title( $title )
			];
			wp_update_post( $orderarr );

			update_post_meta( $order_id, "customer", $post_data["checkout"]["customer"] );
			if ( isset( $_POST["checkout"]["product"] ) ) {
				update_post_meta( $order_id, "products", $post_data["checkout"]["product"] );
			}

			$options = $post_data["checkout"]["options"];

			$shipping_country = $post_data["checkout"]["customer"]["shipping_country"];
			$zone             = new Eso_Shipping_Zone( $shipping_country );

			if ( isset( $options["shipping_method"] ) ) {
				$shipping_method_code                = $options["shipping_method"]["code"];
				$options["shipping_method"]["price"] = $zone->get_shipping_method_prices( $shipping_method_code );

				if ( isset( $options["payment_method"]["code"] ) ) {
					$payment_method_code                = $options["payment_method"]["code"];
					$options["payment_method"]["price"] = $zone->get_payment_method_prices( $shipping_method_code, $payment_method_code );
				}
			}


			if ( isset( $options["coupon_code"] ) ) {
				$coupon = eso_get_coupon_by_code( $options["coupon_code"] );

				if ( $coupon ) {
					$options["coupon_id"] = $coupon->get_id();

					if ( $coupon->get_type() == "-" ) {
						$options["coupon_discount"] = $coupon->get_value( eso_get_active_currency() );
					} else if ( $coupon->get_type() == "%" ) {
						$cart                       = new Eso_Cart( eso_session_token() );
						$percent                    = $coupon->get_value() / 100;
						$options["coupon_discount"] = $cart->get_total_before_discount() * $percent;
					}
				}
			} else {
				unset( $options["coupon_code"] );
			}

			update_post_meta( $order_id, "options", $options );

			update_post_meta( $order_id, "_token", bin2hex( random_bytes( 16 ) ) );

			return $order_id;
		}

		/**
		 * @param $order_id
		 *
		 * @since 2019.5
		 */
		public function process_payment( $order_id ) {
			$order = new Eso_Order( $order_id );

			$payment_method_code = $order->get_payment_method()->get_code();

			switch ( $payment_method_code ) :
				case "comgate_payments":
					$order->set_waiting_for_payment();
					$comgate_payment = new Eso_Comgate_Payment( $order );
					$comgate_payment->pay();
					break;
				case "gopay":
					$order->set_waiting_for_payment();
					$gopay_payment = new Eso_Gopay( $order );
					$gopay_payment->pay();
					break;
				case "gpwebpay":
					$order->set_waiting_for_payment();
					$payment = new Eso_GpWebPay( $order );
					$payment->pay();
					break;
				case "cod":
					$order->set_for_completion();
					echo eso_get_page_link( 'thankyou' ) . "?order_id=" . $order->get_id();
					break;
				default:
					$order->set_waiting_for_payment();
					echo eso_get_page_link( 'thankyou' ) . "?order_id=" . $order->get_id();
					break;
			endswitch;
		}

		/**
		 * @param $order_id
		 *
		 * @since 2019.6
		 */
		public function send_notification( $order_id ) {
			$store = new Eso_Store();

			$admin_email = new Eso_Email( $store->get_manager_email(), "new-order", $order_id );
			$admin_email->send();

			$order = new Eso_Order( $order_id );

			$customer_email = new Eso_Email( $order->get_shipping_email(), "order-thank-you", $order_id );
			$customer_email->send();
		}

		/**
		 * @since 2019.5
		 * @since 2020.1.18 sending notification from here instead do_checkout()
		 */
		public function checkout_result() {
			$order  = null;
			$result = null;

			if ( isset( $_GET["refId"] ) ) { //Comgate
				$order_id        = eso_get_order_id_by_comgate_payment_id( $_GET["refId"] );
				$order           = new Eso_Order( $order_id );
				$comgate_payment = new Eso_Comgate_Payment( $order );
				$result          = $comgate_payment->get_status();
			} else if ( isset( $_GET["id"] ) ) { //GoPay
				$order_id = eso_get_order_id_by_gopay_id( (int) $_GET["id"] );

				$order  = new Eso_Order( $order_id );
				$gopay  = new Eso_Gopay( $order );
				$result = $gopay->get_result( (int) $_GET["id"] );

				if ( ! isset( $order_id ) ) {
					return "failed";
				}
			} else if ( isset( $_GET["ORDERNUMBER"] ) ) { //GP WebPay
				$order_id = (int) $_GET["ORDERNUMBER"];

				if ( ! $order_id ) {
					return "failed";
				}

				$order = new Eso_Order( $order_id );
				$gpwebpay = new Eso_GpWebPay( $order );
				$result   = $gpwebpay->get_result();
				if ( ! $result ) {
					return "failed";
				}
			} else if ( isset( $_GET["order_id"] ) ) {
				$order_id = (int) $_GET["order_id"];
				$order    = new Eso_Order( $order_id );

				if ( $order->get_payment_method()->get_code() == "cod" ) {
					return "thankyou";
				} else if ( $order->get_payment_method()->get_code() == "bank_transfer" ) {
					return "waiting";
				}
			}

			if ( ! $order || ! $result ) {
				return "failed";
			}

			switch ( $result ) :
				case "thankyou" :
					$order->set_for_completion();
					break;
				case "cancelled" :
					$order->set_cancelled();
					break;
				case "waiting" :
					$order->set_waiting_for_payment();
					break;
				default :
					$order->set_failed();
					write_log( "Order ID " . $order->get_id() . " ended with status failed in checkout_result()" );

					return "failed";
					break;
			endswitch;

			$this->send_notification( $order->get_id() );

			return $result;
		}

		/**
		 * @param $order_id
		 *
		 * @since 2019.5
		 * @since 2019.11 we reduce stock amount in Eso_Cart_Item->set_quantity(), so removing it from here
		 * @deprecated will be removed 2020.11
		 */
		public function reduce_stock( $order_id ) {
			/*$order = new Eso_Order( $order_id );

			 @var $order_item Eso_Order_Item
			foreach ( $order->get_items() as $item_id => $order_item ) {
				$product  = new Eso_Product( $item_id );
				$quantity = $order_item->get_quantity();

				$product->reduce_stock_amount( $quantity );
			}*/
		}

		/**
		 * @return Eso_Cart
		 * @since 2019.6
		 *
		 */
		private function get_cart() {
			$cart = new Eso_Cart( eso_session_token() );

			return $cart;
		}

		/**
		 * @since 2019.5
		 */
		public function empty_cart() {
			$this->get_cart()->destroy();
		}

	}
}