<?php
if ( ! class_exists( 'Eso_Checkout_Fields', false ) ) {
	/**
	 * @since 2019.6
	 *
	 * Class Eso_Checkout_Fields
	 */
	class Eso_Checkout_Fields {

		public function __construct() {
			$this->cart = new Eso_Cart( eso_session_token() );
		}

		/**
		 * @return array
		 * @since 2019.6 moved to Eso_Checkout_Fields
		 *
		 */
		public function get_customer_meta_fields() {
			$fieldset = [
				"shipping" => [
					"title"  => __( "Doručovací údaje", "eso" ),
					"fields" => [
						"shipping_first_name" => [
							"label"    => __( "Křestní jméno", "eso" ),
							"required" => true
						],
						"shipping_last_name"  => [
							"label"    => __( "Příjmení", "eso" ),
							"required" => true
						],
						"shipping_phone"      => [
							"label"    => __( "Telefon", "eso" ),
							"type"     => "tel",
							"required" => true
						],
						"shipping_email"      => [
							"label"    => __( "Email", "eso" ),
							"type"     => "email",
							"required" => true
						],
						"shipping_street"     => [
							"label"    => __( "Ulice a č. p.", "eso" ),
							"required" => true
						],
						"shipping_city"       => [
							"label"    => __( "Město", "eso" ),
							"required" => true
						],
						"shipping_postcode"   => [
							"label"    => __( "PSČ", "eso" ),
							"required" => true
						],
						"shipping_country"    => [
							"label"    => __( "Země", "eso" ),
							"type"     => "select",
							"options"  => eso_get_enabled_shipping_zones(),
							"required" => true
						]
					]
				],
				"billing"  => [
					"title"  => __( "Fakturační údaje", "eso" ),
					"fields" => [
						"billing_on"           => [
							"label" => __( "Chci vyplnit fakturační údaje", "eso" ),
							"type"  => "checkbox"
						],
						"billing_company_name" => [
							"label" => __( "Jméno / Název firmy", "eso" )
						],
						"billing_ico"          => [
							"label" => __( "IČO", "eso" ),
						],
						"billing_dic"          => [
							"label" => __( "DIČ", "eso" ),
						],
						"billing_street"       => [
							"label" => __( "Ulice a č. p." ),
						],
						"billing_city"         => [
							"label" => __( "Město", "eso" ),
						],
						"billing_postcode"     => [
							"label" => __( "PSČ", "eso" ),
						],
						"billing_country"      => [
							"label"   => __( "Země", "eso" ),
							"type"    => "select",
							"options" => eso_get_enabled_shipping_zones(),
						]
					]
				]
			];

			return $fieldset;
		}

		/**
		 * @since 2019.7
		 */
		public function render_cart_sum() {
			$cart = new Eso_Cart( eso_session_token() );

			if ( $cart->get_sum() == 0 ) {
				return null;
			}
			?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 offset-md-6">
                        <p class="cart-sum-without-tax">
                            <span><?php _e( "Celkem bez DPH", "eso" ) ?></span>
                            <span class="float-right"><?php echo $cart->get_sum_without_tax( true, true ); ?></span>
                        </p>
                        <p class="cart-sum-with-tax">
                            <span><?php _e( "Celkem s DPH", "eso" ) ?></span>
                            <span class="float-right"><?php echo $cart->get_sum( true, true ) ?></span>
                        </p>
                    </div>
                </div>
            </div>
		<?php }

		/**
		 * @param $field_key
		 *
		 * @since 2019.5
		 */
		public function input_attributes( $field_key ) {
			$check_with_billing = [
				"shipping_street"   => "billing_street",
				"shipping_city"     => "billing_city",
				"shipping_postcode" => "billing_postcode",
				"shipping_country"  => "billing_country"
			];

			$classes = "form-control";

			if ( isset( $check_with_billing[ $field_key ] ) ) {
				$classes .= " check-with-billing";

				echo "data-check='" . $check_with_billing[ $field_key ] . "'";
			}

			echo "class='" . $classes . "'";
		}

		public function render_customer_fields() {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );
			}
			$checkout_fields = new Eso_Checkout_Fields();
			$customer_fields = $checkout_fields->get_customer_meta_fields();

			$shipping_columns = $customer_fields["shipping"];
			$billing_columns  = $customer_fields["billing"];
			?>
            <div class="row checkout-group checkout-customer-group">
                <div class="col-12">
                    <h3><?php echo $shipping_columns["title"] ?></h3>
                </div>
				<?php foreach ( $shipping_columns["fields"] as $field_key => $field ) : ?>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label for="<?php echo $field_key ?>"><?php echo $field["label"]; ?></label>
							<?php if ( isset( $field["type"] ) && $field["type"] == "select" ) : ?>
                                <select name="checkout[customer][<?php echo $field_key ?>]"
                                        id="<?php echo $field_key; ?>"
									<?php $this->input_attributes( $field_key ) ?>>
									<?php foreach ( $field["options"] as $option_key => $option ) : ?>
                                        <option value="<?php echo $option_key ?>" <?php if ( isset( $customer ) && ( $customer->get_meta( $field_key ) == $option_key ) ) {
											echo "selected";
										} ?>><?php echo $option ?></option>
									<?php endforeach; ?>
                                </select>
							<?php else: ?>
                                <input id="<?php echo $field_key ?>" type="<?php if ( isset( $field["type"] ) ) {
									echo $field["type"];
								} else {
									echo "text";
								} ?>" <?php $this->input_attributes( $field_key ) ?> class="form-control"
                                       value="<?php if ( isset( $customer ) ) {
									       echo $customer->get_meta( $field_key );
								       } ?>"
                                       name="checkout[customer][<?php echo $field_key ?>]" <?php if ( isset( $field["required"] ) && $field["required"] ) {
									echo 'required';
								} ?>/>
							<?php endif; ?>
                        </div>
                    </div>
					<?php
					if ( $field_key == "shipping_last_name" ) { ?>
                        <div class="col-md-4 buttons_login">
							<?php if ( ! is_user_logged_in() ) : ?>
                                <a href="<?php eso_the_page_link( 'login' ); ?>"
                                   class="btn btn-secondary"><?php _e( "Přihlásit se", "eso" ) ?></a>
                                <a href="<?php eso_the_page_link( 'register' ); ?>"
                                   class="btn btn-secondary"><?php _e( "Registrovat", "eso" ) ?></a>
							<?php endif; ?>
                        </div>
						<?php
					} else if ( $field_key == "shipping_email" ) { ?>
                        <div class="w-100 eso-customer-fields-divider"></div>
					<?php }
					?>
				<?php endforeach; ?>
            </div>
            <div class="checkout-group checkout-customer-group">
                <label class="container-check" for="billing_on" data-toggle="collapse" data-target="#billing_fields">
					<?php echo $billing_columns["fields"]["billing_on"]["label"] ?>
                    <input type="checkbox" id="billing_on"
                           name="checkout[customer][billing_on]"
                           value="1" <?php if ( isset( $customer ) && $customer->is_billing_on() ) {
						echo "checked";
					} ?>>
                    <span class="checkmark"></span>
                </label>
            </div>

            <div class="row collapse <?php if ( isset( $customer ) && $customer->is_billing_on() ) {
				echo "show";
			} ?>" id="billing_fields">
                <div class="col-12">
                    <h3><?php echo $billing_columns["title"] ?></h3>
                </div>
				<?php foreach ( $billing_columns["fields"] as $field_key => $field ) :
					if ( $field_key == "billing_on" ) {
						continue;
					}
					?>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label for="<?php echo $field_key ?>"><?php echo $field["label"]; ?></label>
							<?php if ( isset( $field["type"] ) && $field["type"] == "select" ) : ?>
                                <select name="checkout[customer][<?php echo $field_key ?>]"
                                        id="<?php echo $field_key; ?>"
                                        class="form-control">
									<?php foreach ( $field["options"] as $option_key => $option ) : ?>
                                        <option value="<?php echo $option_key ?>" <?php if ( isset( $customer ) && ( $customer->get_meta( $field_key ) == $option_key ) ) {
											echo "selected";
										} ?>><?php echo $option ?></option>
									<?php endforeach; ?>
                                </select>
							<?php else: ?>
                                <input id="<?php echo $field_key ?>" type="<?php if ( isset( $field["type"] ) ) {
									echo $field["type"];
								} else {
									echo "text";
								} ?>" class="form-control" value="<?php if ( isset( $customer ) ) {
									echo $customer->get_meta( $field_key );
								} ?>"
                                       name="checkout[customer][<?php echo $field_key ?>]" <?php if ( isset( $field["required"] ) && $field["required"] ) {
									echo 'required';
								} ?>/>
							<?php endif; ?>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
			<?php
		}

		public function render_cart_totals() { ?>
            <div class="order_summary">
                <div class="row order_detail_line">
                    <div class="col-md text_order">
                        <p><?php _e( "Celkem zboží s DPH", "eso" ) ?></p>
                    </div>
                    <div class="col-md price_order checkout_cart_total">
						<?php echo $this->cart->get_sum( true, true ); ?>
                    </div>
                </div>
                <div class="row order_detail_line">
                    <div class="col-md text_order">
                        <p><?php _e( "Doprava a platba", "eso" ) ?></p>
                    </div>
                    <div class="col-md price_order checkout_shipping_payment_total">
						<?php echo $this->cart->get_shipping_payment_price( true ) ?>
                    </div>
                </div>
				<?php
				if ( $this->cart->get_discount() ) : ?>
                    <div class="row order_detail_line">
                        <div class="col-md text_order">
                            <p><?php _e( "Sleva", "eso" ) ?></p>
                        </div>
                        <div class="col-md price_order checkout_discount">
                            - <?php echo $this->cart->get_discount( true, true ) ?>
                        </div>
                    </div>
				<?php endif; ?>
                <div class="row order_detail_line">
                    <div class="col-md text_order">
                        <p><?php _e( "Celkem bez DPH", "eso" ) ?></p>
                    </div>
                    <div class="col-md price_order checkout_total_without_tax">
						<?php echo $this->cart->get_total_without_tax( true, true ) ?>
                    </div>
                </div>
                <div class="row order_detail_line">
                    <div class="col-md final_text_order">
                        <p><?php _e( "Celkem s DPH", "eso" ) ?></p>
                    </div>
                    <div class="col-md final_price_order checkout_total">
                        <p>
							<?php echo $this->cart->get_total( true, true ); ?>
                        </p>
                    </div>
                </div>
            </div>
		<?php }

		/**
		 * @since 2019.4
		 */
		public function render_payment_fields() {
			$session              = new Eso_Session( eso_session_token() );
			$zone                 = $this->cart->get_shipping_zone();
			$shipping_method_code = $session->get_value( "shipping_method" );

			if ( empty( $shipping_method_code ) ) {
				$shipping_method_code = $zone->get_first_enabled_shipping_method();
			}

			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				if ( ! $zone->has_payment_method( $shipping_method_code, $customer->get_payment_method_code() ) ) {
					$customer->set_payment_method( $zone->get_first_enabled_payment_method( $shipping_method_code ) );
				}
			}
			?>
			<?php
			if ( ! empty( $zone->get_enabled_payment_methods( $shipping_method_code ) ) ) {
				echo "<h3>" . __( "Platba", "eso" ) . "</h3>";

				/* @var $shipping_method Eso_Shipping_Method */
				$counter = 0;
				foreach ( $zone->get_enabled_payment_methods( $shipping_method_code ) as $payment_method_code => $payment_method_data ) :
					$payment_method = new Eso_Payment_Method( $payment_method_code );
					?>
                    <div class="form-radio payment-method">
                        <label class="container_radio form-radio-label">
                	<span class="payment-method__name">
                        <?php echo $payment_method->get_name() ?>
                    </span>
                            -
                            <span class="payment-method__price">
                    <?php
                    /* @var $currency Eso_Currency */
                    $currency = eso_get_active_currency();
                    if ( ! $zone->get_payment_method_price( $shipping_method_code, $payment_method->get_code(), $currency ) ) {
	                    _e( "Zdarma", "eso" );
                    } else {
	                    echo $zone->get_payment_method_price( $shipping_method_code, $payment_method->get_code(), $currency ) . " " . $currency->get_symbol();
                    }
                    ?>
                                <small class="payment-method__message"><?php _e( $payment_method->get_message(), "eso" ) ?></small>
                    </span>

                            <input class="form-radio-input payment-method" type="radio"
                                   name="checkout[options][payment_method][code]"
                                   value="<?php echo $payment_method->get_code() ?>" data-key="payment_method"
                                   id="<?php echo $payment_method->get_code() ?>"
								<?php
								if ( $counter == 0 ) {
									$session->set_value( "payment_method", $payment_method->get_code() );
									echo "checked='checked'";
								}
								if ( isset( $customer ) && $customer->get_payment_method_code() ) {
									if ( $customer->get_meta( "payment_method" ) == $payment_method->get_code() ) {
										echo "checked='checked'";
										$session->set_value( "payment_method", $payment_method->get_code() );
									}
								}
								?>>
                            <span class="checkmark_radio"></span>
                        </label>
                    </div>
					<?php
					$counter ++;
				endforeach;
			}
		}

		/**
		 * @since 2019.4
		 */
		public function render_shipment_fields() {
			$session = new Eso_Session( eso_session_token() );
			$zone    = $this->cart->get_shipping_zone();

			if ( empty( $session->get_value( "shipping_method" ) ) || ! $zone->has_shipping_method( $session->get_value( "shipping_method" ) ) ) {
				$session->set_value( "shipping_method", $zone->get_first_enabled_shipping_method() );
			}
			?>
            <h3><?php _e( "Způsob dopravy", "eso" ) ?></h3>
			<?php
			/* @var $shipping_method Eso_Shipping_Method */
			$counter = 0;
			foreach ( $zone->get_shipping_methods() as $shipping_method_code => $shipping_method_data ) :
				$shipping_method = new Eso_Shipping_Method( $shipping_method_code );
				if ( ! $zone->has_any_payment_method( $shipping_method_code ) ) {
					continue;
				}
				?>
                <div class="form-radio shipping-method">
                    <label class="container_radio form-radio-label"
                           for="<?php echo $shipping_method->get_code() ?>">
					<span class="shipping-method__name">
					    <?php echo $shipping_method->get_name() ?>
                    </span>
                        -
                        <span class="shipping-method__price">
                        <?php
                        /* @var $currency Eso_Currency */
                        $currency = eso_get_active_currency();
                        if ( ! $zone->get_shipping_method_price( $shipping_method->get_code(), $currency ) ) {
	                        _e( "Zdarma", "eso" );
                        } else {
	                        echo $zone->get_shipping_method_price( $shipping_method->get_code(), $currency ) . " " . $currency->get_symbol();
                        }
                        ?>
                    </span>
                        <small class="shipping-method__message">
							<?php $shipping_method->render_message(); ?>
                        </small>
                        <input class="form-radio-input" type="radio" name="checkout[options][shipping_method][code]"
                               value="<?php echo $shipping_method->get_code() ?>" data-key="shipping_method"
                               id="<?php echo $shipping_method->get_code() ?>"
							<?php checked( $session->get_value( "shipping_method" ), $shipping_method->get_code() );
							if ( $counter == 0 && empty( $session->get_value( "shipping_method" ) ) ) {
								$session->set_value( "shipping_method", $shipping_method->get_code() );
								echo "checked='checked'";
							} ?>>
                        <span class="checkmark_radio"></span>
                    </label>
                </div>
				<?php
				$counter ++;
			endforeach; ?>
		<?php }

		public function render_summary() {
			$cart = new Eso_Cart( eso_session_token() );

			$session    = new Eso_Session( eso_session_token() );
			$billing_on = $session->is_billing_on();

			$checkout_fields = new Eso_Checkout_Fields();
			$customer_fields = $checkout_fields->get_customer_meta_fields();

			$shipping_columns = $customer_fields["shipping"];
			$billing_columns  = $customer_fields["billing"];

			?>
			<?php if ( $billing_on ) : ?>
                <div class="col-md-4">
                    <h3><?php echo $billing_columns["title"] ?></h3>
					<?php foreach ( $billing_columns["fields"] as $field_key => $field ) :
						if ( $field_key == "billing_on" ) {
							continue;
						}
						if ( $field_key == "billing_country" ) {
							$zone = new Eso_Shipping_Zone( $session->get_value( "billing_country" ) );
							echo $zone->get_name();
							continue;
						}
						?>
                        <span><?php echo $session->get_value( $field_key ) ?></span>
					<?php endforeach; ?>
                </div>
			<?php endif; ?>
            <div class="col-md-4">
                <h3><?php echo $shipping_columns["title"] ?></h3>
				<?php foreach ( $shipping_columns["fields"] as $field_key => $field ) :
					if ( $field_key == "shipping_country" ) {
						$zone = new Eso_Shipping_Zone( $session->get_value( "shipping_country" ) );
						echo "<span class='summary-" . $field_key . "'>" . $zone->get_name() . "</span>";
						continue;
					}
					?>
                    <span class="summary-<?php echo $field_key ?>"><?php echo $session->get_value( $field_key ) ?></span>
				<?php endforeach; ?>

            </div>
            <div class="col-md-4">
                <h3><?php _e( "Doprava a platba", "eso" ) ?></h3>
                <span>
                    <?php
                    $shipping_method = new Eso_Shipping_Method( $session->get_value( "shipping_method" ) );
                    echo $shipping_method->get_name();

                    if ( $shipping_method->get_code() == "zasilkovna" ) {
	                    echo " (" . eso_get_zasilkovna_branch_name( $session->get_value( "zasilkovna_branch" ) ) . ")";
                    } else if ( $shipping_method->get_code() == "personally" ) {
	                    $store = new Eso_Store();
	                    $store->render_personal_collection_address();
                    }
                    ?>
                </span>
                <span>
                    <?php
                    $payment_method = new Eso_Payment_Method( $session->get_value( "payment_method" ) );
                    echo $payment_method->get_name();
                    ?>
                </span>
            </div>
            <div class="w-100 mt-4"></div>
			<?php if ( ! empty( $session->get_value( "order_note" ) ) ) : ?>
                <div class="col-md-4">
                    <h2><?php _e( "Poznámka k objednávce", "eso" ) ?></h2>
                </div>
                <div class="col-md-8">
                    <p id="order_note_final"
                       class="font-italic"><?php echo $session->get_value( "order_note" ); ?></p>
                </div>
			<?php endif; ?>
            <div class="col-md-12 pt-5">
                <table class="table table-responsive" id="cart-content-table">
                    <tbody>
					<?php
					if ( $cart->get_items() ) {
						/* @var $item Eso_Cart_Item */
						foreach ( $cart->get_items() as $item ) : ?>
                            <tr>
                                <td><?php $item->get_product()->the_featured_image( "table" ); ?></td>
                                <td><?php echo $item->get_product()->get_name(); ?></td>
                                <td><?php echo $item->get_product()->get_price( eso_get_active_currency(), true, true ) . " / " . __( "ks", "eso" ) ?></td>
                                <td><?php echo $item->get_quantity() . " " . __( "ks", "eso" ); ?></td>
                                <td><?php echo $item->get_total_with_currency( true ); ?></td>
                            </tr>
						<?php endforeach;
					}
					?>
                    </tbody>
                </table>
                <hr/>
            </div>
            <div class="col-md-6 offset-md-6">
                <div class="cart-totals">
					<?php $this->render_cart_totals() ?>
                </div>
                <hr/>
                <div id="eso-cart-agreements">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="agree-terms" class="custom-control-input"/>
                        <label for="agree-terms" class="custom-control-label">
							<?php echo sprintf( __( "Souhlasím s %s Obchodními podmínkami%s a beru na vědomí %s." ), "<a class='terms-link' href='" . get_permalink( eso_get_page_id( 'terms' ) ) . "'>", "</a>", get_the_privacy_policy_link() ) ?>
                        </label>
                    </div>
                </div>
            </div>
		<?php }
	}
}