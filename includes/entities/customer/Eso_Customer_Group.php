<?php

if ( ! class_exists( 'Eso_Customer_Group' ) ) {
	/**
	 * Class Eso_Customer_Group
	 *
	 * @since 2019.6 rewrite from custom taxonomy to options array
	 */
	class Eso_Customer_Group {

		private $id;

		public function __construct( $id ) {
			$option = get_option( "eso_customer_groups" );

			if ( ! isset( $option[ $id ] ) ) {
				write_log( "Accessing non-existing eso_customer_groups" );
				write_log( $id );
				$this->id = 1;
			} else {
				$this->id   = $id;
				$this->data = $option[ $this->id ];
			}

			return true;
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->id;
		}

		/**
		 * @return array
		 */
		private function get_option() {
			return get_option( "eso_customer_groups" );
		}

		/**
		 * @param $option
		 */
		private function set_option( $option ) {
			update_option( "eso_customer_groups", $option );
		}

		/**
		 * @return string
		 */
		public function get_name() {
			return $this->data["name"];
		}

		/**
		 * @param $name
		 */
		public function set_name( $name ) {
			$option = $this->get_option();

			$option[ $this->get_id() ]["name"] = sanitize_text_field( $name );

			$this->set_option( $option );
		}

		/**
		 * @return string
		 */
		public function get_slug() {
			return $this->data["slug"];
		}

		/**
		 * @param $slug
		 */
		public function set_slug( $slug ) {
			$option = $this->get_option();

			$option[ $this->get_id() ]["slug"] = sanitize_text_field( $slug );
			$this->set_option( $option );
		}

		/**
		 * @return WP_User_Query
		 */
		private function get_customers() {
			$args       = [
				"meta_key"   => "group",
				"meta_value" => $this->get_id(),
				"role"       => "eso_customer"
			];
			$user_query = new WP_User_Query( $args );

			return $user_query;
		}

		/**
		 * @return int
		 */
		public function get_count() {
			$user_query = $this->get_customers();

			return $user_query->get_total();
		}

		/**
		 * @return bool
		 */
		public function has_customers() {
			if ( $this->get_count() > 0 ) {
				return true;
			}

			return false;
		}

		public function delete() {
			$customers = $this->get_customers();

			if ( $customers->get_results() ) {
				/* @var $customer Eso_Customer */
				foreach ( $customers->get_results() as $customer_obj ) {
					$customer = new Eso_Customer( $customer_obj->ID );
					$customer->set_group( 1 );
				}
			}

			$option = $this->get_option();
			unset( $option[ $this->get_id() ] );
			$this->set_option( $option );
		}
	}
}
