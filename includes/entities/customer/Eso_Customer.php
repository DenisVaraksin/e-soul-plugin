<?php

if ( ! class_exists( 'Eso_Customer', false ) ) {
	class Eso_Customer {

		private $user_id;

		public function __construct( $user_id ) {
			$this->user_id = (int) $user_id;
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->user_id;
		}

		/**
		 * @since 0.0.17
		 * @since 2020.1.9 strip_tags
		 *
		 * @param $key
		 *
		 * @return string
		 */
		public function get_meta( $key ) {
			$meta = get_user_meta( $this->get_id(), $key, true );

			return strip_tags( $meta );
		}

		/**
		 * @since 2019.7
		 *
		 * @param $name
		 * @param $value
		 */
		public function set_meta($name, $value) {
			update_user_meta($this->get_id(), $name, $value);
		}

		/**
		 * @since 2019.6 changed from taxonomy to user meta
		 *
		 * @return bool|string
		 */
		public function get_group_id() {

			$group = $this->get_meta("group");

			if ( ! $group ) {
				return 1;
			}

			return (int) $group;
		}

		/**
		 * @since 2019.6
		 *
		 * @return Eso_Customer_Group
		 */
		private function get_group() {
			$group_id = $this->get_group_id();

			return new Eso_Customer_Group( $group_id );
		}

		/**
		 * @since 2019.6
		 *
		 * @return string
		 */
		public function get_group_name() {
			$group = $this->get_group();

			return $group->get_name();
		}

		/**
		 * @since 2019.6
		 *
		 * @return string
		 */
		public function get_group_slug() {
			$group = $this->get_group();

			return $group->get_slug();
		}

		/**
		 * @since 2019.6
		 *
		 * @param $group_id
		 */
		public function set_group( $group_id ) {
			$this->set_field( "group", (int) $group_id );
		}

		/**
		 * @since 2019.6
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return string
		 */
		public function get_payment_method_code() {
			$meta = $this->get_meta("payment_method");

			/**
			 * @deprecated and can be removed 2020.7, fix for a problem where array occurred
			 */
			if ( ! is_string( $meta ) ) {
				return false;
			}

			return $meta;
		}

		/**
		 * @since 2019.5
		 *
		 * @param $payment_method_code
		 */
		public function set_payment_method( $payment_method_code ) {
			$this->set_field( "payment_method", $payment_method_code );
		}

		/**
		 * @since 2019.7
		 *
		 * @param $shipping_method_code
		 */
		public function set_shipping_method( $shipping_method_code ) {
			$this->set_field( "shipping_method", $shipping_method_code );
		}

		/**
		 * @since 0.0.22
		 */
		public function has_orders() {
			if ( $this->get_orders() ) {
				return true;
			}

			return false;
		}


		/**
		 * @return array
		 * @since 0.0.21
		 */
		public function get_orders() {
			$orders_arr = [];

			$args   = array(
				'author'         => $this->get_id(),
				'post_type'      => 'esoul_order',
				'posts_per_page' => 100,
				'order'          => 'DESC',
				'order_by'       => 'date'
			);
			$orders = new WP_Query( $args );

			if ( $orders->have_posts() ) {
				while ( $orders->have_posts() ) {
					$orders->the_post();

					$order = new Eso_Order( get_the_ID() );

					$orders_arr[] = $order;
				}

				return $orders_arr;
			} else {
				return null;
			}
		}

		/**
		 * @since 2019.6
		 * @since 2019.7 saves as meta field, useful for sorting
		 *
		 * @return int
		 */
		public function get_order_count() {
			if($this->get_orders()) {
				$order_count = count( $this->get_orders() );
				update_user_meta( $this->get_id(), "order_count", $order_count );

				return $order_count;
			}

			return 0;

		}

		/**
		 * @return false|string
		 *
		 * @since 2019.6
		 */
		public function get_last_order_date() {
			if ( $this->get_order_count() < 1 ) {
				return null;
			}

			$orders = $this->get_orders();

			/* @var $last_order Eso_Order */
			$last_order = reset( $orders );

			return $last_order->get_date_and_time_created();
		}

		public function get_full_name() {
			return $this->get_shipping_first_name() . " " . $this->get_shipping_last_name();
		}

		/**
		 * @since 2019.7 returns shipping_email meta by default
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return string
		 */
		public function get_email() {
			$meta = $this->get_meta("shipping_email");

			if ( ! $meta ) {
				$user = get_userdata( $this->get_id() );

				if(isset($user->user_email)) {
					return $user->user_email;
				}
			}

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return string
		 */
		public function get_phone() {
			$meta = $this->get_meta("shipping_phone");

			return $meta;
		}

		/**
		 * @since 2019.6 returns bool
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return bool
		 */
		public function is_billing_on() {
			$meta = $this->get_meta("billing_on");

			if ( $meta == 1 ) {
				return true;
			}

			return false;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_billing_company_name() {
			$meta = $this->get_meta("billing_company_name");

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_billing_ico() {
			$meta = $this->get_meta("billing_ico");

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_billing_dic() {
			$meta = $this->get_meta("billing_dic");

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_billing_street() {
			$meta = $this->get_meta("billing_street");

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_billing_postcode() {
			$meta = $this->get_meta("billing_postcode");

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_billing_city() {
			$meta = $this->get_meta("billing_city");

			return $meta;
		}

		/**
		 * @since 2019.6
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 * @return mixed|string
		 */
		public function get_billing_country_code() {
			$meta = $this->get_meta("billing_country");

			if ( ! $meta ) {
				$meta = $this->get_meta("shipping_country");
				$this->set_field( "billing_country", $meta );
			}

			if ( ! $meta ) {
				return "CZ";
			}

			return $meta;
		}

		/**
		 * @since 2019.6
		 *
		 * @return Eso_Shipping_Zone
		 */
		public function get_billing_country() {
			$country_code = $this->get_billing_country_code();

			return new Eso_Shipping_Zone( $country_code );
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed|string
		 */
		public function get_shipping_first_name() {
			$meta = $this->get_meta("shipping_first_name");

			if ( ! $meta ) {
				$meta = $this->get_meta("first_name");
			}

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_shipping_last_name() {
			$meta = $this->get_meta("shipping_last_name");

			if ( ! $meta ) {
				$meta = $this->get_meta("last_name");
			}

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return string
		 */
		public function get_shipping_street() {
			$meta = $this->get_meta("shipping_street");

			if ( ! $meta ) {
				$meta = $this->get_meta("company_name");
			}

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return string
		 */
		public function get_shipping_postcode() {
			$meta = $this->get_meta("shipping_postcode");

			if ( ! $meta ) {
				$meta = $this->get_meta("billing_postcode");
			}

			return $meta;
		}

		/**
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed
		 */
		public function get_shipping_city() {
			$meta = $this->get_meta("shipping_city");

			if ( ! $meta ) {
				$meta = $this->get_meta("billing_city");
			}

			return $meta;
		}

		/**
		 * @since 2019.6 return CZ when is empty
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return mixed|string
		 */
		public function get_shipping_country_code() {
			$meta = $this->get_meta("shipping_country");

			if ( ! $meta ) {
				$meta = $this->get_meta("billing_country");
				$this->set_field( "shipping_country", $meta );
			}

			if ( ! $meta ) {
				return "CZ";
			}

			return $meta;
		}

		/**
		 * @return Eso_Shipping_Zone
		 */
		public function get_shipping_country() {
			$country_code = $this->get_shipping_country_code();

			return new Eso_Shipping_Zone( $country_code );
		}

		/**
		 * @since 2019.6
		 * @since 2019.6 return CZ when is empty
		 * @return string
		 */
		public function get_order_note() {
			$meta = $this->get_meta("order_note");

			return $meta;
		}

		/**
		 * @return string
		 */
		public function get_date_registered() {
			$meta = get_userdata( $this->get_id() );

			$date = date_create_from_format( 'Y-m-d H:i:s', $meta->user_registered );

			return $date->format( 'j. n. Y' );
		}

		/**
		 * @since 2019.6
		 * @since 2019.6 return CZ when is empty
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 *
		 * @return string|null
		 */
		public function get_shipping_method_code() {
			$customer = new Eso_Customer( get_current_user_id() );

			$meta = $this->get_meta("shipping_method");

			return $meta;
		}


		/**
		 * @since 2019.6 return CZ when is empty
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 * @param bool $dash_if_empty
		 *
		 *
		 * @return bool|string
		 */
		public function get_last_login( $dash_if_empty = false ) {
			$meta = $this->get_meta("last_login");

			if ( ! $meta && $dash_if_empty ) {
				return " - ";
			} else if ( ! $meta ) {
				return false;
			}

			$date = date_create_from_format( 'Y-m-d H:i:s', $meta );

			return $date->format( 'j. n. Y' );
		}

		/**
		 * @since 2019.5
		 */
		public function get_role() {
			$meta  = get_userdata( $this->get_id() );
			if(isset($meta->roles)) {
				$roles = $meta->roles;
				if(isset($roles[0])) {
					return $roles[0];
				}
			}

			return null;
		}

		/**
		 * @since 2019.5
		 */
		public function has_role_customer() {
			if ( $this->get_role() == "eso_customer" ) {
				return true;
			}

			return false;
		}

		/**
		 * @return bool
		 *
		 * @since 2019.5
		 */
		public function is_registered() {
			if ( $this->get_role() == "eso_unregistered" ) {
				return false;
			}

			return true;
		}

		/**
		 * @param $field_key
		 * @param $field_value
		 */
		public function set_field( $field_key, $field_value ) {
			update_user_meta( $this->get_id(), $field_key, $field_value );
		}

		/**
		 * @since 2019.5
		 * @since 2019.7 logic moved to set_turnover()
		 * @since 2020.1.9 uses $this->get_meta() instead of get_user_meta()
		 * @return float|int
		 */
		public function get_turnover( Eso_Currency $currency = null, $include_currency = false ) {
			if ( ! isset( $currency ) ) {
				$currency = eso_get_active_currency();
			}

			$meta = $this->get_meta("turnover_" . $currency->get_code());

			if ( empty( $meta ) ) {
				return $this->set_turnover( $currency, $include_currency );
			}

			if ( $include_currency ) {
				return $meta . " " . $currency->get_symbol();
			}

			return $meta;
		}


		/**
		 * @since 2019.7
		 *
		 * @param Eso_Currency $currency
		 * @param bool $include_currency
		 *
		 * @return float|int|string
		 *
		 * @internal This can be slow - use it through Eso_Admin_Ajax or Eso_Ajax to execute asynchronously
		 */
		public function set_turnover( Eso_Currency $currency, $include_currency = false ) {
			$turnover = 0;

			/* @var $order Eso_Order */
			if ( $this->has_orders() ) {
				foreach ( $this->get_orders() as $order ) {
					$turnover += $order->get_sum( $currency );
				}
			}

			update_user_meta( $this->get_id(), "turnover_" . $currency->get_code(), $turnover );

			if ( $include_currency ) {
				$turnover .= " " . $currency->get_symbol();
			}

			return $turnover;
		}

		/**
		 * @return string
		 *
		 * @since 2019.6
		 */
		public function get_edit_url() {
			return admin_url( "admin.php?page=eso-customer-detail&customer-id=" . $this->get_id() );
		}

		/**
		 * @since 2019.6
		 */
		public function the_edit_url() {
			echo $this->get_edit_url();
		}

		/**
		 * @since 2019.6
		 */
		public function render_quick_look() {
			require( ESO_DIR . "/admin/views/customer/quick-look.php" );
		}

		/**
		 * @since 2019.6
		 *
		 * @return string
		 */
		public function get_password_reset_url() {
			$userData   = get_userdata( $this->get_id() );
			$user_login = $userData->user_login;
			$key        = get_password_reset_key( $userData );

			return network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' );
		}

		/**
		 * @since 2019.6
		 *
		 * @return bool
		 */
		public function delete() {
			return wp_delete_user( $this->get_id() );
		}

	}
}

return new Eso_Customer( get_current_user_id() );