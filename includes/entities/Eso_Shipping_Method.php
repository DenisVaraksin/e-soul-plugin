<?php
if(!class_exists('Eso_Shipping_Method', false)) {
	class Eso_Shipping_Method {

		private $code;

		private $name;

		private $message;

		public function __construct($code) {
			$this->code = $code;

			global $wpdb;

			$row = $wpdb->get_row("SELECT name, code, message, enabled, icon FROM " . ESO_SHIPPING_METHOD_TABLE . " WHERE code = '" . $this->get_code() . "'");
			if(!isset($row->name)) {
				write_log("Acessing non existing shipping method " . $code);
			}

			$this->name = $row->name;
			$this->message = $row->message;
			$this->enabled = $row->enabled;
			$this->icon = $row->icon;
		}

		public function get_code() {
			return $this->code;
		}

		public function get_name() {
			return $this->name;
		}

		public function get_message() {
			return $this->message;
		}

		/**
		 * @since 2019.8
		 */
		public function render_message(){
			_e( $this->get_message(), "eso" );

			if ( $this->get_code() == "personally" ) {
				$store = new Eso_Store();
				$store->render_personal_collection_address();
			}
		}

		public function set_name($name) {
			global $wpdb;

			$data = [
				"name" => $name
			];

			$where = [
				"code" => $this->get_code()
			];
			$wpdb->update(ESO_SHIPPING_METHOD_TABLE, $data, $where);

			$this->name = $name;
		}

		public function is_enabled() {
			return $this->enabled;
		}

		public function set_enabled($enabled) {
			global $wpdb;
			$data = [
				"enabled" => $enabled
			];

			$where = [
				"code" => $this->get_code()
			];
			$wpdb->update(ESO_SHIPPING_METHOD_TABLE, $data, $where);
		}

		/**
		 * @since 2019.8
		 */
		public function get_icon() {
			return $this->icon;
		}

		/**
		 * @since 2019.8
		 */
		public function render_icon() {
		    echo eso_icon("comgate", "shipping-method-icon");
		}

		public function render_edit_row(Eso_Shipping_Zone $shipping_zone) {
			require( ESO_DIR . "/admin/templates/shipping-method-edit-row.php" );
		}

	}
}