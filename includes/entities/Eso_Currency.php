<?php
if(!class_exists('Eso_Currency', false)) {
	class Eso_Currency {

		private $code;

		private $symbol;

		private $enabled;

		public function __construct($code) {
			$this->code = $code;
		}

		public function get_code() {
			return $this->code;
		}

		public function get_symbol() {
			if($this->symbol) {
				return $this->symbol;
			}
			$all_currencies = get_option("esoul_currencies");

			if(isset($all_currencies[$this->get_code()])) {
				return $all_currencies[$this->get_code()]["symbol"];
			}

			return null;
		}

		/**
		 * @return bool
		 */
		public function is_enabled() {
			$all_currencies = get_option("esoul_currencies");

			if(isset($all_currencies[$this->get_code()])) {
				return $all_currencies[$this->get_code()]["enabled"];
			} else if($this->enabled) {
				return $this->enabled;
			}

			return false;
		}

		/**
		 * @return bool
		 */
		public function is_active() {
			$session = new Eso_Session(eso_session_token());

			$active_currency = $session->get_active_currency();
			$active_currency_code = $active_currency->get_code();

			if($active_currency_code == $this->get_code()) {
				return true;
			}

			return false;
		}

		/**
		 * @return bool
		 */
		public function is_default() {
			$default_currency = eso_get_default_currency_name();

			if($default_currency == $this->get_code()) {
				return true;
			}

			return false;
		}

		public function set_symbol($symbol) {
			$this->symbol = $symbol;
		}

		public function set_enabled( $enabled = true ) {
			$all_currencies = get_option("esoul_currencies");

			if(isset($all_currencies[$this->get_code()])) {
				$all_currencies[$this->get_code()]["enabled"] = $enabled;
			}

			update_option('esoul_currencies', $all_currencies);

			$this->enabled = $enabled;
		}

		public function set_as_default() {
			$this->set_enabled(true);

			update_option('esoul_default_currency', $this->get_code());
		}
	}
}