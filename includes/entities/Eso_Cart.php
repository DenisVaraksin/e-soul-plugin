<?php
if ( ! class_exists( 'Eso_Cart', false ) ) {
	class Eso_Cart {

		private $token;

		/**
		 * Eso_Cart constructor.
		 *
		 * @param $token (gained e.g. through eso_session_token())
		 */
		public function __construct( $token ) {
			$this->token = $token;
		}

		public function get_token() {
			return $this->token;
		}

		/**
		 * @param $product_id
		 *
		 * @return Eso_Product
		 */
		public function get_product( $product_id ) {
			return new Eso_Product( $product_id );
		}

		/**
		 * @return bool
		 */
		public function has_items() {
			if ( $this->get_items() ) {
				return true;
			}

			return false;
		}

		/**
		 * @param Eso_Cart_Item $cart_item
		 *
		 * @return object|null
		 */
		public function has_item( Eso_Cart_Item $cart_item ) {
			global $wpdb;

			$result = $wpdb->get_row( "SELECT id, token, product_id FROM " . ESO_CART_TABLE . " WHERE token = '" . $this->get_token() . "' AND product_id = " . $cart_item->get_id() . "" );

			if ( $result ) {
				return $result;
			}

			return null;
		}

		/**
		 * @return array|null
		 */
		public function get_items() {
			global $wpdb;

			$results = $wpdb->get_results( "SELECT id, token, product_id, quantity FROM " . ESO_CART_TABLE . " WHERE token = '" . $this->get_token() . "'", ARRAY_A );

			if ( ! $results ) {
				return null;
			}

			$items = [];

			foreach ( $results as $result ) {
				$items[ (int) $result["product_id"] ] = new Eso_Cart_Item( $result["product_id"] );
			}

			return $items;
		}

		/**
		 * @since 2019.8 reduce stock amount when adding to cart
		 *
		 * @param Eso_Cart_Item $cart_item
		 *
		 * @return false|int
		 */
		public function add_item( Eso_Cart_Item $cart_item ) {
			global $wpdb;

			if ( $this->has_item( $cart_item ) ) {
				return $this->update_item( $cart_item );
			}

			$cart_item->get_product()->reduce_stock_amount($cart_item->get_quantity());

			$data = [
				'updated'    => eso_db_now(),
				'token'      => $this->get_token(),
				'product_id' => $cart_item->get_id(),
				'quantity'   => $cart_item->get_quantity(),
			];

			return $wpdb->insert( ESO_CART_TABLE, $data );
		}

		/**
		 * @param Eso_Cart_Item $cart_item
		 *
		 * @return false|int
		 */
		public function update_item( Eso_Cart_Item $cart_item ) {
			global $wpdb;

			$data = [
				'updated'  => eso_db_now(),
				'quantity' => $cart_item->get_quantity()
			];

			$where = [
				'token'      => $this->get_token(),
				'product_id' => $cart_item->get_id()
			];

			$result = $wpdb->update( ESO_CART_TABLE, $data, $where );

			return $result;
		}

		/**
		 * @since 2019.8 add stock quantity when removing from cart
		 *
		 * @param (int) $cart_item_id
		 *
		 * @return false|int
		 */
		public function remove_item( $cart_item_id ) {
			$cart_item = new Eso_Cart_Item($cart_item_id);

			$cart_item->get_product()->add_stock_amount($cart_item->get_quantity());

			global $wpdb;

			$where = [
				"token"      => $this->get_token(),
				"product_id" => $cart_item_id
			];

			$result = $wpdb->delete( ESO_CART_TABLE, $where );

			return $result;
		}

		/**
		 * @param bool $comma_delimiter
		 *
		 * @param bool $include_currency
		 *
		 * @return float|int|mixed|string
		 * @since 2020.2.11 return currency when when empty
		 *
		 * @since 2019.7 $include_currency introduced
		 */
		public function get_sum( $comma_delimiter = false, $include_currency = false ) {
			$cart_items = $this->get_items();

			if ( ! $cart_items ) {
				if($include_currency) {
					return "0 " . eso_get_active_currency_symbol();
				}
				return "0";
			}

			$sum = 0;

			/* @var $cart_item Eso_Cart_Item */
			foreach ( $cart_items as $cart_item ) {
				$sum += $cart_item->get_total( eso_get_active_currency() );
			}

			if ( $comma_delimiter ) {
				$sum = str_replace( ".", ",", $sum );
			}

			if ( $include_currency ) {
				return $sum . " " . eso_get_active_currency_symbol();
			}

			return $sum;
		}

		/**
		 * @since 2019.7
		 *
		 * @param bool $comma_delimiter
		 *
		 * @return float|int|mixed|string
		 */
		public function get_sum_without_tax( $comma_delimiter = false, $include_currency = false ) {
			$cart_items = $this->get_items();

			if ( ! $cart_items ) {
				return 0;
			}

			$sum = 0;

			/* @var $cart_item Eso_Cart_Item */
			foreach ( $cart_items as $cart_item ) {
				$sum += $cart_item->get_total_without_tax( eso_get_active_currency() );
			}

			if ( $comma_delimiter ) {
				$sum = str_replace( ".", ",", $sum );
			}

			if ( $include_currency ) {
				return $sum . " " . eso_get_active_currency_symbol();
			}

			return $sum;
		}

		/**
		 * @since 2020.3.1
		 */
		public function get_items_count() {
			global $wpdb;

			$result = $wpdb->get_var( "SELECT SUM(quantity) as items_count FROM " . ESO_CART_TABLE . " WHERE token = '" . $this->get_token() . "'" );

			if(isset($result)) {
				return (int) $result;
			}

			return 0;
		}

		/**
		 * @param bool $comma_delimiter
		 * @param bool $include_currency
		 *
		 * @return float|int|mixed|string
		 */
		public function get_total( $comma_delimiter = false, $include_currency = false ) {
			$sum = $this->get_sum();

			$total = $sum + $this->get_shipping_payment_price();

			if ( $this->get_discount() ) {
				$total -= $this->get_discount();
			}

			if ( $comma_delimiter ) {
				$total = str_replace( ".", ",", $total );
			}

			if ( $include_currency ) {
				return $total . " " . eso_get_active_currency_symbol();
			}

			return $total;
		}

		/**
		 * @since 2019.7
		 *
		 * @see $this->get_discount()
		 *
		 * @return float|int|mixed|string
		 */
		public function get_total_before_discount() {
			$sum   = $this->get_sum();
			$total = $sum + $this->get_shipping_payment_price();

			return $total;
		}

		/**
		 * @since 2019.6
		 *
		 * @param bool $comma_delimiter
		 *
		 * @param bool $include_currency
		 *
		 * @return string
		 */
		public function get_total_without_tax( $comma_delimiter = false, $include_currency = false ) {
			$cart_items = $this->get_items();

			if ( ! $cart_items ) {
				return "0";
			}

			$total = 0;

			/* @var $cart_item Eso_Cart_Item */
			foreach ( $cart_items as $cart_item ) {
				$total += $cart_item->get_total_without_tax( eso_get_active_currency() );
			}

			$total += $this->get_shipping_payment_price();

			if ( $this->get_discount() ) {
				$total -= $this->get_discount();
			}

			if ( $comma_delimiter ) {
				$total = str_replace( ".", ",", $total );
			}

			if ( $include_currency ) {
				return $total . " " . eso_get_active_currency_symbol();
			}

			return $total;
		}

		/**
		 * @since 2019.6
		 *
		 * @return null|string
		 */
		public function get_payment_method_code() {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				return $customer->get_payment_method_code();
			}

			$session = new Eso_Session( eso_session_token() );

			return $session->get_payment_method_code();

		}

		/**
		 * @since 2019.6
		 *
		 * @return null|string
		 */
		public function get_shipping_method_code() {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				return $customer->get_shipping_method_code();
			}

			$session = new Eso_Session( eso_session_token() );

			return $session->get_shipping_method_code();
		}

		/**
		 * @return float
		 */
		public function get_shipping_method_price() {
			$zone = $this->get_shipping_zone();

			return $zone->get_shipping_method_price( $this->get_shipping_method_code(), eso_get_active_currency() );
		}

		/**
		 * @return mixed
		 */
		public function get_payment_method_price() {
			$zone = $this->get_shipping_zone();

			return $zone->get_payment_method_price( $this->get_shipping_method_code(), $this->get_payment_method_code(), eso_get_active_currency() );
		}

		/**
		 * @param bool $include_currency
		 *
		 * @return mixed
		 */
		public function get_shipping_payment_price( $include_currency = false ) {
			$total = $this->get_shipping_method_price() + $this->get_payment_method_price();

			if ( $include_currency ) {
				return $total . " " . eso_get_active_currency_symbol();
			}

			return $total;
		}

		/**
		 * @since 0.0.30
		 * @since 2019.6 if user is not logged in, take it from sessions
		 *
		 * @return Eso_Shipping_Zone
		 */
		public function get_shipping_zone() {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				return $customer->get_shipping_country();
			}

			$session = new Eso_Session( eso_session_token() );

			if ( $session->get_value( "shipping_country" ) ) {
				$zone = new Eso_Shipping_Zone( $session->get_value( "shipping_country" ) );

				return $zone;
			}

			return new Eso_Shipping_Zone( "CZ" );
		}

		/**
		 * @param bool $comma_delimeter
		 * @param bool $include_currency
		 *
		 * @return string|null
		 * @since 2019.7
		 *
		 */
		public function get_discount( $comma_delimeter = false, $include_currency = false ) {
			$session     = new Eso_Session( eso_session_token() );
			$coupon_code = sanitize_text_field( $session->get_value( "coupon-code" ) );
			$coupon      = eso_get_coupon_by_code( $coupon_code );

			if ( $coupon ) {
				if ( $coupon->is_for_registered_only() && ! is_user_logged_in() ) {
					return null;
				}

				if ( ! $coupon->is_active() ) {
					return null;
				}

				if ( $coupon->get_type() == "-" ) {
					$discount = $coupon->get_value( eso_get_active_currency() );
				} else if ( $coupon->get_type() == "%" ) {
					$percent  = $coupon->get_value() / 100;
					$discount = $this->get_total_before_discount() * $percent;
				}

				if ( $coupon->is_free_shipping() ) {
					$discount += (float) $this->get_shipping_method_price();
				}

				if ( $comma_delimeter ) {
					$discount = str_replace( ".", ",", $discount );
				}

				if ( $include_currency ) {
					$discount .= " " . eso_get_active_currency_symbol();
				}

				return $discount;

			}

			return null;
		}

		/**
		 * Moves all cart items to new token. Used when logging in.
		 *
		 * @param $token
		 *
		 * @see Eso_Login
		 * @since 2019.5
		 */
		public function replace_token( $token ) {
			global $wpdb;

			$data = [
				"token" => $token
			];

			$where = [
				"token" => eso_session_token()
			];

			$wpdb->update( ESO_CART_TABLE, $data, $where );
		}

		/**
		 * Empty cart for user
		 * @since 0.0.31
		 */
		public function destroy() {
			global $wpdb;

			$where = [
				"token" => eso_session_token()
			];

			$wpdb->delete( ESO_CART_TABLE, $where );
		}
	}
}