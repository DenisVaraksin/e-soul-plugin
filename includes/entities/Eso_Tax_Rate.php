<?php
if ( ! class_exists( 'Eso_Tax_Rate', false ) ) {
	class Eso_Tax_Rate {

		private $id;
		private $name;
		private $value;

		public function __construct( $id ) {
			$this->id = $id;

			$rates = eso_get_tax_rates_array();

			if ( isset( $rates[ $id ] ) ) {
				$tax_rate = $rates[$id];
				$this->name = $tax_rate["name"];
				$this->value = $tax_rate["value"];
			} else {
				write_log("Invalid Eso_Tax_Rate __construct");
			}
		}

		public function get_id() {
			return $this->id;
		}

		public function get_name() {
			return $this->name;
		}

		public function get_value() {
			return $this->value;
		}

		public function set_name( $name ) {
			$this->name = $name;
		}

		public function set_value( $value ) {
			$this->value = $value;
		}

	}
}