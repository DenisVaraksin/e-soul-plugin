<?php
if ( ! class_exists( 'Eso_Session', false ) ) {
	class Eso_Session {

		private $token;

		public function __construct( $token ) {
			$this->token = $token;
		}

		public function get_token() {
			return $this->token;
		}

		/**
		 * @since 2019.6 for logged in users load all data from meta fields
		 *
		 * @param $name
		 *
		 * @return mixed|null|string
		 */
		public function get_value( $name ) {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				$meta = $customer->get_meta( $name );
				if ( ! empty( $meta ) ) {
					return $meta;
				}
			}

			global $wpdb;

			$result = $wpdb->get_row( "SELECT value FROM " . ESO_SESSIONS_TABLE . " WHERE token = '" . $this->get_token() . "' AND name = '" . $name . "'", ARRAY_A );

			if ( ! $result ) {
				return null;
			}

			return maybe_unserialize( $result["value"] );
		}

		/**
		 * @return Eso_Currency|mixed
		 */
		public function get_active_currency() {
			$currency_code = $this->get_value( "currency" );

			if ( $currency_code ) {
				$currency = new Eso_Currency( $currency_code );

				if ( $currency->is_enabled() ) {
					return $currency;
				} else {
					return eso_get_default_currency();
				}
			}

			return eso_get_default_currency();
		}

		/**
		 * @param Eso_Currency $currency
		 *
		 * @return bool
		 */
		public function set_active_currency( Eso_Currency $currency ) {

			$result = $this->set_value( "currency", $currency->get_code() );

			if ( $result ) {
				$currency->set_enabled( true );

				return $currency->get_code();
			}

			return false;

		}

		/**
		 * @since 0.0.31
		 * @since 2019.6 returns bool
		 *
		 *
		 * @return bool
		 */
		public function is_billing_on() {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				return $customer->is_billing_on();
			}

			$value = $this->get_value( "billing_on" );

			if ( $value == 1 ) {
				return true;
			}

			return false;
		}

		/**
		 * @since 2019.6
		 *
		 * @return string|null
		 */
		public function get_payment_method_code() {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				$meta = get_user_meta( $customer->get_id(), "payment_method", true );

				return $meta;
			}

			return $this->get_value( "payment_method" );
		}


		/**
		 * @since 2019.6
		 *
		 * @return string|null
		 */
		public function get_shipping_method_code() {
			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				$meta = get_user_meta( $customer->get_id(), "shipping_method", true );

				return $meta;
			}

			return $this->get_value( "shipping_method" );
		}

		/**
		 * @since 2019.6 renamed to set_value
		 *
		 * @param $name
		 * @param $value
		 *
		 * @return false|int
		 */
		public function set_value( $name, $value ) {
			global $wpdb;

			$current_value = $this->get_value( $name );

			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );

				$customer->set_meta( $name, $value );
			}

			$value = maybe_serialize( $value );

			if ( $current_value ) {
				$data = [
					'updated' => eso_db_now(),
					'value'   => $value
				];

				$where = [
					'token' => $this->get_token(),
					'name'  => $name
				];

				$result = $wpdb->update( ESO_SESSIONS_TABLE, $data, $where );

				return $result;
			}

			$data = [
				'token' => $this->get_token(),
				'name'  => $name,
				'value' => $value
			];

			return $wpdb->insert( ESO_SESSIONS_TABLE, $data );
		}

		/**
		 * Moves all sessions to new token. Used when logging in.
		 *
		 * @param $token
		 *
		 * @see Eso_Login
		 * @since 2019.5
		 */
		public function replace_token( $token ) {
			global $wpdb;

			$data = [
				"token" => $token,
			];

			$where = [
				"token" => eso_session_token()
			];

			$wpdb->update( ESO_SESSIONS_TABLE, $data, $where );
		}


	}
}