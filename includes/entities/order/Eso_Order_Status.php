<?php
if ( ! class_exists( 'Eso_Order_Status', false ) ) {
	/**
	 * @since 2019.7
	 *
	 * Class Eso_Order_Status
	 */
	class Eso_Order_Status {
		const TAXONOMY = "order_status";

		private $term_id;

		/**
		 * Eso_Order_Status constructor.
		 *
		 * @param $term_id
		 */
		public function __construct( $term_id ) {
			$this->term_id = $term_id;

			$this->term = get_term( $this->term_id, self::TAXONOMY );
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->term_id;
		}

		/**
		 * @param $name
		 */
		public function set_name( $name ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "name" => $name ] );
		}

		/**
		 * @return string
		 */
		public function get_name() {
			return $this->term->name;
		}

		/**
		 * @param $slug
		 *
		 * @returns null|WP_Error
		 */
		public function set_slug( $slug ) {
			wp_update_term( $this->get_id(), self::TAXONOMY, [ "slug" => $slug ] );
		}

		/**
		 * @return string
		 */
		public function get_slug() {
			return $this->term->slug;
		}

		/**
		 * @return int
		 */
		public function get_count() {
			return $this->term->count;
		}

		/**
		 * @return bool
		 */
		public function can_be_deleted() {
			if(!in_array($this->get_slug(), ["cancelled", "waiting-for-payment", "delivered", "for-completion", "on-the-way", "failed"])) {
				return true;
			}

			return false;
		}

		public function delete() {
			wp_delete_term( $this->get_id(), self::TAXONOMY );
		}

	}
}
