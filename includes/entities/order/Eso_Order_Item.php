<?php
if ( ! class_exists( 'Eso_Order_Item', false ) ) {
	class Eso_Order_Item {

		private $order_id;

		private $product_id;

		public function __construct( $order_id, $product_id ) {
			$this->order_id   = $order_id;
			$this->product_id = $product_id;
		}

		public function get_order_id() {
			return $this->order_id;
		}

		public function the_order_id() {
			echo $this->order_id;
		}

		public function get_product_id() {
			return $this->product_id;
		}

		public function the_product_id() {
			echo $this->product_id;
		}

		/**
		 * @return Eso_Product
		 *
		 * @since 2019.6
		 */
		public function get_product() {
			return new Eso_Product($this->get_product_id());
		}

		public function get_name() {
			$meta = get_the_title( $this->get_product_id() );

			return $meta;
		}

		private function get_item_data() {
			$meta = get_post_meta( $this->get_order_id(), "products", true );

			if ( isset( $meta[ $this->get_product_id() ] ) ) {
				return $meta[ $this->get_product_id() ];
			}

			return null;
		}

		private function get_options_data() {
			$meta = get_post_meta( $this->get_order_id(), "options", true );

			return $meta;
		}

		public function get_quantity() {
			$data = $this->get_item_data();

			if ( $data ) {
				if ( isset( $data["quantity"] ) ) {
					return $data["quantity"];
				}
			}

			return null;
		}

		public function get_price_per_piece( Eso_Currency $currency ) {
			$data = $this->get_item_data();

			if ( isset( $data[ $currency->get_code() ] ) ) {
				return $data[ $currency->get_code() ]["price_per_piece"];
			}

			if ( isset( $data["price_per_piece"] ) ) {
				return $data["price_per_piece"];
			}

			return null;
		}

		public function get_currency() {
			$data = $this->get_options_data();

			if ( isset( $data["currency"] ) ) {
				return $data["currency"];
			}

			return null;
		}

		/**
		 * @return null
		 *
		 * @since 2019.6 deprecated sum array key, counting sum from quantity
		 */
		public function get_sum() {
			$data = $this->get_item_data();

			/**
			 * @internal can be removed 2020.6
			 */
			if ( $data && isset( $data["sum"] ) && $data["sum"] != 0 ) {
				return $data["sum"];
			}

			$options = $this->get_options_data();
			if(!isset($options["currency"])) {
				return null;
			}

			$currency = $options["currency"];

			if(isset($data["quantity"]) && isset($data[$currency]["price_per_piece"])) {
				return (int) $data["quantity"] * (int) $data[$currency]["price_per_piece"];
			}

			return null;
		}

		public function get_total( Eso_Currency $currency ) {
			$product  = new Eso_Product( $this->get_product_id() );
			$quantity = $this->get_quantity();

			$total = $product->get_price( $currency ) * $quantity;

			return $total;

		}

		public function get_total_with_currency() {
			/* @var $currency Eso_Currency */
			$currency = eso_get_active_currency();

			return $this->get_total( $currency ) . " " . $currency->get_symbol();
		}

		/**
		 * @since 2019.7
		 *
		 * @param Eso_Currency $currency
		 *
		 * @return float|int
		 */
		public function get_total_without_tax( Eso_Currency $currency ) {
			$total = $this->get_product()->get_price_without_tax($currency) * $this->get_quantity();

			return $total;
		}

	}
}