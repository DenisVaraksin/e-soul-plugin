<?php
if ( ! class_exists( 'Eso_Order', false ) ) {
	class Eso_Order {

		private $order_id;

		public function __construct( $order_id ) {
			$this->order_id = (int) $order_id;
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->order_id;
		}

		public function get_title() {
			return get_the_title( $this->get_id() );
		}

		private function get_customer_data() {
			return get_post_meta( $this->order_id, "customer", true );
		}

		/**
		 * @param $field_key
		 *
		 * @return string|null
		 * @since 2020.1.9 strip_tags
		 */
		private function get_customer_field( $field_key ) {
			$customer_data = $this->get_customer_data();

			if ( isset( $customer_data[ $field_key ] ) && $customer_data[ $field_key ] != "" ) {
				return strip_tags( $customer_data[ $field_key ] );
			}

			return null;
		}

		/**
		 * @param $field_key
		 * @param $field_value
		 */
		public function set_customer_field( $field_key, $field_value ) {
			$customer_data = get_post_meta( $this->get_id(), "customer", true );

			$customer_data[ $field_key ] = $field_value;

			update_post_meta( $this->get_id(), "customer", $customer_data );
		}

		/**
		 * @return array
		 */
		private function get_options_data() {
			$meta = get_post_meta( $this->get_id(), "options", true );

			return $meta;
		}

		/**
		 * @param $field_key
		 *
		 * @return string|null
		 */
		public function get_options_field( $field_key ) {
			$options_data = $this->get_options_data();

			if ( isset( $options_data[ $field_key ] ) && $options_data[ $field_key ] != "" ) {
				return $options_data[ $field_key ];
			}

			return null;
		}

		/*
		 * @return false|string
		 *
		 * @since 2019.6
		 */
		public function get_date_and_time_created() {
			return get_the_date( "j. n. Y, G:i", $this->get_id() );
		}

		/**
		 * @return false|string
		 */
		public function get_date_created() {
			return get_the_date( "j. n. Y", $this->get_id() );
		}

		/**
		 * @return DateTime
		 *
		 * @since 2019.6
		 */
		public function get_datetime_created() {
			$date = get_the_date( "d. m. Y H:i:s", $this->get_id() );

			return DateTime::createFromFormat( "d. m. Y H:i:s", $date );
		}

		/**
		 * @param bool $update
		 *
		 * @return null|WP_Term
		 * @throws Exception
		 * @since 2019.7 fetches order status from comgate
		 */
		private function get_status( $update = true ) {
			if ( $update && ! empty( $this->get_elogist_order_id() ) && eso_is_comgate_elogist_active() ) {

				$comgate_delivery = new Eso_Comgate_Delivery( $this );

				if ( ! $this->get_status_updated() ) {
					$comgate_delivery->set_order_status_from_comgate();
				} else {
					$diff = $this->get_status_updated()->diff( eso_now() );

					$minutes = ( $diff->days * 24 * 60 ) +
					           ( $diff->h * 60 ) + $diff->i;

					if ( $minutes > 30 ) {
						$comgate_delivery->set_order_status_from_comgate();
					}
				}
			}

			$meta = wp_get_post_terms( $this->get_id(), "order_status" );

			if ( $meta ) {
				return $meta[0];
			}

			return null;
		}

		/**
		 * @return DateTime|null
		 * @since 2019.7
		 *
		 */
		public function get_status_updated() {
			if ( get_post_meta( $this->get_id(), "status_updated", true ) ) {
				$date = get_post_meta( $this->get_id(), "status_updated", true );

				return new DateTime( $date, new DateTimeZone( "Europe/Prague" ) );
			}

			return null;
		}

		/**
		 * @since 2019.7
		 */
		public function set_status_updated() {
			update_post_meta( $this->get_id(), "status_updated", eso_db_now() );
		}

		/**
		 * @since 2019.5
		 */
		public function get_status_id( $update = true ) {
			$status = $this->get_status( $update );

			if ( $status ) {
				return $status->term_id;
			}

			return null;
		}

		/**
		 * @since 2019.5
		 */
		public function get_status_slug( $update = true ) {
			$status = $this->get_status( $update );

			if ( $status ) {
				return $status->slug;
			}

			return null;
		}

		/**
		 * @since 2019.5
		 */
		public function get_status_name( $update = true ) {
			$status = $this->get_status( $update );

			if ( $status ) {
				return $status->name;
			}

			return null;
		}

		/**
		 * @return string
		 *
		 * @since 2019.5
		 */
		public function get_edit_url() {
			return admin_url( "admin.php?page=single-order&id=" . $this->get_id() );
		}

		/**
		 * @since 2019.5
		 */
		public function the_edit_url() {
			echo $this->get_edit_url();
		}

		/**
		 * @param $status_id
		 *
		 * @since 2019.7 added $log and return when the same status is already set
		 *
		 * @since 2019.5
		 */
		public function set_status_by_id( $status_id, $log = "Stav objedvnáky změnen na " ) {
			if ( $this->get_status_id( false ) == $status_id ) {
				return;
			}

			$status = get_term_by( 'id', $status_id, 'order_status' );

			wp_set_post_terms( $this->get_id(), $status->term_id, 'order_status' );

			if ( $status->slug == "for-completion" ) {
				$this->start_delivery();
			}

			if ( ! empty( $log ) ) {
				$this->add_log( $log . $status->name );
			}

			$status_slug = $this->get_status_slug();

			if ( in_array( $status_slug, [
					"for-completion",
					"on-the-way"
				] ) && $this->get_payment_method()->get_code() != "cod" ) {
				$this->set_paid();
			}
		}

		/**
		 * @param $status_slug
		 * @param bool $add_log
		 *
		 * @since 2019.5
		 * @since 2019.7 added $log and return when the same status is already set
		 *
		 */
		public function set_status_by_slug( $status_slug, $log = "Stav objednávky změnen na " ) {
			if ( $this->get_status_slug( false ) == $status_slug ) {
				return;
			}

			$status = get_term_by( 'slug', $status_slug, 'order_status' );

			wp_set_post_terms( $this->get_id(), $status->term_id, 'order_status' );

			if ( ! empty( $log ) ) {
				$this->add_log( $log . $status->name );
			}

			if ( in_array( $status_slug, [
					"for-completion",
					"on-the-way"
				] ) && $this->get_payment_method()->get_code() != "cod" ) {
				$this->set_paid();
			}
		}

		/**
		 * @since 2019.5
		 */
		public function set_waiting_for_payment() {
			$this->set_status_by_slug( 'waiting-for-payment' );
		}

		/**
		 * @since 2019.5
		 */
		public function set_for_completion() {
			$this->set_status_by_slug( "for-completion" );

			$this->start_delivery();
		}

		/**
		 * @since 2019.5
		 */
		public function set_failed() {
			$this->set_status_by_slug( "failed" );
		}

		/**
		 * @since 2019.5
		 */
		public function set_cancelled() {
			$this->set_status_by_slug( "cancelled" );
		}

		/**
		 * @since 2019.5
		 */
		public function set_on_the_way() {
			$this->set_status_by_slug( "on-the-way" );
		}


		/**
		 * @param $date - should be Y-m-d H:i:s
		 *
		 * @return bool|int|null
		 *
		 * @since 2019.7
		 */
		public function set_paid( $date = "now" ) {
			if ( $this->is_paid() ) {
				return null;
			}

			if ( $date == "now" ) {
				$meta = update_post_meta( $this->get_id(), "paid", eso_db_now() );
			} else {
				$meta = update_post_meta( $this->get_id(), "paid", $date );
			}

			return $meta;
		}

		/**
		 * @return bool|string
		 *
		 * @since 2019.7
		 */
		public function get_paid_date() {
			$meta = get_post_meta( $this->get_id(), "paid", true );

			if ( $meta ) {
				return $meta;
			}

			return false;
		}

		/**
		 * @return bool
		 *
		 * @since 2019.7
		 */
		public function is_paid() {
			if ( $this->get_paid_date() ) {
				return true;
			}

			return false;
		}

		public function get_customer_id() {
			$post_author = get_post_field( "post_author", $this->get_id() );

			return $post_author;
		}

		/**
		 * @return Eso_Shipping_Method
		 * @since 2019.6 shipping_method is array
		 *
		 * @since 0.0.31
		 */
		public function get_shipping_method() {
			$shipping_method = $this->get_options_field( "shipping_method" );

			if ( isset( $shipping_method["code"] ) ) {
				return new Eso_Shipping_Method( $shipping_method["code"] );
			}

			return new Eso_Shipping_Method( $shipping_method );
		}

		/**
		 * @return string|null
		 * @since 2019.6
		 *
		 */
		public function get_shipping_method_name() {
			$shipping_method = $this->get_shipping_method();

			if ( ! $shipping_method ) {
				return null;
			}

			return $shipping_method->get_name();
		}

		/**
		 * @param bool $include_currency
		 * @param Eso_Currency|null $currency
		 *
		 * @return int|string
		 * @since 2019.6
		 *
		 */
		public function get_shipping_method_price( $include_currency = false, Eso_Currency $currency = null ) {
			if ( $currency == null ) {
				$currency = $this->get_currency();
			}

			$shipping_method = $this->get_options_field( "shipping_method" );

			if ( isset( $shipping_method["price"] ) ) {
				if ( is_array( $shipping_method["price"] ) ) {
					$price = $shipping_method["price"][ $currency->get_code() ];
				} else {
					$price = $shipping_method["price"];
				}
			} else {
				$price = 0;
			}

			if ( $include_currency ) {
				$price .= " " . $this->get_currency()->get_symbol();

				return $price;
			}

			return (float) $price;
		}

		/**
		 * @return Eso_Payment_Method
		 * @since 2019.6 payment method is array
		 *
		 * @since 0.0.31
		 */
		public function get_payment_method() {
			$payment_method = $this->get_options_field( "payment_method" );

			if ( isset( $payment_method["code"] ) ) {
				return new Eso_Payment_Method( $payment_method["code"] );
			}

			return new Eso_Payment_Method( $payment_method );
		}

		/**
		 * @return string|null
		 *
		 * @since 2019.6
		 */
		public function get_payment_method_name() {
			$payment_method = $this->get_payment_method();

			if ( ! $payment_method ) {
				return null;
			}

			return $payment_method->get_name();
		}

		/**
		 * @param bool $include_currency
		 * @param Eso_Currency|null $currency
		 *
		 * @return int|string
		 * @since 2019.6
		 *
		 */
		public function get_payment_method_price( $include_currency = false, Eso_Currency $currency = null ) {
			if ( $currency == null ) {
				$currency = $this->get_currency();
			}

			$payment_method = $this->get_options_field( "payment_method" );

			if ( isset( $payment_method["price"] ) ) {
				if ( is_array( $payment_method["price"] ) ) {
					$price = $payment_method["price"][ $currency->get_code() ];
				} else {
					$price = $payment_method["price"];
				}
			} else {
				$price = 0;
			}

			if ( $include_currency ) {
				$price .= " " . $this->get_currency()->get_symbol();

				return $price;
			}

			return (float) $price;
		}

		/**
		 * @param bool $include_currency
		 *
		 * @param Eso_Currency|null $currency
		 *
		 * @return int|string
		 * @since 2019.6
		 *
		 */
		public function get_shipping_payment_price( $include_currency = false, Eso_Currency $currency = null ) {
			if ( $currency == null ) {
				$currency = $this->get_currency();
			}

			$price = $this->get_payment_method_price( false, $currency ) + $this->get_shipping_method_price( false, $currency );

			if ( $include_currency ) {
				return $price . " " . $this->get_currency()->get_symbol();
			}

			return $price;
		}

		/**
		 * @return array
		 *
		 * @since 2019.5
		 */
		function get_zasilkovna_array() {
			$meta = $this->get_options_field( "zasilkovna_branch" );

			if ( $meta ) {
				return explode( ':', $meta );
			}

			return null;
		}

		/**
		 * @return string
		 *
		 * @since 2019.5
		 */
		public function get_zasilkovna_branch_id() {
			$meta = $this->get_zasilkovna_array();

			if ( is_array( $meta ) ) {
				return $meta[0];
			}

			return null;
		}

		/**
		 * @return string
		 *
		 * @since 2019.5
		 */
		public function get_zasilkovna_branch_name() {
			$meta = $this->get_zasilkovna_array();

			if ( is_array( $meta ) ) {
				return $meta[1];
			}

			return null;
		}

		/**
		 * @return Eso_Currency
		 *
		 * @since 2019.5
		 */
		public function get_currency() {
			$currency_code = $this->get_currency_code();

			return new Eso_Currency( $currency_code );
		}

		/**
		 * @return string|null
		 *
		 * @since 2019.5
		 */
		public function get_currency_code() {
			$currency_code = $this->get_options_field( "currency" );

			return $currency_code;
		}

		/**
		 * @return Eso_Customer
		 */
		public function get_customer() {
			$customer = new Eso_Customer( $this->get_customer_id() );

			return $customer;
		}

		public function is_billing_on() {
			return $this->get_customer_field( "billing_on" );
		}

		/**
		 * @return null|string
		 */
		public function get_billing_company_name() {
			return $this->get_customer_field( "billing_company_name" );
		}


		public function get_billing_ico() {
			return $this->get_customer_field( "billing_ico" );
		}

		public function get_billing_dic() {
			return $this->get_customer_field( "billing_dic" );
		}

		public function get_billing_street() {
			return $this->get_customer_field( "billing_street" );
		}

		/**
		 * @return null
		 */
		public function get_billing_postcode() {
			return $this->get_customer_field( "billing_postcode" );
		}

		public function get_billing_city() {
			return $this->get_customer_field( "billing_city" );
		}

		public function get_billing_country() {
			return $this->get_customer_field( "billing_country" );
		}

		public function get_shipping_name() {
			return $this->get_customer_field( "shipping_first_name" ) . " " . $this->get_customer_field( "shipping_last_name" );
		}

		/**
		 * @return null|string
		 * @since 2019.8
		 *
		 */
		public function get_shipping_first_name() {
			return $this->get_customer_field( "shipping_first_name" );
		}

		/**
		 * @return null|string
		 * @since 2019.8
		 *
		 */
		public function get_shipping_last_name() {
			return $this->get_customer_field( "shipping_last_name" );
		}

		public function get_shipping_street() {
			return $this->get_customer_field( "shipping_street" );
		}

		public function get_shipping_postcode() {
			return $this->get_customer_field( "shipping_postcode" );
		}

		public function get_shipping_city() {
			return $this->get_customer_field( "shipping_city" );
		}

		public function get_shipping_country() {
			return $this->get_customer_field( "shipping_country" );
		}

		public function get_shipping_email() {
			return $this->get_customer_field( "shipping_email" );
		}

		public function get_shipping_phone() {
			return $this->get_customer_field( "shipping_phone" );
		}

		public function get_note() {
			return $this->get_customer_field( "order_note" );
		}

		/**
		 * @return array of Eso_Order_Item
		 */
		public function get_items() {
			$meta = get_post_meta( $this->get_id(), "products", true );

			$items = [];

			if ( ! empty( $meta ) ) {
				foreach ( $meta as $product_id => $product_data ) {
					$order_item = new Eso_Order_Item( $this->get_id(), $product_id );

					$items[ $product_id ] = $order_item;
				}
			}

			return $items;
		}

		/**
		 * @param $item_id
		 *
		 * @return bool
		 *
		 * @since 2019.5
		 */
		public function has_item( $item_id ) {
			if ( array_key_exists( $item_id, $this->get_items() ) ) {
				return true;
			}

			return false;
		}

		/**
		 * @param $item_id
		 *
		 * @return bool|Eso_Order_Item
		 *
		 * @since 2019.5
		 */
		public function get_item( $item_id ) {
			$items = $this->get_items();

			if ( array_key_exists( $item_id, $items ) ) {
				$order_item = new Eso_Order_Item( $this->get_id(), $item_id );

				return $order_item;
			}

			return false;
		}

		/**
		 * @param Eso_Currency $currency
		 * @param bool $include_currency
		 *
		 * @return float|int
		 * @see $this->get_currency() returns currency in which the order was made
		 *
		 * @since 2019.5 $currency parameter introduced
		 * @since 2019.6 $include_currency parameter introduced, renamed to get_sum
		 *
		 * @see eso_get_active_currency() can be used for a total of currently active currency
		 */
		public function get_sum( Eso_Currency $currency, $include_currency = false ) {
			$order_items = $this->get_items();

			if ( ! $order_items ) {
				return 0;
			}

			$total = 0;

			/* @var $order_item Eso_Cart_Item */
			foreach ( $order_items as $order_item ) {
				$total += $order_item->get_total( $currency );
			}

			if ( $include_currency ) {
				return $total . " " . $this->get_currency()->get_symbol();
			}

			return $total;
		}

		/**
		 * @return string
		 *
		 * @since 2019.6
		 */
		public function get_sum_with_original_currency() {
			$currency = $this->get_currency();

			return $this->get_sum( $currency ) . " " . $currency->get_symbol();
		}

		/**
		 * @return string
		 * @since 2019.5 renamed from get_total_with_currency() to get_total_with_active_currency()
		 *
		 */
		public function get_sum_with_active_currency() {

			/* @var $currency Eso_Currency */
			$currency = eso_get_active_currency();

			return $this->get_sum( $currency ) . " " . $currency->get_symbol();
		}

		/**
		 * @param bool $include_currency
		 *
		 * @param Eso_Currency|null $currency
		 *
		 * @return float|int|string
		 * @since 2019.7 parameter $currency introduced, moved the logic to set_total()
		 *
		 * @since 2019.6
		 */
		public function get_total( $include_currency = false, Eso_Currency $currency = null ) {
			if ( ! isset( $currency ) ) {
				$currency = $this->get_currency();
			}

			$meta = get_post_meta( $this->get_id(), "total_" . $currency->get_code(), true );

			if ( empty( $meta ) ) {
				$total = $this->set_total( $currency );
			} else {
				$total = $meta;
			}

			if ( $include_currency ) {
				return $total . " " . $currency->get_symbol();
			}

			return $total;
		}

		/**
		 * @param Eso_Currency $currency
		 *
		 * @return float|int|string
		 * @since 2019.7
		 *
		 */
		public function set_total( Eso_Currency $currency ) {

			$sum = $this->get_sum( $currency );

			$total = $sum + $this->get_shipping_payment_price( false, $currency );

			$total = (float) $total;

			if ( $this->get_discount() ) {
				$total -= $this->get_discount();
			}

			update_post_meta( $this->get_id(), "total_" . $currency->get_code(), $total );

			return $total;
		}

		/**
		 * @param bool $comma_delimiter
		 *
		 * @param bool $include_currency
		 *
		 * @return string
		 * @since 2019.7
		 *
		 */
		public function get_total_without_tax( $comma_delimiter = false, $include_currency = false ) {
			$items = $this->get_items();

			if ( ! $items ) {
				return "0";
			}

			$total = 0;

			/* @var $item Eso_Order_Item */
			foreach ( $items as $item ) {
				$total += $item->get_total_without_tax( $this->get_currency() );
			}

			$total += $this->get_shipping_payment_price();

			$total = (float) $total;

			if ( $this->get_discount() ) {
				$total -= $this->get_discount();
			}

			if ( $comma_delimiter ) {
				$total = str_replace( ".", ",", $total );
			}

			if ( $include_currency ) {
				return $total . " " . $this->get_currency()->get_symbol();
			}

			return $total;
		}

		/**
		 * @since 2019.5
		 */
		public function get_integer_total() {
			$total = $this->get_total();

			return (int) ( $total * 100 );
		}

		/**
		 * @param bool $comma_delimiter
		 * @param bool $include_currency
		 *
		 * @return string|null
		 * @since 2019.7
		 *
		 */
		public function get_discount( $comma_delimiter = false, $include_currency = false ) {
			if ( ! $this->get_coupon_id() ) {
				return null;
			}

			$discount = $this->get_options_field( "coupon_discount" );

			if ( $comma_delimiter ) {
				$discount = str_replace( ".", ",", $discount );
			}

			if ( $include_currency ) {
				$discount .= " " . eso_get_active_currency_symbol();
			}

			return $discount;
		}

		/**
		 * @return null|string
		 * @since 2019.7
		 *
		 */
		public function get_coupon_id() {
			$meta = $this->get_options_field( "coupon_id" );

			return $meta;
		}

		/**
		 * @return null|string
		 * @since 2019.7
		 *
		 */
		public function get_coupon_code() {
			$meta = $this->get_options_field( "coupon_code" );

			return $meta;
		}

		/**
		 * @since 2019.5
		 */
		public function get_comgate_payment_id() {
			$meta = get_post_meta( $this->get_id(), "comgate_payment_id", true );

			return $meta;
		}

		/**
		 * @return mixed
		 *
		 * @since 2019.5
		 */
		public function get_log() {
			$meta = get_post_meta( $this->get_id(), "log" );

			return array_reverse( $meta );
		}

		/**
		 * @param $message
		 *
		 * @param bool $notification
		 * @param string $type
		 *
		 * @since 2019.5
		 */
		public function add_log( $message, $notification = false, $type = "info" ) {
			if ( is_user_logged_in() ) {
				$current_user      = wp_get_current_user();
				$current_user_name = $current_user->display_name;
			} else {
				$current_user_name = "nepřihlášen";
			}

			$log = [];

			$log["time"]    = eso_db_now();
			$log["user"]    = $current_user_name;
			$log["message"] = $message;
			$log["type"]    = $type;

			add_post_meta( $this->get_id(), "log", $log );

			if ( $notification ) {
				write_log( $message, true );
			}
		}

		/**
		 * @since 2019.5
		 */
		public function render_log() {
			if ( $this->get_log() ) {
				foreach ( $this->get_log() as $log ) :
					if ( isset( $log["time"] ) ) {
						$time = new DateTime( $log["time"] );
						echo "<hr>" . $time->format( "d. m. Y H:i:s" ) . " | " . $log["user"] . " | " . $log["message"];
					} else {
						print_r( $log );
					}
				endforeach;
			} else { ?>
                <p><?php _e( "Zatím žádné změny.", "eso" ) ?></p>
			<?php }
		}

		/**
		 * @return string|null
		 * @since 2019.7
		 *
		 */
		public function get_elogist_order_id() {
			$meta = get_post_meta( $this->get_id(), 'elogist_order_id', true );

			if ( ! empty( $meta ) ) {
				return $meta;
			}

			return null;
		}

		/**
		 * @since 2019.5
		 */
		public function start_delivery() {
			if ( ! eso_is_comgate_elogist_active() ) {
				return;
			}

			if ( get_post_meta( $this->get_id(), "_order_sent_to_comgate", true ) ) {
				return;
			}

			$comgate_logistika    = new Eso_Module( "comgate_logistika" );
			$comgate_logistika_sk = new Eso_Module( "comgate_logistika" );

			if ( $comgate_logistika->is_enabled() && $this->get_shipping_country() == "CZ" ) {
				$comgate_elogist = new Eso_Comgate_Delivery( $this );
				$comgate_elogist->delivery_order();
			}

			if ( $comgate_logistika_sk->is_enabled() && $this->get_shipping_country() == "SK" ) {
				$comgate_elogist = new Eso_Comgate_Delivery( $this );
				$comgate_elogist->delivery_order();
			}
		}

		/**
		 * @return string
		 * @since 2019.7
		 *
		 */
		public function get_token() {
			$meta = get_post_meta( $this->get_id(), "_token", true );

			return $meta;
		}

		/**
		 * @param $token
		 *
		 * @return bool
		 * @since 2019.7
		 *
		 */
		public function can_access( $token ) {
			if ( current_user_can( "install_plugins" ) ) {
				return true;
			}

			$meta = get_post_meta( $this->get_id(), "_token", true );

			if ( ! empty( $meta ) && $meta == $token ) {
				return true;
			}

			return false;
		}

		/**
		 * @since 2019.6
		 */
		public function render_quick_look() {
			require( ESO_DIR . "/admin/views/order/quick-look.php" );
		}

		/**
		 * @return Eso_Invoice
		 * @since 2019.7
		 *
		 */
		public function get_invoice() {
			return new Eso_Invoice( $this->get_id() );
		}

		/**
		 * @return false|null|WP_Post
		 * @since 2019.6
		 *
		 */
		public function delete() {
			return wp_delete_post( $this->get_id() );
		}

	}
}