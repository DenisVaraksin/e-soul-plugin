<?php
if ( ! class_exists( 'Eso_Shipping_Zone', false ) ) {
	class Eso_Shipping_Zone {

		private $code;

		private $name;

		private $shipping_methods;

		private $payment_methods;

		private $enabled;

		/**
		 * Eso_Shipping_Zone constructor.
		 *
		 * @param $code
		 */
		public function __construct( $code ) {
			$this->code = $code;

			global $wpdb;
			$row = $wpdb->get_row( "SELECT name, code, shipping_methods, enabled FROM " . ESO_SHIPPING_ZONE_TABLE . " WHERE code = '" . $this->get_code() . "'" );

			if ( isset( $row->name ) ) {
				$this->name = $row->name;
			}
			if ( isset( $row->shipping_methods ) ) {
				$this->shipping_methods = $row->shipping_methods;
			}
			if ( isset( $row->enabled ) ) {
				$this->enabled = $row->enabled;
			}
		}

		public function get_code() {
			return $this->code;
		}

		public function get_name() {
			return $this->name;
		}

		public function get_shipping_methods() {
			return unserialize( $this->shipping_methods );
		}

		/**
		 * @param $shipping_method_code
		 *
		 * @return null
		 */
		public function get_shipping_method_prices( $shipping_method_code ) {
			$shipping_methods = $this->get_shipping_methods();

			if ( isset( $shipping_methods[ $shipping_method_code ] ) && isset( $shipping_methods[ $shipping_method_code ]['prices'] ) ) {
				return $shipping_methods[ $shipping_method_code ]['prices'];
			}

			return null;
		}

		/**
		 * @param $shipping_method_code
		 * @param Eso_Currency $currency
		 *
		 * @return mixed
		 * @since 0.0.31
		 */
		public function get_shipping_method_price( $shipping_method_code, Eso_Currency $currency ) {
			$prices = $this->get_shipping_method_prices( $shipping_method_code );

			if ( isset( $prices[ $currency->get_code() ] ) ) {
				return round( $prices[ $currency->get_code() ], 2 );
			}

			return null;

		}

		/**
		 * @param $shipping_method_code
		 * @param $prices
		 *
		 * @since 0.0.30
		 */
		public function update_shipping_method_prices( $shipping_method_code, $prices ) {
			$shipping_methods = $this->get_shipping_methods();

			if ( isset( $shipping_methods[ $shipping_method_code ] ) ) {
				$shipping_methods[ $shipping_method_code ]["prices"] = $prices;
			}

			$this->update_shipping_methods( $shipping_methods );
		}

		/**
		 * @param $shipping_methods
		 */
		public function update_shipping_methods( $shipping_methods ) {
			global $wpdb;
			$data = [
				"shipping_methods" => serialize( $shipping_methods )
			];

			$where = [
				"code" => $this->get_code()
			];

			$wpdb->update( ESO_SHIPPING_ZONE_TABLE, $data, $where );
		}

		/**
		 * @since 0.0.31
		 * @since 2019.7 payment methods belong now to shipping methods
		 *
		 * @return array
		 */
		public function get_payment_methods( $shipping_method_code ) {

			$shipping_methods = $this->get_shipping_methods();

			if ( ! empty( $shipping_methods ) ) {
				if ( isset( $shipping_methods[ $shipping_method_code ] ) ) {
					if ( isset( $shipping_methods[ $shipping_method_code ]["payment_methods"] ) ) {
						return $shipping_methods[ $shipping_method_code ]["payment_methods"];
					}
				}
			}

			return null;
		}

		/**
		 * @return array
		 *
		 * @since 2019.5
		 */
		public function get_enabled_payment_methods( $shipping_method_code ) {
			$payment_methods = $this->get_payment_methods( $shipping_method_code );

			$enabled_payment_methods = [];

			if(!empty($payment_methods)) {
				foreach ( $payment_methods as $payment_method_code => $payment_method_data ) {
					if ( isset( $payment_method_data['enabled'] ) && $payment_method_data['enabled'] == 1 ) {
						$enabled_payment_methods[ $payment_method_code ] = $payment_method_data;
					}
				}
			}

			return $enabled_payment_methods;
		}

		/**
		 * @since 2019.5
		 *
		 * @return string
		 */
		public function get_first_enabled_payment_method( $shipping_method_code ) {
			$methods = $this->get_enabled_payment_methods( $shipping_method_code );

			foreach ( $methods as $payment_method_code => $payment_method_data ) {
				return $payment_method_code;
			}

			return "bank_transfer";
		}

		/**
		 * @since 2019.7
		 *
		 * @param $shipping_method_code
		 * @param $payment_method_code
		 *
		 * @return mixed|null
		 */
		public function get_payment_method($shipping_method_code, $payment_method_code) {
			$payment_methods = $this->get_payment_methods($shipping_method_code);

			if(isset($payment_methods[$payment_method_code])) {
				return $payment_methods[$payment_method_code];
			}

			return null;
		}

		/**
		 * @since 2019.7
		 *
		 * @param $shipping_method_code
		 *
		 * @return bool
		 */
		public function has_any_payment_method($shipping_method_code) {
			if(!empty($this->get_payment_methods($shipping_method_code))) {
				return true;
			}

			return false;
		}

		/**
		 * @param $payment_method_code
		 *
		 * @return bool
		 * @since 2019.5
		 */
		public function has_payment_method( $shipping_method_code, $payment_method_code ) {
			if($this->get_payment_method_enabled($shipping_method_code, $payment_method_code) == 1) {
				return true;
			}

			return false;
		}

		/**
		 * @since 2019.7
		 *
		 * @param $shipping_method_code
		 * @param $payment_method_code
		 */
		public function get_payment_method_enabled($shipping_method_code, $payment_method_code) {
			$payment_methods = $this->get_payment_methods( $shipping_method_code );

			if ( ! empty( $payment_methods ) ) {
				if ( ! empty ( $payment_methods[$payment_method_code] ) ) {
					if ( isset( $payment_methods[ $payment_method_code ]['enabled'] ) ) {
						return (int) $payment_methods[ $payment_method_code ]['enabled'];
					}
				}
			}

			return 0;

		}

		/**
		 * @param $payment_method_code
		 *
		 * @return array|null
		 * @since 0.0.31
		 */
		public function get_payment_method_prices( $shipping_method_code, $payment_method_code ) {
			$payment_methods = $this->get_payment_methods( $shipping_method_code );

			if ( isset( $payment_methods[ $payment_method_code ] ) && isset( $payment_methods[ $payment_method_code ]["prices"] ) ) {
				return $payment_methods[ $payment_method_code ]["prices"];
			}

			return null;
		}

		/**
		 * @param $payment_method_code
		 *
		 * @return bool
		 */
		public function is_empty_payment_method( $shipping_method_code, $payment_method_code ) {
			$prices = $this->get_payment_method_prices( $shipping_method_code, $payment_method_code );

			if ( $prices ) {
				foreach ( $prices as $price ) {
					if ( $price == "" || $price == 0 ) {
						return true;
					}
				}
			}

			return false;
		}

		/**
		 * @param $payment_method_code
		 * @param Eso_Currency $currency
		 *
		 * @return int|float
		 * @since 0.0.31
		 */
		public function get_payment_method_price( $shipping_method_code, $payment_method_code, Eso_Currency $currency ) {

			if ( ! is_scalar( $payment_method_code ) ) {
				return 0;
			}

			$prices = $this->get_payment_method_prices( $shipping_method_code, $payment_method_code );

			if ( isset( $prices[ $currency->get_code() ] ) ) {
				return (float) $prices[ $currency->get_code() ];
			}

			return 0;
		}

		/**
		 * @return bool
		 * @since 0.0.30
		 */
		public function has_insertable_shipping_methods() {
			if ( ! empty( $this->get_insertable_shipping_methods() ) ) {
				return true;
			}

			return false;
		}

		/**
		 * @return array
		 * @since 0.0.23
		 */
		public function get_insertable_shipping_methods() {
			$all_shipping_methods = eso_get_enabled_shipping_methods();
			$current_shipping_methods = $this->get_shipping_methods();

			if ( empty( $current_shipping_methods ) ) {
				return $all_shipping_methods;
			}

			return array_diff_key( $all_shipping_methods, $current_shipping_methods );
		}

		/**
		 * @param $shipping_method_code
		 *
		 * @since 0.0.23
		 */
        public function add_shipping_method( $shipping_method_code ) {
            $shipping_methods = $this->get_shipping_methods();

            if ( ! isset( $shipping_methods[ $shipping_method_code ] ) ) {
                $default_options          = new Eso_Default_Options();
                $default_shipping_methods = $default_options->shipping_methods();
                $custom_shipping_method = eso_get_custom_shipping_method( $shipping_method_code );

                if ( isset( $default_shipping_methods[ $shipping_method_code ] ) ) {
                    $shipping_methods[ $shipping_method_code ] = $default_shipping_methods[ $shipping_method_code ];
                } else if( isset($custom_shipping_method) ) {
                    $shipping_methods[ $shipping_method_code ] = $custom_shipping_method;
                } else {
                    $comgate_elogist          = new Eso_Comgate_Elogist();
                    $comgate_shipping_methods = $comgate_elogist->get_shipping_methods();

                    if ( isset( $comgate_shipping_methods[ $shipping_method_code ] ) ) {
                        $shipping_methods[ $shipping_method_code ] = $comgate_shipping_methods[ $shipping_method_code ];
                    }
                }
                $this->update_shipping_methods( $shipping_methods );

            }
        }

		/**
		 * @since 2019.7
		 *
		 * @param $shipping_method_code
		 *
		 * @return bool
		 */
		public function has_shipping_method( $shipping_method_code ) {
			$shipping_methods = $this->get_shipping_methods();

			if ( ! empty( $shipping_methods ) ) {
				if ( ! empty ( $shipping_methods->{$shipping_method_code} ) ) {
					if ( isset( $shipping_methods[ $shipping_method_code ]['enabled'] ) ) {
						if ( $shipping_methods[ $shipping_method_code ]['enabled'] == 1 ) {
							return true;
						}
					}
				}
			}

			return false;
		}

		/**
		 * @since 2019.7
		 *
		 * @return string
		 */
		public function get_first_enabled_shipping_method() {
			$methods = $this->get_shipping_methods();

			foreach ( $methods as $shipping_method_code => $shipping_method_data ) {
				return $shipping_method_code;
			}

			return "czech_post";
		}

		/**
		 * @param $shipping_method_code
		 *
		 * @since 0.0.30
		 */
		public function remove_shipping_method( $shipping_method_code ) {

			$shipping_methods = $this->get_shipping_methods();

			if ( isset( $shipping_methods[ $shipping_method_code ] ) ) {
				unset( $shipping_methods[ $shipping_method_code ] );
			};

			$this->update_shipping_methods( $shipping_methods );
		}

		/**
		 * @param $payment_methods
		 */
		public function update_payment_methods( $shipping_method_code, $payment_methods ) {
			global $wpdb;

			$shipping_methods = $this->get_shipping_methods();

			$shipping_methods[$shipping_method_code]["payment_methods"] = $payment_methods;

			$data = [
				"shipping_methods" => serialize( $shipping_methods )
			];

			$where = [
				"code" => $this->get_code()
			];

			$wpdb->update( ESO_SHIPPING_ZONE_TABLE, $data, $where );
		}

		/**
		 * @return bool
		 */
		public function is_enabled() {
			if ( isset( $this->enabled ) ) {
				return $this->enabled;
			}

			return false;
		}

		public function set_enabled( $enabled = true ) {
			global $wpdb;

			$data = [
				"enabled" => $enabled
			];

			$where = [
				"code" => $this->get_code()
			];

			$wpdb->update( ESO_SHIPPING_ZONE_TABLE, $data, $where );
		}

		public function remove() {
			global $wpdb;

			$where = [
				"code" => $this->get_code()
			];

			$wpdb->delete( ESO_SHIPPING_ZONE_TABLE, $where );
		}

		/**
		 * @since 0.0.26
		 */
		public function render_edit_item() {
			require( ESO_DIR . "/admin/templates/shipping-zone-edit-item.php" );
		}

		/**
		 * @since 0.0.30
		 */
		public function render_add_methods_options() {
			require( ESO_DIR . "/admin/templates/shipping-zone-add-methods-options.php" );
		}
	}
}