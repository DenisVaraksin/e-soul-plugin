<?php

if ( ! class_exists( 'Eso_Newsletter_Subscriber', false ) ) {
	/**
	 * Class Eso_Newsletter_Subscriber
	 * @since 2020.1.2
	 */
	class Eso_Newsletter_Subscriber {

		private $email;

		public function __construct( $email ) {
			$this->email = $email;
		}

		/**
		 * @return string
		 */
		public function get_email() {
			return sanitize_email($this->email);
		}

		public function is_verified() {
			if($this->get_verified() == 1) {
				return true;
			}

			return false;
		}

		public function get_verified() {
			global $wpdb;

			$result = $wpdb->get_row("SELECT email, verified FROM " . ESO_NEWSLETTER_TABLE . " WHERE email = '" . $this->get_email() . "'");

			if($result) {
				return $result->verified;
			}

			return false;
		}

		/**
		 * @return bool|string
		 */
		public function get_registered() {
			global $wpdb;

			$result = $wpdb->get_row("SELECT email, registered FROM " . ESO_NEWSLETTER_TABLE . " WHERE email = '" . $this->get_email() . "'");

			if($result) {
				return $result->registered;
			}

			return false;
		}

		/**
		 * @return bool|string
		 */
		public function get_token() {
			global $wpdb;

			$result = $wpdb->get_row("SELECT email, hash FROM " . ESO_NEWSLETTER_TABLE . " WHERE email = '" . $this->get_email() . "'");

			if($result) {
				return $result->hash;
			}

			return false;
		}

		public function get_verify_url() {
			return home_url() . "?verifyNewsletter=" . $this->get_token();
		}

		public function send_verification_email() {
			$email = new Eso_Email($this->get_email(), "newsletter-verify", $this->get_email());
			$email->send();
		}
	}
}