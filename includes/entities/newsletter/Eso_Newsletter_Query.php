<?php
if ( ! class_exists( 'Eso_Newsletter_Query', false ) ) {
	/**
	 * Class Eso_Newsletter_Query
	 * @since 2020.1.2
	 */
	class Eso_Newsletter_Query {

		public function __construct() {
		}

		public function get_all_subscribers($limit = 20) {
			global $wpdb;

			if($limit > -1) {
				$results = $wpdb->get_results("SELECT * FROM " . ESO_NEWSLETTER_TABLE . " LIMIT '" . $limit . "'");
			} else {
				$results = $wpdb->get_results("SELECT * FROM " . ESO_NEWSLETTER_TABLE);
			}

			if($results) {
				return $results;
			}

			return false;
		}
	}
}