<?php
if ( ! class_exists( 'Eso_Invoice', false ) ) {
	/**
	 * Class Eso_Invoice
	 * @see eso_generate_invoice()
	 *
	 * @since 2019.6
	 */
	class Eso_Invoice {
		private $order_id;

		public function __construct( $order_id ) {
			$this->order_id = $order_id;
		}

		/**
		 * @return int
		 */
		public function get_id() {
			return (int) $this->order_id;
		}

		/**
		 * @return Eso_Order
		 */
		public function get_order() {
			return new Eso_Order( $this->get_id() );
		}

		/**
		 * @return DateTime|null
		 */
		private function get_due_datetime() {
			$settings = new Eso_Invoice_Settings();
			$days     = $settings->get_due_days();

			if ( ! $days ) {
				return null;
			}

			$interval = "P" . $days . "D";

			$from = $this->get_order()->get_datetime_created();

			try {
				return $from->add( new DateInterval( $interval ) );
			} catch ( Exception $e ) {
				write_log( $e );
			}

			return null;
		}

		/**
		 * @return string
		 */
		private function get_year() {
			return $this->get_order()->get_datetime_created()->format( "y" );
		}

		/**
		 * @return string
		 */
		public function get_number() {
			$settings = new Eso_Invoice_Settings();
			$limit    = $settings->get_number_length();

			return sprintf( "%0{$limit}d", $this->get_id() );
		}

		/**
		 * @return string
		 */
		public function get_due_date() {
			if($this->get_due_datetime()) {
				return $this->get_due_datetime()->format("j. n. Y");
			}

			return null;
		}

		/**
		 * @return string
		 */
		public function get_filename() {
			$settings = new Eso_Invoice_Settings();
			$prefix   = $settings->get_prefix();

			$number = $this->get_number();

			if ( $settings->has_prefix_year() ) {
				$year = $this->get_year();

				return $prefix . $year . $number;
			}

			return $prefix . $number;
		}

		/**
		 * @return string
		 */
		public function get_variable_symbol() {
			$settings = new Eso_Invoice_Settings();

			if($settings->has_prefix_year()) {
				return $this->get_year() . $this->get_number();
			}

			return $this->get_number();
		}

	}
}