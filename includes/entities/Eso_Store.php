<?php
if ( ! class_exists( 'Eso_Store', false ) ) {
	/**
	 * Class Eso_Store
	 *
	 * @since 2019.6
	 */
	class Eso_Store {

		public function __construct() {

		}

		/**
		 * @return string
		 */
		public function get_name() {
			return get_bloginfo( "name" );
		}

		/**
		 * @return string
		 */
		public function get_description() {
			return get_bloginfo( 'description' );
		}

		/**
		 *  @return string|null
		 */
		public function get_owner() {
			return get_option( 'eso_store_owner' );
		}

		/**
		 * @return string|null
		 */
		public function get_manager_email() {
			return get_option( "eso_store_manager_email" );
		}

		/**
		 * @return string|null
		 */
		public function get_phone() {
			return get_option( "eso_store_phone" );
		}

		/**
		 * @return int|null
		 */
		public function get_logo_id() {
			return get_option( "eso_store_logo" );
		}

		/**
		 * @param $size
		 *
		 * @return string
		 */
		public function get_logo( $size ) {
			return wp_get_attachment_image( $this->get_logo_id(), $size );
		}

		/**
		 * @param $size
		 */
		public function the_logo( $size ) {
			echo $this->get_logo( $size );
		}

		/**
		 * @return int|null
		 */
		public function get_favicon_id() {
			return get_option( "eso_store_favicon" );
		}

		/**
		 * @param $size
		 *
		 * @return string
		 */
		public function get_favicon( $size ) {
			return wp_get_attachment_image( $this->get_favicon_id(), $size );
		}

		/**
		 * @param $size
		 */
		public function the_favicon( $size ) {
			echo $this->get_favicon( $size );
		}

		/**
		 * @since 2019.7
		 *
		 * @return string
		 */
		public function get_account_number() {
			return get_option("eso_store_account_number");
		}

		/**
		 * @since 2019.7
		 *
		 * @return string
		 */
		public function get_iban() {
			return get_option("eso_store_iban");
		}

		/**
		 * @since 2019.7
		 *
		 * @return string
		 */
		public function get_bic() {
			return get_option("eso_store_bic");
		}

		/**
		 * @since 2019.8
		 *
		 * @param string $format raw|pretty
		 *
		 * @return string|null
		 */
		public function get_personal_collection_address($format = "raw") {
			$option = get_option("eso_personal_collection_address");

			if($format == "pretty"){
				return nl2br($option);
			}

			return $option;
		}

		/**
		 * @since 2019.8
		 *
		 * @param string $format
		 */
		public function the_personal_collection_address($format = "pretty") {
			echo $this->get_personal_collection_address($format);
		}

		/**
		 * @since 2019.8
		 */
		public function render_personal_collection_address() {
			if(!$this->get_personal_collection_address()) return;
			?>
			<span class="personal-collection-address">
				<?php $this->the_personal_collection_address() ?>
			</span>
		<?php }
	}
}
