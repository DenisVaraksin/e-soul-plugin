<?php
function eso_post_types_and_taxonomies()
{
	$args = array(
		'hierarchical'      => true,
		'labels'            => array(
			'name'              => _x( 'Kategorie', 'eso' ),
			'singular_name'     => _x( 'Kategorie', 'eso' ),
			'add_new_item'      => __( 'Přidat novou kategorii', 'eso' ),
			'menu_name'         => __( 'Kategorie', 'eso' ),
		),
		'show_ui'           => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'product_category' ),
	);
	register_taxonomy( 'product_category', array( 'esoul_product' ), $args );

	$args = array(
		'hierarchical'      => false,
		'labels'            => array(
			'name'              => _x( 'Tagy', 'eso' ),
			'singular_name'     => _x( 'Tag', 'eso' ),
			'add_new_item'      => __( 'Přidat nový tag', 'eso' ),
			'menu_name'         => __( 'Tagy', 'eso' ),
		),
		'show_ui'           => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'product_tag' ),
	);
	register_taxonomy( 'product_tag', array( 'esoul_product' ), $args );

	$args = array(
		'hierarchical'      => true,
		'labels'            => array(
			'name'              => _x( 'Stav produktu', 'eso' ),
			'singular_name'     => _x( 'Stav produktu', 'eso' ),
			'add_new_item'      => __( 'Přidat nový stav', 'eso' ),
			'menu_name'         => __( 'Stavy produktů', 'eso' ),
		),
		'show_ui'           => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'stock_status' ),
		'meta_box_cb'       => false
	);
	register_taxonomy( 'stock_status', array( 'esoul_product' ), $args );

	$args = array(
		'hierarchical'      => true,
		'labels'            => array(
			'name'              => _x( 'Stavy objednávek', 'eso' ),
			'singular_name'     => _x( 'Stav objednávky', 'eso' ),
			'add_new_item'      => __( 'Přidat nový stav', 'eso' ),
			'menu_name'         => __( 'Stavy objednávek', 'eso' ),
		),
		'show_ui'           => true,
		'show_in_rest'      => true,
		'show_admin_column' => true,
		'show_in_menu'      => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'order_status' ),
	);
	register_taxonomy( 'order_status', array( 'esoul_order' ), $args );

	register_post_type('esoul_product',
		array(
			'labels' => array(
				'name' => __('Seznam produktů', 'eso'),
				'singular_name' => __('Produkt', 'eso'),
				'add_new' => __('Nový produkt', 'eso'),
				'add_new_item' => __('Nový produkt', 'eso'),
			),
			'public' => true,
			'has_archive' => true,
			'show_in_rest' => true,
			'hierarchical' => false,
			'can_export' => true,
			'show_ui' => true,
			'register_meta_box_cb' => 'eso_product_entity',
			'rewrite' => ['slug' => "produkty"],
			'supports' => [
				'title',
				'revisions',
				'editor'
			]
		)
	);

	register_post_type('esoul_order',
		array(
			'labels' => array(
				'name' => __('Objednávky', 'eso'),
				'singular_name' => __('Objednávka', 'eso'),
			),
			'public' => true,
			'show_ui' => true,
			'map_meta_cap' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_in_menu' => true,
			'hierarchical' => false,
			'show_in_nav_menus' => false,
			'query_var' => false,
			'has_archive' => false,
			'show_in_rest' => false,
			'can_export' => true,
			'rewrite' => ['slug' => __("objednavka", "eso")],
			'supports' => [
				'title',
				'revisions'
			]
		)
	);

	register_post_type('esoul_storageorder',
		array(
			'public' => false,
			'has_archive' => true,
			'show_in_rest' => false,
			'hierarchical' => false,
			'can_export' => true,
		)
	);

}

add_action('init', 'eso_post_types_and_taxonomies');

/**
 * @since 2019.7
 */
add_filter( 'init', function() {
	if ( isset( $_GET["generateInvoice"] ) && isset( $_GET["key"] ) && function_exists( 'eso_generate_invoice' ) ) {
		eso_generate_invoice( (int) $_GET["generateInvoice"], sanitize_text_field( $_GET["key"] ) );
	} else if(isset($_GET["generateXls"]) && function_exists('eso_generate_xls')) {
		eso_generate_xls($_GET);
	} else if(isset($_GET["verifyNewsletter"])) {
		eso_verify_subscriber_email($_GET["verifyNewsletter"]);
	} else if( isset($_GET['xmlorder']) ) {

        header('Content-type: text/xml');

        $dom = new domDocument("1.0", "utf-8"); // Создаём XML-документ версии 1.0 с кодировкой utf-8
        $dom->formatOutput=true;
        $root = $dom->createElement("orders"); // Создаём корневой элемент
        $dom->appendChild($root);

        //Calling Products
        $args = [
            "post_type"   => "esoul_order",
            "post_status" => "publish",
            "posts_per_page" => -1
        ];

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ):
            while ( $the_query->have_posts() ) : $the_query->the_post();

                $order = new Eso_Order( get_the_ID() );
                $customer = $order->get_customer();

                //main product tag
                $item = $dom->createElement("order"); // Creating element "item"
                $root->appendChild($item);
                //language
                $lang = 'cs';
                $lang_elem = $dom->createElement("lang", $lang); // Creating element "lang" with Lanugage value inside
                $item->appendChild($lang_elem);
                //order ID
                $id = $order -> get_id();
                $id_elem = $dom->createElement("code", $id); // Creating element "id" with ID value inside
                $item->appendChild($id_elem);
                //time
                $time = $order->get_date_and_time_created();
                $time_elem = $dom->createElement("time", $time); // Creating element "time"
                $item->appendChild($time_elem);
                //price
                $price = $order->get_total();
                $price_elem = $dom->createElement("price_total", $price); // Creating element "price"
                $item->appendChild($price_elem);
                //status text
                $status = $order->get_status_name();
                $status_elem = $dom->createElement("status_text", $status); // Creating element "status"
                $item->appendChild($status_elem);
                //email
                $email = $order->get_shipping_email();
                $email_elem = $dom->createElement("email", $email); // Creating element "email"
                $item->appendChild($email_elem);
                //phone
                $phone =  $customer->get_phone();
                $phone_elem = $dom->createElement("phone", $phone); // Creating element "phone"
                $item->appendChild($phone_elem);
                //currency
                $currency =  $order->get_currency_code();
                $currency_elem = $dom->createElement("currency_iso", $currency); // Creating element "currency"
                $item->appendChild($currency_elem);
                //delivery_name
                $delivery_name =  $order->get_shipping_method()->get_name();
                $delivery_name_elem = $dom->createElement("delivery_name", $delivery_name); // Creating element "delivery_name"
                $item->appendChild($delivery_name_elem);
                //delivery_cost
                $delivery_cost =  $order->get_shipping_payment_price();
                $delivery_cost_elem = $dom->createElement("delivery_cost", $delivery_cost); // Creating element "delivery_cost"
                $item->appendChild($delivery_cost_elem);
                //payment_name
                $payment_name =  $order->get_payment_method()->get_name();
                $payment_name_elem = $dom->createElement("payment_name", $payment_name); // Creating element "payment_name"
                $item->appendChild($payment_name_elem);
                //payment_cost
                $payment_cost =  $order->get_payment_method_price();
                $payment_cost_elem = $dom->createElement("payment_cost", $payment_cost); // Creating element "payment_cost"
                $item->appendChild($payment_cost_elem);
                //username
                $username =  $customer->get_full_name();
                $username_elem = $dom->createElement("username", $username); // Creating element "username"
                $item->appendChild($username_elem);

                //Invoice Address
                $invoice_address = $dom->createElement("invoice_address");
                $item->appendChild($invoice_address);
                //person
                $person =  $customer->get_full_name();
                $person_elem = $dom->createElement("person", $person); // Creating element "person"
                $invoice_address->appendChild($person_elem);
                //street
                $street =  $order->get_billing_street();
                $street_elem = $dom->createElement("street", $street);
                $invoice_address->appendChild($street_elem);
                //city
                $city =  $order->get_billing_city();
                $city_elem = $dom->createElement("city", $city);
                $invoice_address->appendChild($city_elem);
                //psc
                $psc =  $order->get_billing_postcode();
                $psc_elem = $dom->createElement("psc", $psc);
                $invoice_address->appendChild($psc_elem);
                //company
                $company_name =  $order->get_billing_company_name();
                $company_name_elem = $dom->createElement("company", $company_name);
                $invoice_address->appendChild($company_name_elem);
                //ico
                $ico =  $order->get_billing_ico();
                $ico_elem = $dom->createElement("ico", $ico);
                $invoice_address->appendChild($ico_elem);
                //dic
                $dic =  $order->get_billing_dic();
                $dic_elem = $dom->createElement("psc", $dic);
                $invoice_address->appendChild($dic_elem);

                //Delivery Address
                $delivery_address = $dom->createElement("delivery_address");
                $item->appendChild($delivery_address);
                //person
                $person =  $order->get_shipping_name();
                $person_elem = $dom->createElement("person", $person); // Creating element "person"
                $delivery_address->appendChild($person_elem);
                //street
                $street =  $order->get_shipping_street();
                $street_elem = $dom->createElement("street", $street);
                $delivery_address->appendChild($street_elem);
                //city
                $city =  $order->get_shipping_city();
                $city_elem = $dom->createElement("city", $city);
                $delivery_address->appendChild($city_elem);
                //psc
                $psc =  $order->get_shipping_postcode();
                $psc_elem = $dom->createElement("psc", $psc);
                $delivery_address->appendChild($psc_elem);
                //coutry
                $country =  $order->get_shipping_country();
                $country_elem = $dom->createElement("coutry", $country);
                $delivery_address->appendChild($country_elem);

                //Items
                $items = $dom->createElement("items");
                $item->appendChild($items);

                $j=0;
                foreach ( $order->get_items() as $product_id => $product_daata ) :
                    $j = $j + 1;
                       $order_item = new Eso_Order_Item( $order->get_id(), $product_id );
                        if ( ! isset( $order_currency ) ) {
                           $order_currency = new Eso_Currency( $order_item->get_currency() );
                      }
                    //item
                    $item_elem = $dom->createElement("item");
                    $items->appendChild($item_elem);
                    //price
                    $price =  $order_item->get_price_per_piece( $order_currency );
                    $price_elem = $dom->createElement("price", $price);
                    $item_elem->appendChild($price_elem);
                    //count
                    $count =  $j;
                    $count_elem = $dom->createElement("count", $j);
                    $item_elem->appendChild($count_elem);
                    //name
                    $name =  $order_item->get_name();
                    $name_elem = $dom->createElement("name", $name);
                    $item_elem->appendChild($name_elem);
                    //id
                    $code = $order_item->get_product_id();
                    $code_elem = $dom->createElement("id", $code);
                    $item_elem->appendChild($code_elem);
                    //url
                    $url = $order_item->get_product()->get_view_url();
                    $url_elem = $dom->createElement("url", $url);
                    $item_elem->appendChild($url_elem);
                endforeach;
            endwhile;
        endif;

        //print '<xmp>' . $dom->saveXML() . '</xmp>';
        print $dom->saveXML();
        die();

    } else if( isset($_GET['xml'])) {
        // Creating XML at DOM
        header('Content-type: text/xml');


        $dom = new domDocument("1.0", "utf-8"); // Создаём XML-документ версии 1.0 с кодировкой utf-8
        $dom->formatOutput=true;
        $root = $dom->createElement("channel"); // Создаём корневой элемент
        $dom->appendChild($root);

        //Calling Products
        $args = [
            "post_type"   => "esoul_product",
            "post_status" => "publish",
            "posts_per_page" => -1
        ];

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ):
            while ( $the_query->have_posts() ) : $the_query->the_post();

                $product = new Eso_Product( get_the_ID() );
                //main product tag
                $item = $dom->createElement("item"); // Creating element "item"
                $root->appendChild($item);
                //product ID
                $id = $product -> get_id();
                $id_elem = $dom->createElement("id", $id); // Creating element "id" with ID value inside
                $item->appendChild($id_elem);
                //product Title
                $title = $product->get_name();
                $title_elem = $dom->createElement("title", $title); // Creating element "title" with Title value inside
                $item->appendChild($title_elem);
                //product EAN code
                $ean_code = $product->get_ean_code();
                $ean_code_elem = $dom->createElement("ean_code", $ean_code); // Creating element "title" with Title value inside
                $item->appendChild($ean_code_elem);
                //Product Desctiption
                $description = $product->get_content();
                $description_elem = $dom->createElement("description", $description);
                $item->appendChild($description_elem);
                //Product Link
                $link = $product->get_view_url();
                $link_elem = $dom->createElement("link", $link);
                $item->appendChild($link_elem);
                //Image Link
                //$image_link = $product->get_featured_image();
                //$image_link_elem = $dom->createElement("image_link", $image_link);
                //$item->appendChild($image_link_elem);
                //availability
                $availability = $product->get_stock_status_name();
                $availability_elem = $dom->createElement("availability", $availability);
                $item->appendChild($availability_elem);
                //price
                $price = $product->get_price(eso_get_active_currency());
                $price_elem = $dom->createElement("price", $price);
                $item->appendChild($price_elem);

            endwhile;
        endif;

        //print '<xmp>' . $dom->saveXML() . '</xmp>';
        print $dom->saveXML();
        die();
    }
} );

