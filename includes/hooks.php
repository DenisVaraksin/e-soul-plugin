<?php

/**
 * @since 2020.1.20
 */
add_action('eso_body_start_hook', function() {
	if ( function_exists( "eso_google_analytics" ) ) {
		eso_google_analytics();
	}

	//GTM module
	$module = new Eso_Module( "gtm" );
	$measurement_code = $module->get_data_value( "service_id" );
	if ( $measurement_code && $module->is_enabled() ) {
		echo '
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
		new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
		\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,\'script\',\'dataLayer\',\'GTM-' . $measurement_code . '\');</script>
		<!-- End Google Tag Manager -->';
	}
});

/**
 * @since 2020.1.20
 */
add_action('eso_body_end_hook', function() {

	//GTM module
	$module = new Eso_Module( "gtm" );
	$measurement_code = $module->get_data_value( "service_id" );
	if ( $measurement_code && $module->is_enabled() ) {
		echo '
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-' . $measurement_code . '"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->';
	}
});