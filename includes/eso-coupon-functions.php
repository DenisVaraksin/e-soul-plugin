<?php

/**
 * @since 2019.7
 *
 * @return array
 */
function eso_query_all_coupons() {
	$coupons = [];

	global $wpdb;

	$rows = $wpdb->get_results( "SELECT id FROM " . ESO_COUPON_TABLE . " ORDER BY name" );

	foreach ( $rows as $row ) {
		$coupons[ $row->id ] = new Eso_Coupon( $row->id );
	}

	return $coupons;
}

/**
 * @since 2019.7
 *
 * @param $data array of key value pairs to be inserted as a DB row
 */
function eso_insert_coupon( $data ) {
	global $wpdb;

	$wpdb->insert( ESO_COUPON_TABLE, $data );
}

/**
 * @since 2019.7
 *
 * @param $code
 *
 * @return Eso_Coupon|null
 */
function eso_get_coupon_by_code( $code ) {
	global $wpdb;
	$result = $wpdb->get_row( "SELECT * FROM " . ESO_COUPON_TABLE . " WHERE code = '" . $code . "'" );

	if ( $result ) {
		$coupon = new Eso_Coupon( $result->id );

		return $coupon;
	}

	return null;
}

/**
 * @since 2019.7
 *
 * @param $code
 */
function eso_apply_coupon( $code ) {
	$code = sanitize_text_field( $code );

	if ( empty( $code ) ) {
		return null;
	}

	$coupon = eso_get_coupon_by_code( $code );

	if ( $coupon ) {
		if ( $coupon->is_for_registered_only() && ! is_user_logged_in() ) {
			return "<span class='text-danger'>" . __( "Tento slevový kód je pouze pro registrované zákazníky.", "eso" ) . "</span>";
		}

		if(!$coupon->is_active()) {
			return "<span class='text-danger'>" . __( "Tento kód je neplatný.", "eso" ) . "</span>";
		}

		return "<span class='text-success'>" . __( "Uplatnili jsme slevový kód ", "eso" ) . " " . $code . "</span>";
	}

	return "<span class='text-danger'>" . __( "Tento slevový kód neexistuje.", "eso" ) . "</span>";
}