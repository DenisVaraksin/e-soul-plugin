<?php

/**
 * @since 2020.2.17
 *
 * @param $path string after /templates/ without .php extension
 *
 * @return false|string
 */
function eso_get_template_url($path) {
	//Child template first
	if ( file_exists( get_template_directory() . "/templates/" . $path . ".php" ) ) {
		return get_template_directory() . "/templates/" . $path . ".php";
	}
	//Parent template second
	else if ( file_exists( get_template_directory() . "/templates/" . $path . ".php" ) ) {
		return get_template_directory() . "/templates/" . $path . ".php";
	}
	//Lastly
	else if ( file_exists( ESO_DIR . '/templates/' . $path . '.php' ) ) {
		return ESO_DIR . '/templates/' . $path .  '.php';
	}

	return false;
}

/**
 * @since 2020.2.17
 *
 * @param $path string after /templates/ without .php extension
 *
 * @return false|string
 */
function eso_get_template($path) {
	$template_url = eso_get_template_url($path);
	ob_start();
	/** @noinspection PhpIncludeInspection */
	require( $template_url );
	$content = ob_get_contents();
	ob_get_clean();

	return $content;
}