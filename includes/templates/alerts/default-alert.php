<?php
/**
 * @since 2019.8
 *
 * @var string $message
 * @var string $type
 * @var string $position
 * @var bool $dismissible
 */
?>
<div class="eso-alert eso-alert--<?php echo $position ?>">
    <div class="alert alert-<?php echo $type ?> <?php if($dismissible) : ?>alert-dismissible <?php endif; ?>fade show" role="alert">
        <?php echo $message ?>
		<?php if($dismissible) : ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
		<?php endif; ?>
    </div>
</div>
