<?php
if ( ! class_exists( 'Eso_Fields', false ) ) {
	/**
	 * @since 2019.6
	 */
	class Eso_Fields {
		public function __construct() {

		}

		/**
		 * @since 2019.7 allows null label
         * @since 2019.12 allow custom ID
		 *
		 * @param $name
		 * @param $label
		 * @param $current_value
		 * @param null $default_value
		 * @param string $type Should be text|number|email|tel|date|month|password|range|search|time|url|week
		 * @param array $attributes
		 */
		public function form_group_input( $name, $label, $current_value = null, $default_value = null, $type = "text", $attributes = [] ) {
			?>
            <div class="form-group form-group-<?php echo $name ?> form-group--<?php echo $type ?>">
				<?php if ( ! empty( $label ) ) : ?>
                    <label for="<?php echo $name ?>"
                           class="eso-box__label"><?php _e( $label, "eso" ) ?></label>
				<?php endif; ?>
                <input type="<?php echo $type ?>"
                       id="<?php if(!empty($attributes["id"])) echo $attributes["id"]; else echo $name; ?>"
                       name="<?php echo $name ?>"
                       value="<?php if ( $current_value ) {
					       echo $current_value;
				       } else {
					       echo $default_value;
				       } ?>"
					<?php $this->render_attributes( $attributes ) ?>
                />
            </div>
		<?php }

		/**
		 * @since 2019.8
		 *
		 * @param $name
		 * @param $label
		 * @param $current_value
		 * @param null $default_value
		 * @param array $attributes
		 */
		public function form_group_textarea( $name, $label, $current_value = null, $default_value = null, $attributes = [] ) {
			?>
            <div class="form-group form-group-<?php echo $name ?> form-group--textarea">
				<?php if ( ! empty( $label ) ) : ?>
                    <label for="<?php echo $name ?>"
                           class="eso-box__label"><?php _e( $label, "eso" ) ?></label>
				<?php endif; ?>
                <textarea id="<?php echo $name ?>"
                          name="<?php echo $name ?>"
                    <?php $this->render_attributes( $attributes ) ?>
                ><?php if ( $current_value ) {
		                echo $current_value;
	                } else {
		                echo $default_value;
	                } ?></textarea>
            </div>
		<?php }

		/**
		 * @since 2019.7
		 *
		 * @param $name
		 * @param $label
		 * @param array $options
		 * @param $current_value
		 * @param $required
		 * @param array $attributes
		 */
		public function form_group_select( $name, $label, $options = [], $current_value = null, $required = true, $attributes = [] ) {
			?>
            <div class="form-group form-group-<?php echo $name ?> form-group--select">
				<?php if ( ! empty( $label ) ) : ?>
                    <label for="<?php echo $name ?>"
                           class="eso-box__label"><?php _e( $label, "eso" ) ?></label>
				<?php endif; ?>
                <select id="<?php echo $name ?>"
                        name="<?php echo $name ?>" <?php $this->render_attributes( $attributes ) ?>>
					<?php if ( !$required ) : ?>
                        <option value=""><?php if ( ! is_bool( $required ) )
								echo $required ?></option>
					<?php endif; ?>
					<?php foreach ( $options as $value => $name ) : ?>
                        <option value="<?php echo $value ?>" <?php if($current_value == $value) echo "selected"; ?>><?php echo $name ?></option>
					<?php endforeach; ?>
                </select>

            </div>
		<?php }

		/**
		 * @param $name
		 * @param $label
		 * @param bool $show_inactive
		 * @param null $current_value
		 * @param array $attributes
		 */
		public function render_country_select( $name, $label, $show_inactive = true, $current_value = null, $attributes = [] ) { ?>
            <select name="<?php echo $name ?>"
                    title="<?php echo $label ?>"
				<?php $this->render_attributes( $attributes ) ?>
            >
				<?php
				if ( $show_inactive ) {
					$zones = eso_get_all_shipping_zones();
				} else {
					$zones = eso_get_enabled_shipping_zones();
				}
				foreach ( $zones as $shipping_zone_code => $shipping_zone_name ) {
					?>
                    <option value="<?php echo $shipping_zone_code ?>" <?php if ( ! empty( $current_value ) ) {
						selected( $current_value, $shipping_zone_code );
					} ?>
                    ><?php echo $shipping_zone_name ?></option>
				<?php }
				?>
            </select>
		<?php }

		/**
		 * @param $name
		 * @param $current_value
		 * @param $label
		 * @param int $value
		 */
		public function checkbox( $name, $label, $current_value, $value = 1, $attributes = [] ) { ?>
            <label class="container-check form-check-label" for="<?php echo $name ?>"><?php _e( $label, "eso" ) ?>
                <input type="checkbox"
                       id="<?php echo $name ?>"
                       value="<?php echo $value ?>"
                       name="<?php echo $name ?>"
					<?php checked( $value, $current_value );
					$this->render_attributes( $attributes ) ?>
                />
                <span class="checkmark"></span>
            </label>
		<?php }

		/**
		 * @param $attributes
		 */
		public function render_attributes( $attributes ) {
			if ( ! empty( $attributes ) ) {
				foreach ( $attributes as $attribute => $value ) {
					if ( $attribute == "class" ) {
						echo $attribute . "='form-control " . $value . "'";
					}
					echo $attribute . "='" . $value . "'";
				}
			}
			if ( ( ! empty( $attributes ) && ! isset( $attributes["class"] ) ) || empty( $attributes ) ) {
				echo "class='form-control'";
			}

		}

		/**
		 * @since 2019.7
		 */
		public function render_bank_transfer_prompt( Eso_Order $order ) {
			$store = new Eso_Store;
			?>
            <div class="col-sm-12 mb-3">
                <h4 class="font-weight-bold"><?php _e( "Platbu prosím proveďte na náš bankovní účet: ", "eso" ) ?></h4>
            </div>
            <div class="col-sm">
                <div><?php _e( "Číslo účtu", "eso" ) ?>: <strong><?php echo $store->get_account_number(); ?></strong>
                </div>
                <div><?php _e( "Variabilní symbol", "eso" ) ?>:
                    <strong><?php echo $order->get_invoice()->get_variable_symbol() ?></strong></div>
            </div>

			<?php if ( ! empty( $store->get_iban() ) && ! empty( $store->get_bic() ) ) : ?>
                <div class="col-sm">
                    <div><?php _e( "Pro platby ze zahraničí:", "eso" ) ?></div>
                    <div><?php _e( "IBAN", "eso" ) ?>: <strong><?php echo $store->get_iban(); ?></strong></div>
                    <div><?php _e( "BIC / SWIFT", "eso" ) ?>: <strong><?php echo $store->get_bic(); ?></strong></div>
                </div>
			<?php endif; ?>

		<?php }
	}
}