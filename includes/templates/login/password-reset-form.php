<div id="password-reset-form" class="widecolumn">
	<?php if ( $attributes['show_title'] ) : ?>
		<h3><?php _e( 'Nastavte si nové heslo', 'eso' ); ?></h3>
	<?php endif; ?>

	<form name="resetpassform" id="resetpassform" action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>" method="post" autocomplete="off">
		<input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $attributes['login'] ); ?>" autocomplete="off" />
		<input type="hidden" name="rp_key" value="<?php echo esc_attr( $attributes['key'] ); ?>" />

		<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
			<?php foreach ( $attributes['errors'] as $error ) : ?>
				<p>
					<?php echo $error; ?>
				</p>
			<?php endforeach; ?>
		<?php endif; ?>

		<div class="form-group">
			<label for="pass1"><?php _e( 'Heslo', 'eso' ) ?></label>
			<input type="password" name="pass1" id="pass1" class="input form-control" size="20" value="" autocomplete="off"/>
		</div>
		<div class="form-group">
			<label for="pass2"><?php _e( 'Kontrola hesla', 'eso' ) ?></label>
			<input type="password" name="pass2" id="pass2" class="input form-control" size="20" value="" autocomplete="off" />
		</div>

		<p class="description"><?php echo wp_get_password_hint(); ?></p>

		<div class="resetpass-submit mt-4">
			<input type="submit" name="submit" id="resetpass-button" class="button btn btn-primary btn-lg" value="<?php _e( 'Nastavit heslo', 'eso' ); ?>" />
		</div>
	</form>
</div>