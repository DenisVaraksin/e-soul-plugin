<div id="register-form" class="widecolumn">
    <h2><?php _e( 'Nový účet', 'eso' ); ?></h2>
	<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
		<?php foreach ( $attributes['errors'] as $error ) : ?>
			<p>
				<?php echo $error; ?>
			</p>
		<?php endforeach; ?>
	<?php endif; ?>

	<form id="signupform" action="<?php echo wp_registration_url(); ?>" method="post">
		<div class="form-group">
			<label for="email"><?php _e( 'Email', 'eso' ); ?></label>
			<input type="text" name="email" id="email" class="form-control" required="required">
            <label><small><?php _e( 'Poznámka: Na Váš email budou odeslány instrukce pro přihlášení.', 'eso' ); ?></small></label>
		</div>

        <div class="form-group mt-4">
            <input type="submit" name="submit" class="button button-primary btn btn-lg btn-primary" value="<?php _e("Založit účet", "eso") ?>" />
        </div>

        <div class="text-center pt-4">
            <div id="eso-register-form-status"></div>
        </div>

	</form>
</div>