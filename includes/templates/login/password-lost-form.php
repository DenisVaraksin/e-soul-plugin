<div id="password-lost-form" class="widecolumn">
	<p>
		<?php
		_e(
			"Zadejte svou emailovou adresu a my Vám zašleme odkaz na nastavení nového hesla.",
			'eso'
		);
		?>
	</p>

	<?php if ( isset($attributes) && count( $attributes['errors'] ) > 0 ) : ?>
		<?php foreach ( $attributes['errors'] as $error ) : ?>
			<p>
				<?php echo $error; ?>
			</p>
		<?php endforeach; ?>
	<?php endif; ?>

	<form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
		<div class="form-group">
            <label for="user_login"><?php _e( 'Email', 'eso' ); ?></label>
            <input type="text" name="user_login" id="user_login" class="form-control">
		</div>

		<div class="lostpassword-submit mt-5">
			<input type="submit" name="submit" class="lostpassword-button btn btn-primary btn-lg"
			       value="<?php _e( 'Obnovit heslo', 'eso' ); ?>"/>
		</div>
	</form>
</div>