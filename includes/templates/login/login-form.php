<div class="login-form-container">
	<?php if ( $attributes['show_title'] ) : ?>
		<h2><?php _e( 'Přihlásit se', 'eso' ); ?></h2>
	<?php endif; ?>
    <div class="login-form-container">
        <!-- Show errors if there are any -->
	    <?php if ( isset($attributes['errors']) && count( $attributes['errors'] ) > 0 ) : ?>
		    <?php foreach ( $attributes['errors'] as $error ) : ?>
                <p class="alert alert-danger" role="alert">
				    <?php echo $error; ?>
                </p>
		    <?php endforeach; ?>
	    <?php endif; ?>
        <!-- Show logged out message if user just logged out -->
	    <?php if ( $attributes['logged_out'] ) : ?>
            <div class="alert alert-primary" role="alert">
			    <?php _e( 'Byli jste úspěšně odhlášeni. Chcete se znovu přihlásit?', 'eso' ); ?>
            </div>
	    <?php endif; ?>

	    <?php if ( $attributes['registered'] ) : ?>
            <div class="alert alert-primary" role="alert">
			    <?php _e( 'Děkujeme za registraci do našeho e-shopu. Na Váš email jsme odeslali instrukce pro přihlášení. ', 'eso' ); ?>
            </div>
	    <?php endif; ?>

	    <?php if ( $attributes['lost_password_sent'] ) : ?>
            <div class="alert alert-primary" role="alert">
			    <?php _e( 'Na Váš email jsme odeslali email s informacemi o nastavení nového hesla.', 'eso' ); ?>
            </div>
	    <?php endif; ?>

	    <?php if ( $attributes['password_updated'] ) : ?>
            <div class="alert alert-success" role="alert">
			    <?php _e( 'Vaše heslo bylo změněno. Nyní se můžete přihlásit.', 'eso' ); ?>
            </div>
	    <?php endif; ?>

        <form method="post" action="<?php echo wp_login_url(); ?>">
            <div class="login-username form-group">
                <label for="user_login"><?php _e( 'Email', 'eso' ); ?></label>
                <input type="text" name="log" id="user_login" class="form-control">
            </div>
            <div class="login-password form-group">
                <label for="user_pass"><?php _e( 'Heslo', 'eso' ); ?></label>
                <input type="password" name="pwd" id="user_pass" class="form-control">
            </div>
            <div class="login-submit mt-4 mb-4">
                <input type="submit" class="btn btn-primary btn-lg" value="<?php _e( 'Přihlásit se', 'eso' ); ?>">
            </div>
        </form>
    </div>
	<a class="forgot-password" href="<?php echo wp_lostpassword_url(); ?>">
		<?php _e( 'Zapomenuté heslo?', 'eso' ); ?>
	</a>
</div>