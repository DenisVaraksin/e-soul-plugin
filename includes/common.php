<?php

function eso_login_css() {
	echo '<link rel="stylesheet" type="text/css" href="' . ESO_URL . '/assets/css/login.css">';
}

add_action( 'login_head', 'eso_login_css' );

/**
 * Default role for a new user will be eso_customer
 */
add_filter( 'pre_option_default_role', function () {
	return 'eso_customer';
} );

/**
 * Prints out inline-svg
 *
 * @param $file
 * @param string $wrapper_class
 *
 * @return string
 * @since 0.0.4
 * @since 2019.5 return same html element with either class or not
 */
function eso_icon( $name, $wrapper_class = "" ) {

	$file = file_get_contents( ESO_DIR . "/assets/img/icons/" . $name . ".svg" );

	if ( ! $wrapper_class ) {
		return "<div class='eso-icon icon-{$name} '>" . $file . "</div>";
	} else {
		return "<div class='" . $wrapper_class . " eso-icon-wrap eso-icon icon-{$name}'>" . $file . "</div>";
	}
}

/**
 * @since 2019.6
 *
 * @return string
 */
function eso_sort_icon() {
	$arrow = file_get_contents( ESO_DIR . "/assets/img/icons/sort.svg" );

	return "<div class='eso-icon icon-sort'><span class='sort-up'>" . $arrow . "</span><span class='sort-down'>" . $arrow . "</span></div>";
}

/**
 * @param $value
 * @param null $value2
 * @param bool $print
 *
 * @return bool|string
 */
function active( $value, $value2 = null, $print = true ) {
	if ( $value2 == null ) {
		if ( $value && $print ) {
			echo "active";

			return true;
		} else if ( $value ) {
			return "active";
		}

		return false;
	} else {
		if ( ( $value == $value2 ) && $print ) {
			echo "active";

			return true;
		} else if ( $value == $value2 ) {
			return true;
		}

		return false;
	}
}

/**
 * Allow only Administrators and Editors to access dashboard
 */
function eso_no_admin_access() {
	$redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : eso_get_page_link( 'account' );

	if ( ! current_user_can( 'edit_pages' ) && ! defined( 'DOING_AJAX' ) ) {
		exit( wp_redirect( $redirect ) );
	}
}

add_action( 'admin_init', 'eso_no_admin_access', 100 );

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 *
 * @return string
 */
function eso_login_redirect( $redirect_to, $request, $user ) {
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'administrator', $user->roles ) || in_array( 'eso_editor', $user->roles ) ) {
			$redirect_to = admin_url( "admin.php?page=eso-dashboard" );
		} else {
			$redirect_to = home_url();
		}
	}

	return $redirect_to;
}

add_filter( 'login_redirect', 'eso_login_redirect', 10, 3 );

/**
 * replace username with user_email field on registration
 * @since 0.0.21
 */
add_filter( 'sanitize_user', function ( $sanitized_user, $raw_user, $strict ) {
	if ( $raw_user != '' ) {
		return $sanitized_user;
	}

	if ( ! empty ( $_REQUEST['action'] ) && $_REQUEST['action'] === 'register' && is_email( $_POST['user_email'] ) ) {
		return $_POST['user_email'];
	}

	return $sanitized_user;
}, 10, 3 );
add_filter( 'validate_username', function ( $valid, $username ) {
	if ( $valid ) {
		return $valid;
	}

	if ( ! empty ( $_REQUEST['action'] ) && $_REQUEST['action'] === 'register' && is_email( $_POST['user_email'] ) ) {
		return true;
	}

	return is_email( $username );
}, 10, 2 );

add_action( 'admin_menu', 'eso_remove_menu_item', 99 );
function eso_remove_menu_item() {
	remove_menu_page( 'index.php' );

	if(!current_user_can('administrator')) {
		remove_menu_page( 'tools.php' );
    }
}

/**
 * @return DateTime
 * @since 2019.7
 *
 */
function eso_now() {
	try {
		return new DateTime( "now", new DateTimeZone( "Europe/Prague" ) );
	} catch ( Exception $e ) {
	    write_log($e);
	}

	return null;
}

/**
 * @return string
 * @since 2019.5
 */
function eso_db_now() {
	$now = eso_now();

	return $now->format( "Y-m-d H:i:s" );
}

/**
 * @since 2019.8
 */
function eso_alert( $message, $type = "warning", $position = "fixed", $dismissible = true ) {
	require( ESO_DIR . "/includes/templates/alerts/default-alert.php" );
}

/**
 * @since 2019.8.25
 *
 * @param $phone
 *
 * @return bool
 */
function eso_is_phone( $phone ) {
	if ( strlen( str_replace( " ", "", $phone ) ) < 13 ) {
		return false;
	} else if ( ! preg_match( '/^\+?\d+$/', $phone ) ) {
		return false;
	}

	return true;
}

/**
 * Capture user login and add it as timestamp in user meta data
 *
 */
function eso_user_last_login( $user_login, $user ) {
	update_user_meta( $user->ID, 'last_login', date( 'Y-m-d H:i:s', time() ) );
}

add_action( 'wp_login', 'eso_user_last_login', 10, 2 );

// Social site login hook for login form
function eso_social_login_form() {
	?>
    <div id="login-social-site">
        <p><?php _e( "Přihlášení přes:", "eso" ) ?></p>
    </div>
	<?php
}

add_action( 'login_form', 'eso_social_login_form' );

function eso_session_start() {
	if ( ! session_id() ) {
		session_start();
	}
}

function eso_session_end() {
	session_destroy();
}

add_action( 'init', 'eso_session_start', 1 );
add_action( 'wp_logout', 'eso_session_end' );
add_action( 'wp_login', 'eso_session_end' );

/**
 * @since 0.0.22
 */
function eso_ajax() {

	$eso_ajax = new Eso_Ajax( $_POST );

	$eso_ajax->do_action();

	wp_die();
}

add_action( 'wp_ajax_eso_ajax', 'eso_ajax' );
add_action( 'wp_ajax_nopriv_eso_ajax', 'eso_ajax' );

if ( ! function_exists( 'write_log' ) ) {
	/**
	 * @param $log
	 * @param bool $notification
	 *
	 * @since 0.0.30
	 */
	function write_log( $log, $notification = true ) {
		if ( true === WP_DEBUG ) {
			if ( is_array( $log ) || is_object( $log ) ) {
				error_log( "ESOUL_CUSTOM_LOG" );
				$log = print_r( $log, true );
				error_log( $log );
			} else {
				error_log( "ESOUL_CUSTOM_LOG: " . $log );
			}
			if ( $notification ) {
				wp_mail( "esoul-bug@milansvehla.com", "ESOUL debug.log - " . get_home_url(), "500: " . $log );
			}
		}
	}
}

if ( ! function_exists( 'test_log' ) ) {
	/**
	 * write_log wrapper for easily finding testing messages in the code
	 *
	 * @since 2019.7
	 */
	function test_log( $log ) {
		write_log( $log, false );
	}
}

if ( ! function_exists( "scaled_image_path" ) ) {
	/**
	 * @param $attachment_id
	 * @param string $size
	 *
	 * @return bool|string
	 *
	 * @since 2019.6
	 */
	function scaled_image_path( $attachment_id, $size = 'thumbnail' ) {
		$file = get_attached_file( $attachment_id, true );
		if ( empty( $size ) || $size === 'full' ) {
			// for the original size get_attached_file is fine
			return realpath( $file );
		}
		if ( ! wp_attachment_is_image( $attachment_id ) ) {
			return false; // the id is not referring to a media
		}
		$info = image_get_intermediate_size( $attachment_id, $size );
		if ( ! is_array( $info ) || ! isset( $info['file'] ) ) {
			return false; // probably a bad size argument
		}

		return realpath( str_replace( wp_basename( $file ), $info['file'], $file ) );
	}
}

if ( ! function_exists( "current_url" ) ) {
	/**
	 * @since 2019.6
	 *
	 * @return string
	 */
	function current_url() {
		if ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off' ) {
			$protocol = "https";
		} else {
			$protocol = "http";
		}

//		$protocol = strpos( strtolower( $_SERVER['SERVER_PROTOCOL'] ), 'https' ) === false ? 'http' : 'https';
		$host   = $_SERVER['HTTP_HOST'];
		$script = $_SERVER['SCRIPT_NAME'];
		$params = $_SERVER['QUERY_STRING'];

		return $protocol . '://' . $host . $script . '?' . $params;
	}
}