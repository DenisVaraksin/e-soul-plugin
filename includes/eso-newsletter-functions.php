<?php
/**
 * @since 2020.1.2
 *
 * @param $email
 *
 * @return array|bool|object|void|null
 */
function eso_get_newsletter_subscriber($email) {
	$email = sanitize_email($email);

	global $wpdb;

	$result = $wpdb->get_row( "SELECT * FROM " . ESO_NEWSLETTER_TABLE . " WHERE email = '" . $email . "'" );

	if ( $result ) {
		return new Eso_Newsletter_Subscriber($result->email);
	}

	return false;
}

/**
 * @param $email
 * @param bool $response
 *
 * @return string
 *
 * @throws Exception
 * @since 2020.1.2
 *
 */
function eso_newsletter_do_signup($email, $response = false) {
	if(!is_email($email)) {
		if($response) {
			return "<p class='text-danger'>" . __("Zkontrolujte prosím, zda jste zadali správný email", "eso") . "</p>";
		}

		return false;
	}

	$email = sanitize_email($email);

	/* @var $subscriber Eso_Newsletter_Subscriber */
	$subscriber = eso_get_newsletter_subscriber($email);

	if($subscriber && $subscriber->is_verified()) {

		if($response) {
			return "<p class='text-success'>" . __("Tato emailová adresa je již k odběru přihlášena. Děkujeme!", "eso") . "</p>";
		}

		return false;
	}

	global $wpdb;
	$data = [
		"email" => $email,
		"enabled" => 1,
		"registered" => eso_db_now(),
		"hash" => bin2hex( random_bytes( 16 ) )
	];

	$result = $wpdb->replace(ESO_NEWSLETTER_TABLE, $data);

	if($result) {
		$subscriber = new Eso_Newsletter_Subscriber($email);
		$subscriber->send_verification_email();

		if($response) {
			return "<p class='text-success'>" . __("Zkontrolujte prosím Vaší emailovou schránku, kam jsme odeslali email s potvrzením.", "eso") . "</p>";
		}

		return true;
	} else {
		if($response) {
			return "<p class='text-danger'>" . __("Něco se pokazilo. Omlouváme se za komplikace, zkuste to prosím později", "eso") . "</p>";
		}

		return false;
	}
}

/**
 * @since 2020.1.2
 * @param $hash
 */
function eso_verify_subscriber_email($hash) {
	global $wpdb;

	$exists = $wpdb->get_row("SELECT * FROM " . ESO_NEWSLETTER_TABLE . " WHERE hash = '" . $hash . "'");

	if($exists) {
		$data = [
			"verified" => 1
		];
		$where = [
			"email" => $exists->email
		];

		$result = $wpdb->update(ESO_NEWSLETTER_TABLE, $data, $where);

		if($result) {
			eso_alert(__("Emailová adresa byla úspěšně ověřena", "eso"), "success");
		} else {
			eso_alert(__("Požadavek jsme nemohli zpracovat, ale chyba je na naší straně. Zkuste to prosím později.", "eso"), "error");
		}

	} else {
		eso_alert(__("Požadavek jsme nemohli oveřit. Pravděpodobně odkaz ztratil platnost", "eso"));
	}
}