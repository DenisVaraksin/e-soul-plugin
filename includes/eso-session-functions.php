<?php
/**
 * Returns unique identifier for both logged-in and not logged-in visitor
 *
 * @since 0.0.15
 *
 * @return string
 */
function eso_session_token() {
	if ( is_user_logged_in() ) {
		if ( ! get_user_meta( get_current_user_id(), 'eso_session_token', true ) ) {
			$token = get_current_user_id() . "at" . bin2hex( random_bytes( 12 ) );
			update_user_meta( get_current_user_id(), 'eso_session_token', $token );
		}

		return get_user_meta( get_current_user_id(), 'eso_session_token', true );

	} else {
		if ( ! isset( $_SESSION["eso_session_token"] ) ) {
			$token                         = bin2hex( random_bytes( 12 ) );
			$_SESSION["eso_session_token"] = $token;
		}

		return $_SESSION["eso_session_token"];
	}
}

/**
 * Returns unique identifier by user id
 *
 * @since 2019.5
 *
 * @param $user_id
 *
 * @return mixed|null
 */
function eso_session_token_by_user_id( $user_id ) {
	if ( ! get_user_meta( $user_id, 'eso_session_token', true ) ) {
		$token = $user_id . "at" . bin2hex( random_bytes( 12 ) );
		update_user_meta( $user_id, 'eso_session_token', $token );
	}

	return get_user_meta( $user_id, 'eso_session_token', true );
}