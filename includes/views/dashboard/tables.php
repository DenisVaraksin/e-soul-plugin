<?php
/**
 * Renders frontend dashboard tables
 *
 * @since 2019.7
 *
 * @see Eso_Dashboard::render
 *
 * @var $this Eso_Dashboard
 */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm">
            <img src="<?php echo ESO_URL ?>/assets/img/logo.svg" class="frontend-dashboard__logo"/>
        </div>
        <div class="col-sm text-sm-right">
            <a href="<?php echo admin_url( "admin.php?page=eso-dashboard" ) ?>"
               class="btn btn-primary"><?php _e( "Zobrazit administraci", "eso" ) ?></a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-4 mb-4 mb-lg-0">
			<?php $this->render_graph_small() ?>
            <a style="margin-top: 34px" href="<?php echo admin_url( "admin.php?page=eso-dashboard" ) ?>"
               class="btn btn-primary button-new "><?php _e( "Všechny přehledy", "eso" ) ?>
            </a>
        </div>
        <div class="col-12 col-lg-4">
			<?php $this->render_orders(); ?>
        </div>
        <div class="col-12 col-lg-4">
		    <?php $this->render_products(); ?>
        </div>
    </div>

</div>

<!--
button styling:
    border-radius: 200px;
    background: transparent;
    text-shadow: none;
    color: #4485e1;
    position: relative;
    transition: .3s all;
    height: auto;
    padding: 6px 15px;
    border: 1px solid #4485e1;
    box-shadow: none;
-->