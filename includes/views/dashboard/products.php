<div class="dashboard-card">
    <h2><?php _e( "Nejprodávanější produkty", "eso" ) ?></h2>
	<?php
	/* @var $products WP_Query */ ?>
	<?php if ( $products->have_posts() ) : ?>
        <table class="table table-responsive table-striped text-nowrap">
            <thead>
            <tr>
                <td><?php _e( "Název", "eso" ) ?></td>
                <td><?php _e( "Stav", "eso" ) ?></td>
                <td><?php _e( "Cena", "eso" ) ?></td>
                <td><?php _e( "Tržby", "eso" ) ?></td>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach ( $products->get_posts() as $post ) :
				$product = new Eso_Product( $post->ID );
				?>
                <tr>
                    <td>
                        <a href="<?php $product->the_edit_url() ?>"><?php echo $product->get_name() ?></a>
                    </td>
                    <td class="stock-status--<?php echo $product->get_stock_status_slug() ?>"><?php echo $product->get_stock_status_name() ?></td>
                    <td><?php
                        $currency = new Eso_Currency( "CZK" );
                        echo $product->get_price( $currency, true, true ); ?></td>
                    <td>
                        <?php echo $product->get_turnover($currency, true) ?>
                    </td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
        <a href="<?php echo admin_url( "edit.php?post_type=esoul_product" ) ?>"
           class="btn btn-primary button-new"><?php _e( "Seznam produktů", "eso" ) ?></a>
	<?php else : ?>
        <p><?php _e( "Zatím žádné produkty.", "eso" ); ?></p>
        <a href="<?php echo admin_url( "post-new.php?post_type=esoul_product" ) ?>"
           class="btn btn-primary"><?php _e( "Přidat produkt", "eso" ) ?></a>
	<?php endif; ?>
</div>
