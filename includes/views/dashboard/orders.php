<div class="dashboard-card">
	<h2><?php _e( "Nejnovější objednávky", "eso" ) ?></h2>
	<?php
	/* @var $orders WP_Query */ ?>
	<?php if ( $orders->have_posts() ) : ?>
		<table class="table table-responsive table-striped text-nowrap">
			<thead>
			<tr>
				<td><?php _e( "Zákazník", "eso" ) ?></td>
				<td><?php _e( "Stav", "eso" ) ?></td>
				<td><?php _e( "Suma", "eso" ) ?></td>
				<td><?php _e( "Datum", "eso" ) ?></td>
				<td><?php _e( "Náhled", "eso" ) ?></td>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ( $orders->get_posts() as $post ) :
				$order = new Eso_Order( $post->ID );
				?>
				<tr>
					<td>
						<a href="<?php $order->get_customer()->the_edit_url() ?>"><?php echo $order->get_customer()->get_full_name(); ?></a>
					</td>
					<td class="order-status--<?php echo $order->get_status_slug() ?>"><?php echo $order->get_status_name() ?></td>
					<td><?php echo $order->get_total(true); ?></td>
					<td><?php echo $order->get_date_and_time_created(); ?></td>
					<td>
						<a href="<?php $order->the_edit_url() ?>"><?php echo eso_icon( "eye" ) ?></a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<a href="<?php echo admin_url( "admin.php?page=eso-orders" ) ?>"
		   class="btn btn-primary button-new"><?php _e( "Všechny objednávky", "eso" ) ?></a>
	<?php else : ?>
		<p><?php _e( "Zatím žádné objednávky.", "eso" ); ?></p>
	<?php endif; ?>
</div>
