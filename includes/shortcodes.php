<?php
add_shortcode( "eso_currency_switcher", "eso_render_currency_switcher" );
function eso_render_currency_switcher() {
	if ( count( eso_get_enabled_currencies() ) < 2 ) {
		return null;
	}

	$content = "<ul class='navbar-nav ml-auto mr-lg-3'>";

	/* @var $active Eso_Currency */
	$active_currency = eso_get_active_currency();

	$content .= "<li class='nav-item menu-item dropdown menu-item-has-children'>
		<a href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' class='dropdown-toggle nav-link'>" . $active_currency->get_symbol() . "</a>
		<ul class='dropdown-menu currency-switcher-list'>";

	/* @var $currency Eso_Currency */
	foreach ( eso_get_enabled_currencies( true ) as $currency ) {
		$content .= "<li class='menu-item nav-item currency-switcher-item-" . $currency->get_code() . "'>";
		$content .= "<a class='dropdown-item currency-switch currency-switch-" . $currency->get_symbol() . "' data-currency='" . $currency->get_code() . "' href='#'>" . $currency->get_symbol() . "</a>";
		$content .= "</li>";
	}

	$content .= "</ul>";
	$content .= "</li>";

	$content .= "</ul>";

	return $content;
}

add_shortcode( "eso_newest_products", "eso_shortcode_newest_products" );
function eso_shortcode_newest_products( $atts ) {
	$a = shortcode_atts( array(
		'limit' => 4,
	), $atts );

	$products = new WP_Query( [
		"posts_per_page" => intval( $a['limit'] ),
		"post_type"      => "esoul_product",
		"orderby"        => "ID",
		"order"          => "ASC"
	] );

	$content = "";

    if ( $products->have_posts() ) {
        $content .= "<div class='container'>
                     <div class='row'> ";
        $content .= " <!-- <div class=\"col-12\">
                        <h2>" . __( 'Nejnovější produkty', 'eso' ) . "</h2>
                    </div> -->";

        foreach ( $products->get_posts() as $post ) {
            $product = new Eso_Product( $post->ID );
            $content .= $product->get_list_item_template();
        }

        $content .= "</div> </div>";

    }

	return $content;
}

add_shortcode( "eso_newsletter_signup", "eso_shortcode_newsletter_signup" );
function eso_shortcode_newsletter_signup( $atts ) {
	$a = shortcode_atts( array(
		'show_terms' => 1,
		'inline' => 1
	), $atts );

	$content = "";

	$content .= "<div class='eso-newsletter-subscribe-form'>";
	$content .= "<form method='post' action='#' class='ajax-form";

	if($a['inline'] == 1) {
		$content .= " form-inline";
	}

	$content .= "'>
				<input type='hidden' name='action' value='eso_ajax' />
				<input type='hidden' name='eso_action' value='the_newsletter_signup' />
				";

	$content .= "<input type='email' class='form-control form-control-lg' name='newsletter-email' placeholder='" . __("chci@newsletter.com", "eso") . "' required />";

	$submitButton = "<button type='submit' class='btn btn-secondary btn-lg'>" . __("Přihlásit") . "</button>";

	if($a['inline'] == 1) {
		$content .= $submitButton;
	}


	if($a['show_terms'] == 1 && eso_get_page_id("privacy_policy")) {
		$content .= "
		<div class='custom-control custom-checkbox'>
			<input type='checkbox' name='newsletter-terms' id='newsletter-terms' class='custom-control-input' required/>
			<label class='newsletter-terms-label custom-control-label' for='newsletter-terms'>
				" . __("Souhlasím se &nbsp;", "eso") . "<a href='" . eso_get_page_link("privacy_policy") . "' target='_blank'>" . __("Zpracováním osobních údajů.", "eso") . "</a>" . "
			</label>
		</div>";
	}                    

	if($a['inline'] != 1) {
		$content .= $submitButton;
	}

	$content .= "<div class='ajax-result ajax-result--silent'></div>";

	$content .= "</form>";
	$content .= "</div>";


	return $content;
}