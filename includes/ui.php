<?php
function plugin_theme_styles()
{
	if (!is_admin()) {
		wp_enqueue_script('esoul-scripts', ESO_URL . 'assets/js/scripts.min.js', array('jquery'), ESO_VERSION, true);

		wp_enqueue_script('esoul-cart-common', ESO_URL . 'assets/js/cart-common.min.js', array('jquery'), ESO_VERSION, true);

		$data = array(
			'_wait_please' => __("Vyčkejte prosím...", "eso"),
			'_adding' => __("Přidávám", "eso"),
			'_error' => __("Vyskytla se chyba", "eso"),
			'site_url' => get_site_url(),
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);
		wp_localize_script( 'esoul-cart-common', 'php_vars', $data );

		wp_enqueue_script('esoul-ajax', ESO_URL . 'assets/js/ajax.min.js', array('jquery'), ESO_VERSION, true);

		wp_enqueue_style('esoul-main', ESO_URL . 'assets/css/main.css', array(), ESO_VERSION, 'all');

		if (get_post_type() == "esoul_order" && is_single() || eso_is_page_id('thankyou')) {
			wp_enqueue_script( 'esoul-single-order', ESO_URL . 'assets/js/order/single.min.js', array( 'jquery' ), ESO_VERSION, true );
		}
	}

	if(function_exists("eso_is_page_id") && eso_is_page_id('cart')) {
		wp_enqueue_script('esoul-cart', ESO_URL . 'assets/js/cart.min.js', array('jquery'), ESO_VERSION, true);
	}

	if(function_exists('eso_is_module_active') && (eso_is_module_active("comgate_logistika") || eso_is_module_active("comgate_logistika_sk"))) {
		wp_enqueue_script('esoul-comgate-elogist', ESO_URL . 'assets/js/comgate-elogist.min.js', array('jquery'), ESO_VERSION, true);
	}

	if(current_user_can('install_plugins')) {

		wp_enqueue_script('esoul-administrator', ESO_URL . 'assets/js/administrator.min.js', array('jquery'), ESO_VERSION, true);

		wp_enqueue_style('esoul-administrator', ESO_URL . 'assets/css/administrator.css', array(), ESO_VERSION, 'all');
	}

	if(function_exists("eso_is_page_id") && eso_is_page_id('account')) {
		wp_enqueue_script('esoul-account', ESO_URL . 'assets/js/account.min.js', array('jquery'), ESO_VERSION, true);
	}

}

add_action('wp_enqueue_scripts', 'plugin_theme_styles');