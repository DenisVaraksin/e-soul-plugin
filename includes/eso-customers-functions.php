<?php
add_action( 'show_user_profile', 'add_customer_meta_fields' );
add_action( 'edit_user_profile', 'add_customer_meta_fields' );
add_action( 'user_new_form', 'add_customer_meta_fields' );

add_action( 'personal_options_update', 'save_customer_meta_fields' );
add_action( 'edit_user_profile_update', 'save_customer_meta_fields' );

function add_customer_meta_fields( $user ) {

	$checkout_fields = new Eso_Checkout_Fields();

	$show_fields = $checkout_fields->get_customer_meta_fields();

	foreach ( $show_fields as $fieldset_key => $fieldset ) { ?>
        <h2><?php echo $fieldset["title"] ?></h2>
        <table class="form-table" id="<?php echo esc_attr( 'fieldset-' . $fieldset_key ); ?>">
			<?php foreach ( $fieldset["fields"] as $key => $field ) {
				$key = esc_attr( $key );
				?>
                <tr>
                    <th>
                        <label for="<?php echo $key; ?>"><?php echo esc_html( $field['label'] ); ?></label>
                    </th>
                    <td>
						<?php if ( ! empty( $field['type'] ) && 'select' === $field['type'] ) : ?>
                            <select name="<?php echo $key; ?>" id="<?php echo $key; ?>"
                                    class="<?php echo isset( $field['class'] ) ? esc_attr( $field['class'] ) : ''; ?>"
                                    style="width: 25em;">
								<?php
								if ( isset( $user->ID ) ) {
									$selected = esc_attr( get_user_meta( $user->ID, $key, true ) );
								} else {
									$selected = null;
								}
								foreach ( $field['options'] as $option_key => $option_value ) :
									?>
                                    <option value="<?php echo esc_attr( $option_key ); ?>" <?php selected( $selected, $option_key, true ); ?>><?php echo esc_attr( $option_value ); ?></option>
								<?php endforeach; ?>
                            </select>
						<?php elseif ( ! empty( $field['type'] ) && 'checkbox' === $field['type'] ) : ?>
                            <input type="checkbox" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="1"
                                   class="<?php if ( isset( $field['class'] ) ) {
								       echo esc_attr( $field['class'] );
							       } ?>" <?php checked( (int) get_user_meta( $user->ID, $key, true ), 1, true ); ?> />
						<?php else : ?>
                            <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>"
                                   value="<?php echo ( isset( $user->ID ) ) ? esc_attr( get_user_meta( $user->ID, $key, true ) ) : ''; ?>"
                                   class="<?php echo( ! empty( $field['class'] ) ? esc_attr( $field['class'] ) : 'regular-text' ); ?>"/>
						<?php endif; ?>
                    </td>
                </tr>
			<?php } ?>

        </table>
	<?php }
}

function save_customer_meta_fields( $user_id ) {
	$checkout_fields = new Eso_Checkout_Fields();

	$save_fields = $checkout_fields->get_customer_meta_fields();

	foreach ( $save_fields as $fieldset ) {

		foreach ( $fieldset['fields'] as $key => $field ) {

			if ( isset( $field['type'] ) && 'checkbox' === $field['type'] ) {
				update_user_meta( $user_id, $key, isset( $_POST[ $key ] ) );
			} elseif ( isset( $_POST[ $key ] ) ) {
				update_user_meta( $user_id, $key, sanitize_text_field( $_POST[ $key ] ) );
			}
		}
	}
}

/**
 * @param bool $hide_empty
 *
 * @return mixed    array of Eso_Customer_Group
 * @since 2019.6
 *
 */
function eso_get_customer_groups( $hide_empty = false ) {
	$option = get_option( "eso_customer_groups" );

	if ( ! is_array( $option ) ) {
		return null;
	}

	$groups = [];

	foreach ( $option as $group_id => $group_data ) {
		$group = new Eso_Customer_Group( $group_id );
		if ( $hide_empty ) {
			if ( $group->has_customers() ) {
				$groups[] = $group;
			}
		} else {
			$groups[] = $group;
		}

	}

	return $groups;
}

/**
 * @return bool
 * @since 2019.6
 *
 */
function eso_has_customer_groups() {
	$option = get_option( "eso_customer_groups" );

	if ( is_array( $option ) && count( $option ) > 0 ) {
		return true;
	}

	return false;
}

/**
 * @param $id
 * @param $name
 * @param string $slug
 *
 * @return bool
 * @since 2019.6
 *
 */
function eso_insert_customer_group( $name, $slug = null ) {
	$option = get_option( "eso_customer_groups" );

	$id = max( array_keys( $option ) ) + 1;

	$option[ $id ]["name"] = sanitize_text_field( $name );

	if ( empty( $slug ) ) {
		$option[ $id ]["slug"] = sanitize_title( $slug );
	} else {
		$option[ $id ]["slug"] = sanitize_text_field( $slug );
	}

	return update_option( "eso_customer_groups", $option );
}

add_filter( 'user_search_columns', 'eso_modify_user_search_columns', 10, 3 );

function eso_modify_user_search_columns( $search_columns, $search, $wp_user_query ) {
	$search_columns[] = 'display_name';

	return $search_columns;
}