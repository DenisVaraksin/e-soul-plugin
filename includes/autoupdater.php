<?php
require_once( ESO_DIR . '/addons/plugin-update-checker/plugin-update-checker.php' );
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/marketsoul/e-soul-plugin/',
	ESO_FILE,
	'e-soul'
);

$myUpdateChecker->setAuthentication('xKxg4MzJzyHmb4xCBA9v');

if(get_option("eso_branch")) {
	$myUpdateChecker->setBranch(get_option("eso_branch"));
}
