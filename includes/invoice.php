<?php /* @var $invoice Eso_Invoice */
$order    = $invoice->get_order();
$settings = new Eso_Invoice_Settings();
?>
<html lang="cs-CZ">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body {
            font-family: DejaVu Sans, sans-serif;
            font-size: 12px;
        }

        h1 {
            text-transform: uppercase;
            font-size: 16px;
            text-align: right;
        }

        h2 {
            font-size: 14px;
            margin-bottom: 5px;
        }

        .col {
            display: inline-block;
            vertical-align: top;
        }

        .col-50 {
            width: 400px;
        }

        .fullwidth {
            width: 700px;
        }

        .small,
        small {
            font-size: .75em;
        }

        span {
            display: block;
        }

        .strong {
            font-weight: bold;
        }

        .alert {
            font-weight: bold;
            color: red;
        }

        #company-logo {
            margin-bottom: -20px;
            max-height: 70px;
            max-width: 150px;
        }

        #top {

        }

        #parties {
            margin-bottom: -85px;
        }

        #figures {
            margin-bottom: -50px;
        }

        #figures td.label {
            padding-right: 30px;

        }

        #items table {
            border-collapse: collapse
        }

        #items td {
            border: 0;
            margin: 0;
            padding: 10px 10px 10px 0;
            border-bottom: 1px solid black;
        }

        #items tr.header td {
            border-top: 0;
            font-weight: bold;
        }

        #items tr.footer td {
            border-bottom: 1px solid transparent;
        }

        #items tr.discount td {
            padding-top: .8rem;
            padding-bottom: .2rem;
        }

        #items tr.total-without-tax td {
            padding-top: .8rem;
            padding-bottom: .2rem;
        }

        #contact {
            margin-top: 30px;
            margin-left: 487px;
        }
    </style>
</head>
<body>
<div id="top">
	<?php if ( $settings->get_logo() ) : ?>
        <img src="<?php echo $settings->get_logo() ?>" id="company-logo"/>
	<?php endif; ?>
    <h1><?php _e( "Faktura č. ", "eso" ) ?><?php echo $invoice->get_number() ?></h1>
</div>
<hr>
<div id="parties">
    <div class="row">
        <div class="col col-50">
            <h2><?php _e( "Dodavatel", "eso" ) ?></h2>
			<?php if ( $settings->get_issued_by() ) : ?>
                <span><strong><?php echo $settings->get_issued_by(); ?></strong></span>
			<?php endif; ?>
			<?php if ( $settings->get_street() ) : ?>
                <span> <?php echo $settings->get_street(); ?></span>
			<?php endif; ?>
			<?php if ( $settings->get_city() ) : ?>
                <span><?php echo $settings->get_city(); ?></span>
			<?php endif; ?>
			<?php if ( $settings->get_country() ) : ?>
                <span><?php $shipping_zone = new Eso_Shipping_Zone( $settings->get_country() );
					echo $shipping_zone->get_name() ?></span>
			<?php endif; ?>
            <span><small>
                <?php if ( $settings->is_vat_payer() ) : ?>
	                <?php _e( "Plátce DPH", "eso" ) ?>
                <?php else : ?>
	                <?php _e( "Neplátce DPH", "eso" ) ?>
                <?php endif; ?>
            </small></span>
			<?php if ( $settings->get_file_number() ) : ?>
                <span><small><?php echo $settings->get_file_number() ?></small></span>
			<?php endif; ?>
        </div>
        <div class="col col-50">
            <h2><?php _e( "Odběratel", "eso" ) ?></h2>
            <span><strong>
	        <?php if ( $order->get_billing_company_name() ) : ?>
		        <?php echo $order->get_billing_company_name() ?>
	        <?php else : ?>
		        <?php echo $order->get_customer()->get_full_name() ?>
	        <?php endif; ?>
            </strong></span>
            <span><?php echo $order->get_billing_street() ?></span>
            <span><?php echo $order->get_billing_city() ?></span>
            <span><?php echo $order->get_billing_postcode() ?></span>
            <span><?php $country = new Eso_Shipping_Zone( $order->get_billing_country() );
				echo $country->get_name(); ?></span>
        </div>
    </div>

</div>
<hr>
<div id="figures">
    <div class="row">
        <div class="col col-50">
            <table border="0">
                <tr>
                    <td class="label"><?php _e( "Číslo objednávky", "eso" ) ?></td>
                    <td>#<?php echo $order->get_id() ?></td>
                </tr>
                <tr>
                    <td class="label"><?php _e( "Datum vystavení", "eso" ) ?></td>
                    <td><?php echo $order->get_date_created() ?></td>
                </tr>
                <tr>
                    <td class="label"><?php _e( "Datum uskutečnení plnění", "eso" ) ?></td>
                    <td><?php echo $order->get_date_created() ?></td>
                </tr>
				<?php if ( $invoice->get_due_date() ) : ?>
                    <tr>
                        <td class="label"><?php _e( "Datum splatnosti", "eso" ) ?></td>
                        <td><?php echo $invoice->get_due_date() ?></td>
                    </tr>
				<?php endif; ?>
                <tr>
                    <td class="label"><?php _e( "Forma úhrady: ", "eso" ) ?></td>
                    <td><?php echo $order->get_payment_method_name(); ?></td>
                </tr>
            </table>
        </div>
        <div class="col col-50">
            <table border="0">
				<?php if ( $settings->get_bank_account() ) : ?>
                    <tr>
                        <td class="label"><?php _e( "Číslo účtu", "eso" ) ?></td>
                        <td><?php echo $settings->get_bank_account() ?></td>
                    </tr>
				<?php endif; ?>
				<?php if ( $settings->get_iban() ) : ?>
                    <tr class="small">
                        <td class="label"><?php _e( "IBAN", "eso" ) ?></td>
                        <td><?php echo $settings->get_iban() ?></td>
                    </tr>
				<?php endif; ?>
				<?php if ( $settings->get_bic() ) : ?>
                    <tr class="small">
                        <td class="label"><?php _e( "BIC / SWIFT", "eso" ) ?></td>
                        <td><?php echo $settings->get_bic() ?></td>
                    </tr>
				<?php endif; ?>
				<?php
				if ( $settings->get_specific_symbol() ) : ?>
                    <tr>
                        <td class="label"><?php _e( "Specifický symbol", "eso" ) ?></td>
                        <td><?php echo $settings->get_specific_symbol() ?></td>
                    </tr>
				<?php endif; ?>
				<?php
				if ( $settings->get_constant_symbol() ) : ?>
                    <tr>
                        <td class="label"><?php _e( "Konstatní symbol", "eso" ) ?></td>
                        <td><?php echo $settings->get_constant_symbol() ?></td>
                    </tr>
				<?php endif; ?>
				<?php
				if ( ! empty( $invoice->get_variable_symbol() ) ) : ?>
                    <tr>
                        <td class="label"><?php _e( "Variabilní symbol", "eso" ) ?></td>
                        <td class="strong"><?php echo $invoice->get_variable_symbol() ?></td>
                    </tr>
				<?php endif; ?>
				<?php
				if ( $order->get_sum( $order->get_currency() ) ) : ?>
                    <tr>
                        <td class="label"><?php _e( "K úhradě", "eso" ) ?></td>
                        <td class="strong"><?php echo $order->get_total( true ) ?></td>
                    </tr>
				<?php endif; ?>
				<?php
				if ( $order->is_paid() ) : ?>
                    <tr>
                        <td></td>
                        <td class="alert"><?php _e( "Již uhrazeno", "eso" ) ?></td>
                    </tr>
				<?php endif; ?>
            </table>
        </div>
    </div>

</div>
<div id="items">
	<?php if ( $settings->get_before_note() ) : ?>
    <p><?php echo $settings->get_before_note() ?></p>
	<?php endif; ?>
    <table class="fullwidth" border="0">
        <tr class="header">
            <td><?php _e( "Kód položky", "eso" ) ?></td>
            <td><?php _e( "Popis", "eso" ) ?></td>
            <td><?php _e( "Množství", "eso" ) ?></td>
            <td><?php _e( "Cena za ks", "eso" ) ?></td>
            <td><?php _e( "Celkem", "eso" ) ?></td>
        </tr>
		<?php
		/* @var $order_item Eso_Order_Item */
		foreach ( $order->get_items() as $product_id => $product_data ) :
		$order_item = new Eso_Order_Item( $order->get_id(), $product_id );
		if ( ! isset( $order_currency ) ) {
			$order_currency = new Eso_Currency( $order_item->get_currency() );
		}
		?>
        <tr>
            <td>#<?php echo $order_item->get_product_id() ?></td>
            <td><?php echo $order_item->get_name() ?></td>
            <td><?php echo $order_item->get_quantity() ?><?php _e( "ks", "eso" ) ?></td>
            <td><?php echo $order_item->get_price_per_piece( $order_currency ) . " " . $order_currency->get_symbol(); ?></td>
            <td><?php echo $order_item->get_sum() . " " . $order_currency->get_symbol() ?></td>
        </tr>
		<?php endforeach; ?>
		<?php if ( $order->get_payment_method_price() > 0 ) : ?>
        <tr>
            <td></td>
            <td colspan="3"><?php echo $order->get_payment_method_name() ?></td>
            <td><?php echo $order->get_payment_method_price( true ) ?></td>
        </tr>
		<?php endif; ?>
		<?php if ( $order->get_shipping_method_price() > 0 ) : ?>
        <tr>
            <td></td>
            <td colspan="3"><?php echo $order->get_shipping_method_name() ?></td>
            <td><?php echo $order->get_shipping_method_price( true ) ?></td>
        </tr>
		<?php endif; ?>
		<?php if($order->get_discount()) : ?>
        <tr class="footer discount">
            <td></td>
            <td></td>
            <td></td>
            <td colspan="1"><?php _e( "Sleva", "eso" ) ?></td>
            <td>- <?php echo $order->get_discount( true, true ) ?></td>
        </tr>
        <?php endif; ?>
        <tr class="footer total-without-tax">
            <td></td>
            <td></td>
            <td></td>
            <td colspan="1"><?php _e( "Celkem bez DPH", "eso" ) ?></td>
            <td><?php echo $order->get_total_without_tax( true, true ) ?></td>
        </tr>
        <tr class="footer">
            <td></td>
            <td></td>
            <td></td>
            <td class="strong" colspan="1"><?php _e( "Celkem k úhradě", "eso" ) ?></td>
            <td class="strong"><?php echo $order->get_total( true ) ?></td>
        </tr>
    </table>
	<?php if ( $settings->get_after_note() ) : ?>
    <p><?php echo $settings->get_after_note() ?></p>
	<?php endif; ?>
</div>
<div id="contact">
    <span class="strong"><?php _e( "Kontakt", "eso" ) ?></span>
	<?php if ( $settings->get_contact_name() ) : ?>
        <span><?php echo $settings->get_contact_name() ?></span>
	<?php endif; ?>
	<?php if ( $settings->get_contact_phone() ) : ?>
        <span><?php _e( "Tel: ", "eso" ) ?><?php echo $settings->get_contact_phone() ?></span>
	<?php endif; ?>
	<?php if ( $settings->get_contact_email() ) : ?>
        <span><?php echo $settings->get_contact_email() ?></span>
	<?php endif; ?>
</div>
</body>
</html>





