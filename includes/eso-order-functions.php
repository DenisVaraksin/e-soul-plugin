<?php

use Dompdf\Dompdf;

/**
 * @since 0.0.22
 */
function eso_get_order_status_id( $slug ) {
	$term = get_term_by( 'slug', $slug, 'order_status' );

	return (int) $term->term_id;
}

/**
 * @param $gopay_payment_id
 *
 * @return false|int
 * @since 2019.5
 */
function eso_get_order_id_by_gopay_id( $gopay_payment_id ) {
	$args = [
		"post_type"      => "esoul_order",
		"meta_key"       => "gopay_payment_id",
		"meta_value"     => $gopay_payment_id,
		"posts_per_page" => 1
	];

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$id = get_the_ID();
			wp_reset_postdata();

			return $id;
		}
	}

	return false;
}

/**
 * @param $comgate_payment_id
 *
 * @return false|int
 *
 * @since 2019.5
 */
function eso_get_order_id_by_comgate_payment_id( $comgate_payment_id ) {
	$args = [
		"post_type"      => "esoul_order",
		"meta_key"       => "comgate_payment_id",
		"meta_value"     => $comgate_payment_id,
		"posts_per_page" => 1
	];

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$id = get_the_ID();
			wp_reset_postdata();

			return $id;
		}
	}

	return false;
}

/**
 * @param $postcode
 * @param $country
 *
 * @return bool
 *
 * @since 2019.5
 */
function eso_valid_postcode( $postcode, $country ) {
	$postcode = intval( str_replace( " ", "", $postcode ) );

	if ( in_array( $country, [ "CZ", "SK" ] ) ) {
		//Checking postcode validity with external API
		$postcodeMatch = file_get_contents( "http://api.marketsoul.cz/?psc=" . $postcode . "&country=" . $country );

		if ( empty( $postcodeMatch ) ) {
			return false;
		}

		return true;
	}

	return true;
}

/**
 * @param $phone
 *
 * @return bool
 *
 * @since 2019.5
 */
function eso_valid_phone( $phone ) {

	if ( strlen( $phone ) < 10 ) {
		return false;
	}

	return true;

}

/**
 * @param $order_id
 *
 * @since 2019.6
 */
function eso_generate_invoice( $order_id, $token ) {
	$invoice = new Eso_Invoice( $order_id );

	if ( ! $invoice->get_order()->can_access( $token ) ) {
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
	}

	$dompdf = new Dompdf();

	ob_start();
	require( ESO_DIR . '/includes/invoice.php' );
	$html = ob_get_contents();
	ob_get_clean();

	$dompdf->loadHtml( $html );

	$dompdf->setPaper( 'A4', 'portrait' );

	$dompdf->render();

	$dompdf->stream( $invoice->get_filename(), 0 );
}

/**
 * @since 2019.7
 *
 * @param $order_id
 *
 * @return string
 */
function eso_get_invoice_url( $order_id ) {
	$order = new Eso_Order( $order_id );

	return home_url() . "?generateInvoice=" . $order->get_id() . "&key=" . $order->get_token();
}


/**
 * @since 2019.8
 *
 * @param $branch_string
 *
 * @return mixed
 */
function eso_get_zasilkovna_branch_name( $branch_string ) {
	$branch_array = explode( ":", $branch_string );

	if ( isset( $branch_array[1] ) ) {
		return $branch_array[1];
	}

	return null;
}
