<?php
/**
 * Add a post display state for special pages in the page list table.
 *
 * @param array $post_states An array of post display states.
 * @param WP_Post $post The current post object.
 *
 * @return array
 */
function eso_add_display_post_states( $post_states, $post ) {
	if ( eso_get_page_id( 'thankyou' ) === $post->ID ) {
		$post_states[] = __( 'Stránka dokončení objednávky', 'eso' );
	}

	if ( eso_get_page_id( 'cart' ) === $post->ID ) {
		$post_states[] = __( 'Stránka košíku', 'eso' );
	}

	if ( eso_get_page_id( 'account' ) === $post->ID ) {
		$post_states[] = __( 'Stránka profilu', 'eso' );
	}

	if ( eso_get_page_id( 'terms' ) === $post->ID ) {
		$post_states[] = __( 'Stránka obchodních podmínek', 'eso' );
	}

	if ( eso_get_page_id( 'orders' ) === $post->ID ) {
		$post_states[] = __( 'Stránka objednávek uživatele', 'eso' );
	}

	if ( eso_get_page_id( 'set-password' ) === $post->ID ) {
		$post_states[] = __( 'Nastavit nové heslo', 'eso' );
	}

	if ( eso_get_page_id( 'lost-password' ) === $post->ID ) {
		$post_states[] = __( 'Zapomenuté heslo', 'eso' );
	}

	if ( eso_get_page_id( 'login' ) === $post->ID ) {
		$post_states[] = __( 'Stránka přihlášení', 'eso' );
	}

	if ( eso_get_page_id( 'register' ) === $post->ID ) {
		$post_states[] = __( 'Založení účtu', 'eso' );
	}

	return $post_states;
}

add_filter( 'display_post_states', 'eso_add_display_post_states', 10, 2 );

/**
 * @since 0.0.12
 * @since 2020.1.2 supports privacy_policy argument
 * @param $page
 *
 * @return bool|int
 */
function eso_get_page_id( $page ) {
	if($page == "privacy_policy") {
		$option_name = "wp_page_for_privacy_policy";
	} else {
		$option_name = "eso_page_" . $page . "_id";
	}

	$page_id = get_option($option_name);
	if($page_id) {
		return (int) $page_id;
	}

	return false;
}

/**
 * @since 0.0.21
 */
function eso_get_page_link( $page, $params = "" ) {
	return get_permalink(eso_get_page_id( $page )) . $params;
}

/**
 * @since 0.0.21
 */
function eso_the_page_link( $page, $params = "" ) {
	the_permalink(eso_get_page_id( $page ));
	echo $params;
}

/**
 * @since 0.0.21
 */
function eso_is_page_id( $page ) {
	if(eso_get_page_id( $page ) == get_the_ID()) {
		return true;
	}

	return false;
}

add_action( 'before_delete_post', 'eso_before_delete_page' );
function eso_before_delete_page( $postid ){

	global $post_type;
	global $post;

	if ( $post_type != 'page' ) return;

	if(isset($post->post_name)) {
		$post_name = str_replace("__trashed", "", $post->post_name);

		if( get_option('eso_page_' . $post_name . '_id') ) {
			delete_option('eso_page_' . $post_name . '_id');
		}
	}

	return;
}