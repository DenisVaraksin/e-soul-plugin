<?php
if(!class_exists('Eso_Validate', false)) {
	/**
	 * @since 2019.6
	 * @since 2020.1.19 validate postcodes
	 *
	 * Class Eso_Validate
	 */
	class Eso_Validate {

		public function __construct() {

		}

		/**
		 * @param $key
		 * @param $value
		 *
		 * @return null|string
		 */
		public function validation_error( $key, $value ) {
			$checkout_fields = new Eso_Checkout_Fields();
			$customer_meta = $checkout_fields->get_customer_meta_fields();

			switch ( $key ) {
				case "shipping_email":
					if(!is_email($value)) {
						return __("Zadejte prosím platný email", "eso");
					}
					break;
				case "shipping_phone":
					if ( substr( $value, 0, 1 ) != "+" ) {
						return __( "Zadávejte prosím číslo včetně předvolby (+420, +421...)", "eso" );
					} else if(!eso_is_phone($value)) {
						return __("Zkontrolujte prosím zadané telefonní číslo", "eso");
					}
					break;
				case "zasilkovna_branch":
					if(empty($value)) {
						return __("Vyberte prosím pobočku Zásilkovny", "eso");
					}
					break;
				case "shipping_postcode":
					$session = new Eso_Session(eso_session_token());
					$country = $session->get_value("shipping_country");
					if(empty($country)) {
						$country = "CZ";
					}
					if(!eso_valid_postcode($value, $country)) {
						return __("Zkontrolujte prosím zadané PSČ", "eso");
					}
					break;
				case "billing_postcode":
					$session = new Eso_Session(eso_session_token());
					$country = $session->get_value("billing_country");
					if(!eso_valid_postcode($value, $country)) {
						return __("Zkontrolujte prosím zadané PSČ", "eso");
					}
					break;
				default:
					if(isset($customer_meta["shipping"]["fields"][$key]["required"])) {
						if($customer_meta["shipping"]["fields"][$key]["required"] == true) {
							if(empty($value)) {
								return __("Vyplňte prosím toto pole", "eso");
							}
						}
					}
					return null;
					break;
			}

			return null;
		}

	}
}