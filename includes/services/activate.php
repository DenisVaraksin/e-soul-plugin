<?php
/**
 * @since 2019.7
 */
?>
<html>
<head>
    <meta charset="UTF-8">
    <style>
        body {
            padding: 50px;
            text-align: center;
            font-family: sans-serif;
        }
    </style>
</head>
<body>
<h1>Plugin E-soul byl na této doméně úspěšně aktivován</h1>
<p>Ponechte tento soubor na svém místě pro správnou funkčnost pluginu.</p>
<small>Naposledy vygenerováno <?php $date = eso_now(); echo $date->format("j. n. Y H:i") ?> na <?php echo get_home_url() ?></small>
</body>
</html>
