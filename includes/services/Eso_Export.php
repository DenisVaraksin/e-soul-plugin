<?php

use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if ( ! class_exists( 'Eso_Export', false ) ) {
	/**
	 * @since 2019.7
	 *
	 * Class Eso_Export
	 */
	class Eso_Export {

		public function __construct() {

		}

		private function generate( Spreadsheet $spreadsheet, $filename ) {
			$writer = new Xlsx( $spreadsheet );

			header( 'Content-Type: application/vnd.ms-excel' );
			header( 'Content-Disposition: attachment; filename="' . $filename . '.xlsx"' );
			try {
				$writer->save( "php://output" );
			} catch ( \PhpOffice\PhpSpreadsheet\Writer\Exception $e ) {
				write_log($e->getMessage());
			}

			die();
		}

		/**
		 * @throws \PhpOffice\PhpSpreadsheet\Exception
		 */
		public function customers() {
			$spreadsheet = new Spreadsheet();
			$sheet       = $spreadsheet->getActiveSheet();

			$admin_query = new Eso_Admin_Query();

			$customers = $admin_query->customer_index( - 1 );

			if ( ! empty( $customers->get_results() ) ) {
				$sheet->setCellValue( "A1", __( "ID", "eso" ) );
				$sheet->setCellValue( "B1", __( "Jméno", "eso" ) );
				$sheet->setCellValue( "C1", __( "Email", "eso" ) );
				$sheet->setCellValue( "D1", __( "Telefon", "eso" ) );
				$sheet->setCellValue( "E1", __( "Datum reg.", "eso" ) );
				$sheet->setCellValue( "F1", __( "Objednávky", "eso" ) );
				$sheet->setCellValue( "G1", __( "Tržby", "eso" ) );
				$sheet->setCellValue( "H1", __( "Skupina", "eso" ) );

				$counter = 2;
				foreach ( $customers->get_results() as $customer_obj ) {
					$customer = new Eso_Customer( $customer_obj->ID );

					$sheet->setCellValue( "A$counter", $customer->get_full_name() );
					$sheet->setCellValue( "B$counter", $customer->get_full_name() );
					$sheet->setCellValue( "C$counter", $customer->get_email() );
					$sheet->setCellValueExplicit( "D$counter", $customer->get_phone(), DataType::TYPE_STRING );
					$sheet->setCellValue( "E$counter", $customer->get_date_registered() );
					$sheet->setCellValue( "F$counter", $customer->get_order_count() );
					$sheet->setCellValue( "G$counter", $customer->get_turnover( null, true ) );
					$sheet->setCellValue( "H$counter", $customer->get_group_name() );

					$counter ++;
				}
			}

			$sheet->getDefaultColumnDimension()->setWidth( 20 );

			$this->generate( $spreadsheet, "zakaznici" );
		}

		/**
		 * @throws \PhpOffice\PhpSpreadsheet\Exception
		 */
		public function orders() {
			$spreadsheet = new Spreadsheet();
			$sheet       = $spreadsheet->getActiveSheet();

			$admin_query = new Eso_Admin_Query();

			$orders = $admin_query->posts_index( "esoul_order", - 1 );

			if ( $orders->have_posts() ) {

				$sheet->setCellValue( "A1", __( "ID", "eso" ) );
				$sheet->setCellValue( "B1", __( "Zákazník", "eso" ) );
				$sheet->setCellValue( "C1", __( "Email", "eso" ) );
				$sheet->setCellValue( "D1", __( "Stav", "eso" ) );
				$sheet->setCellValue( "E1", __( "Částka vč. DPH", "eso" ) );
				$sheet->setCellValue( "F1", __( "Částka bez DPH", "eso" ) );
				$sheet->setCellValue( "G1", __( "Datum", "eso" ) );
				$sheet->setCellValue( "H1", __( "Doprava", "eso" ) );
				$sheet->setCellValue( "I1", __( "Platba", "eso" ) );

				$counter = 2;

				while ( $orders->have_posts() ) {
					$orders->the_post();
					$order = new Eso_Order( get_the_ID() );

					$sheet->setCellValue( "A$counter", $order->get_id() );
					$sheet->setCellValue( "B$counter", $order->get_customer()->get_full_name() );
					$sheet->setCellValue( "C$counter", $order->get_shipping_email() );
					$sheet->setCellValue( "D$counter", $order->get_status_name() );
					$sheet->setCellValue( "E$counter", $order->get_total( true ) );
					$sheet->setCellValue( "F$counter", $order->get_total_without_tax( false, true ) );
					$sheet->setCellValue( "G$counter", $order->get_date_and_time_created() );
					$sheet->setCellValue( "H$counter", $order->get_shipping_method_name() );
					$sheet->setCellValue( "I$counter", $order->get_payment_method_name() );

					$counter++;
				}
			}

			$sheet->getDefaultColumnDimension()->setWidth( 20 );

			$this->generate($spreadsheet, "objednavky");
		}

		/**
		 * @since 2020.1.4
		 * @throws \PhpOffice\PhpSpreadsheet\Exception
		 */
		public function newsletter_subscribers(){
			$spreadsheet = new Spreadsheet();
			$sheet       = $spreadsheet->getActiveSheet();

			$newsletter_query = new Eso_Newsletter_Query();

			$subscribers = $newsletter_query->get_all_subscribers(-1);

			if ( $subscribers ) {
				$sheet->setCellValue( "A1", __( "Email", "eso" ) );
				$sheet->setCellValue( "B1", __( "Datum registrace", "eso" ) );
				$sheet->setCellValue( "C1", __( "Oveřený", "eso" ) );

				$counter = 2;

				foreach($subscribers as $subscriber_obj) {
					$subscriber = new Eso_Newsletter_Subscriber($subscriber_obj->email);

					$sheet->setCellValue( "A$counter", $subscriber->get_email() );
					$sheet->setCellValue( "B$counter", $subscriber->get_registered() );
					$sheet->setCellValue( "C$counter", $subscriber->get_verified() );

					$counter++;
				}
			}

			$sheet->getDefaultColumnDimension()->setWidth( 30 );

			$this->generate($spreadsheet, "newsletter");
		}
	}
}