<?php

if ( ! class_exists( 'Eso_Default_Options', false ) ) {
	class Eso_Default_Options {

		public function currencies() {
			$currencies = [
				"CZK" => [
					"symbol"  => "Kč",
					"enabled" => true,
				],
				"USD" => [
					"symbol"  => "$",
					"enabled" => false,
				],
				"EUR" => [
					"symbol"  => "€",
					"enabled" => false,
				],
				"GBP" => [
					"symbol"  => "£",
					"enabled" => false,
				]
			];

			return $currencies;
		}

		/**
		 * @return array
		 * @since 2019.6 changed from taxonomy to options array
		 *
		 */
		function user_groups() {
			$groups = [
				1 => [
					"name" => "Nový zákazník",
					"slug" => "new"
				],
				2 => [
					"name" => "Stávající zákazník",
					"slug" => "current",
				],
				3 => [
					"name" => "Neaktivní",
					"slug" => "archived"
				],
				4 => [
					"name" => "VIP",
					"slug" => "vip"
				]
			];

			return $groups;
		}

		function shipping_zones() {
			$zones = [
				"CZ" => [
					"name"             => __( "Česká republika", "eso" ),
					"enabled"          => 1,
					"shipping_methods" => [
						"czech_post" => [
							"prices"          => [
								"CZK" => 100,
								"EUR" => 4,
								"USD" => 5,
								"GBP" => 4
							],
							"payment_methods" => [
								"cod"           => [
									"prices" => [
										"CZK" => 50,
										"EUR" => 2,
										"USD" => 3,
										"GBP" => 2
									]
								],
								"bank_transfer" => [
									"prices" => []
								]
							]
						],
						"ppl"        => [
							"prices"          => [
								"CZK" => 100,
								"EUR" => 4,
								"USD" => 5,
								"GBP" => 4
							],
							"payment_methods" => [
								"bank_transfer" => [
									"prices" => []
								]
							]
						]
					],
				],
				"SK" => [
					"name"             => __( "Slovenská republika", "eso" ),
					"enabled"          => 1,
					"shipping_methods" => [
						"ppl" => [
							"prices"          => [
								"CZK" => 150,
								"EUR" => 6,
								"USD" => 8,
								"GBP" => 6
							],
							"payment_methods" => [
								"bank_transfer" => [
									"prices" => []
								]
							]
						]
					],
				],
				"AT" => [
					"name"    => __( "Rakousko", "eso" ),
					"enabled" => 0,
				],
				"DE" => [
					"name"    => __( "Německo", "eso" ),
					"enabled" => 0,
				],
				"GB" => [
					"name"    => __( "Velká Británie", "eso" ),
					"enabled" => 0,
				],
				"BE" => [
					"name"    => __( "Belgie", "eso" ),
					"enabled" => 0,
				],
				"BG" => [
					"name"    => __( "Bulharsko", "eso" ),
					"enabled" => 0,
				],
				"DK" => [
					"name"    => __( "Dánsko", "eso" ),
					"enabled" => 0,
				],
				"EE" => [
					"name"    => __( "Estonsko", "eso" ),
					"enabled" => 0,
				],
				"FI" => [
					"name"    => __( "Finsko", "eso" ),
					"enabled" => 0,
				],
				"FR" => [
					"name"    => __( "Francie", "eso" ),
					"enabled" => 0,
				],
				"IE" => [
					"name"    => __( "Irsko", "eso" ),
					"enabled" => 0,
				],
				"IT" => [
					"name"    => __( "Itálie", "eso" ),
					"enabled" => 0,
				],
				"CY" => [
					"name"    => __( "Kypr", "eso" ),
					"enabled" => 0,
				],
				"LT" => [
					"name"    => __( "Litva", "eso" ),
					"enabled" => 0,
				],
				"LV" => [
					"name"    => __( "Lotyšsko", "eso" ),
					"enabled" => 0,
				],
				"LU" => [
					"name"    => __( "Lucembursko", "eso" ),
					"enabled" => 0,
				],
				"HU" => [
					"name"    => __( "Maďarsko", "eso" ),
					"enabled" => 0,
				],
				"MT" => [
					"name"    => __( "Malta", "eso" ),
					"enabled" => 0,
				],
				"NL" => [
					"name"    => __( "Nizozemsko", "eso" ),
					"enabled" => 0,
				],
				"PL" => [
					"name"    => __( "Polsko", "eso" ),
					"enabled" => 0,
				],
				"PT" => [
					"name"    => __( "Portugalsko", "eso" ),
					"enabled" => 0,
				],
				"RO" => [
					"name"    => __( "Rumunsko", "eso" ),
					"enabled" => 0,
				],
				"GR" => [
					"name"    => __( "Řecko", "eso" ),
					"enabled" => 0,
				],
				"SI" => [
					"name"    => __( "Slovinsko", "eso" ),
					"enabled" => 0,
				],
				"ES" => [
					"name"    => __( "Španělsko", "eso" ),
					"enabled" => 0,
				],
				"SE" => [
					"name"    => __( "Švédsko", "eso" ),
					"enabled" => 0,
				],
			];

			return $zones;
		}

		function shipping_methods() {
			$methods = [
				"czech_post" => [
					"name"            => "Česká pošta",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"GBP" => 4,
						"USD" => 5
					],
					"payment_methods" => [
						"cod"           => [
							"prices" => [
								"CZK" => 50,
								"EUR" => 2,
								"GBP" => 2,
								"USD" => 3
							]
						],
						"bank_transfer" => [
							"prices" => []
						]
					]
				],
				"ppl"        => [
					"name"            => "PPL",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"GBP" => 4,
						"USD" => 5
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					]
				],
				"geis"       => [
					"name"            => "GEIS",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"GBP" => 4,
						"USD" => 5
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					]
				],
				"dhl"        => [
					"name"            => "DHL",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"GBP" => 4,
						"USD" => 5
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					]
				],
				"ups"        => [
					"name"            => "UPS",
					"prices"          => [
						"CZK" => 100,
						"EUR" => 4,
						"GBP" => 4,
						"USD" => 5
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						]
					]
				],
				"personally" => [
					"name"            => "Osobní odběr",
					"prices"          => [
						"CZK" => 0,
						"EUR" => 0,
						"GBP" => 0,
						"USD" => 0
					],
					"payment_methods" => [
						"bank_transfer" => [
							"prices" => []
						],
						"cash"          => [
							"prices" => []
						]
					]
				]
			];

			return $methods;
		}

		/**
		 * @return array
		 * @since 0.0.23
		 * @since 2019.5 moved from Eso_Activate to Eso_Default_Options
		 * @since 2020.1.19 hiding facebook_feed
		 */
		function modules() {
			$modules = [
//				"facebook_feed"        => [
//					"name"     => "Facebook Feed",
//					"enabled"  => false,
//					"category" => 1,
//					"data"     => [
//						"page_id"     => [
//							"name"     => "ID stránky",
//							"type"     => "text",
//							"required" => true
//						],
//						"posts_limit" => [
//							"name"     => "Počet příspěvků",
//							"type"     => "number",
//							"required" => true,
//							"value"    => 2
//						],
//						"shortcode"   => [
//							"name"     => "Zkrácený zápis",
//							"type"     => "text",
//							"required" => false,
//							"value"    => "[eso_facebook_feed]",
//							"atts"     => [
//								"readonly" => "readonly",
//								"disabled" => "disabled"
//							]
//						]
//					]
//				],
				"instagram_feed"       => [
					"name"     => "Instagram Feed",
					"enabled"  => false,
					"category" => 1,
					"data"     => [
						"account"     => [
							"name"     => "Jméno účtu",
							"type"     => "text",
							"required" => true
						],
						"posts_limit" => [
							"name"     => "Počet příspěvků",
							"type"     => "number",
							"required" => true,
							"value"    => 8,
							"atts"     => [
								"min" => "1",
								"max" => "12"
							]
						],
						"shortcode"   => [
							"name"     => "Zkrácený zápis",
							"type"     => "text",
							"required" => false,
							"value"    => "[eso_instagram_feed]",
							"atts"     => [
								"readonly" => "readonly",
								"disabled" => "disabled"
							]
						]
					]
				],
				"google_analytics"     => [
					"name"     => "Google Analytics",
					"enabled"  => false,
					"category" => 4,
					"data"     => [
						"service_id" => [
							"name"     => "Číslo pro měření",
							"type"     => "text",
							"required" => true,
						]
					]
				],
				"gtm"                  => [
					"name"     => "Google Tag Manager",
					"enabled"  => false,
					"category" => 4,
					"data"     => [
						"service_id" => [
							"name"     => "ID",
							"type"     => "text",
							"required" => true,
						]
					]
				],
				"comgate_logistika"    => [
					"name"     => "Comgate Logistika",
					"enabled"  => false,
					"category" => 3,
					"data"     => [
						"production" => [
							"name"     => "Ostrý provoz",
							"type"     => "checkbox",
							"required" => false,
							"value"    => 0
						],
						"login"      => [
							"name"     => "Uživatelské jméno",
							"required" => true,
							"type"     => "text"
						],
						"pass"       => [
							"name"     => "Heslo",
							"required" => true,
							"type"     => "password"
						],
						"project"    => [
							"name"     => "Název projektu",
							"required" => true,
							"type"     => "text"
						]
					]
				],
				"comgate_logistika_sk" => [
					"name"     => "Comgate Logistika SK",
					"enabled"  => false,
					"category" => 3,
					"data"     => [
						"production" => [
							"name"     => "Ostrý provoz",
							"type"     => "checkbox",
							"required" => false,
							"value"    => 0
						],
						"login"      => [
							"name"     => "Uživatelské jméno",
							"required" => true,
							"type"     => "text"
						],
						"pass"       => [
							"name"     => "Heslo",
							"required" => true,
							"type"     => "password"
						],
						"project"    => [
							"name"     => "Název projektu",
							"required" => true,
							"type"     => "text"
						]
					]
				],
				"comgate_payments"     => [
					"name"     => "Comgate Platební brána",
					"enabled"  => false,
					"category" => 2,
					"data"     => [
						"production" => [
							"name"     => "Ostrý provoz",
							"type"     => "checkbox",
							"required" => false,
							"value"    => 0
						],
						"store_id"   => [
							"name"     => "ID obchodu",
							"type"     => "text",
							"required" => true,
						],
						"secure_key" => [
							"name"     => "Klíč",
							"type"     => "password",
							"required" => true,
						]
					]
				],
				"gopay"                => [
					"name"     => "GoPay Platební brána",
					"enabled"  => false,
					"category" => 2,
					"data"     => [
						"production"    => [
							"name"     => "Ostrý provoz",
							"type"     => "checkbox",
							"required" => false,
							"value"    => 0
						],
						"go_id"         => [
							"name"     => "GoID",
							"type"     => "text",
							"required" => true,
						],
						"client_id"     => [
							"name"     => "ClientID",
							"type"     => "text",
							"required" => true,
						],
						"client_secret" => [
							"name"     => "ClientSecret",
							"type"     => "password",
							"required" => true
						]
					]
				],
				"gpwebpay"             => [
					"name"     => "GP WebPay Platební brána",
					"enabled"  => false,
					"category" => 2,
					"data"     => [
						"production"       => [
							"name"     => "Ostrý provoz",
							"type"     => "checkbox",
							"required" => false,
							"value"    => 0
						],
						"merchant_number"  => [
							"name"     => "Číslo obchodníka",
							"type"     => "text",
							"required" => true,
						],
						"private_key_pass" => [
							"name"     => "Heslo ke klíči",
							"type"     => "password",
							"required" => true,
						],
						"private_key"      => [
							"name"     => "Soukromý klíč (*.key)",
							"type"     => "file",
							"required" => false,
							"atts"     => [
								"accept" => ".key"
							]
						],
						"public_cert_test" => [
							"name"     => "Veřejný certifikát pro testovací prostředí (*.pem)",
							"type"     => "file",
							"required" => false,
							"atts"     => [
								"accept" => ".pem"
							]
						],
						"public_cert_prod" => [
							"name"     => "Veřejný certifikát pro produkční prostředí (*.pem)",
							"type"     => "file",
							"required" => false,
							"atts"     => [
								"accept" => ".pem"
							]
						]
					]
				],
//				"zasilkovna" => [
//					"name" => "Zásilkovna",
//					"enabled" => false,
//					"category" => 3,
//					"data" => [
//						"api_key" => [
//							"name" => "API klíč",
//							"type" => "text",
//							"required" => true,
//						]
//					]
//				]
			];

			return $modules;
		}

		/**
		 * @return array
		 * @since 0.0.31
		 * @since 2019.5 moved from Eso_Activete to Eso_Default_Options
		 */
		function payment_methods() {
			$payment_methods = [
				"bank_transfer" => [
					"name"    => "Bankovní převod",
					"message" => "",
				],
				"cod"           => [
					"name"    => "Dobírka",
					"message" => "",
				],
				"cash"          => [
					"name"    => "Na místě v hotovosti",
					"message" => ""
				]
			];

			return $payment_methods;
		}

		/**
		 * @return array
		 *
		 * @since 2019.6 moved to Eso_Default_Options
		 */
		function stock_statuses() {
			$terms = [
				"Dodání do 7 dní"      => [
					"taxonomy" => "stock_status",
					"args"     => [
						"slug" => "in-one-week"
					]
				],
				"Dodání do Vánoc"      => [
					"taxonomy" => "stock_status",
					"args"     => [
						"slug" => 'until-christmas'
					]
				],
				"Na dotaz"             => [
					"taxonomy" => "stock_status",
					"args"     => [
						"slug" => "on-request"
					]
				],
				"Poslední kus"         => [
					"taxonomy" => "stock_status",
					"args"     => [
						"slug" => "last-item"
					]
				],
				"Dočasně vyprodáno"    => [
					"taxonomy" => "stock_status",
					"args"     => [
						"slug" => "sold-out"
					]
				],
				"Již není k dispozici" => [
					"taxonomy" => "stock_status",
					"args"     => [
						"slug" => "not-available"
					]
				],
				"Skladem"              => [
					"taxonomy" => "stock_status",
					"args"     => [
						"slug" => "in-stock"
					]
				]
			];

			return $terms;
		}

		/**
		 * @return array
		 *
		 * @since 2019.6
		 */
		function email_templates() {
			$templates = [
				"new-order"         => [
					"subject" => "Nová objednávka",
					"type"    => "admin",
					"enabled" => true
				],
				"order-thank-you"   => [
					"subject" => "Děkujeme za Vaši objednávku!",
					"type"    => "customer",
					"enabled" => true,
				],
				"invoice"           => [
					"subject" => "Faktura za objednávku",
					"type"    => "customer",
					"enabled" => true
				],
				"reset-password"    => [
					"subject" => "Požadavek na změnu hesla",
					"type"    => "customer",
					"enabled" => true
				],
				"newsletter-verify" => [
					"subject" => "Potvrzení newsletteru",
					"type"    => "customer",
					"enabled" => true
				]
			];

			return $templates;
		}

	}
}