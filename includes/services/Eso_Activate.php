<?php
/**
 * Triggers on plugin activation
 * @since 0.0.24
 * @since 2019.6 moved to services
 */
if ( ! class_exists( 'Eso_Activate', false ) ) {
	class Eso_Activate {

		public function __construct() {
			register_activation_hook( ESO_FILE, [ $this, 'activate_functions' ] );
		}

		function activate_functions() {
			$this->create_tables();
			$this->create_log();
			$this->edit_roles_on_plugin_activation();
			$this->create_default_pages();
			$this->create_default_menu();
			$this->set_default_options();
			$this->create_default_stock_statuses();
			$this->create_default_order_statuses();
			$this->create_default_user_groups();
			$this->allow_user_registration();
			$this->create_shipping_zones();
			$this->create_shipping_methods();
			$this->create_modules();
			$this->verify_modules();
			$this->create_module_categories();
			$this->create_payment_methods();
			$this->create_email_templates();
		}

		function create_tables() {
			global $wpdb;

			$installed_version = get_option( "eso_db_version" );

			if ( $installed_version == ESO_VERSION ) {
				return;
			}

			$charset_collate = $wpdb->get_charset_collate();

			$sql_sessions = "CREATE TABLE " . ESO_SESSIONS_TABLE . " (
			  id mediumint(9) NOT NULL AUTO_INCREMENT,
			  updated varchar(20),
			  token text NOT NULL,
			  name varchar(55) NOT NULL,
			  value text NULL,
			  PRIMARY KEY (id)
			) $charset_collate;";

			$sql_cart = "CREATE TABLE " . ESO_CART_TABLE . " (
			  id mediumint(9) NOT NULL AUTO_INCREMENT,
			  updated varchar(20),
			  token text NOT NULL,
			  product_id int NOT NULL,
			  quantity int NOT NULL,
			  PRIMARY KEY (id)
			) $charset_collate;";

			$sql_shipping_zone = "CREATE TABLE " . ESO_SHIPPING_ZONE_TABLE . " (
			  code varchar(10) NOT NULL,
			  enabled bit DEFAULT 0,
			  name varchar(55) NOT NULL,
			  shipping_methods text NOT NULL,
			  PRIMARY KEY (code)
			) $charset_collate;";

			$sql_shipping_method = "CREATE TABLE " . ESO_SHIPPING_METHOD_TABLE . " (
			  code varchar(10) NOT NULL,
			  name varchar(55) NOT NULL,
			  message text,
			  icon varchar(55),
			  default_prices text NOT NULL,
			  default_payment_methods text NOT NULL,
			  enabled bit DEFAULT 1,
			  PRIMARY KEY (code)
			) $charset_collate;";

			$sql_module = "CREATE TABLE " . ESO_MODULE_TABLE . " (
			  code varchar(30) NOT NULL,
			  name varchar(55) NOT NULL,
			  enabled bit DEFAULT 0,
			  category int,
			  PRIMARY KEY (code)
			) $charset_collate;";

			$sql_module_data = "CREATE TABLE " . ESO_MODULE_DATA_TABLE . " (
			  id mediumint(9) NOT NULL AUTO_INCREMENT,
			  code varchar(30) NOT NULL,
			  name varchar(55) NOT NULL,
			  module varchar(30) NOT NULL,
			  type varchar(55),
			  required bit DEFAULT 0,
			  value text,
			  atts text,
			  PRIMARY KEY (id)
			) $charset_collate;";

			$sql_module_categories = "CREATE TABLE " . ESO_MODULE_CAT_TABLE . " (
			  id int NOT NULL AUTO_INCREMENT,
			  name varchar(55) NOT NULL,
			  PRIMARY KEY (id)
			) $charset_collate;";

			$sql_payment_method = "CREATE TABLE " . ESO_PAYMENT_METHOD_TABLE . " (
			  code varchar(25) NOT NULL,
			  name varchar(55) NOT NULL,
			  message text,
			  enabled bit DEFAULT 0,
			  PRIMARY KEY (code)
			) $charset_collate;";

			$sql_coupon = "CREATE TABLE " . ESO_COUPON_TABLE . " (
			  id int NOT NULL AUTO_INCREMENT,
			  name varchar(55) NOT NULL,
			  code varchar(55) NOT NULL,
			  type varchar(5) NOT NULL,
			  value varchar(255) NOT NULL,
			  free_shipping bit DEFAULT 0,
			  registered_only bit DEFAULT 0,
			  date_start varchar(20),
			  date_end varchar(20),
			  enabled bit DEFAULT 0,
			  PRIMARY KEY (id)
			) $charset_collate;";

			$sql_newsletter = "CREATE TABLE " . ESO_NEWSLETTER_TABLE . " (
			  email varchar(155) NOT NULL,
			  registered varchar(20),
			  enabled bit DEFAULT 1,
			  verified bit DEFAULT 0,
			  hash varchar(40) NOT NULL,
			  PRIMARY KEY (email)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql_sessions );
			dbDelta( $sql_cart );
			dbDelta( $sql_shipping_zone );
			dbDelta( $sql_shipping_method );
			dbDelta( $sql_module );
			dbDelta( $sql_module_data );
			dbDelta( $sql_module_categories );
			dbDelta( $sql_payment_method );
			dbDelta( $sql_coupon );
			dbDelta( $sql_newsletter );

			update_option( "eso_db_version", ESO_VERSION );
			write_log( "Updated DB to version " . ESO_VERSION );
		}

		/**
		 * @since 2019.7
		 */
		function create_log() {
			$uploads = wp_upload_dir();
			$basedir = $uploads["basedir"];

			$parentFolder = substr( $basedir, 0, strrpos( $basedir, '/' ) );

			$log_url = $parentFolder . "/eshop.log";
			$log = fopen( $log_url, "a" ) or write_log( "Unable to open eshop.log file" );

			fwrite( $log, __FILE__ . " on " . eso_db_now() . "\n" );
			fclose( $log );

			$mark_url = $basedir . "/activate.html";
			$mark = fopen( $mark_url, "w" ) or write_log( "Unable to open activate.html file" );

			ob_start();
			require( ESO_DIR . "/includes/services/activate.php" );
			$html = ob_get_contents();
			ob_get_clean();

			fwrite( $mark, $html );
			fclose( $mark );

			eso_board_check();
		}

		/**
		 * Edit customer roles
		 *
		 * @since 2019.6 remove default Wordpress roles
		 */
		function edit_roles_on_plugin_activation() {
			remove_role( 'editor' );
			remove_role( 'author' );
			remove_role( 'contributor' );
			remove_role( 'subscriber' );
			remove_role( 'eso_store_manager' );

			add_role( 'eso_store_manager', 'Manažer obchodu',
				[
					'read'                   => true,
					'activate_plugins'       => true,
					'delete_others_pages'    => true,
					'delete_others_posts'    => true,
					'delete_pages'           => true,
					'delete_posts'           => true,
					'delete_private_pages'   => true,
					'delete_private_posts'   => true,
					'delete_published_pages' => true,
					'delete_published_posts' => true,
					'edit_others_pages'      => true,
					'edit_others_posts'      => true,
					'edit_pages'             => true,
					'edit_posts'             => true,
					'edit_private_pages'     => true,
					'edit_private_posts'     => true,
					'edit_published_pages'   => true,
					'edit_published_posts'   => true,
					'edit_theme_options'     => true,
					'manage_categories'      => true,
					'manage_links'           => true,
					'publish_pages'          => true,
					'publish_posts'          => true,
					'read_private_pages'     => true,
					'read_private_posts'     => true,
					'remove_users'           => true,
					'switch_themes'          => true,
					'upload_files'           => true,
					'customize'              => true,
					'update_core'            => true,
					'update_plugins'         => true,
					'update_themes'          => true,
					'install_plugins'        => true,
					'install_themes'         => true,
					'delete_themes'          => true,
					'delete_plugins'         => true,
					'delete_users'           => true
				]
			);
			add_role( 'eso_customer', 'Zákazník', [ 'read' => true, 'edit_posts' => true ] );
			add_role( 'eso_unregistered', 'Neregistrovaný', [ 'read' => true, 'edit_posts' => true ] );
		}

		/**
		 * Creates default user groups
		 *
		 * @since 2019.6 removed custom taxonomy and using options array
		 */
		function create_default_user_groups() {
			$default_options = new Eso_Default_Options();

			$groups = $default_options->user_groups();

			$option = get_option( "eso_customer_groups" );

			foreach ( $groups as $group_id => $group ) {
				if ( ! isset( $option[ $group_id ] ) ) {
					$option[ $group_id ]["name"] = $group["name"];
					$option[ $group_id ]["slug"] = $group["slug"];
				}
			}

			update_option( "eso_customer_groups", $option );
		}

		/**
		 * Creates default stock statuses for products
		 */
		function create_default_stock_statuses() {
			$default_options = new Eso_Default_Options();

			$terms = $default_options->stock_statuses();

			register_taxonomy( 'stock_status', array( 'esoul_product' ) );

			foreach ( $terms as $term => $term_values ) {
				$term_array = wp_insert_term( $term, $term_values["taxonomy"], $term_values["args"] );

				if ( ! is_wp_error( $term_array ) ) {
					$term_id = $term_array["term_id"];

					add_term_meta( $term_id, "default", 1, true );
				}
			}
		}

		/**
		 * @since 0.0.18
		 * Creates default order statuses
		 */
		function create_default_order_statuses() {
			/**
			 * @internal added 2019.7 as a temporary fix, taxonomy should be registered here, maybe not needed
			 */
			$args = array(
				'hierarchical'      => true,
				'labels'            => array(
					'name'          => _x( 'Stavy objednávek', 'eso' ),
					'singular_name' => _x( 'Stav objednávky', 'eso' ),
					'add_new_item'  => __( 'Přidat nový stav', 'eso' ),
					'menu_name'     => __( 'Stavy objednávek', 'eso' ),
				),
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
				'show_in_menu'      => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'order_status' ),
			);
			register_taxonomy( 'order_status', array( 'esoul_order' ), $args );

			$terms = $this->get_default_order_statuses();

			foreach ( $terms as $term => $term_values ) {
				wp_insert_term( $term, $term_values["taxonomy"], $term_values["args"] );
			}
		}

		function set_default_options() {
			foreach ( $this->get_default_options() as $option_name => $option_value ) {
				if ( get_option( $option_name ) == "" ) {
					update_option( $option_name, $option_value );
				} else {
					add_option( $option_name, $option_value );
				}
			}
		}

		function create_default_pages() {
			foreach ( $this->get_default_pages() as $page ) {
				if ( ! eso_get_page_id( $page['post_name'] ) ) {
					$page_id = wp_insert_post( $page );

					update_option( 'eso_page_' . $page['post_name'] . '_id', $page_id );
				}
			}
		}

		function create_default_menu() {
			$menu_items = [
				[
					'menu-item-title'   => __( 'Domů', 'eso' ),
					'menu-item-classes' => 'home',
					'menu-item-url'     => home_url( '/' ),
					'menu-item-status'  => 'publish'
				],
				[
					'menu-item-title'   => __( 'Produkty', 'eso' ),
					'menu-item-classes' => 'esoul_product',
					'menu-item-object'  => 'esoul_product',
					'menu-item-type'    => 'post_type_archive',
					'menu-item-status'  => 'publish'
				]
			];

			eso_create_nav_menu( 'Main Menu', $menu_items, 'header-menu' );
		}


		/**
		 * @since 0.0.21
		 */
		function allow_user_registration() {
			update_option( "users_can_register", 1 );
		}

		/**
		 * @since 0.0.22
		 */
		function create_shipping_zones() {
			$default_options = new Eso_Default_Options();

			global $wpdb;
			foreach ( $default_options->shipping_zones() as $shipping_zone_code => $shipping_zone ) {

				if ( empty( $wpdb->get_col( "SELECT code FROM " . ESO_SHIPPING_ZONE_TABLE . " WHERE code = '" . $shipping_zone_code . "'" ) ) ) {
					$data = [
						"code"             => $shipping_zone_code,
						"name"             => $shipping_zone["name"],
						"shipping_methods" => [],
						"enabled"          => $shipping_zone["enabled"]
					];

					if ( isset( $shipping_zone["shipping_methods"] ) ) {
						$data["shipping_methods"] = serialize( $shipping_zone["shipping_methods"] );
					}

					$wpdb->insert( ESO_SHIPPING_ZONE_TABLE, $data );
				}

			}
		}

		/**
		 * @since 0.0.23
		 */
		function create_shipping_methods() {
			$default_options = new Eso_Default_Options();

			global $wpdb;
			foreach ( $default_options->shipping_methods() as $shipping_method_code => $shipping_method ) {

				if ( empty( $wpdb->get_col( "SELECT code FROM " . ESO_SHIPPING_METHOD_TABLE . " WHERE code = '" . $shipping_method_code . "'" ) ) ) {
					$data = [
						"code"           => $shipping_method_code,
						"name"           => $shipping_method["name"],
						"default_prices" => serialize( $shipping_method["prices"] ),
						"default_payment_methods" => serialize($shipping_method["payment_methods"])
					];

					$wpdb->insert( ESO_SHIPPING_METHOD_TABLE, $data );
				}
			}
		}

		/**
		 * @since 0.0.31
		 */
		function create_payment_methods() {
			$default_options = new Eso_Default_Options();
			$payment_methods = $default_options->payment_methods();

			global $wpdb;
			foreach ( $payment_methods as $payment_method_code => $payment_method ) {
				if ( empty( $wpdb->get_col( "SELECT code FROM " . ESO_PAYMENT_METHOD_TABLE . " WHERE code = '" . $payment_method_code . "'" ) ) ) {
					$data = [
						"code"    => $payment_method_code,
						"name"    => $payment_method["name"],
						"message" => $payment_method["message"],
						"enabled" => 1
					];

					$wpdb->insert( ESO_PAYMENT_METHOD_TABLE, $data );
				}
			}
		}

		/**
		 * @since 0.0.24
		 */
		function create_module_categories() {
			global $wpdb;

			foreach ( $this->get_module_categories() as $module_category_code => $module_category ) {
				if ( empty( $wpdb->get_col( "SELECT id FROM " . ESO_MODULE_CAT_TABLE . " WHERE id = " . $module_category_code ) ) ) {
					$data = [
						"id"   => $module_category_code,
						"name" => $module_category["name"],
					];

					$wpdb->insert( ESO_MODULE_CAT_TABLE, $data );
				}
			}
		}

		/**
		 * @since 0.0.24
		 */
		function get_module_categories() {
			$module_categories = [
				1 => [
					"name" => "Sociální sítě"
				],
				2 => [
					"name" => "Platby"
				],
				3 => [
					"name" => "Logistika"
				],
				4 => [
					"name" => "Analytika"
				]
			];

			return $module_categories;
		}

		/**
		 * @since 0.0.24
		 */
		function create_modules() {
			global $wpdb;

			$default_options = new Eso_Default_Options();

			foreach ( $default_options->modules() as $module_code => $module ) {
				if ( empty( $wpdb->get_col( "SELECT code FROM " . ESO_MODULE_TABLE . " WHERE code = '" . $module_code . "'" ) ) ) {
					$data = [
						"code"     => $module_code,
						"name"     => $module["name"],
						"enabled"  => $module["enabled"],
						"category" => $module["category"]
					];

					$wpdb->insert( ESO_MODULE_TABLE, $data );
				}

				if ( isset( $module["data"] ) ) {
					foreach ( $module["data"] as $field_key => $field ) {
						if ( empty( $wpdb->get_col( "SELECT code FROM " . ESO_MODULE_DATA_TABLE . " WHERE code = '" . $field_key . "' AND module = '" . $module_code . "'" ) ) ) {
							if ( isset( $field["value"] ) ) {
								$field_data = $field["value"];
							} else {
								$field_data = null;
							}

							if ( isset( $field["atts"] ) ) {
								$field_atts = maybe_serialize( $field["atts"] );
							} else {
								$field_atts = null;
							}

							$data = [
								"code"     => $field_key,
								"name"     => $field["name"],
								"type"     => $field["type"],
								"required" => $field["required"],
								"module"   => $module_code,
								"value"    => $field_data,
								"atts"     => $field_atts
							];

							$wpdb->insert( ESO_MODULE_DATA_TABLE, $data );
						}
					}
				}
			}
		}

		/**
		 * @since 2020.1.19 to delete unnecessary modules
		 */
		function verify_modules() {
			global $wpdb;

			$default_options = new Eso_Default_Options();
			$modules_data = $default_options->modules();
			$modules = [];

			foreach($modules_data as $key => $value){
				$modules[] = $key;
			}

			$existing_modules = $wpdb->get_results("SELECT code FROM " . ESO_MODULE_TABLE);

			foreach($existing_modules as $existing_module) {
				if(!in_array($existing_module->code, $modules)) {
					$wpdb->delete(ESO_MODULE_DATA_TABLE, ["module" => $existing_module->code]);
					$wpdb->delete(ESO_MODULE_TABLE, ["code" => $existing_module->code]);
				}
			}
		}

		/**
		 * @return array
		 */
		function get_default_options() {
			$default_options = new Eso_Default_Options();

			$options = [
				"esoul_currencies"       => $default_options->currencies(),
				"esoul_default_currency" => "CZK",
			];

			return $options;
		}

		function get_default_order_statuses() {
			$statuses = [
				"Ke kompletaci"  => [
					"taxonomy" => "order_status",
					"args"     => [
						"slug" => "for-completion",
					]
				],
				"Na cestě"       => [
					"taxonomy" => "order_status",
					"args"     => [
						"slug" => "on-the-way",
					]
				],
				"Doručena"       => [
					"taxonomy" => "order_status",
					"args"     => [
						"slug" => "delivered",
					]
				],
				"Čeká na platbu" => [
					"taxonomy" => "order_status",
					"args"     => [
						"slug" => "waiting-for-payment",
					]
				],
				"Selhalo"        => [
					"taxonomy" => "order_status",
					"args"     => [
						"slug" => "failed",
					]
				],
				"Zrušeno"        => [
					"taxonomy" => "order_status",
					"args"     => [
						"slug" => "cancelled",
					]
				]
			];

			return $statuses;
		}

		function get_default_pages() {
			$pages = [
				[
					'post_title'   => __( "Nákupní košík", "eso" ),
					'post_content' => "",
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'cart'
				],
				[
					'post_title'   => __( "Přihlásit se", "eso" ),
					'post_content' => "[eso_login_form]",
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'login'
				],
				[
					'post_title'   => __( "Nový účet", "eso" ),
					'post_content' => "[eso_register_form]",
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'register'
				],
				[
					'post_title'   => __( "Můj účet", "eso" ),
					'post_content' => "",
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'account'
				],
				[
					'post_title'   => __( "Nastavit nové heslo", "eso" ),
					'post_content' => "[eso_set_password]",
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'set-password'
				],
				[
					'post_title'   => __( "Zapomenuté heslo", "eso" ),
					'post_content' => "[eso_lost_password]",
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'lost-password'
				],
				[
					'post_title'   => __( "Děkujeme Vám!", "eso" ),
					'post_content' => __( "Vaše objednávka byla přijata ke zpracování. Další informcae o Vaší objednávce najdete na své emailové adrese nebo ve Vašem profilu po prihlášení.", "eso" ),
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'thankyou'
				],
				[
					'post_title'   => __( "Obchodní podmínky", "eso" ),
					'post_content' => __( "Výchozí text obchodních podmínek.", "eso" ),
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'terms'
				],
				[
					'post_title'   => __( "Mé objednávky", "eso" ),
					'post_content' => "",
					'post_status'  => 'publish',
					'post_author'  => 1,
					'post_type'    => 'page',
					'post_name'    => 'orders'
				]
			];

			return $pages;
		}

		/**
		 * @since 2019.6
		 */
		function create_email_templates() {
			$default_options = new Eso_Default_Options();

			$current = get_option( "eso_email_templates" );

			foreach ( $default_options->email_templates() as $code => $template ) {
				if ( isset( $current[ $code ] ) ) {
					foreach ( $template as $label => $value ) {
						if ( ! isset( $current[ $code ][ $label ] ) ) {
							$current[ $code ][ $label ] = $value;
						}
					}
				} else {
					$current[ $code ] = $template;
				}
			}

			update_option( "eso_email_templates", $current );
		}
	}
}

$eso_activate = new Eso_Activate();