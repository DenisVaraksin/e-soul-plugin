<?php
if ( ! class_exists( 'Eso_Dashboard', false ) ) {
	class Eso_Dashboard {

		public function __construct() {
			$this->reports = new Eso_Reports();
		}

		/**
		 * @since 2019.5
		 * @since 2019.7 returns always in CZK
		 *
		 * @param int $number_of_weeks
		 *
		 * @return int
		 */
		private function get_turnover( DateTime $from, DateTime $till ) {
			$orders   = $this->reports->get_orders_between( $from, $till );
			$turnover = 0;

			$currency = new Eso_Currency( "CZK" );

			if ( $orders->have_posts() ) {
				while ( $orders->have_posts() ) {
					$orders->the_post();
					$order    = new Eso_Order( get_the_ID() );
					$turnover += $order->get_total();
				}
			}

			return round( $turnover );
		}

		/**
		 * @since 2019.5
		 * @since 2019.7 returns always in CZK
		 *
		 * @param $number_of_weeks
		 *
		 * @return string
		 */
		public function get_average_daily_turnover( DateTime $from, DateTime $till ) {
			$turnover = $this->get_turnover( $from, $till );

			$days = $till->diff( $from )->format( "%a" );

			$average = round( $turnover / $days );

			return $average . " Kč";
		}

		/**
		 * @since 2019.5
		 *
		 * @param $number_of_weeks
		 *
		 * @return int
		 */
		public function get_number_of_orders( DateTime $from, DateTime $till ) {
			$orders = $this->reports->get_orders_between( $from, $till );

			return $orders->post_count;
		}

		/**
		 * @since 2019.5
		 *
		 * @param $number_of_weeks
		 *
		 * @return int
		 */
		public function get_average_total_of_recent_orders( $from, $till ) {

			if ( $this->get_number_of_orders( $from, $till ) == 0 ) {
				return 0;
			}

			$turnover = $this->get_turnover( $from, $till ) / $this->get_number_of_orders( $from, $till );

			return round( $turnover );
		}

		/**
		 * @since 2019.6
		 * @deprecated and can be removed 2019.8
		 * @return array
		 */
		private function get_months() {
			return [
				__( "Leden", "eso" ),
				__( "Únor", "eso" ),
				__( "Březen", "eso" ),
				__( "Duben", "eso" ),
				__( "Květen", "eso" ),
				__( "Červen", "eso" ),
				__( "Červenec", "eso" ),
				__( "Září", "eso" ),
				__( "Říjen", "eso" ),
				__( "Listopad", "eso" ),
				__( "Prosinec", "eso" ),
			];
		}

		/**
		 * @param $type
		 *
		 * @return array
		 * @throws Exception
		 */
		public function get_overview_data( DateTime $from, DateTime $till ) {
			$days_diff = $till->diff( $from )->days;

			if ( $days_diff < 32 ) {
				$interval = DateInterval::createFromDateString( '1 day' );
				$period   = new DatePeriod( $from, $interval, $till->modify( "+1 day" ) );

				/* @var $day DateTime */
				foreach ( $period as $day ) {
					$day->setTime( 0, 0 );
					$labels[] = $day->format( "j. n." );

					$this_day = new DateTime( $day->format( "Y-m-d" ) );
					$next_day = new DateTime( $day->modify( "+1 day" )->format( "Y-m-d" ) );

					$data[] = $this->get_turnover( $this_day, $next_day );

				}

			} else {
				$interval = new DateInterval( 'P1M' );
				$period   = new DatePeriod( $from, $interval, $till->modify( "+1 day" ) );

				/* @var $day DateTime */
				foreach ( $period as $day ) {

					$this_day = new DateTime( $day->format( "Y-m-d" ) );
					$next_day = new DateTime( $day->modify( "last day of this month" )->format( "Y-m-d" ) );

					$data[] = $this->get_turnover( $this_day, $next_day );
				}

				$labels = [ "1.", "2.", "3.", "4.", "5.", "6.", "7.", "8.", "9.", "10.", "11.", "12." ];
			}

			$overview = [
				"labels"   => $labels,
				"datasets" => [
					"label" => __( "Tržby", "eso" ),
					"data"  => $data
				]
			];

			return $overview;
		}

		public function render_graph() {
			require_once( ESO_DIR . "/admin/views/dashboard/graph.php" );
		}

		public function render_graph_small() {
			require_once( ESO_DIR . "/admin/views/dashboard/graph-small.php" );
		}

		public function render_orders() {
			$orders = new WP_Query( [
				"posts_per_page" => 5,
				"post_type"      => "esoul_order"
			] );

			require_once( ESO_DIR . "/includes/views/dashboard/orders.php" );
		}

		public function render_products() {
			$products = new WP_Query( [
				"posts_per_page" => 5,
				"post_type"      => "esoul_product",
				"orderby"        => "meta_value_num",
				"order"          => "DESC",
				"meta_key"       => "turnover_CZK"
			] );

			require_once( ESO_DIR . "/includes/views/dashboard/products.php" );
		}

		/**
		 * @param $from
		 * @param $till
		 */
		public function render_summary( $from, $till ) {
			$from = new DateTime( $from );
			$till = new DateTime( $till );

			require_once( ESO_DIR . "/admin/views/dashboard/summary.php" );
		}

		public function render() { ?>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
            <script type="text/javascript" src="<?php echo ESO_URL . 'admin/assets/js/dashboard.min.js' ?>"></script>
			<?php
			require_once( ESO_DIR . "/includes/views/dashboard/tables.php" );
		}
	}
}