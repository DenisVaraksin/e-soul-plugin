<?php
if ( ! class_exists( 'Eso_Ajax', false ) ) {
	class Eso_Ajax {

		private $data;

		public function __construct( $data ) {
			$this->data = $data;
		}

		public function get_eso_action() {
			if ( isset( $this->data["do_success_callback"] ) ) {
				return $this->data["eso_success_callback"];
			}

			if ( isset( $this->data["eso_action"] ) ) {
				return $this->data["eso_action"];
			}

			return null;
		}

		public function do_action() {
			$this->{$this->get_eso_action()}();
		}

		/**
		 * @since 0.0.18
		 */
		function cart_remove_item() {
			$cart_item_id = intval( $this->data["eso-product-id"] );

			$cart = new Eso_Cart( eso_session_token() );

			$cart->remove_item( $cart_item_id );
		}

		/**
		 * @since 0.0.18
		 */
		function cart_item_update_quantity() {
			$product_id       = intval( $this->data["eso_product_id"] );
			$product_quantity = intval( $this->data["eso_product_quantity"] );

			$cart_item = new Eso_Cart_Item( $product_id );
			$cart_item->set_quantity( $product_quantity );

			echo $cart_item->get_total_with_currency( true );
		}


		/**
		 * @since 0.0.17
		 * @since 2019.7 renders added-to-cart template
		 */
		function add_to_cart() {
			$product_id       = intval( $this->data["eso_product_id"] );
			$product_quantity = intval( $this->data["eso_product_quantity"] );

			$cart_item = new Eso_Cart_Item( $product_id );
			$cart_item->add_quantity( $product_quantity );

			$cart = new Eso_Cart( eso_session_token() );
			$cart->add_item( $cart_item );

			require_once( get_stylesheet_directory() . "/templates/cart/added-to-cart.php" );
		}

		/**
		 * @since 2019.7
		 */
		function render_cart_quick_look() {
			$cart = new Eso_Cart( eso_session_token() );
			if ( ! empty( $cart->get_items() ) ) {
				require_once( get_stylesheet_directory() . "/templates/cart/cart-quick-look.php" );
			}
		}

		/**
		 * @since 0.0.17
		 * @since 2019.5 process_payment(), empty_cart(), reduce_stock()
		 */
		function process_order() {
			$checkout = new Eso_Checkout();
			$checkout->do_checkout( $this->data );
		}

		/**
		 * @since 2019.7
		 */
		function pay_order_after() {
			$checkout = new Eso_Checkout();
			$checkout->process_payment( $this->data["order_id"] );
		}

		/**
		 * @since 0.0.17
		 */
		function get_cart_sum_with_currency() {
			$cart = new Eso_Cart( eso_session_token() );

			echo $cart->get_sum( true, true );
		}

		/**
		 * @since 2020.1.3
		 */
		function get_cart_items_count() {
			$cart = new Eso_Cart( eso_session_token() );

			echo $cart->get_items_count();
		}

		/**
		 * @since 2019.7
		 */
		function render_cart_sum() {
			$checkout_fields = new Eso_Checkout_Fields();
			$checkout_fields->render_cart_sum();
		}

		/**
		 * @since 2019.6
		 * @since 2019.7 calling just rendering function instead of JSON
		 */
		function render_cart_totals() {
			$checkout_fields = new Eso_Checkout_Fields();
			$checkout_fields->render_cart_totals();
		}

		/**
		 * @since 2019.7
		 */
		function apply_coupon() {
			echo eso_apply_coupon( $this->data["code"] );
		}

		/**
		 * @since 0.0.18
		 */
		function currency_switch() {

			$currency_code = sanitize_text_field( $this->data["eso-currency-code"] );
			$currency      = new Eso_Currency( $currency_code );

			$session = new Eso_Session( eso_session_token() );
			$session->set_active_currency( $currency );

			echo $currency->get_symbol();
		}

		/**
		 * @since 0.0.30
		 * @since 2019.7 uses $customer->set_field();
		 */
		function update_customer_meta() {
			$field_key   = sanitize_text_field( $this->data["field_key"] );
			$field_value = sanitize_text_field( $this->data["field_value"] );

			if ( is_user_logged_in() ) {
				$customer = new Eso_Customer( get_current_user_id() );
				$customer->set_field( $field_key, $field_value );
			} else {
				$session = new Eso_Session( eso_session_token() );
				$session->set_value( $field_key, $field_value );
			}

			echo $field_key . $field_value;
		}

		/**
		 * @since 2019.6
		 */
		function validate_customer_meta() {
			$validate = new Eso_Validate();
			echo $validate->validation_error( $this->data["key"], $this->data["value"] );
		}

		/**
		 * @since 2019.5
		 */
		function update_shipping_methods() {
			$checkout_fields = new Eso_Checkout_Fields();

			$checkout_fields->render_shipment_fields();
		}

		/**
		 * @since 2019.5
		 */
		function update_payment_methods() {
			$checkout_fields = new Eso_Checkout_Fields();

			$checkout_fields->render_payment_fields();
		}

		/**
		 * @since 2019.5
		 */
		function show_zasilkovna_branches() {
			$comgate_elogist = new Eso_Comgate_Elogist();
			$comgate_elogist->zasilkovna_branches();
		}

		/**
		 * @since 2019.6
		 */
		function render_checkout_summary() {
			$checkout_fields = new Eso_Checkout_Fields();
			$checkout_fields->render_summary();
		}

		/**
		 * @since 2019.6
		 */
		function update_instagram_feed() {
			$instagram_feed = new Eso_Instagram_Feed();
			$instagram_feed->update_data();
		}

		/**
		 * @since 2019.6
		 */
		function save_customer_data() {
			$customer = new Eso_Customer( $this->data['customer_id'] );

			foreach ( $this->data["fields"] as $key => $value ) {
				$customer->set_field( $key, $value );
			}

			if ( ! isset( $this->data["fields"]["billing_on"] ) ) {
				$customer->set_field( "billing_on", 0 );
			}
		}

		/**
		 * @since 2019.8
		 *
		 * @return string
		 */
		function render_saved_notification() { ?>
            <div class="alert alert-success d-inline-block alert-dismissible fade show" role="alert">
				<?php _e( "Uloženo", "eso" ); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="<?php _e( "Zavřít", "eso" ) ?>">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
			<?php

			return null;
		}

		/**
		 * @since 2019.6
		 * @throws Exception
		 */
		function get_overview_data() {
			$dashboard = new Eso_Dashboard();

			echo json_encode( $dashboard->get_overview_data( new DateTime( $this->data["from"] ), new DateTime( $this->data["till"] ) ) );
		}

		/**
		 * @since 2019.8
		 */
		function the_out_of_stock_alert() {
			eso_alert( __( "Produktu ", "eso" ) . "<strong>" . sanitize_text_field( $this->data["product_name"] ) . "</strong>" . __( " máme pouze ", "eso" ) . "<strong>" . (int) $this->data["max"] . " </strong>" . __( "kusů skladem", "eso" ) . ".", "primary" );
		}

		/**
		 * @return string
		 *
		 * @throws Exception
		 * @since 2020.1.2
		 */
		function the_newsletter_signup() {
		    echo eso_newsletter_do_signup($this->data["newsletter-email"], true);
        }
	}
}