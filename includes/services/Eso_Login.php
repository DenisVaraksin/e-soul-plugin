<?php
//https://code.tutsplus.com/tutorials/build-a-custom-wordpress-user-flow-part-1-replace-the-login-page--cms-23627
if ( ! class_exists( 'Eso_Login', false ) ) {
	class Eso_Login {

		public function __construct() {
			add_shortcode( 'eso_login_form', array( $this, 'render_login_form' ) );
			add_shortcode( 'eso_register_form', array( $this, 'render_register_form' ) );
			add_shortcode( 'eso_lost_password', array( $this, 'render_password_lost_form' ) );
			add_shortcode( 'eso_set_password', array( $this, 'render_password_reset_form' ) );

			add_action( 'login_form_login', array( $this, 'redirect_to_custom_login' ) );
			add_action( 'login_form_register', array( $this, 'redirect_to_custom_register' ) );
			add_action( 'login_form_register', array( $this, 'do_register_user' ) );
			add_action( 'login_form_lostpassword', array( $this, 'redirect_to_custom_lostpassword' ) );
			add_action( 'login_form_lostpassword', array( $this, 'do_password_lost' ) );
			add_action( 'login_form_rp', array( $this, 'redirect_to_custom_password_reset' ) );
			add_action( 'login_form_resetpass', array( $this, 'redirect_to_custom_password_reset' ) );
			add_action( 'login_form_rp', array( $this, 'do_password_reset' ) );
			add_action( 'login_form_resetpass', array( $this, 'do_password_reset' ) );

			add_action( 'wp_logout', array( $this, 'redirect_after_logout' ) );

			add_filter( 'authenticate', array( $this, 'maybe_redirect_at_authenticate' ), 101, 3 );
			add_filter( 'login_redirect', array( $this, 'redirect_after_login' ), 10, 3 );
			add_filter( 'retrieve_password_message', array( $this, 'replace_retrieve_password_message' ), 10, 4 );

		}

		/**
		 * A shortcode for rendering the login form.
		 *
		 * @param  array $attributes Shortcode attributes.
		 * @param  string $content The text content for shortcode. Not used.
		 *
		 * @return string  The shortcode output
		 */
		public function render_login_form( $attributes, $content = null ) {
			// Parse shortcode attributes
			$default_attributes = array( 'show_title' => true );
			$attributes         = shortcode_atts( $default_attributes, $attributes );
			$show_title         = $attributes['show_title'];

			if ( is_user_logged_in() ) {
				return sprintf( __( 'Již jste přihlášeni. %sPřejít na stránku účtu%s', 'eso' ), "<a href='" . eso_get_page_link( 'account' ) . "'>", "</a>" );
			}

			// Pass the redirect parameter to the WordPress login functionality: by default,
			// don't specify a redirect, but if a valid redirect URL has been passed as
			// request parameter, use it.
			$attributes['redirect'] = '';
			if ( isset( $_REQUEST['redirect_to'] ) ) {
				$attributes['redirect'] = wp_validate_redirect( $_REQUEST['redirect_to'], $attributes['redirect'] );
			}

			// Error messages
			$errors = array();
			if ( isset( $_REQUEST['login'] ) ) {
				$error_codes = explode( ',', $_REQUEST['login'] );

				foreach ( $error_codes as $code ) {
					$errors [] = $this->get_error_message( $code );
				}
			}
			$attributes['errors'] = $errors;

			// Check if user just logged out
			$attributes['logged_out'] = isset( $_REQUEST['logged_out'] ) && $_REQUEST['logged_out'] == true;

			// Check if the user just registered
			$attributes['registered'] = isset( $_REQUEST['registered'] );

			// Check if the user just requested a new password
			$attributes['lost_password_sent'] = isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'confirm';

			// Check if user just updated password
			$attributes['password_updated'] = isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed';

			// Render the login form using an external template
			return $this->get_template_html( 'login-form', $attributes );
		}

		/**
		 * A shortcode for rendering the new user registration form.
		 *
		 * @param  array $attributes Shortcode attributes.
		 * @param  string $content The text content for shortcode. Not used.
		 *
		 * @return string  The shortcode output
		 */
		public function render_register_form( $attributes, $content = null ) {
			// Parse shortcode attributes
			$default_attributes = array( 'show_title' => false );
			$attributes         = shortcode_atts( $default_attributes, $attributes );

			if ( is_user_logged_in() ) {
				return __( 'Jste již přihlášení.', 'eso' );
			} elseif ( ! get_option( 'users_can_register' ) ) {
				return __( 'Registrace jsou v současnosti vypnuté. Omlouvám se.', 'eso' );
			} else {

				// Retrieve possible errors from request parameters
				$attributes['errors'] = array();
				if ( isset( $_REQUEST['register-errors'] ) ) {
					$error_codes = explode( ',', $_REQUEST['register-errors'] );

					foreach ( $error_codes as $error_code ) {
						$attributes['errors'] [] = $this->get_error_message( $error_code );
					}
				}

				return $this->get_template_html( 'register-form', $attributes );
			}
		}

		/**
		 * A shortcode for rendering the form used to initiate the password reset.
		 *
		 * @param  array $attributes Shortcode attributes.
		 * @param  string $content The text content for shortcode. Not used.
		 *
		 * @return string  The shortcode output
		 */
		public function render_password_lost_form( $attributes, $content = null ) {
			// Parse shortcode attributes
			$default_attributes = array( 'show_title' => false );
			$attributes         = shortcode_atts( $default_attributes, $attributes );

			if ( is_user_logged_in() ) {
				return __( 'Již jste přihlášeni.', 'eso' );
			} else {

				// Retrieve possible errors from request parameters
				$attributes['errors'] = array();
				if ( isset( $_REQUEST['errors'] ) ) {
					$error_codes = explode( ',', $_REQUEST['errors'] );

					foreach ( $error_codes as $error_code ) {
						$attributes['errors'] [] = $this->get_error_message( $error_code );
					}
				}

				return $this->get_template_html( 'password-lost-form', $attributes );
			}
		}

		/**
		 * A shortcode for rendering the form used to reset a user's password.
		 *
		 * @param  array $attributes Shortcode attributes.
		 * @param  string $content The text content for shortcode. Not used.
		 *
		 * @return string  The shortcode output
		 */
		public function render_password_reset_form( $attributes, $content = null ) {
			// Parse shortcode attributes
			$default_attributes = array( 'show_title' => false );
			$attributes         = shortcode_atts( $default_attributes, $attributes );

			if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
				$attributes['login'] = $_REQUEST['login'];
				$attributes['key']   = $_REQUEST['key'];

				// Error messages
				$errors = array();
				if ( isset( $_REQUEST['error'] ) ) {
					$error_codes = explode( ',', $_REQUEST['error'] );

					foreach ( $error_codes as $code ) {
						$errors [] = $this->get_error_message( $code );
					}
				}
				$attributes['errors'] = $errors;

				return $this->get_template_html( 'password-reset-form', $attributes );
			} else {
				return __( 'Neplatný odkaz na změnu hesla.', 'eso' );
			}
		}

		private function get_template_html( $template_name, $attributes = null ) {
			if ( ! $attributes ) {
				$attributes = array();
			}

			ob_start();

			do_action( 'personalize_login_before_' . $template_name );

			require( ESO_DIR . '/includes/templates/login/' . $template_name . '.php' );

			do_action( 'personalize_login_after_' . $template_name );

			$html = ob_get_contents();
			ob_end_clean();

			return $html;
		}

		/**
		 * Redirect the user to the custom login page instead of wp-login.php.
		 */
		function redirect_to_custom_login() {
			if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
				$redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;

				if ( is_user_logged_in() ) {
					$this->redirect_logged_in_user( $redirect_to );
					exit;
				}

				// The rest are redirected to the login page
				$login_url = eso_get_page_link( 'login' );
				if ( ! empty( $redirect_to ) ) {
					$login_url = add_query_arg( 'redirect_to', $redirect_to, $login_url );
				}

				wp_redirect( $login_url );
				exit;
			}
		}

		/**
		 * Redirects the user to the correct page depending on whether he / she
		 * is an admin or not.
		 *
		 * @param string $redirect_to An optional redirect_to URL for admin users
		 */
		private function redirect_logged_in_user( $redirect_to = null ) {
			$user = wp_get_current_user();
			if ( user_can( $user, 'install_plugins' ) ) {
				if ( $redirect_to ) {
					wp_safe_redirect( $redirect_to );
				} else {
					wp_redirect( admin_url() );
				}
			} else {
				wp_redirect( eso_get_page_link( 'account' ) );
			}
		}

		/**
		 * Redirect the user after authentication if there were any errors.
		 *
		 * @param Wp_User|Wp_Error $user The signed in user, or the errors that have occurred during login.
		 * @param string $username The user name used to log in.
		 * @param string $password The password used to log in.
		 *
		 * @return Wp_User|Wp_Error The logged in user, or error information if there were errors.
		 */
		function maybe_redirect_at_authenticate( $user, $username, $password ) {
			// Check if the earlier authenticate filter (most likely,
			// the default WordPress authentication) functions have found errors
			if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
				if ( is_wp_error( $user ) ) {
					$error_codes = join( ',', $user->get_error_codes() );

					$login_url = eso_get_page_link( 'login' );
					$login_url = add_query_arg( 'login', $error_codes, $login_url );

					wp_redirect( $login_url );
					exit;
				}
			}

			return $user;
		}

		/**
		 * Finds and returns a matching error message for the given error code.
		 *
		 * @param string $error_code The error code to look up.
		 *
		 * @return string               An error message.
		 */
		private function get_error_message( $error_code ) {
			switch ( $error_code ) {
				case 'expiredkey':
				case 'invalidkey':
					return __( 'Tento odkaz bohužel již není platný.', 'eso' );

				case 'password_reset_mismatch':
					return __( "Zadejte prosím dvě stejná hesla", 'eso' );

				case 'password_reset_empty':
					return __( "Zadejte prosím heslo", 'eso' );

				case 'empty_username':
					return __( 'Pro pokračování zadejte prosím Vaší emailovou adrese', 'eso' );

				case 'invalid_email':
				case 'invalidcombo':
					return sprintf( __( 'Tento email není zaregistrovaný. <a href="%s">Vytvořit nový účet</a>', 'eso' ), eso_get_page_link( "register" ) );
				case 'email':
					return __( 'Zadejte prosím platný email', 'eso' );

				case 'email_exists':
					return sprintf( __( 'Tento email je již zaregistrovaný. Zkuste se prosím <a href="%s">přihlásit</a>', 'eso' ), eso_get_page_link( "login" ) );

				case 'closed':
					return __( 'Registrace jsou v současnosti vypnuté. Omlouvám se.', 'eso' );

				case 'empty_password':
					return __( 'Zadejte prosím heslo.', 'eso' );

				case 'invalid_username':
					return __(
						"Tuto emailovou adresu neznáme. Nepoužili jste při registraci jinou?",
						'eso'
					);

				case 'incorrect_password':
					$err = __(
						"Heslo, které jste zadali, není správné. Chcete <a href='%s'>nastavit nové heslo</a>?",
						'eso'
					);

					return sprintf( $err, wp_lostpassword_url() );

				default:
					break;
			}

			return __( 'Vyskytla se neznámá chyba, zkuste to prosím za chvíli.', 'eso' );
		}

		/**
		 * Redirect to custom login page after the user has been logged out.
		 */
		public function redirect_after_logout() {
			$redirect_url = eso_get_page_link( 'login' ) . '?logged_out=true';
			wp_safe_redirect( $redirect_url );
			exit;
		}

		/**
		 * @since 2020.1.18 change role to customer it user logged in and is unregistered
		 *
		 * Returns the URL to which the user should be redirected after the (successful) login.
		 *
		 * @param string $redirect_to The redirect destination URL.
		 * @param string $requested_redirect_to The requested redirect destination URL passed as a parameter.
		 * @param WP_User|WP_Error $user WP_User object if login was successful, WP_Error object otherwise.
		 *
		 * @return string Redirect URL
		 */
		public function redirect_after_login( $redirect_to, $requested_redirect_to, $user ) {
			$redirect_url = home_url();

			if ( ! isset( $user->ID ) ) {
				return $redirect_url;
			}

			if ( user_can( $user, 'install_plugins' ) ) {
				// Use the redirect_to parameter if one is set, otherwise redirect to admin dashboard.
				if ( $requested_redirect_to == '' ) {
					$redirect_url = admin_url( "admin.php?page=eso-dashboard" );
				} else {
					$redirect_url = $requested_redirect_to;
				}
			} else {
				$user = get_userdata( $user->ID );
				$user_roles = $user->roles;

				if ( in_array( 'eso_unregistered', $user_roles, true ) ) {
					$user->remove_role('eso_unregistered');
					$user->add_role('eso_customer');
				}

				if ( $requested_redirect_to == '' ) {
					$redirect_url = eso_get_page_link( 'account' );
				} else {
					$redirect_url = $requested_redirect_to;
				}
			}

			$this->merge_cart_to( $user->ID );
			$this->merge_session_to( $user->ID );

			return wp_validate_redirect( $redirect_url, home_url() );
		}

		/**
		 * Move current cart items to user who just logged id.
		 *
		 * @param $user_id
		 *
		 * @since 2019.5
		 */
		public function merge_cart_to( $user_id ) {
			$token_when_logged = eso_session_token_by_user_id( $user_id );

			$old_cart       = new Eso_Cart( eso_session_token() );
			$old_cart_items = $old_cart->get_items();
			$cart           = new Eso_Cart( $token_when_logged );

			if(!empty($old_cart_items)) {
				/* @var $item Eso_Cart_Item */
				foreach ( $old_cart_items as $item_id => $item ) {
					$cart->add_item( $item );
				}
			}

			$old_cart->destroy();
		}

		/**
		 * Move current sessions to user who just logged id.
		 *
		 * @param $user_id
		 *
		 * @since 2019.5
		 */
		public function merge_session_to( $user_id ) {
			$token_when_logged = eso_session_token_by_user_id( $user_id );
			$session           = new Eso_Session( eso_session_token() );

			$session->replace_token( $token_when_logged );
		}

		/**
		 * Redirects the user to the custom registration page instead
		 * of wp-login.php?action=register.
		 */
		public function redirect_to_custom_register() {
			if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
				if ( is_user_logged_in() ) {
					$this->redirect_logged_in_user();
				} else {
					wp_redirect( eso_get_page_link( 'register' ) );
				}
				exit;
			}
		}

		/**
		 * Validates and then completes the new user signup process if all went well.
		 *
		 * @param string $email The new user's email address
		 *
		 * @return int|WP_Error         The id of the user that was created, or error if failed.
		 * @since 2019.6 adding group as user meta
		 *
		 */
		private function register_user( $email ) {
			$errors = new WP_Error();

			// Email address is used as both username and email. It is also the only
			// parameter we need to validate
			if ( ! is_email( $email ) ) {
				$errors->add( 'email', $this->get_error_message( 'email' ) );

				return $errors;
			}

			if ( username_exists( $email ) || email_exists( $email ) ) {
				$errors->add( 'email_exists', $this->get_error_message( 'email_exists' ) );

				return $errors;
			}

			$user_data = array(
				'user_login' => $email,
				'user_email' => $email,
				'user_pass'  => null,
				'nickname'   => $email
			);

			$user_id = wp_insert_user( $user_data );

			wp_new_user_notification( $user_id, null, 'user' );

			update_user_meta( $user_id, 'shipping_email', $email );

			update_user_meta( $user_id, "group", 1 );

			return $user_id;
		}

		/**
		 * Handles the registration of a new user.
		 *
		 * Used through the action hook "login_form_register" activated on wp-login.php
		 * when accessed through the registration action.
		 */
		public function do_register_user() {
			if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
				$redirect_url = eso_get_page_link( 'register' );

				if ( ! get_option( 'users_can_register' ) ) {
					// Registration closed, display error
					$redirect_url = add_query_arg( 'register-errors', 'closed', $redirect_url );
				} else {
					$email = sanitize_email( $_POST['email'] );

					$result = $this->register_user( $email );

					if ( is_wp_error( $result ) ) {
						// Parse errors into a string and append as parameter to redirect
						$errors       = join( ',', $result->get_error_codes() );
						$redirect_url = add_query_arg( 'register-errors', $errors, $redirect_url );
					} else {
						$redirect_url = eso_get_page_link( 'login' );
						$redirect_url = add_query_arg( 'registered', $email, $redirect_url );
					}
				}

				wp_redirect( $redirect_url );
				exit;
			}
		}

		/**
		 * Redirects the user to the custom "Forgot your password?" page instead of
		 * wp-login.php?action=lostpassword.
		 */
		public function redirect_to_custom_lostpassword() {
			if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
				if ( is_user_logged_in() ) {
					$this->redirect_logged_in_user();
					exit;
				}

				wp_redirect( eso_get_page_link( 'lost-password' ) );
				exit;
			}
		}

		/**
		 * Initiates password reset.
		 */
		public function do_password_lost() {
			if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
				$errors = retrieve_password();
				if ( is_wp_error( $errors ) ) {
					// Errors found
					$redirect_url = eso_get_page_link( 'lost-password' );
					$redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
				} else {
					// Email sent
					$redirect_url = eso_get_page_link( 'login' );
					$redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
				}

				wp_redirect( $redirect_url );
				exit;
			}
		}

		/**
		 * Returns the message body for the password reset mail.
		 * Called through the retrieve_password_message filter.
		 *
		 * @param string $message Default mail message.
		 * @param string $key The activation key.
		 * @param string $user_login The username for the user.
		 * @param WP_User $user_data WP_User object.
		 *
		 * @return string   The mail message to send.
		 */
		public function replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
			// Create new message
			$msg = __( 'Dobrý den', 'eso' ) . "\r\n\r\n";
			$msg .= sprintf( __( 'Právě jsme zaregistrovali, že nás někdo požádal o vygenerování nového hesla k Vašemu účtu %s.', 'eso' ), $user_login ) . "\r\n\r\n";
			$msg .= __( "Pokud se jedná o omyl, nebo jste o změnu nežádali, jednoduše tento email ignorujte a žádné změne nedojde.", 'eso' ) . "\r\n\r\n";
			$msg .= __( 'Pro nastavení nového hesla prosím klikněte na tento odkaz:', 'eso' ) . "\r\n\r\n";
			$msg .= site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n\r\n";
			$msg .= __( 'Hezký den!', 'eso' ) . "\r\n";
			$msg .= get_bloginfo( 'name' ) . "\r\n";

			return $msg;
		}

		/**
		 * Redirects to the custom password reset page, or the login page
		 * if there are errors.
		 */
		public function redirect_to_custom_password_reset() {
			if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
				// Verify key / login combo
				$user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
				if ( ! $user || is_wp_error( $user ) ) {
					if ( $user && $user->get_error_code() === 'expired_key' ) {
						wp_redirect( eso_get_page_link( 'login' ) . '?login=expiredkey' );
					} else {
						wp_redirect( eso_get_page_link( 'login' ) . '?login=invalidkey' );
					}
					exit;
				}

				$redirect_url = eso_get_page_link( 'set-password' );
				$redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
				$redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );

				wp_redirect( $redirect_url );
				exit;
			}
		}

		/**
		 * Resets the user's password if the password reset form was submitted.
		 */
		public function do_password_reset() {
			if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
				$rp_key   = $_REQUEST['rp_key'];
				$rp_login = $_REQUEST['rp_login'];

				$user = check_password_reset_key( $rp_key, $rp_login );

				if ( ! $user || is_wp_error( $user ) ) {
					if ( $user && $user->get_error_code() === 'expired_key' ) {
						wp_redirect( eso_get_page_link( 'login' ) . '?login=expiredkey' );
					} else {
						wp_redirect( eso_get_page_link( 'login' ) . '?login=invalidkey' );
					}
					exit;
				}

				if ( isset( $_POST['pass1'] ) ) {
					if ( $_POST['pass1'] != $_POST['pass2'] ) {
						// Passwords don't match
						$redirect_url = eso_get_page_link( 'set-password' );

						$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
						$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
						$redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );

						wp_redirect( $redirect_url );
						exit;
					}

					if ( empty( $_POST['pass1'] ) ) {
						// Password is empty
						$redirect_url = eso_get_page_link( 'set-password' );

						$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
						$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
						$redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );

						wp_redirect( $redirect_url );
						exit;
					}

					// Parameter checks OK, reset password
					reset_password( $user, $_POST['pass1'] );
					wp_redirect( eso_get_page_link( 'login' ) . '?password=changed' );
				} else {
					echo "Invalid request.";
				}

				exit;
			}
		}

		/**
		 * @since 2019.6
		 */
		public function send_reset_password_notification( $user_id ) {
			$userData   = get_userdata( $user_id );
			$user_email = $userData->user_email;

			write_log( "Password reset for user " . $user_email . " manually send from admin area" );

			$email = new Eso_Email( $user_email, "reset-password", $user_id );
			$email->send();
		}
	}
}

$eso_login = new Eso_Login();