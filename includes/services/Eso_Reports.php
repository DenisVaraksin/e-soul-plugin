<?php
if ( ! class_exists( 'Eso_Reports', false ) ) {

	/**
	 * @since 2019.7
	 *
	 * Class Eso_Report
	 */
	class Eso_Reports {

		public function __construct() {
		}

		/**
		 * @since 2019.5
		 * @since 2019.7 moved to Eso_Reports
		 *
		 * @param int $number_of_weeks
		 *
		 * @return WP_Query
		 */
		public function get_orders_between( DateTime $from, DateTime $till ) {
			$orders = new WP_Query( [
				"post_per_page" => - 1,
				"post_type"     => "esoul_order",
				"post_status"   => "publish",
				'date_query'    => [
					[
						"after"     => $from->format( "Y-m-d H:i:s" ),
						"before"    => $till->format( "Y-m-d H:i:s" ),
						"inclusive" => true
					]
				]
			] );

			return $orders;
		}
	}
}