<?php
/**
 * @since 0.0.23
 */
function eso_get_all_shipping_methods() {
	global $wpdb;

	$shipping_methods = $wpdb->get_results( "SELECT code, name, message FROM " . ESO_SHIPPING_METHOD_TABLE );

	$shipping_methods_arr = [];

	foreach ( $shipping_methods as $row ) {
		$shipping_method = new Eso_Shipping_Method( $row->code );

		$shipping_methods_arr[ $row->code ] = $shipping_method;
	}

	return $shipping_methods_arr;
}

/**
 * @since 0.0.23
 */
function eso_get_custom_shipping_method( $custom_shipping_method_code ) {
    global $wpdb;

    $result = $wpdb->get_row( "SELECT * FROM " . ESO_SHIPPING_METHOD_TABLE . " WHERE code = '" . $custom_shipping_method_code . "'" );

    if ( $result ) {
        $custom_code = $result->code;
        $custom_name = $result->name;
        $custom_prices = unserialize($result->default_prices);

        $shipping_method_arr = [
            //"name"            => $custom_name,
            "prices"          => $custom_prices,
            "payment_methods" => [
                "bank_transfer" => [
                    "prices" => []
                ]
            ]
        ];
        return $shipping_method_arr;
    }
    return NULL;
}

/**
 * @since 2019.8
 *
 * @return array
 */
function eso_get_enabled_shipping_methods() {
	global $wpdb;

	$shipping_methods = $wpdb->get_results( "SELECT code, name, message FROM " . ESO_SHIPPING_METHOD_TABLE . " WHERE enabled = 1" );

	$shipping_methods_arr = [];

	foreach ( $shipping_methods as $row ) {
		$shipping_method = new Eso_Shipping_Method( $row->code );

		$shipping_methods_arr[ $row->code ] = $shipping_method;
	}

	return $shipping_methods_arr;
}

/**
 * @since 0.0.22
 * @return array
 */
function eso_get_all_shipping_zones( $include_enabled = true ) {
	global $wpdb;

	$shipping_zones = $wpdb->get_results( "SELECT name, code, shipping_methods, enabled FROM " . ESO_SHIPPING_ZONE_TABLE );

	foreach ( $shipping_zones as $zone ) {
		if ( $include_enabled ) {
			$shipping_zones_arr[ $zone->code ] = $zone->name;
		} else {
			if ( ! $zone->enabled ) {
				$shipping_zones_arr[ $zone->code ] = $zone->name;
			}
		}

	}

	return $shipping_zones_arr;
}

/**
 * @since 0.0.22
 * @return array
 */
function eso_get_enabled_shipping_zones() {
	global $wpdb;

	$shipping_zones = $wpdb->get_results( "SELECT name, code, shipping_methods FROM " . ESO_SHIPPING_ZONE_TABLE . " WHERE enabled = 1" );

	$shipping_zones_arr = [];

	foreach ( $shipping_zones as $zone ) {
		$shipping_zones_arr[ $zone->code ] = $zone->name;
	}

	return $shipping_zones_arr;
}

/**
 * @since 0.0.4
 * @return array
 */
function eso_get_all_currencies() {
	$all_currencies = get_option( "esoul_currencies" );

	$currencies_objs = [];

	foreach ( $all_currencies as $currency_code => $currency ) {
		$currency_obj = new Eso_Currency( $currency_code );

		$currency_obj->set_symbol( $currency["symbol"] );
		$currency_obj->set_enabled( $currency["enabled"] );

		$currencies_objs[] = $currency_obj;
	}

	return $currencies_objs;
}

/**
 * @since 0.0.4
 * @return array
 */
function eso_get_enabled_currencies( $exclude_active = false ) {
	$all_currencies = eso_get_all_currencies();

	$available_currencies = [];

	/* @var $currency Eso_Currency */
	foreach ( $all_currencies as $currency ) {

		if ( $exclude_active ) {
			if ( $currency->is_enabled() && ! $currency->is_active() ) {
				$available_currencies[ $currency->get_code() ] = $currency;
			}
		} else {
			if ( $currency->is_enabled() ) {
				$available_currencies[ $currency->get_code() ] = $currency;
			}
		}
	}

	return $available_currencies;
}

/**
 * @return mixed|null
 *
 * @since 0.0.17
 */
function eso_get_active_currency() {
	$session = new Eso_Session( eso_session_token() );

	return $session->get_active_currency();
}

/**
 * @return mixed
 *
 * @since 2019.5
 */
function eso_get_active_currency_code() {
	$session = new Eso_Session( eso_session_token() );

	return $session->get_active_currency()->get_code();
}

/**
 * @return string
 *
 * @since 2019.5
 */
function eso_get_active_currency_symbol() {
	$session = new Eso_Session( eso_session_token() );

	return $session->get_active_currency()->get_symbol();
}

/**
 * @param $language_code
 *
 * @since 0.0.17
 * @return false|int
 */
function eso_set_active_currency( $language_code ) {
	$session = new Eso_Session( eso_session_token() );

	$currency = new Eso_Currency( $language_code );

	if ( $currency->is_enabled() ) {
		return $session->set_value( 'currency', $language_code );
	}

	return $session->set_value( 'currency', eso_get_default_currency_name() );
}

/**
 * Returns currency entity
 * @return mixed
 */
function eso_get_default_currency() {
	$name = eso_get_default_currency_name();

	$available_currencies = eso_get_enabled_currencies();

	if ( isset( $available_currencies[ $name ] ) ) {
		return $available_currencies[ $name ];
	}

	return reset( $available_currencies );
}

/**
 * @return string
 */
function eso_get_default_currency_name() {
	$option = get_option( "esoul_default_currency" );

	return $option;
}

/**
 * Tax rates are hardcoded for now
 *
 * @since 2019.7 array moved to separate function
 *
 * @return array
 */
function eso_get_tax_rates_array() {
	$rates = [];

	$rates[1] = [
		"id"    => 1,
		"name"  => __( "Standardní sazba DPH", "eso" ),
		"value" => 21
	];

	$rates[2] = [
		"id"    => 2,
		"name"  => __( "Snížená sazba DPH", "eso" ),
		"value" => 15
	];

	$rates[3] = [
		"id"    => 3,
		"name"  => __( "2. Snížená sazba DPH", "eso" ),
		"value" => 10
	];

	$rates[4] = [
		"id"    => 4,
		"name"  => __( "Nulová sazba DPH", "eso" ),
		"value" => 0
	];

	return $rates;
}

function eso_get_tax_rates() {

	$rates = eso_get_tax_rates_array();

	$rateObjs = [];

	foreach ( $rates as $rate ) {
		$rateObj = new Eso_Tax_Rate( $rate["id"] );

		$rateObj->set_name( $rate["name"] );
		$rateObj->set_value( $rate["value"] );

		$rateObjs[] = $rateObj;
	}

	return $rateObjs;
}

/**
 * @since 0.0.31
 */
function eso_get_all_payment_methods() {
	global $wpdb;

	$payment_methods     = $wpdb->get_results( "SELECT code, name, message FROM " . ESO_PAYMENT_METHOD_TABLE );
	$payment_methods_arr = [];

	foreach ( $payment_methods as $payment_method ) {
		$payment_methods_arr[ $payment_method->code ]["code"]    = $payment_method->code;
		$payment_methods_arr[ $payment_method->code ]["name"]    = $payment_method->name;
		$payment_methods_arr[ $payment_method->code ]["message"] = $payment_method->message;
		$payment_methods_arr[ $payment_method->code ]["enabled"] = $payment_method->enbled;
	}

	return $payment_methods;

}

/**
 * @since 2019.7
 *
 * @return object|null
 */
function eso_get_enabled_payment_methods() {
	global $wpdb;

	$payment_methods     = $wpdb->get_results( "SELECT code, name, message, enabled FROM " . ESO_PAYMENT_METHOD_TABLE . " WHERE enabled = 1");

	return $payment_methods;
}
