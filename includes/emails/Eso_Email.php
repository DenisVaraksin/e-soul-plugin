<?php
if ( ! class_exists( 'Eso_Email', false ) ) {
	/**
	 * Class Eso_Email
	 *
	 * @since 2019.6
	 */
	class Eso_Email {

		private $send_to;
		private $template;

		/**
		 * @var null    Represents ID of order
		 */
		private $object_id;

		public function __construct( $send_to, $template_code, $object_id = null ) {
			$this->send_to   = $send_to;
			$this->template  = $template_code;
			$this->object_id = $object_id;
		}

		private function get_send_to() {
			$email = sanitize_email( $this->send_to );

			return $email;
		}

		private function get_domain() {
			$permalink = get_site_url();
			$find      = array( 'http://', 'https://', "www." );
			$replace   = '';

			return str_replace( $find, $replace, $permalink );
		}

		private function get_headers() {
			$store_name = get_bloginfo( 'name' );

			$headers[] = "Content-Type: text/html; charset=UTF-8";
			$headers[] = "From: {$store_name} <info@{$this->get_domain()}>";

			/**
			 * @TODO
			 */
//			$headers[] = "Reply-To: Person Name <person.name@example.com>";

			return $headers;
		}

		public function get_template_code() {
			return $this->template;
		}

		/**
		 * @return Eso_Email_Template
		 */
		public function get_email_template() {
			$template = new Eso_Email_Template( $this->get_template_code() );

			return $template;
		}

		/**
		 * @return bool
		 */
		public function send() {
			$mail = wp_mail( $this->get_send_to(), $this->get_email_template()->get_subject(), $this->get_email_template()->get_message( $this->object_id ), $this->get_headers() );

			return $mail;
		}

	}
}