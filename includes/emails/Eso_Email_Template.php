<?php
if ( ! class_exists( 'Eso_Email_Template', false ) ) {
	/**
	 * Class Eso_Email_Template
	 *
	 * @since 2019.6
	 *
	 * @see Eso_Email for actually sending an email
	 */
	class Eso_Email_Template {
		public function __construct( $code ) {
			$this->code = $code;

			$options = get_option( "eso_email_templates" );

			if ( isset( $options[ $this->get_code() ] ) ) {
				$this->options = $options[ $this->get_code() ];
			} else {
				write_log( "Accessing wrong email template " . $this->get_code(), true );
				die;
			}
		}

		public function get_code() {
			return $this->code;
		}

		/**
		 * @return bool
		 */
		public function is_enabled() {
			if ( isset( $this->options["enabled"] ) && $this->options["enabled"] == 1 ) {
				return true;
			}

			return false;
		}

		/**
		 * @return string|null
		 */
		public function get_subject() {
			if ( isset( $this->options["subject"] ) ) {
				return $this->options["subject"];
			}

			return null;
		}

		/**
		 * @return string|null
		 */
		public function get_type() {
			if ( isset( $this->options["type"] ) ) {
				return $this->options["type"];
			}

			return null;
		}

		/**
		 * @return bool
		 */
		public function is_for_customer() {
			if ( $this->get_type() == "customer" ) {
				return true;
			}

			return false;
		}

		/**
		 * @return bool
		 */
		public function is_for_admin() {
			if ( $this->get_type() == "admin" ) {
				return true;
			}

			return false;
		}

		/**
		 * @since 2020.1.2 check also plugin directory
		 * @return bool|string
		 * @TODO implement and test eso_get_template()
		 */
		public function get_message( $object_id ) {

			//Child template first
			if ( file_exists( get_stylesheet_directory() . "/templates/emails/" . $this->get_code() . ".php" ) ) {
				ob_start();
				require( get_stylesheet_directory() . "/templates/emails/" . $this->get_code() . ".php" );
				$content = ob_get_contents();
				ob_get_clean();

				return $content;
			}

			//Parent template second
			if ( file_exists( get_template_directory() . "/templates/emails/" . $this->get_code() . ".php" ) ) {
				ob_start();
				require( get_template_directory() . "/templates/emails/" . $this->get_code() . ".php" );
				$content = ob_get_contents();
				ob_get_clean();

				return $content;
			}

			//Lastly
			if ( file_exists( ESO_DIR . "/templates/emails/" . $this->get_code() . ".php" ) ) {
				ob_start();
				require( ESO_DIR . "/templates/emails/" . $this->get_code() . ".php" );
				$content = ob_get_contents();
				ob_get_clean();

				return $content;
			}

			write_log("get_message with object_id " . $object_id . " not found");
			return false;
		}

	}
}