<?php
/**
 * @since 2019.6
 * @see Eso_Customer->render_quick_look()
 *
 * @var $this Eso_Customer
 */
$fields = new Eso_Admin_Fields();
?>
<div class="quick-look-close">
    <?php echo eso_icon("close") ?>
</div>
<div class="container">
    <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md-12">
                    <h3><?php _e( "Základní údaje", "eso" ); ?></h3>
                </div>
            </div>
			<?php $fields->box_row( "ID", $this->get_id() ) ?>
			<?php $fields->box_row( "Jméno", $this->get_full_name() ) ?>
			<?php $fields->box_row( "Email", $this->get_email() ) ?>
			<?php $fields->box_row( "Telefon", $this->get_phone() ) ?>
            <div class="row">
                <div class="col-md-12">
                    <h3><?php _e( "Dodací adresa", "eso" ) ?></h3>
                </div>
            </div>
			<?php $fields->box_row( "Ulice", $this->get_shipping_street() ) ?>
			<?php $fields->box_row( "Město", $this->get_shipping_city() ) ?>
			<?php $fields->box_row( "PSČ", $this->get_shipping_postcode() ) ?>
        </div>
		<?php if ( $this->is_billing_on() ) : ?>
            <div class="col-md">
                <div class="row">
                    <div class="col-md-12">
                        <h3><?php _e( "Fakturační údaje", "eso" ) ?></h3>
                    </div>
                </div>
				<?php $fields->box_row( "Jméno", $this->get_billing_company_name() ) ?>
				<?php $fields->box_row( "IČ", $this->get_billing_ico() ) ?>
				<?php $fields->box_row( "DIČ", $this->get_billing_dic() ) ?>
				<?php $fields->box_row( "Ulice", $this->get_billing_street() ) ?>
				<?php $fields->box_row( "Město", $this->get_billing_city() ) ?>
				<?php $fields->box_row( "PSČ", $this->get_billing_postcode() ) ?>
            </div>
		<?php endif; ?>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="mb-4"><?php echo $this->get_turnover() ?></div>
            <a href="<?php $this->the_edit_url() ?>" class="btn btn-primary btn-lg"><?php echo eso_icon("info") . " " . __("Detail profilu", "eso") ?></a>
        </div>
    </div>
</div>
