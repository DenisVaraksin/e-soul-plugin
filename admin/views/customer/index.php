<?php
function eso_customer_index() {
	$fields       = new Eso_Fields();
	$admin_fields = new Eso_Admin_Fields();
	$admin_query  = new Eso_Admin_Query();
	?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <h1><?php _e( "Všichni zákazníci", "eso" ) ?></h1>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo current_url() ?>&generateXls=true"
                   class="btn btn-secondary btn-xls float-sm-right btn-lg mt-2 mt-md-0">
					<?php _e( "Export", "eso" ) ?></a>
            </div>
        </div>
    </div>
    <form class="form-inline index-filter">
        <input type="hidden" name="page" value="eso-customers"/>
		<?php
		( ! empty( $_GET["search"] ) ) ? $search = $_GET["search"] : $search = null;
		$fields->form_group_input( "search", "Zákazník", $search, $search, "text", [
			"placeholder" => __( "Jméno / Email", "eso" ),
			"class"       => "form-control--short"
		] );
		?>

		<?php if ( ! empty( eso_get_customer_groups( true ) ) ) : ?>
            <div class="form-group">
                <label for="customer_group" class="eso-box__label">
					<?php _e( "Skupina", "eso" ) ?>
                </label>
				<?php
				( ! empty( $_GET["customer_group"] ) ) ? $customer_group = (int) $_GET["customer_group"] : $customer_group = null;

				$admin_fields->render_customer_group_select( true, $customer_group, false ); ?>
            </div>
		<?php endif;

		( ! empty( $_GET["date_from"] ) ) ? $from = $_GET["date_from"] : $from = null;

		( ! empty( $_GET["date_to"] ) ) ? $to = $_GET["date_to"] : $to = null;

		$fields->form_group_input( "date_from", "Datum", $from, $from, "date", [ 'placeholder' => __( "Od", "eso" ) ] );
		$fields->form_group_input( "date_to", "–", $to, $to, "date", [ 'placeholder' => __( "Do", "eso" ) ] );

		( ! empty( $_GET["turnover_from"] ) ) ? $turnover_from = $_GET["turnover_from"] : $turnover_from = null;
		$fields->form_group_input( "turnover_from", "Tržby od", $turnover_from, $turnover_from, "number", [
			"class"       => "form-control--short has-pencil",
			"min"         => 1,
			"placeholder" => "Kč"
		] );

		?>
        <button type="submit" class="btn btn-primary btn-lg">
			<?php echo eso_icon( "filter" ) . " " . __( "Filtrovat", "eso" ) ?>
        </button>

		<?php eso_index_filter_reset( "eso-customers" ) ?>

    </form>

    <form id="bulk-form" class="admin-ajax-form mb-2">
        <input type="hidden" name="action" value="eso_admin_ajax"/>
        <input type="hidden" name="eso_action" value="index_bulk_actions"/>
        <input type="hidden" name="eso_success_redirect" value=""/>
		<?php $admin_fields->render_customer_group_select( false, null, true, "inputs[customer-group]" ); ?>
        <button type="submit" class="btn btn-link btn-primary"><?php _e( "Provést změny", "eso" ) ?></button>
    </form>

	<?php
	$customers = $admin_query->customer_index();

	if ( ! empty( $customers->get_results() ) ) {
		$order = $admin_query->switch_ordering();
		?>
        <table class="table table-responsive-xl table-hovered table-striped text-nowrap">
        <thead>
        <tr>
            <th scope="col">
                <input type="checkbox" id="bulk-select" title="Vybrat vše">
            </th>
            <th scope="col">
                <a href="<?php echo current_url() ?>&orderby=display_name&order=<?php echo $order ?>">
					<?php _e( "Jméno", "eso" ) ?><?php echo eso_sort_icon(); ?>
                </a>
            </th>
            <th scope="col">
                <a href="<?php echo current_url() ?>&orderby=email&order=<?php echo $order ?>">
					<?php _e( "Email", "eso" ) ?><?php echo eso_sort_icon(); ?>
                </a>
            </th>
            <th scope="col">
				<?php _e( "Telefon", "eso" ) ?>
            </th>
            <th scope="col">
                <a href="<?php echo current_url() ?>&orderby=registered&order=<?php echo $order ?>">
					<?php _e( "Datum reg.", "eso" ) ?><?php echo eso_sort_icon(); ?>
                </a>
            </th>
            <th scope="col">
                <a href="<?php echo current_url() ?>&orderby=meta_value_num&meta_key=order_count&order=<?php echo $order ?>">
					<?php _e( "Objednávky", "eso" ) ?><?php echo eso_sort_icon(); ?>
                </a>
            </th>
            <th scope="col">
                <a href="<?php echo current_url() ?>&orderby=meta_value_num&meta_key=turnover_CZK&order=<?php echo $order ?>">
					<?php _e( "Tržby", "eso" ) ?><?php echo eso_sort_icon(); ?>
                </a>
            </th>
            <th scope="col">
                <a href="<?php echo current_url() ?>&orderby=meta_value_num&meta_key=group&order=<?php echo $order ?>">
					<?php _e( "Skupina", "eso" ) ?><?php echo eso_sort_icon(); ?>
                </a>
            </th>
            <th scope="col">
				<?php _e( "Akce", "eso" ) ?>
            </th>
        </tr>
        </thead>
        <tbody>
		<?php
		foreach ( $customers->get_results() as $customer_obj ) {
			$customer = new Eso_Customer( $customer_obj->ID );
			?>
            <tr id="customer-<?php echo $customer->get_id() ?>">
                <td>
                    <input type="checkbox" value="<?php echo $customer->get_id() ?>" name="items[]"
                           title="<?php _e( "Zaškrtnout", "eso" ) ?>" form="bulk-form">
                </td>
                <td>
                    <a href="<?php $customer->the_edit_url() ?>"><?php echo $customer->get_full_name() ?>
                </td>
                <td>
					<?php echo $customer->get_email() ?>
                </td>
                <td>
					<?php echo $customer->get_phone(); ?>
                </td>
                <td>
					<?php echo $customer->get_date_registered(); ?>
                </td>
                <td>
					<?php echo $customer->get_order_count() ?>
                </td>
                <td>
                    <form class="admin-ajax--onready">
                        <div class="admin-ajax-result">
                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                            <input type="hidden" name="eso_action" value="get_customer_turnover"/>
                            <input type="hidden" name="customer_id" value="<?php echo $customer->get_id() ?>"/>
                            <input type="hidden" name="currency_code" value="CZK"/>
                    </form>
                </td>
                <td>
					<?php echo $customer->get_group_name() ?>
                </td>
                <td>
                    <ul class="table-actions actions">
                        <li>
                            <a href="<?php $customer->the_edit_url() ?>"
                               data-toggle="tooltip"
                               title="<?php _e( "Detail zákazníka", "eso" ) ?>"
                            >
								<?php echo eso_icon( "eye" ) ?>
                            </a>
                        </li>
                        <li>
                            <form class="admin-ajax-form"
                                  data-confirm="<?php echo sprintf( __( "Smazáním zákazníka dojde i ke smazání jeho objednávek (%d). Nejde to vrátit zpět. Skutečně jste si jisti?", "eso" ), $customer->get_order_count() ) ?>">
                                <input type="hidden" name="action" value="eso_admin_ajax">
                                <input type="hidden" name="eso_action" value="delete_customer">
                                <input type="hidden" name="customer_id" value="<?php echo $customer->get_id() ?>">
                                <input type="hidden" name="eso_success_redirect" value="
							<?php echo current_url() ?>"/>
                                <button type="submit" class="btn btn-link"
                                        data-toggle="tooltip"
                                        title="<?php _e( "Odstranit zákazníka", "eso" ) ?>"
                                >
									<?php echo eso_icon( "trash" ) ?>
                                </button>
                            </form>
                        </li>
                    </ul>
                </td>

            </tr>
		<?php } ?>
        </tbody>
        </table><?php
		$admin_query->pagination( ceil( $customers->get_total() / 20 ) );
	} else {
		echo "<p>";
		if ( ! empty( $_GET["customer_group"] ) || ! empty( $_GET["date_from"] ) || ! empty( $_GET["date_to"] ) || ! empty( $_GET["search"] ) ) {
			_e( "Zadaným filtrům neodpovídají žádní zákazníci", "eso" );
			echo "<br><br><a class='btn btn-primary' href='" . admin_url( "admin.php?page=eso-customers" ) . "'>" . __( "Resetovat parametry vyhledávání", "eso" ) . "</a>";
		} else {
			_e( "Zatím žádní zákazníci.", "eso" );
		}
		echo "</p>";
	}
}