<?php
function eso_customer_groups_page() {
    $fields = new Eso_Fields();
    $admin_tables = new Eso_Admin_Tables();
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1><?php _e( "Skupiny zákazníků", "eso" ) ?></h1>
            </div>
        </div>
        <div class="row">
			<?php
			if ( eso_has_customer_groups() ) { ?>
                <div class="col-lg-6">
                    <div class="eso-box mb-default" id="customer-groups-table">
                        <?php $admin_tables->render_customer_groups_table() ?>
                    </div>
                    <h1><?php _e("Vytvořit novou skupinu", "eso") ?></h1>
                    <div class="eso-box">
                        <form class="admin-ajax-form">
                            <input type="hidden" name="action" value="eso_admin_ajax" />
                            <input type="hidden" name="eso_action" value="insert_customer_group" />
                            <input type="hidden" name="eso_success_callback" value="render_customer_groups_table" />
                            <input type="hidden" name="eso_callback_target" value="#customer-groups-table" />
                            <?php $fields->form_group_input("group_name", "Název skupiny", null) ?>
                            <button type="submit" class="btn btn-primary btn-lg"><?php _e("Vytvořit", "eso") ?></button>
                        </form>
                    </div>
                </div>
			<?php } ?>
        </div>
    </div>
	<?php
}

