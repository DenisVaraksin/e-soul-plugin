<?php
function eso_customer_detail_page() {
	if ( empty( $_GET["customer-id"] ) ) { ?>
        <script>window.location = "<?php echo admin_url( 'admin.php?page=eso-customers' ); ?>";</script>
	<?php }

	$customer = new Eso_Customer( (int) $_GET["customer-id"] );
	$fields   = new Eso_Admin_Fields();

	if ( ! $customer ) { ?>
        <h3><?php _e( "Tento zákazník již neexistuje", "eso" ); ?></h3>
		<?php echo "<a class='btn btn-primary btn-lg' href='" . admin_url( "admin.php?page=eso-customers" ) . "'>" . __( "Výpis zákazníků", "eso" ) . "</a>";
	} else {
		?>
        <div id="customer-detail" class="container-fluid">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <h1><?php echo $customer->get_full_name() ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xl-4 mb-default">
                    <div class="eso-box inline">
                        <div class="eso-box__title">
							<?php echo eso_icon( "id-card", "eso-box__icon" ); ?>
                            <h3 class="eso-box__heading"><?php _e( "Zákazník", "eso" ) ?></h3>
                        </div>
                        <div class="eso-box__rows">
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="eso-box__label"><?php _e( "ID", "eso" ) ?></span>
                                </div>
                                <div class="col-sm-8">
									<?php echo $customer->get_id() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="eso-box__label"><?php _e( "Zákazník", "eso" ) ?></span>
                                </div>

                                <div class="col-sm-8">
									<?php echo $customer->get_full_name() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="eso-box__label"><?php _e( "Email", "eso" ) ?></span>
                                </div>

                                <div class="col-sm-8">
									<?php echo $customer->get_email() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="eso-box__label"><?php _e( "Telefon", "eso" ) ?></span>
                                </div>

                                <div class="col-sm-8">
									<?php echo $customer->get_phone() ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="eso-box__label"><?php _e( "Skupina", "eso" ) ?></span>
                                </div>
                                <div class="col-sm-8">
                                    <form class="admin-ajax-form--onchange">
                                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                                        <input type="hidden" name="eso_action" value="update_customer_group"/>
                                        <input type="hidden" name="customer_id"
                                               value="<?php echo $customer->get_id() ?>"/>
										<?php $fields->render_customer_group_select( false, $customer->get_group_id() ) ?>
                                    </form>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="eso-box__label">
							            <?php _e( "Reset hesla", "eso" ) ?>
                                    </span>
                                </div>
                                <div class="col-sm-8">
                                    <form class="admin-ajax-form"
                                          data-confirm="<?php _e( "Odešle pokyny o nastavení nového hesla na email zákazníka. Jste si jisti?", "eso" ) ?>">
                                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                                        <input type="hidden" name="eso_action" value="reset_customer_password">
                                        <input type="hidden" name="customer_id"
                                               value="<?php echo $customer->get_id() ?>">
                                        <button class="btn btn-link pl-0">
											<?php _e( "Poslat nové heslo", "eso" ) ?>
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 mb-default">
                    <div class="eso-box inline">
                        <form class="form-dynamic admin-ajax-form">
                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                            <input type="hidden" name="eso_action" value="customer_fields_update">
                            <input type="hidden" name="customer" value="<?php echo $customer->get_id() ?>"/>
                            <div class="eso-box__title">
								<?php echo eso_icon( "faktura", "eso-box__icon" ); ?>
                                <h3 class="eso-box__heading"><?php _e( "Fakturační údaje", "eso" ) ?></h3>
                                <button type="button"
                                        class="btn btn-link dynamic-edit float-right"><?php echo eso_icon( "edit-interface-sign" ) ?></button>
                            </div>

							<?php
							$fields->dynamic_row_input( "fields[billing_company_name]", "Jméno", $customer->get_billing_company_name() );
							$fields->dynamic_row_input( "fields[billing_ico]", "IČ", $customer->get_billing_ico() );
							$fields->dynamic_row_input( "fields[billing_dic]", "DIČ", $customer->get_billing_dic() );
							$fields->dynamic_row_input( "fields[billing_street]", "Ulice", $customer->get_billing_street() );
							$fields->dynamic_row_input( "fields[billing_city]", "Město", $customer->get_billing_city() );
							$fields->dynamic_row_input( "fields[billing_postcode]", "PSČ", $customer->get_billing_postcode() );
							$fields->dynamic_row_select( "fields[billing_country]", "Stát", "all_countries", $customer->get_billing_country_code(), [ "readonly" => "readonly" ] );
							?>

                            <div class="row">
                                <div class="col text-center">
                                    <button type="submit"
                                            class="btn btn-primary invisible"><?php _e( "Uložit", "eso" ) ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4  mb-default">
                    <div class="eso-box inline">
                        <form class="form-dynamic admin-ajax-form">
                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                            <input type="hidden" name="eso_action" value="customer_fields_update">
                            <input type="hidden" name="customer" value="<?php echo $customer->get_id() ?>"/>
                            <div class="eso-box__title">
								<?php echo eso_icon( "pin", "eso-box__icon" ); ?>
                                <h3 class="eso-box__heading"><?php _e( "Dodací adresa", "eso" ) ?></h3>
                                <button type="button"
                                        class="btn btn-link dynamic-edit float-right"><?php echo eso_icon( "edit-interface-sign", false ) ?></button>
                            </div>

							<?php
							$fields->dynamic_row_input( "fields[shipping_first_name]", "Jméno", $customer->get_shipping_first_name() );
							$fields->dynamic_row_input( "fields[shipping_last_name]", "Příjmení", $customer->get_shipping_last_name() );
							$fields->dynamic_row_input( "fields[shipping_street]", "Ulice", $customer->get_shipping_street() );
							$fields->dynamic_row_input( "fields[shipping_city]", "Město", $customer->get_shipping_city() );
							$fields->dynamic_row_input( "fields[shipping_postcode]", "PSČ", $customer->get_shipping_postcode() );
							$fields->dynamic_row_select( "fields[shipping_country]", "Stát", "all_countries", $customer->get_shipping_country_code(), [ "readonly" => "readonly" ] );
							?>

                            <div class="row">
                                <div class="col text-center">
                                    <button type="submit"
                                            class="btn btn-primary invisible"><?php _e( "Uložit", "eso" ) ?></button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 mb-default">
                    <div class="eso-box">
                        <div class="eso-box__title">
							<?php echo eso_icon( "market", "eso-box__icon" ); ?>
                            <h3 class="eso-box__heading"><?php _e( "Údaje e-shopu", "eso" ) ?></h3>
                        </div>
                        <div class="eso-box__rows">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="eso-box__label"><?php _e( "Registrace", "eso" ) ?></span>
                                </div>

                                <div class="col-sm-6">
									<?php echo $customer->get_date_registered() ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="eso-box__label"><?php _e( "Poslední přihl.", "eso" ) ?></span>
                                </div>
                                <div class="col-sm-6">
									<?php echo $customer->get_last_login( true ) ?>
                                </div>
                            </div>
							<?php if ( $customer->get_last_order_date() ) : ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <span class="eso-box__label"><?php _e( "Posledí obj.", "eso" ) ?></span>
                                    </div>
                                    <div class="col-sm-6">
										<?php echo $customer->get_last_order_date(); ?>
                                    </div>
                                </div>
							<?php endif; ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="eso-box__label"><?php _e( "Počet obj.", "eso" ) ?></span>
                                </div>
                                <div class="col-sm-6">
									<?php echo $customer->get_order_count(); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="eso-box__label"><?php _e( "Tržby", "eso" ) ?></span>
                                </div>
                                <div class="col-sm-6">
                                    <form class="admin-ajax--onready">
                                        <div class="admin-ajax-result">
                                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                                            <input type="hidden" name="eso_action" value="get_customer_turnover"/>
                                            <input type="hidden" name="customer_id"
                                                   value="<?php echo $customer->get_id() ?>"/>
                                            <input type="hidden" name="currency_code"
                                                   value="CZK"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h2><?php _e( "Historie objednávek", "eso" ) ?></h2>
					<?php if ( $customer->has_orders() ) : ?>
                        <table class="table table-responsive-xl table-hovered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th scope="col">
									<?php _e( "ID", "eso" ) ?>
                                </th>
                                <th scope="col">
									<?php _e( "Datum", "eso" ) ?>
                                </th>
                                <th scope="col">
									<?php _e( "Množství prod.", "eso" ) ?>
                                </th>
                                <th scope="col">
									<?php _e( "Stav", "eso" ) ?>
                                </th>
                                <th scope="col">
									<?php _e( "Celková cena", "eso" ) ?>
                                </th>
                                <th scope="col">
									<?php _e( "Doprava", "eso" ) ?>
                                </th>
                                <th scope="col">
									<?php _e( "Platba", "eso" ) ?>
                                </th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php
							/* @var $order Eso_Order */
							foreach ( $customer->get_orders() as $order ) : ?>
                                <tr>
                                    <td><?php echo $order->get_id() ?></td>
                                    <td><?php echo $order->get_date_and_time_created() ?></td>
                                    <td><?php echo count( $order->get_items() ) ?></td>
                                    <td class="order-status--<?php echo $order->get_status_slug() ?>"><?php echo $order->get_status_name() ?></td>
                                    <td><?php echo $order->get_sum_with_active_currency() ?></td>
                                    <td><?php echo $order->get_shipping_method_name() ?></td>
                                    <td><?php echo $order->get_payment_method_name() ?></td>
                                    <td>
                                        <ul class="table-actions actions">
                                            <li>
                                                <a href="<?php $order->the_edit_url() ?>"
                                                   data-toggle="tooltip"
                                                   title="<?php _e( "Detail objednávky", "eso" ) ?>"
                                                ><?php echo eso_icon( "eye" ) ?></a>
                                            </li>
                                            <li class="actions__generate-invoice">
                                                <a href="<?php echo eso_get_invoice_url( $order->get_id() ) ?>"
                                                   target="_blank"
                                                   data-toggle="tooltip"
                                                   title="<?php _e( "Zobrazit fakturu", "eso" ) ?>">
													<?php echo eso_icon( "pdf" ) ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
							<?php endforeach; ?>
                            </tbody>
                        </table>
					<?php else : ?>
                        <p><?php _e( "Zatím žádné objednávky", "eso" ) ?></p>
					<?php endif; ?>
                </div>
            </div>
        </div>


	<?php }

}

