<?php
function eso_plugin_orders_statuses_page() {
    $fields       = new Eso_Fields();
    $admin_tables = new Eso_Admin_Tables();
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1><?php _e( "Stavy objednávek", "eso" ) ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="eso-box mb-default" id="order-status-table">
					<?php $admin_tables->render_order_status_table() ?>
                </div>
                <h1><?php _e( "Vytvořit nový stav", "eso" ) ?></h1>
                <div class="eso-box">
                    <form class="admin-ajax-form">
                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                        <input type="hidden" name="eso_action" value="insert_taxonomy_term"/>
                        <input type="hidden" name="eso_success_callback" value="render_order_status_table"/>
                        <input type="hidden" name="eso_callback_target" value="#order-status-table"/>
                        <input type="hidden" name="taxonomy" value="order_status"/>
						<?php $fields->form_group_input( "term_name", "Název", null ) ?>
                        <button type="submit" class="btn btn-primary btn-lg"><?php _e( "Vytvořit", "eso" ) ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php }