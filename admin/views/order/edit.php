<?php
function eso_plugin_orders_single() {
	$order    = new Eso_Order( $_GET["id"] );
	$customer = $order->get_customer();
	$fields   = new Eso_Admin_Fields();
	?>
    <div class="container-fluid">
        <div class="row mb-4 mt-1">
            <div class="col-xl-3 mb-4 mb-xl-0">
                <h1><?php _e( "Objednávka č. ", "eso" ) ?><?php echo $order->get_id() ?></h1>
            </div>
            <div class="col-xl-9">
                <ul class="single-actions actions text-xl-right">
                    <li class="actions__edit-status">
                        <form class="admin-ajax-form">
                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                            <input type="hidden" name="eso_action" value="edit_order_status"/>
                            <input type="hidden" name="order_id" value="<?php echo $order->get_id(); ?>"/>
                            <span><?php _e( "Stav objednávky", "eso" ) ?>: </span>
							<?php $fields->render_order_status_select( $order->get_status_id() ) ?>
                            <button type="submit"
                                    class="btn btn-link btn-sm"><?php _e( "Změnit stav", "eso" ) ?></button>
                        </form>
                    </li>
                    <?php
                    if ( ! $order->is_paid() ) {  ?>
                        <li class="actions__pay">
                        <?php
                        $payment_method = $order->get_payment_method()->get_code();
                        if ( $payment_method == "gopay" || $payment_method == "comgate_payments" ) { ?>
                            <button id="single-esoul_order-pay" data-order="<?php echo $order->get_id() ?>"
                                    data-method="<?php echo $payment_method ?>" type="button"
                                    class="btn btn-primary"><?php _e( "Platba", "eso" ) ?></button>
                        <?php } ?>
                        </li><?php
                    }
                    ?>
                    <li class="actions__send-invoice">
                        <form class="admin-ajax-form"
                              data-confirm="Potvrzením dojde k odeslání zprávy s odkazem pro stažení faktury na email zákazníka">
                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                            <input type="hidden" name="eso_action" value="send_invoice"/>
                            <input type="hidden" name="id" value="<?php echo $order->get_id() ?>"/>
                            <button type="submit" class="btn btn-text"
                                    title="<?php _e( "Odeslat fakturu", "eso" ) ?>">
								<?php echo eso_icon( "right-arrow" ); ?>
                                <small><?php _e( "Odeslat fakturu", "eso" ) ?></small>
                            </button>
                        </form>
                    </li>
                    <li class="actions__generate-invoice">
                        <a href="<?php echo eso_get_invoice_url( $order->get_id() ) ?>"
                           target="_blank"
                           title="<?php _e( "Zobrazit fakturu", "eso" ) ?>">
							<?php echo eso_icon( "pdf" ) ?>
                            <small><?php _e( "Zobrazit fakturu", "eso" ) ?></small>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <div class="eso-box__title">
						<?php echo eso_icon( "zoom", "eso-box__icon" ) ?>
                        <h3 class="eso-box__heading"><?php _e( "Detail objednávky", "eso" ) ?></h3>
                    </div>
                    <div class="eso-box__rows">
						<?php
						$fields->box_row( "ID", $order->get_id() );
						$fields->box_row( "Datum", $order->get_date_and_time_created() );
						$fields->box_row( "Platba", $order->get_payment_method()->get_name() );
						$fields->box_row( "Měna", $order->get_currency_code() );
						$fields->box_row( "Doprava", $order->get_shipping_method()->get_name() );
						if ( $order->get_note() ) {
							$fields->box_row( "Poznámka", $order->get_note() );
						}
						if($order->get_coupon_code()) {
						    $fields->box_row("Kupon", $order->get_coupon_code());
                        }
						?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <div class="eso-box__title">
						<?php echo eso_icon( "id-card", "eso-box__icon" ) ?>
                        <h3 class="eso-box__heading"><?php _e( "Zákazník", "eso" ) ?></h3>
						<?php if ( ! $customer->is_registered() ) : ?>
                            <span class="eso-box__subheading text-danger"><?php _e( "Neregistrovaný", "eso" ) ?>
                            </span>
						<?php else : ?>
                            <a href="<?php $customer->the_edit_url() ?>"
                               class="eso-box__action"><?php echo eso_icon( "eye" ) ?></a>
						<?php endif; ?>

                    </div>
                    <div class="eso-box__rows">
						<?php
						$fields->box_row( "ID", $order->get_customer_id() );
						$fields->box_row( "Jméno", $customer->get_full_name() );
						$fields->box_row( "Email", $customer->get_email() );
						$fields->box_row( "Telefon", $customer->get_phone() );
						$fields->box_row( "Skupina", $customer->get_group_name() );
						?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <form></form>
                    <form class="form-dynamic admin-ajax-form">
                        <input type="hidden" name="eso_action" value="order_billing_update"/>
                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                        <input type="hidden" name="order_id" value="<?php echo $order->get_id(); ?>"/>
                        <div class="eso-box__title">
							<?php echo eso_icon( "invoice", "eso-box__icon" ) ?>
                            <h3 class="eso-box__heading"><?php _e( "Fakturační údaje", "eso" ) ?></h3>
                            <button type="button"
                                    class="btn btn-link dynamic-edit float-right"><?php echo eso_icon( "edit-interface-sign", false ) ?></button>
                        </div>
                        <div class="eso-box__rows">
							<?php
							$fields->dynamic_row_input( "fields[billing_company_name]", "Jméno", $order->get_billing_company_name() );
							$fields->dynamic_row_input( "fields[billing_ico]", "IČ", $order->get_billing_ico() );
							$fields->dynamic_row_input( "fields[billing_dic]", "DIČ", $order->get_billing_dic() );
							$fields->dynamic_row_input( "fields[billing_street]", "Ulice", $order->get_billing_street() );
							$fields->dynamic_row_input( "fields[billing_city]", "Město", $order->get_billing_city() );
							$fields->dynamic_row_input( "fields[billing_postcode]", "PSČ", $order->get_billing_postcode() );
							$fields->dynamic_row_select( "fields[billing_country]", "Stát", "all_countries", $customer->get_billing_country_code(), [ "readonly" => "readonly" ] );
							?>
                            <div class="row">
                                <div class="col text-center">
                                    <button type="submit"
                                            class="btn btn-primary invisible"><?php _e( "Uložit", "eso" ) ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <div class="eso-box__title">
						<?php echo eso_icon( "pin", "eso-box__icon" ) ?>
                        <h3 class="eso-box__heading"><?php _e( "Dodací adresa", "eso" ) ?></h3>
                    </div>
                    <div class="eso-box__rows">
						<?php
						$fields->box_row( "Jméno", $order->get_shipping_name() );
						$fields->box_row( "Ulice a č. p", $order->get_shipping_street() );
						$fields->box_row( "Město", $order->get_shipping_city() );
						$fields->box_row( "PSČ", $order->get_shipping_postcode() );
						$shipping_country = new Eso_Shipping_Zone( $order->get_shipping_country() );
						$fields->box_row( "Země", $shipping_country->get_name() );
						$fields->box_row( "Email", $order->get_shipping_email() );
						$fields->box_row( "Telefon", $order->get_shipping_phone() );
						?>
                    </div>
                </div>
            </div>
			<?php if ( $order->get_log() ) : ?>
                <div class="col-md-6 col-xl-4 mb-default">
                    <div class="whitebox uibox log-wrap">
                        <h3><?php _e( "Průběh objednávky", "eso" ) ?></h3>
						<?php $order->render_log() ?>
                    </div>
                </div>
			<?php endif; ?>
			<?php if ( $order->get_elogist_order_id() ) : ?>
                <div class="col-md-6 col-xl-4">
                    <div class="whitebox uibox">
                        <h3><?php _e( "Comgate Logistika", "eso" ) ?></h3>
                        <form class="admin-ajax--onready">
                            <div class="admin-ajax-result"></div>
                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                            <input type="hidden" name="eso_action" value="render_comgate_delivery_info"/>
                            <input type="hidden" name="order_id" value="<?php echo $order->get_id() ?>"/>
                        </form>
                    </div>
                </div>
			<?php endif; ?>
        </div>
        <div class="row">
            <div class="col">
                <h2><?php _e( "Produkty" ) ?></h2>
                <table class="table table-responsive-xl table-striped text-nowrap">
                    <thead>
                    <tr>
                        <th scope="col"><?php _e( "ID", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Foto", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Název", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Za kus", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Množství", "eso" ) ?></th>
                        <th scope="col"><?php _e( "Součet", "eso" ) ?></th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
					/* @var $order_item Eso_Order_Item */
					foreach ( $order->get_items() as $product_id => $product_daata ) :
						$order_item = new Eso_Order_Item( $order->get_id(), $product_id );
						if ( ! isset( $order_currency ) ) {
							$order_currency = new Eso_Currency( $order_item->get_currency() );
						}
						?>
                        <tr>
                            <td>
                                <a href="<?php $order_item->get_product()->the_edit_url() ?>"><?php echo $order_item->get_product_id() ?></a>
                            </td>
                            <td><?php $order_item->get_product()->the_featured_image( "table" ) ?></td>
                            <td><?php echo $order_item->get_name() ?></td>
                            <td><?php echo $order_item->get_price_per_piece( $order_currency ) . " " . $order_currency->get_symbol(); ?></td>
                            <td><?php echo $order_item->get_quantity() ?><?php _e( "ks", "eso" ) ?></td>
                            <td><?php echo $order_item->get_sum() . " " . $order_currency->get_symbol() ?></td>
                        </tr>
					<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-6 col-xl-4 offset-md-6 offset-xl-8">
                <div class="eso-box">
					<?php $fields->box_row_info( "Celkem zboží s DPH", $order->get_sum_with_original_currency() ) ?>
					<?php $fields->box_row_info( "Doprava a platba", $order->get_shipping_payment_price( true ) ) ?>
					<?php $fields->box_row_info( "Sleva", "- " . $order->get_discount( true, true ) ) ?>
					<?php $fields->box_row_info( "Celkem bez DPH", $order->get_total_without_tax( true, true ) ) ?>
                    <hr>
					<?php $fields->box_row_info( "Celková cena vč. DPH", $order->get_total( true ) ) ?>
                </div>

                <form class="admin-ajax-form"
                      data-confirm="<?php _e( "Skutečně chcete tuto objednávku smazat?", "eso" ) ?>">
                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                    <input type="hidden" name="eso_action" value="delete_order"/>
                    <input type="hidden" name="order_id" value="<?php echo $order->get_id() ?>"/>

                    <input type="hidden" name="eso_success_redirect" value="
							<?php echo admin_url( "admin.php?page=eso-orders" ) ?>"/>
                    <button type="submit" class="btn btn-text pt-4">
						<?php echo eso_icon( "trash" ) ?><?php _e( "Smazat objednávku", "eso" ) ?>
                    </button>
                </form>
            </div>
        </div>
    </div>
	<?php
}