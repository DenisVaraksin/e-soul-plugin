<?php
/**
 * @since 2019.7
 * @see Eso_Order->render_quick_look()
 *
 * @var $this Eso_Order
 */
$fields = new Eso_Admin_Fields();
?>
<div class="container-fluid">
    <div class="row border-between">
        <div class="col-md">
            <div class="row d-flex align-items-center mb-4">
                <div class="col-md-2">
					<?php echo eso_icon( "id-card" ) ?>
                </div>
                <div class="col-md-10">
					<?php $fields->box_row( "ID", $this->get_id() ) ?>
					<?php $fields->box_row( "Jméno", $this->get_customer()->get_full_name() ) ?>
					<?php $fields->box_row( "Email", $this->get_customer()->get_email() ) ?>
					<?php $fields->box_row( "Telefon", $this->get_customer()->get_phone() ) ?>
					<?php $fields->box_row( "Ulice", $this->get_shipping_street() ) ?>
					<?php $fields->box_row( "Město", $this->get_shipping_city() ) ?>
                </div>
            </div>
            <div class="row d-flex align-items-center mb-4">
                <div class="col-md-2">
					<?php echo eso_icon( "boxes" ) ?>
                </div>
                <div class="col-md-10">
					<?php $fields->box_row( "Doprava", $this->get_shipping_method_name() ) ?>
                </div>
            </div>
            <div class="row d-flex align-items-center mb-4">
                <div class="col-md-2">
					<?php echo eso_icon( "purse" ) ?>
                </div>
                <div class="col-md-10">
					<?php $fields->box_row( "Platba", $this->get_payment_method_name() ) ?>
                </div>
            </div>
        </div>
        <div class="col-md">
            <h3 class="mt-2 mb-3"><?php _e( "Položky", "eso" ) ?></h3>
			<?php if ( ! empty( $this->get_items() ) ) { ?>
                <table class="table table-striped mb-4">
					<?php
					/* @var $item Eso_Order_Item */
					foreach ( $this->get_items() as $item ) { ?>
                        <tr>
                            <td><?php echo $item->get_name() ?></td>
                            <td><?php echo $item->get_quantity() ?>&nbsp;<?php _e( "ks", "eso" ) ?></td>
                            <td><?php echo $item->get_total_with_currency() ?></td>
                        </tr>
					<?php } ?>
                </table>
				<?php
			} else {
				_e( "Tato objednávka nemá žádné položky", "eso" );
			} ?>
	        <?php $fields->box_row_info("Celková cena bez DPH", $this->get_total_without_tax(true, true)) ?>
            <?php $fields->box_row_info("Celková cena s DPH", $this->get_total(true)) ?>
        </div>
    </div>
</div>
