<?php
function eso_orders_page() {
	$fields       = new Eso_Fields();
	$admin_fields = new Eso_Admin_Fields();
	$admin_query  = new Eso_Admin_Query();
	?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <h1><?php _e( "Seznam objednávek", "eso" ) ?></h1>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo current_url() ?>&generateXls=true"
                   class="btn btn-secondary btn-xls float-sm-right btn-lg mt-2 mt-md-0">
					<?php _e( "Export", "eso" ) ?></a>
            </div>
        </div>
    </div>

    <form class="index-filter form-inline" action="" method="get">
        <input type="hidden" name="page" value="eso-orders"/>
		<?php
		if ( ! empty( $_GET["search_id"] ) ) {
			$search_id = (int) $_GET["search_id"];
		} else {
			$search_id = null;
		}
		$fields->form_group_input( "search_id", "ID", $search_id, $search_id, "number", [
			'class'       => 'form-control--digit',
			'placeholder' => '#',
			'min'         => 1
		] ) ?>
        <div class="form-group">
            <label for="stock_status" class="eso-box__label">
				<?php _e( "Stav", "eso" ) ?>
            </label>
			<?php
			( ! empty( $_GET["order_status"] ) ) ? $order_status = (int) $_GET["order_status"] : $order_status = null;

			$admin_fields->render_order_status_select( $order_status, false ); ?>
        </div>

		<?php
		( ! empty( $_GET["date_from"] ) ) ? $from = $_GET["date_from"] : $from = null;

		( ! empty( $_GET["date_to"] ) ) ? $to = $_GET["date_to"] : $to = null;

		$fields->form_group_input( "date_from", "Datum", $from, $from, "date", [ 'placeholder' => __( "Od", "eso" ) ] );
		$fields->form_group_input( "date_to", "–", $to, $to, "date", [ 'placeholder' => __( "Do", "eso" ) ] );

		( ! empty( $_GET["total_from"] ) ) ? $total_from = $_GET["total_from"] : $total_from = null;
		$fields->form_group_input( "total_from", "Cena od", $total_from, $total_from, "number", [
			"class"       => "form-control--digit",
			"min"         => 1,
			"placeholder" => "     Kč"
		] );

        if ( ! empty( $_GET["coupon_code"] ) ) {
            $coupon_code = $_GET["coupon_code"];
        } else {
            $coupon_code = null;
        }
		$fields->form_group_input("coupon_code", "Kupón", $coupon_code, $coupon_code, "varchar", [
		    "minlength" => 2,
        ])

        ?>

        <button type="submit" class="btn btn-primary btn-lg">
			<?php echo eso_icon( "filter" ) . " " . __( "Filtrovat", "eso" ) ?>
        </button>

		<?php eso_index_filter_reset( "eso-orders" ) ?>
    </form>

	<?php
	$orders = $admin_query->posts_index( "esoul_order" );

	if ( $orders->have_posts() ) {
		$order = $admin_query->switch_ordering();
		?>
        <table class="table table-responsive-xl table-hovered table-striped text-nowrap">
            <thead>
            <tr>
                <th scope="col">
                    <a href="<?php echo current_url() ?>&orderby=ID&order=<?php echo $order ?>">
						<?php _e( "ID", "eso" ) ?><?php echo eso_sort_icon(); ?>
                    </a>
                </th>
                <th scope="col">
                    <a href="<?php echo current_url() ?>&orderby=date&order=<?php echo $order ?>">
			            <?php _e( "Datum", "eso" ) ?><?php echo eso_sort_icon(); ?>
                    </a>
                </th>
                <th scope="col">
					<?php _e( "Zákazník", "eso" ) ?>
                </th>
                <th scope="col">
					<?php _e( "E-mail", "eso" ) ?>
                </th>
                <th scope="col">
					<?php _e( "Stav", "eso" ) ?>
                </th>
                <th scope="col">
                    <a href="<?php echo current_url() ?>&orderby=meta_value_num&meta_key=total_CZK&order=<?php echo $order ?>">
						<?php _e( "Částka", "eso" ) ?><?php echo eso_sort_icon(); ?>
                    </a>
                </th>
                <th scope="col">
                    <?php _e( "Kupón", "eso" ) ?>
                </th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
			<?php while ( $orders->have_posts() ) {
				$orders->the_post();
				$order = new Eso_Order( get_the_ID() );
				?>
                <tr>
                    <td>
                        <a href="<?php $order->the_edit_url() ?>">#<?php echo $order->get_id() ?></a>
                    </td>
                    <td><?php echo $order->get_date_and_time_created() ?></td>
                    <td>
                        <a href="<?php $order->get_customer()->the_edit_url() ?>"><?php echo $order->get_customer()->get_full_name() ?></a>
                    </td>
                    <td><?php echo $order->get_shipping_email() ?></td>
                    <td>
						<?php if ( $order->get_elogist_order_id() ) : ?>
                            <form class="admin-ajax--onready">
                                <div class="admin-ajax-result"></div>
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="get_order_status"/>
                                <input type="hidden" name="order_id" value="<?php echo $order->get_id() ?>"/>
                            </form>
						<?php else : ?>
                            <span class="order-status--<?php echo $order->get_status_slug() ?>"><?php echo $order->get_status_name() ?></span>
						<?php endif; ?>
                    </td>
                    <td><?php echo $order->get_total( true ) ?></td>
                    <td><?php echo (!empty($order->get_coupon_code()) ?
                            '<span target="_blank" data-toggle="tooltip" title="'.$order->get_coupon_code().'">'.eso_icon( "info" ).'</span>' : null); ?>
                    </td>
                    <td>
                        <ul class="table-actions actions">
                            <li class="actions__send-invoice">
                                <form class="admin-ajax-form"
                                      data-confirm="Potvrzením dojde k odeslání zprávy s odkazem pro stažení faktury na email zákazníka">
                                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                                    <input type="hidden" name="eso_action" value="send_invoice"/>
                                    <input type="hidden" name="id" value="<?php echo $order->get_id() ?>"/>
                                    <button type="submit" class="btn btn-text"
                                            data-toggle="tooltip"
                                            title="<?php _e( "Odeslat fakturu", "eso" ) ?>"
                                    >
										<?php echo eso_icon( "right-arrow" ); ?>
                                    </button>
                                </form>
                            </li>
                            <li class="actions__generate-invoice">
                                <a href="<?php echo eso_get_invoice_url( $order->get_id() ) ?>"
                                   target="_blank"
                                   data-toggle="tooltip"
                                   title="<?php _e( "Zobrazit fakturu", "eso" ) ?>">
									<?php echo eso_icon( "pdf" ) ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php $order->the_edit_url() ?>"
                                   data-toggle="tooltip"
                                   title="<?php _e( "Detail objednávky", "eso" ) ?>"
                                ><?php echo eso_icon( "edit-interface-sign" ) ?></a>
                            </li>
                            <li>
                                <div class="quick-look-wrapper">
                                    <a href="<?php $order->the_edit_url() ?>" data-toggle="quick-look"
                                       data-id="<?php echo $order->get_id() ?>"
                                       data-type="order">
										<?php echo eso_icon( "eye" ) ?>
                                    </a>
                                </div>
                            </li>
                        </ul>

                    </td>
                </tr>
			<?php } ?>
            </tbody>
        </table>
		<?php
		$admin_query->pagination( $orders->max_num_pages );
		wp_reset_postdata();
	} else {
		echo "<p>";
		if ( ! empty( $_GET["search_id"] ) || ! empty( $_GET["order_status"] ) || ! empty( $_GET["date_from"] ) || ! empty( $_GET["date_to"] ) || ! empty( $_GET["total_from"] ) ) {
			_e( "Zadaným filtrům neodpovídají žádné objednávky", "eso" );
			echo "<br><br><a class='btn btn-primary' href='" . admin_url( "admin.php?page=eso-orders" ) . "'>" . __( "Resetovat parametry vyhledávání", "eso" ) . "</a>";
		} else {
			_e( "Zatím žádné objednávky", "eso" );
		}
		echo "</p>";
		?>
	<?php }

}


