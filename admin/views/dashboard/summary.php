<?php
/* @var DateTime $from
 * @var DateTime $till
 * @var $this Eso_Dashboard
 */
?>
<div class="row">
    <div class="col-md col-sm-6">
        <h4><?php _e( "Hrubé tržby", "eso" ); ?></h4>
        <div class="text-primary">
            <?php echo $this->get_turnover($from, $till) . " Kč";  ?>
        </div>
    </div>
    <div class="col-md col-sm-6">
        <h4><?php _e( "Prům. denní prodej", "eso" ); ?></h4>
        <div class="text-primary">
            <?php echo $this->get_average_daily_turnover($from, $till); ?>
        </div>
    </div>
    <div class="col-md col-sm-6">
        <h4><?php _e( "Počet objednávek", "eso" ); ?></h4>
        <div class="text-primary">
            <?php echo $this->get_number_of_orders($from, $till); ?>
        </div>
    </div>
    <div class="col-md col-sm-6">
        <h4><?php _e( "Prům. hodnota objednávky", "eso" ); ?></h4>
        <div class="text-primary">
            <?php echo $this->get_average_total_of_recent_orders($from, $till) . " Kč"; ?>
        </div>
    </div>
</div>