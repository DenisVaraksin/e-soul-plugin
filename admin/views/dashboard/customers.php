<div class="dashboard-card">
    <h2><?php _e( "Nejnovější zákazníci", "eso" ) ?></h2>
	<?php
	/* @var $users WP_User_Query */ ?>
	<?php if ( $users->get_total() > 0 ) : ?>
        <table class="table table-responsive table-striped text-nowrap">
            <thead>
            <tr>
                <td><?php _e( "Jméno", "eso" ) ?></td>
                <td><?php _e( "Email", "eso" ) ?></td>
                <td><?php _e( "Registrace", "eso" ) ?></td>
                <td><?php _e( "Obrat", "eso" ) ?></td>
                <td><?php _e( "Upravit", "eso" ) ?></td>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach ( $users->get_results() as $user ) :
				$customer = new Eso_Customer( $user->ID );
				?>
                <tr>
                    <td>
                        <a href="<?php $customer->the_edit_url() ?>"><?php echo $customer->get_full_name() ?></a>
                    </td>
                    <td><?php echo $customer->get_email(); ?></td>
                    <td><?php echo $customer->get_date_registered(); ?></td>
                    <td>
                        <form class="admin-ajax--onready">
                            <div class="admin-ajax-result"></div>
                            <input type="hidden" name="action" value="eso_admin_ajax" />
                            <input type="hidden" name="eso_action" value="get_customer_turnover" />
                            <input type="hidden" name="customer_id" value="<?php echo $customer->get_id() ?>" />
                            <input type="hidden" name="currency_code" value="CZK" />
                        </form>
                    </td>
                    <td>
                        <a href="<?php $customer->the_edit_url() ?>"><?php echo eso_icon( "edit-interface-sign" ) ?></a>
                    </td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
        <a href="<?php echo admin_url( "admin.php?page=eso-customers" ) ?>"
           class="btn btn-primary"><?php _e( "Seznam zákazníků", "eso" ) ?></a>
	<?php else : ?>
        <p><?php _e( "Zatím žádné registrace.", "eso" ); ?></p>
	<?php endif; ?>
</div>
