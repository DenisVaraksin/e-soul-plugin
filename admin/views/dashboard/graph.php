<h2><?php _e("Vývoj objednávek", "eso") ?></h2>
<div class="whitebox mb-default">
<?php $admin_fields = new Eso_Admin_Fields(); $admin_fields->render_chart_nav(); ?>
<div class="tab-content">
	<div id="overview">
		<canvas id="overview-graph" width="400" height="300"></canvas>
	</div>
</div>

</div>
