<div class="dashboard-card">
    <h2><?php _e( "Nejprodávanější produkty", "eso" ) ?></h2>
	<?php
	/* @var $products WP_Query */ ?>
	<?php if ( $products->have_posts() ) : ?>
        <table class="table table-responsive table-striped text-nowrap">
            <thead>
            <tr>
                <td><?php _e( "ID", "eso" ) ?></td>
                <td><?php _e( "Název", "eso" ) ?></td>
                <td><?php _e( "Stav", "eso" ) ?></td>
                <td><?php _e( "Cena", "eso" ) ?></td>
                <td><?php _e( "Tržby", "eso" ) ?></td>
                <td><?php _e( "Upravit", "eso" ) ?></td>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach ( $products->get_posts() as $post ) :
				$product = new Eso_Product( $post->ID );
				?>
                <tr>
                    <td>#<?php echo $product->get_id() ?></td>
                    <td>
                        <a href="<?php $product->the_edit_url() ?>"><?php echo $product->get_name() ?></a>
                    </td>
                    <td class="stock-status--<?php echo $product->get_stock_status_slug() ?>"><?php echo $product->get_stock_status_name() ?></td>
                    <td><?php echo $product->get_price( new Eso_Currency("CZK"), true, true ); ?></td>
                    <td>
                        <form class="admin-ajax--onready">
                            <div class="admin-ajax-result"></div>
                            <input type="hidden" name="action" value="eso_admin_ajax"/>
                            <input type="hidden" name="eso_action" value="get_product_turnover"/>
                            <input type="hidden" name="product_id" value="<?php echo $product->get_id() ?>"/>
                            <input type="hidden" name="currency_code" value="CZK" />
                        </form>
                    </td>
                    <td>
                        <a href="<?php $product->the_edit_url() ?>"><?php echo eso_icon( "edit-interface-sign" ) ?></a>
                    </td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
        <a href="<?php echo admin_url( "edit.php?post_type=esoul_product" ) ?>"
           class="btn btn-primary"><?php _e( "Seznam produktů", "eso" ) ?></a>
	<?php else : ?>
        <p><?php _e( "Zatím žádné produkty.", "eso" ); ?></p>
        <a href="<?php echo admin_url( "post-new.php?post_type=esoul_product" ) ?>"
           class="btn btn-primary"><?php _e( "Přidat produkt", "eso" ) ?></a>
	<?php endif; ?>
</div>
