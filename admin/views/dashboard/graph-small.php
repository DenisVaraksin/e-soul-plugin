<h2><?php _e("Vývoj objednávek - posledních 7 dní", "eso") ?></h2>
<div class="whitebox mb-default">
	<?php $admin_fields = new Eso_Admin_Fields(); $admin_fields->render_chart_nav_small(); ?>
	<div class="tab-content">
		<div id="overview" style="height: 192px;">
			<canvas id="overview-graph" class="overview-graph-small" width="400px" height="200px" ></canvas>
		</div>
	</div>

</div>
