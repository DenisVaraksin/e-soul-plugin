<?php /* @var $this Eso_Admin_Dashboard */
$dashboard = new Eso_Dashboard();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12 col-lg-6">
            <?php $dashboard->render_graph() ?>
		</div>
		<div class="col-12 col-lg-6">
			<?php $this->render_orders(); ?>
		</div>

		<div class="col-12">
            <div class="eso-card mb-5">
                <h3 id="summary-range-header" class="text-uppercase text-primary"><?php _e( "Výsledky", "eso") ?>&nbsp;(<span id="summary-range"><?php _e("posledních 7 dní", "eso" ); ?></span>)</h3>
                <div id="dashboard-summary">
                    <form class="admin-ajax--onready">
                        <div class="admin-ajax-result"></div>
                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                        <input type="hidden" name="eso_action" value="render_dashboard_summary"/>
                        <input type="hidden" name="from" value="<?php $date = new DateTime("today -7 days"); echo $date->format("Y-m-d"); ?>"/>
                        <input type="hidden" name="till" value="<?php $date = new DateTime(); echo $date->format("Y-m-d"); ?>"/>
                    </form>
                </div>
            </div>
		</div>

		<div class="col-12 col-lg-6">
			<?php $this->render_customers(); ?>
		</div>
		<div class="col-12 col-lg-6">
			<?php $this->render_products(); ?>
		</div>
	</div>
    <div class="row">
        <div class="col">
            <form class="admin-ajax--onready">
                <div class="admin-ajax-result ajax-result--silent"></div>
                <input type="hidden" name="action" value="eso_admin_ajax" />
                <input type="hidden" name="eso_action" value="board_check" />
            </form>
        </div>
    </div>
</div>