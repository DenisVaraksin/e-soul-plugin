<?php
if ( ! class_exists( 'Eso_Admin_Tables', false ) ) {
	/**
	 * @since 2019.6
	 */
	class Eso_Admin_Tables {
		public function __construct() {

		}

		/**
		 * @since 2019.7
		 */
		public function render_order_status_table() { ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col"><?php _e( "Název", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Počet objednávek", "eso" ) ?></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
				<?php
				/* @var $status Eso_Order_Status */
				foreach ( eso_get_order_statuses() as $status ) { ?>
                    <tr>
                        <td>
                            <form class="admin-ajax-form--onchange">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="rename_order_status"/>
                                <input type="hidden" name="term_id" value="<?php echo $status->get_id() ?>"/>
                                <input type="hidden" name="taxonomy" value="order_status"/>
                                <input type="text" name="term_name"
                                       value="<?php echo $status->get_name() ?>"
                                       title="<?php echo $status->get_name() ?>"
									<?php if ( ! $status->can_be_deleted() ) : ?>
                                        readonly="readonly" class="form-control-plaintext"
									<?php endif; ?>
                                />
                            </form>
                        </td>
                        <td><?php echo $status->get_count() ?></td>
                        <td>
							<?php if ( $status->can_be_deleted() ) : ?>
                                <form class="admin-ajax-form"
                                      data-confirm="<?php _e( "Jste si jisti? Nejde to vrátit zpět.", "eso" ) ?>">
                                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                                    <input type="hidden" name="eso_action" value="delete_taxonomy_term"/>
                                    <input type="hidden" name="eso_success_callback"
                                           value="render_order_status_table"/>
                                    <input type="hidden" name="eso_callback_target" value="#order-status-table"/>
                                    <input type="hidden" name="term_id" value="<?php echo $status->get_id() ?>"/>
                                    <input type="hidden" name="taxonomy" value="order_status"/>
                                    <button type="submit" class="btn btn-link">
										<?php echo eso_icon( "trash" ) ?>
                                    </button>
                                </form>
							<?php endif; ?>
                        </td>
                    </tr>
				<?php } ?>
                </tbody>
            </table>
		<?php }

		public function render_customer_groups_table() { ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col"><?php _e( "Název skupiny", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Počet členů", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Smazat", "eso" ) ?></th>
                </tr>
                </thead>
                <tbody>
				<?php
				/* @var $group Eso_Customer_Group */
				foreach ( eso_get_customer_groups() as $group ) { ?>
                    <tr>
                        <td>
                            <form class="admin-ajax-form--onchange">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="rename_customer_group"/>
                                <input type="hidden" name="group_id" value="<?php echo $group->get_id() ?>"/>
                                <input type="text" name="group_name"
                                       value="<?php echo $group->get_name() ?>"
                                       title="<?php echo $group->get_name() ?>"/>
                            </form>
                        </td>
                        <td><?php echo $group->get_count() ?></td>
                        <td>
							<?php if ( $group->get_id() != 1 ) : ?>
                                <form class="admin-ajax-form"
                                      data-confirm="<?php echo sprintf( __( "Smazáním skupiny budou zákazníci patříci do skupiny (%d) přiřazeni v první skupině.", "eso" ), $group->get_count() ) ?>">
                                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                                    <input type="hidden" name="eso_action" value="delete_customer_group"/>
                                    <input type="hidden" name="eso_success_callback"
                                           value="render_customer_groups_table"/>
                                    <input type="hidden" name="eso_callback_target" value="#customer-groups-table"/>
                                    <input type="hidden" name="group_id" value="<?php echo $group->get_id() ?>"/>
                                    <button type="submit" class="btn btn-link">
										<?php echo eso_icon( "trash" ) ?>
                                    </button>
                                </form>
							<?php endif; ?>
                        </td>
                    </tr>
				<?php } ?>
                </tbody>
            </table>

		<?php }

		/**
		 * @since 2019.7
		 * @uses Eso_Admin_Ajax::change_product_category_parent()
		 * @uses Eso_Admin_Ajax::rename_product_category()
		 * @uses Eso_Admin_Ajax::change_taxonomy_priority()
		 */
		public function render_product_category_table() { ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" class="text-muted"><?php _e( "ID", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Název", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Počet produktů", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Nadřazená kategorie", "eso" ) ?></th>
                    <th></th>
                    <th scope="col"><?php _e( "Smazat", "eso" ) ?></th>
                </tr>
                </thead>
                <tbody>
				<?php
				foreach ( eso_get_product_categories() as $category ) {
				    ?>
                    <tr>
                        <td class="text-muted"><?php echo $category->term_id ?></td>
                        <td>
                            <form class="admin-ajax-form--onchange">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="rename_product_category"/>
                                <input type="hidden" name="category_id" value="<?php echo $category->term_id ?>"/>
                                <input type="text" name="category_name"
                                       value="<?php echo $category->name ?>"
                                       title="<?php echo $category->name ?>"/>
                            </form>
                        </td>
                        <td><?php echo $category->count ?></td>
                        <td>
                            <form class="admin-ajax-form--onchange">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="change_product_category_parent"/>
                                <input type="hidden" name="category_id" value="<?php echo $category->term_id ?>"/>
                                <select name="category_parent" title="<?php _e("Kategorie", "eso") ?>">
                                    <option value=""></option>
									<?php
                                    $children = eso_get_product_categories( $category->term_id );

									$exclude = [];
									$exclude[] = $category->term_id;
									foreach($children as $child) {
									    $exclude[] = $child->term_id;
                                    }

									$categories = eso_get_product_categories( null, $exclude );

									foreach ( $categories as $item ) { ?>
                                        <option value="<?php echo $item->term_id ?>" <?php if ( $item->term_id == $category->parent ) {
											echo "selected";
										} ?>><?php echo $item->name ?></option>
									<?php } ?>
                                </select>
                                <input type="hidden" name="eso_success_callback"
                                       value="render_product_category_table"/>
                                <input type="hidden" name="eso_callback_target" value="#product-category-table"/>
                            </form>
                        </td>
                        <td>
                            <form class="admin-ajax-form--onchange">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="change_taxonomy_priority"/>
                                <input type="hidden" name="taxonomy" value="product_category"/>
                                <input type="hidden" name="term_id" value="<?php echo $category->term_id ?>"/>
                                <input type="text" name="priority" class="form-control--digit"
                                       value="<?php echo get_term_meta($category->term_id, 'priority', true) ?>"
                                       title="<?php _e("Priorita", "eso") ?>>"/>
                            </form>
                        </td>
                        <td>
                            <form class="admin-ajax-form"
                                  data-confirm="<?php _e( "Jste si jisti? Nejde to vrátit zpět.", "eso" ) ?>">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="delete_taxonomy_term"/>
                                <input type="hidden" name="eso_success_callback"
                                       value="render_product_category_table"/>
                                <input type="hidden" name="eso_callback_target" value="#product-category-table"/>
                                <input type="hidden" name="term_id" value="<?php echo $category->term_id ?>"/>
                                <input type="hidden" name="taxonomy" value="product_category"/>
                                <button type="submit" class="btn btn-link">
									<?php echo eso_icon( "trash" ) ?>
                                </button>
                            </form>
                        </td>
                    </tr>
				<?php } ?>
                </tbody>
            </table>

		<?php }

		/**
		 * @since 2019.7
		 */
		public function render_stock_status_table() { ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col"><?php _e( "Název", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Počet produktů", "eso" ) ?></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
				<?php
				/* @var $status Eso_Stock_Status */
				foreach ( eso_get_stock_statuses() as $status ) { ?>
                    <tr>
                        <td>
                            <form class="admin-ajax-form--onchange">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="rename_stock_status"/>
                                <input type="hidden" name="term_id" value="<?php echo $status->get_id() ?>"/>
                                <input type="text" name="term_name"
                                       value="<?php echo $status->get_name() ?>"
                                       title="<?php echo $status->get_name() ?>"/>
                            </form>
                        </td>
                        <td><?php echo $status->get_count() ?></td>
                        <td>
							<?php if ( $status->can_be_deleted() ) : ?>
                                <form class="admin-ajax-form"
                                      data-confirm="<?php _e( "Jste si jisti? Nejde to vrátit zpět.", "eso" ) ?>">
                                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                                    <input type="hidden" name="eso_action" value="delete_taxonomy_term"/>
                                    <input type="hidden" name="eso_success_callback"
                                           value="render_stock_status_table"/>
                                    <input type="hidden" name="eso_callback_target" value="#stock-status-table"/>
                                    <input type="hidden" name="term_id" value="<?php echo $status->get_id() ?>"/>
                                    <input type="hidden" name="term_id" value="stock_status"/>
                                    <button type="submit" class="btn btn-link">
										<?php echo eso_icon( "trash" ) ?>
                                    </button>
                                </form>
							<?php endif; ?>
                        </td>
                    </tr>
				<?php } ?>
                </tbody>
            </table>
		<?php }

		/**
		 * @since 2019.7
		 */
		public function render_product_tag_table() { ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col"><?php _e( "Název", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Počet produktů", "eso" ) ?></th>
                    <th scope="col"><?php _e( "Smazat", "eso" ) ?></th>
                </tr>
                </thead>
                <tbody>
				<?php
				/* @var $tag Eso_Product_Tag */
				foreach ( eso_get_product_tags() as $tag ) { ?>
                    <tr>
                        <td>
                            <form class="admin-ajax-form--onchange">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="rename_product_tag"/>
                                <input type="hidden" name="term_id" value="<?php echo $tag->get_id() ?>"/>
                                <input type="text" name="term_name"
                                       value="<?php echo $tag->get_name() ?>"
                                       title="<?php echo $tag->get_name() ?>"/>
                            </form>
                        </td>
                        <td><?php echo $tag->get_count() ?></td>
                        <td>
                            <form class="admin-ajax-form"
                                  data-confirm="<?php _e( "Jste si jisti? Nejde to vrátit zpět.", "eso" ) ?>">
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="delete_taxonomy_term"/>
                                <input type="hidden" name="eso_success_callback"
                                       value="render_product_tag_table"/>
                                <input type="hidden" name="eso_callback_target" value="#product-tag-table"/>
                                <input type="hidden" name="term_id" value="<?php echo $tag->get_id() ?>"/>
                                <input type="hidden" name="taxonomy" value="product_tag"/>
                                <button type="submit" class="btn btn-link">
									<?php echo eso_icon( "trash" ) ?>
                                </button>
                            </form>
                        </td>
                    </tr>
				<?php } ?>
                </tbody>
            </table>
		<?php }

		/**
		 * @since 2019.7
		 */
		public function render_coupons_table() {
			$coupons = eso_query_all_coupons();
			if ( ! empty( $coupons ) ) { ?>
                <h2><?php _e( "Seznam slevových kuponů", "eso" ) ?></h2>
                <table class="table table-responsive-xl table-striped table-hovered text-nowrap">
                    <thead>
                    <tr>
                        <th scope="col">
							<?php _e( "Název kuponu", "eso" ) ?>
                        </th>
                        <th scope="col">
							<?php _e( "Kód", "eso" ) ?>
                        </th>
                        <th scope="col">
							<?php _e( "Sleva", "eso" ) ?>
                        </th>
                        <th scope="col">
							<?php _e( "Stav", "eso" ) ?>
                        </th>
                        <th scope="col">
							<?php _e( "Začátek", "eso" ) ?>
                        </th>
                        <th scope="col">
							<?php _e( "Konec", "eso" ) ?>
                        </th>
                        <th scope="col">

                        </th>
                    </tr>
                    </thead>
					<?php
					/* @var $coupon Eso_Coupon */
					foreach ( $coupons as $coupon ) { ?>
                        <tr>
                            <td>
								<?php echo $coupon->get_name() ?>
                            </td>
                            <td>
								<?php echo $coupon->get_code() ?>
                            </td>
                            <td>
								<?php echo $coupon->get_pretty_discount() ?>
                            </td>
                            <td>
								<?php if ( $coupon->is_enabled() ) {
									echo "<span class='text-success'>Aktivní</span>";
								} else {
									echo "<span class='text-danger'>Neaktivní</span>";
								} ?>
                            </td>
                            <td>
								<?php echo $coupon->get_date_start() ?>
                            </td>
                            <td>
								<?php echo $coupon->get_date_end() ?>
                            </td>
                            <td class="table-actions">
                                <a href="<?php $coupon->the_edit_url() ?>" class="d-inline-block" data-toggle="tooltip"
                                   title="<?php _e( "Upravit", "eso" ) ?>"><?php echo eso_icon( "edit-interface-sign" ) ?></a>
                                <form class="admin-ajax-form d-inline-block"
                                      data-confirm="<?php _e( "Skutečně chcete tento slevový kupon smazat?", "eso" ) ?>">
                                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                                    <input type="hidden" name="eso_action" value="delete_coupon"/>
                                    <input type="hidden" name="coupon_id" value="<?php echo $coupon->get_id() ?>"/>
                                    <input type="hidden" name="eso_success_redirect" value="
							<?php echo admin_url( "admin.php?page=eso-coupons-options" ) ?>"/>
                                    <button type="submit" class="btn btn-text p-0" data-toggle="tooltip"
                                            title="<?php _e( "Smazat", "eso" ) ?>">
										<?php echo eso_icon( "trash" ) ?>
                                    </button>
                                </form>
                            </td>
                        </tr>
					<?php } ?>
                </table>

			<?php }
		}

		/**
		 * @since 2019.7
		 */
		public function render_insert_coupon_form() {
			$admin_fields = new Eso_Admin_Fields();
			$fields       = new Eso_Fields();
			?>
            <form class="admin-ajax-form" id="insert-coupon-form">
                <input type="hidden" name="action" value="eso_admin_ajax"/>
                <input type="hidden" name="eso_action" value="insert_coupon"/>
                <input type="hidden" name="eso_success_redirect" value=""/>
                <div class="row">
                    <div class="col-lg-7">
						<?php $fields->form_group_input( "coupon[name]", "Název kuponu", null, null, "text", [ "required" => "required" ] ) ?>
						<?php $fields->form_group_input( "coupon[code]", "Kód kuponu", null, null, "text", [ "required" => "required" ] ) ?>
						<?php $fields->form_group_select( "coupon[type]", "Sleva", [
							"%" => __( "V procentech", "eso" ),
							"-" => __( "Fixní částka - hodnoty oddělené čárkami (Kč, $, €, £)", "eso" )
						] ) ?>
						<?php $fields->form_group_input( "coupon[value]", "Hodnota" ) ?>
						<?php $admin_fields->checkbox( "coupon[enabled]", "Aktivní", 1 ) ?>
                    </div>
                    <div class="col-lg-5">
						<?php $fields->form_group_input( "coupon[date_start]", "Začátek platnosti", null, null, "datetime-local", [ "class" => "" ] ) ?>
						<?php $fields->form_group_input( "coupon[date_end]", "Konec platnosti", null, null, "datetime-local" ) ?>
                        <div>
							<?php $admin_fields->checkbox( "coupon[free_shipping]", "Doprava zdarma", 0 ) ?>
                        </div>
                        <div>
							<?php $admin_fields->checkbox( "coupon[registered_only]", "Pouze pro registrované", 0 ) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col mt-3">
                        <button type="submit" class="btn btn-lg btn-secondary"><?php _e( "Vytvořit", "eso" ) ?></button>
                    </div>
                </div>
            </form>
		<?php }

		/**
		 * @since 2019.7
		 */
		public function render_edit_coupon_form( Eso_Coupon $coupon ) {
			$admin_fields = new Eso_Admin_Fields();
			$fields       = new Eso_Fields();
			?>
            <form class="admin-ajax-form">
                <input type="hidden" name="action" value="eso_admin_ajax"/>
                <input type="hidden" name="eso_action" value="edit_coupon"/>
                <input type="hidden" name="coupon_id" value="<?php echo $coupon->get_id(); ?>"/>
                <input type="hidden" name="eso_success_redirect"
                       value="<?php echo admin_url( "admin.php?page=eso-coupons-options" ) ?>"/>
                <div class="row">
                    <div class="col-lg-7">
						<?php $fields->form_group_input( "coupon[name]", "Název kuponu", $coupon->get_name(), null, "text", [ "required" => "required" ] ) ?>
						<?php $fields->form_group_input( "coupon[code]", "Kód kuponu", $coupon->get_code(), null, "text", [ "required" => "required" ] ) ?>
						<?php $fields->form_group_select( "coupon[type]", "Sleva", [
							"%" => __( "V procentech", "eso" ),
							"-" => __( "Fixní částka (Kč, $, €, £)", "eso" )
						], $coupon->get_type() ) ?>
						<?php $fields->form_group_input( "coupon[value]", "Hodnota", $coupon->get_value() ) ?>
						<?php $admin_fields->checkbox( "coupon[enabled]", "Aktivní", $coupon->is_enabled() ) ?>
                    </div>
                    <div class="col-lg-5">
						<?php
						$fields->form_group_input( "coupon[date_start]", "Začátek platnosti", $coupon->get_date_start( "datetime-local" ), null, "datetime-local", [ "class" => "" ] ) ?>
						<?php $fields->form_group_input( "coupon[date_end]", "Konec platnosti", $coupon->get_date_end( "datetime-local" ), null, "datetime-local" ) ?>
                        <div>
							<?php $admin_fields->checkbox( "coupon[free_shipping]", "Doprava zdarma", $coupon->is_free_shipping() ) ?>
                        </div>
                        <div>
							<?php $admin_fields->checkbox( "coupon[registered_only]", "Pouze pro registrované", $coupon->is_for_registered_only() ) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col mt-3">
                        <button type="submit" class="btn btn-lg btn-secondary"><?php _e( "Upravit", "eso" ) ?></button>
                    </div>
                </div>
            </form>
			<?php
		}

	}
}