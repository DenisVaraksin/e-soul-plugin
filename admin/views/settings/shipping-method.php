<?php
function eso_plugin_settings_shipping_method() {
	eso_plugin_settings_header();

	if ( ! isset( $_GET["shipping-method"] ) || ! isset( $_GET["shipping-zone"] ) ) {
		write_log( "Both shipping-method and shipping-zone have to be set for accessing this page." );
		wp_die( "Both shipping-method and shipping-zone have to be set for accessing this page." );
	}

	$admin_fields = new Eso_Admin_Fields();

	$shipping_method_code = sanitize_text_field( $_GET["shipping-method"] );
	$shipping_zone_code   = sanitize_text_field( $_GET["shipping-zone"] );

	$shipping_method = new Eso_Shipping_Method( $shipping_method_code );
	$shipping_zone   = new Eso_Shipping_Zone( $shipping_zone_code );

	$shipping_method_prices = $shipping_zone->get_shipping_method_prices( $shipping_method_code );
	?>
    <div class="container container--left pt-4 pt-md-3">
        <div class="row">
            <div class="col pb-3">
                <a href="<?php menu_page_url( 'eso-payments-options' ) ?>"
                   class="btn btn-lg btn-primary btn-back"><?php _e( "Zpět", "eso" ) ?></a>
                <h1 class="mt-3"><?php echo $shipping_zone->get_name() . ": " . $shipping_method->get_name() ?></h1>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col">
                <form class="admin-ajax-form--onchange">
                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                    <input type="hidden" name="eso_action" value="shipping_method_update_prices"/>
                    <input type="hidden" name="shipping_method_code"
                           value="<?php echo $shipping_method_code ?>"/>
                    <input type="hidden" name="shipping_zone_code"
                           value="<?php echo $shipping_zone_code ?>"/>

                    <h3><?php _e( "Cena dopravy", "eso" ) ?></h3>
                    <div class="eso-card">
                        <div class="form-row">
							<?php
							/* @var $currency Eso_Currency */
							foreach ( eso_get_all_currencies() as $currency ) : ?>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="eso-box__label"><?php echo $currency->get_code(); ?></label>

                                        <div class="input-group">
                                            <input class="form-control" type="number" min="0"
                                                   name="prices[<?php echo $currency->get_code(); ?>]"
                                                   value="<?php if ( isset( $shipping_method_prices[ $currency->get_code() ] ) ) {
												       echo $shipping_method_prices[ $currency->get_code() ];
											       } else {
												       $default_options          = new Eso_Default_Options();
												       $default_shipping_methods = $default_options->shipping_methods();
												       if ( isset( $default_shipping_methods[ $shipping_method_code ] ) ) {
													       echo $default_shipping_methods[ $shipping_method_code ]["prices"][ $currency->get_code() ];
												       }
											       } ?>"
                                                   title="<?php echo $currency->get_code() ?>"/>
                                            <div class="input-group-append">
                                                <div class="input-group-text"><?php echo $currency->get_symbol() ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php endforeach; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <form class="admin-ajax-form--onchange">
                    <input type="hidden" name="action" value="eso_admin_ajax"/>
                    <input type="hidden" name="eso_action" value="shipping_method_update_payment_methods"/>
                    <input type="hidden" name="shipping_zone_code"
                           value="<?php echo $shipping_zone_code ?>"/>
                    <input type="hidden" name="shipping_method_code" value="<?php echo $shipping_method_code ?>"/>
                    <h3><?php _e( "Metody platby", "eso" ) ?></h3>
                    <div class="eso-card">
						<?php
						foreach ( eso_get_enabled_payment_methods() as $payment_method ) :
							$payment_method_prices = $shipping_zone->get_payment_method_prices( $shipping_method_code, $payment_method->code );
							?>
                            <div class="row mb-4 mt-3 d-sm-flex align-items-center">
                                <div class="col-12 col-md-4">
									<?php $admin_fields->checkbox( "methods[" . $payment_method->code . "][enabled]", $payment_method->name, $shipping_zone->has_payment_method( $shipping_method_code, $payment_method->code ) ) ?>
                                </div>
								<?php
								if ( $shipping_zone->is_empty_payment_method( $shipping_method_code, $payment_method->code ) ) : ?>
                                    <div class="col">
                                        <button class="btn btn-link text-success d-block w-100" type="button"
                                                onclick="parentNode.remove()" data-toggle="collapse"
                                                data-target="#prices_<?php echo $payment_method->code ?>"
                                                aria-expanded="false"
                                                aria-controls="prices_<?php echo $payment_method->code ?>"><?php _e( "Zdarma", "eso" ) ?></button>
                                    </div>
								<?php
								endif;
								/* @var $currency Eso_Currency */
								foreach ( eso_get_all_currencies() as $currency ) : ?>
                                    <div class="col-6 col-md-2 <?php if ( $shipping_zone->is_empty_payment_method( $shipping_method_code, $payment_method->code ) )
										echo 'collapse' ?>" id="prices_<?php echo $payment_method->code ?>">
                                        <div class="form-group">
                                            <label class="eso-box__label"><?php echo $currency->get_code(); ?></label>
                                            <div class="input-group">
                                                <input class="form-control" type="number" min="0"
                                                       name="methods[<?php echo $payment_method->code ?>][prices][<?php echo $currency->get_code(); ?>]"
                                                       value="<?php if ( isset( $payment_method_prices[ $currency->get_code() ] ) ) {
													       echo $payment_method_prices[ $currency->get_code() ];
												       } ?>"
                                                       title="<?php echo $currency->get_code() ?>"/>
                                                <div class="input-group-append">
                                                    <div class="input-group-text"><?php echo $currency->get_symbol() ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php endforeach; ?>
                            </div>
						<?php endforeach; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php }