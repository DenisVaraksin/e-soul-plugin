<?php
function eso_plugin_settings_invoices() {
	eso_plugin_settings_header();
	$fields = new Eso_Fields();
	$admin_fields = new Eso_Admin_Fields();
	$invoice_settings = new Eso_Invoice_Settings();
	$store = new Eso_Store();

	?>
    <form class="admin-ajax-form--onchange">
        <input type="hidden" name="action" value="eso_admin_ajax"/>
        <input type="hidden" name="eso_action" value="save_invoices_settings"/>
        <div class="container-fluid mb-5">
            <div class="row">
                <div class="col-lg-5 mb-default">
                    <h2 class="mt-4"><?php _e( "Fakturační údaje", "eso" ) ?></h2>
                    <div class="eso-box mt-1">
                        <?php $fields->form_group_input("options[eso_invoice_issuedby]", "Fakturu vystavil", $invoice_settings->get_issued_by()); ?>
                        <?php $fields->form_group_input("options[eso_billing_street]", "Ulice, číslo popisné", $invoice_settings->get_street()); ?>
                        <?php $fields->form_group_input("options[eso_billing_city]", "Město, PSČ", $invoice_settings->get_city()); ?>
                        <div class="form-group">
                            <label for="billing-country" class="eso-box__label"><?php _e( "Stát", "eso" ) ?></label>
                            <select name="options[eso_billing_country]" id="billing-country" class="form-control">
								<?php
								/* @var $shipping_zone Eso_Shipping_Zone */
								foreach ( eso_get_enabled_shipping_zones() as $shipping_zone_code => $shipping_zone_name ) : ?>
                                    <option value="<?php echo $shipping_zone_code ?>" <?php selected( $shipping_zone_code, get_option( "eso_billing_country" ) ); ?>><?php echo $shipping_zone_name ?></option>
								<?php endforeach; ?>
                            </select>
                        </div>
	                    <?php $fields->form_group_input("options[eso_ico]", "IČO", $invoice_settings->get_ico()); ?>
	                    <?php $fields->form_group_input("options[eso_dic]","DIČ", $invoice_settings->get_dic()); ?>
	                    <?php $fields->form_group_input("options[eso_billing_file_number]","Spisová značka", $invoice_settings->get_file_number()); ?>

                        <div class="form-group">
                            <label for="eso_billing_vat_payer"
                                   class="eso-box__label d-block">
			                    <?php _e( "Plátce DPH", "eso" ) ?></label>
		                    <?php $admin_fields->checkbox( "eso_billing_vat_payer", "", get_option( "eso_billing_vat_payer" ) ); ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 mb-default">
                    <h2 class="mt-4"><?php _e( "Vlastnosti faktur", "eso" ) ?></h2>
                    <div class="eso-box mt-1">
	                    <?php $fields->form_group_input("options[eso_invoice_prefix]","Předpona", $invoice_settings->get_prefix(), "VF"); ?>
                        <div class="form-group">
                            <label for="eso_invoice_prefix_year"
                                   class="eso-box__label d-block">
                                <?php _e( "Rok objednávky před číslem faktury", "eso" ) ?></label>
							<?php $admin_fields->checkbox( "eso_invoice_prefix_year", "", get_option( "eso_invoice_prefix_year" ) ); ?>
                        </div>
	                    <?php $fields->form_group_input("options[eso_invoice_number_length]", "Počet znaků", $invoice_settings->get_number_length(), 6, "number", ["min" => 1, "max" => 10]); ?>
                        <div class="form-group">
                            <label for="invoice-due-days"
                                   class="eso-box__label"><?php _e( "Doba splatnosti", "eso" ) ?></label>
                            <select name="options[eso_invoice_due_days]" id="invoice-due-days" class="form-control">
                                <?php $due_days = $invoice_settings->get_due_days() ?>
                                <option value="14" <?php selected( 14, $due_days); ?>>
                                    14 <?php _e( "dní", "eso" ) ?></option>
                                <option value="7" <?php selected( 7, $due_days ); ?>>
                                    7 <?php _e( "dní", "eso" ) ?></option>
                                <option value="21" <?php selected( 21, $due_days ); ?>>
                                    21 <?php _e( "dní", "eso" ) ?></option>
                                <option value="28" <?php selected( 28, $due_days ); ?>>
                                    28 <?php _e( "dní", "eso" ) ?></option>
                            </select>
                        </div>
	                    <?php $fields->form_group_input("options[eso_constant_symbol]","Konstantní symbol", $invoice_settings->get_constant_symbol()); ?>
	                    <?php $fields->form_group_input("options[eso_specific_symbol]","Specifický symbol", $invoice_settings->get_specific_symbol()); ?>
	                    <?php $fields->form_group_input("options[eso_billing_before_note]","Poznámka před položkami faktury", $invoice_settings->get_before_note(), __( "Fakturujeme Vám za zakoupené položky: ", "eso" )); ?>
	                    <?php $fields->form_group_input("options[eso_billing_after_note]", "Poznámka pod položkami faktury", $invoice_settings->get_after_note() ); ?>
                    </div>
                </div>
                <div class="col-lg-5 mb-default">
                    <h2 class="mt-4"><?php _e( "Kontaktní údaje", "eso" ) ?></h2>
                    <div class="eso-box mt-1">
			            <?php $fields->form_group_input("options[eso_invoice_contact_name]","Jméno a příjmení", $invoice_settings->get_contact_name(), $store->get_name()); ?>
			            <?php $fields->form_group_input("options[eso_invoice_contact_phone]","Telefon", $invoice_settings->get_contact_phone(), $store->get_phone(), "tel"); ?>
			            <?php $fields->form_group_input("options[eso_invoice_contact_email]","Email", $invoice_settings->get_contact_email(), $store->get_manager_email(), "email"); ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
	<?php
}