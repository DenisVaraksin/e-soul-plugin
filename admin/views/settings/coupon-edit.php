<?php
function eso_plugin_coupon_edit_page() {
	eso_plugin_settings_header();

	if ( ! isset( $_GET["id"] ) ) {
		write_log( "Acessing wrong coupon with id {$_GET['id']}" );
		wp_die( "Acessing wrong coupon with id {$_GET['id']}." );
	}
	$admin_tables = new Eso_Admin_Tables();
	$coupon       = new Eso_Coupon( $_GET["id"] );
	?>
    <div class="container container--left">
        <div class="row mt-4">
            <div class="col-lg-9">
                <a href="<?php menu_page_url( 'eso-coupons-options' ) ?>"
                class="btn btn-lg btn-primary btn-back"><?php _e( "Zpět", "eso" ) ?></a>
                <h2 class="mt-4"><?php echo __( "Upravit", "eso" ) . " " . $coupon->get_name(); ?></h2>
                <div class="eso-box">
					<?php $admin_tables->render_edit_coupon_form( $coupon ) ?>
                </div>
            </div>
        </div>
    </div>
<?php }