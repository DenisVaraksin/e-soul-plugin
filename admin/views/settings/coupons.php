<?php
function eso_plugin_settings_coupons() {
	eso_plugin_settings_header();
	$admin_tables = new Eso_Admin_Tables();
	?>
    <div class="container container--left">
        <div class="row">
            <div class="col mt-4">
                <div id="coupons-table">
	                <?php $admin_tables->render_coupons_table(); ?>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-9">
                <h2><?php _e( "Nový kupon", "eso" ) ?></h2>
                <div class="eso-box">
	                <?php eso_the_docs_link("coupons"); ?>
                    <?php $admin_tables->render_insert_coupon_form() ?>
                </div>
            </div>
        </div>
    </div>
	<?php
}