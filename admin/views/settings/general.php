<?php
function eso_plugin_settings_general() {
	eso_plugin_settings_header();
    $fields = new Eso_Fields();
	$store = new Eso_Store();
	?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-9">
                <div class="eso-box mt-4">
                    <form class="admin-ajax-form">
                        <input type="hidden" name="action" value="eso_admin_ajax" />
                        <input type="hidden" name="eso_action" value="save_general_settings" />
                        <div class="row">
                            <div class="col-lg-6">
	                            <?php $fields->form_group_input("options[blogname]", "Název webu" , $store->get_name()); ?>
	                            <?php $fields->form_group_input("options[eso_store_owner]", "Majitel obchodu", $store->get_owner(), "", "text", ["required" => "required"]); ?>
                                <div class="form-group">
                                    <label for="store-logo" class="eso-box__label"><?php _e("Logo obchodu", "eso") ?></label>
                                    <div class="image-preview-wrap">
                                        <div class="btn-gallery-trigger" id="store-logo">
                                            <?php if($store->get_logo_id()) {
                                                $store->the_logo("medium");
                                            } else echo eso_icon("gallery"); ?>
                                            <div class="eso-add" <?php if($store->get_logo_id()) echo "style='display: none;'"; ?>></div>
                                        </div>
                                        <div class="eso-remove eso-remove--outter" data-target="#store-logo" <?php if(!$store->get_logo_id()) echo "style='display: none;'"; ?>></div>
                                        <input type="hidden" name="store-logo" value="<?php echo $store->get_logo_id(); ?>" id="store-logo-value">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="store-favicon" class="eso-box__label"><?php _e("Favicon", "eso") ?></label>
                                    <div class="image-preview-wrap">
                                        <div class="btn-gallery-trigger" id="store-favicon" data-accept="image/png">
                                            <?php if($store->get_favicon_id()) $store->the_favicon("medium"); else echo eso_icon("gallery"); ?>
                                            <div class="eso-add" <?php if($store->get_favicon_id()) echo "style='display: none;'"; ?>></div>
                                        </div>
                                        <div class="eso-remove eso-remove--outter" data-target="#store-favicon" <?php if(!$store->get_favicon_id()) echo "style='display: none;'"; ?>></div>
                                        <input type="hidden" name="store-favicon" value="<?php echo $store->get_favicon_id(); ?>" id="store-favicon-value">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
	                            <?php $fields->form_group_input("options[blogdescription]", "Podtitul obchodu",  $store->get_description()); ?>
	                            <?php $fields->form_group_input("options[eso_store_manager_email]", "Email",  $store->get_manager_email(), get_option("admin_email"), "email", ["required" => "required"]); ?>
	                            <?php $fields->form_group_input("options[eso_store_phone]", "Telefon", $store->get_phone(), "", "tel"); ?>
	                            <?php $fields->form_group_textarea("options[eso_personal_collection_address]", "Adresa osobního odběru", $store->get_personal_collection_address(), "", ["rows" => 3]); ?>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <button type="submit" class="btn btn-lg btn-primary dynamic-form-submit"><?php _e("Uložit", "eso") ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
}