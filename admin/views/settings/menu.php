<?php
function eso_plugin_settings_header() {
	if ( !current_user_can( 'install_plugins' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	} ?>
    <div class="wrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php $screen = get_current_screen();
                    $screen_base = $screen->base;
                    ?>
                    <div class="eso-settings-nav-wrap eso-pills eso-pills-lg eso-pills-wide">
                        <a <?php if($screen_base == "toplevel_page_eso-main-options") echo "class='active'"; ?> href="<?php menu_page_url("eso-main-options") ?>"><?php _e("Obchod", "eso") ?>
                        <a <?php if($screen_base == "obchod_page_eso-api-options") echo "class='active'"; ?> href="<?php menu_page_url('eso-api-options') ?>"><?php _e("Napojení API", "eso") ?>
                        <a <?php if($screen_base == "obchod_page_eso-payments-options" || $screen_base == "admin_page_eso-shipping-method-options") echo "class='active'"; ?> href="<?php menu_page_url('eso-payments-options') ?>"><?php _e("Doprava a platby", "eso") ?>
                        <a <?php if($screen_base == "obchod_page_eso-invoices-options") echo "class='active'"; ?> href="<?php menu_page_url('eso-invoices-options') ?>"><?php _e("Faktury", "eso") ?>
                        <a <?php if($screen_base == "obchod_page_eso-coupons-options" || $screen_base == "admin_page_eso-coupon-edit") echo "class='active'"; ?> href="<?php menu_page_url('eso-coupons-options') ?>"><?php _e("Slevové kupony", "eso") ?>
                        <a <?php if($screen_base == "obchod_page_eso-newsletter-options" ) echo "class='active'"; ?> href="<?php menu_page_url('eso-newsletter-options') ?>"><?php _e("Newsletter", "eso") ?>
                        <a <?php if($screen_base == "obchod_page_eso-advanced-options") echo "class='active'"; ?> href="<?php menu_page_url('eso-advanced-options') ?>"><?php _e("Pokročilé", "eso") ?></a>
                    </div>
                </div>
            </div>
        </div>
	<?php
}

