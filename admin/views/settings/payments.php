<?php function eso_plugin_settings_payments() {
	eso_plugin_settings_header();
?>
<div class="container-fluid pt-2 pt-md-3">
    <div class="row">
        <div class="col pb-3">
            <h1><? _e("Doprava a platby", "eso") ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-xl-4">
            <h2><?php _e("Platby", "eso") ?></h2>
            <div class="whitebox uibox">
                <form class="admin-ajax-form admin-ajax-form--onchange">
                    <input type="hidden" name="action" value="eso_admin_ajax" />
                    <input type="hidden" name="eso_action" value="enabled_currencies_edit" />
                    <p><?php _e("Seznam povolených měn", "eso") ?></p>
					<?php
					/* @var $currency Eso_Currency */
					foreach(eso_get_all_currencies() as $currency) : ?>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label class="switch">
                                    <input type="checkbox" id="eso_currency_<?php echo $currency->get_code() ?>" name="eso_currency[]" value="<?php echo $currency->get_code() ?>" <?php checked($currency->is_enabled()) ?>>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label for="eso_currency_<?php echo $currency->get_code() ?>"><?php echo $currency->get_code() ?></label>
                            </div>
                            <div class="col-sm-6">
								<?php echo $currency->get_symbol() ?>
                            </div>
                        </div>
					<?php endforeach; ?>
                </form>
            </div>
            <div class="default-currency-select mt-4 mb-4">
                <form class="admin-ajax-form admin-ajax-form--onchange">
                    <input type="hidden" name="action" value="eso_admin_ajax" />
                    <input type="hidden" name="eso_action" value="default_currency_edit" />
                    <label for="eso_default_currency"><?php _e("Předvolená měna e-shopu", "eso") ?></label>
                    <select name="eso_default_currency" id="eso_default_currency">
						<?php
						$default_currency = eso_get_default_currency_name();
						/* @var $currency Eso_Currency */
						foreach(eso_get_enabled_currencies() as $currency) : ?>
                            <option value="<?php echo $currency->get_code() ?>" <?php selected($default_currency, $currency->get_code()) ?>><?php echo $currency->get_code() ?></option>
						<?php endforeach; ?>
                    </select>
                </form>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <h2><?php _e("Daně", "eso") ?></h2>
            <div class="uibox whitebox">
				<?php
				/* @var $tax_rate Eso_Tax_Rate */
				foreach(eso_get_tax_rates() as $tax_rate) : ?>
                    <div class="form-group row">
                        <span class="col-sm-8 col-form-label"><?php echo $tax_rate->get_name() ?></span>
                        <div class="col-sm-4">
                            <label><input type="text" class="form-control-plaintext text-sm-right" value="<?php echo $tax_rate->get_value() ?>%" readonly /></label>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-6 col-xl-4 mb-default">
            <h2><?php _e("Platba převodem") ?></h2>
            <div class="uibox whitebox">
                <form class="admin-ajax-form">
                    <input type="hidden" name="action" value="eso_admin_ajax" />
                    <input type="hidden" name="eso_action" value="save_payment_transfer_settings" />
                    <div class="form-group">
                        <label for="store-account-number" class="eso-box__label"><?php _e("Číslo účtu", "eso") ?></label>
                        <input type="text" class="form-control" id="store-account-number" name="store-account-number" value="<?php echo get_option("eso_store_account_number") ?>" />
                    </div>
                    <div class="form-group">
                        <label for="store-iban" class="eso-box__label"><?php _e("IBAN", "eso") ?></label>
                        <input type="text" class="form-control" id="store-iban" name="store-iban" value="<?php echo get_option("eso_store_iban") ?>" />
                    </div>
                    <div class="form-group">
                        <label for="store-bic" class="eso-box__label"><?php _e("BIC", "eso") ?></label>
                        <input type="text" class="form-control" id="store-bic" name="store-bic" value="<?php echo get_option("eso_store_bic") ?>" />
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <button type="submit" class="btn btn-lg btn-primary dynamic-form-submit"><?php _e("Uložit", "eso") ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h2><?php _e("Zóny dopravy", "eso") ?></h2>
        </div>
    </div>
    <div class="row">
		<?php
		/* @var $shipping_zone Eso_Shipping_Zone */
		foreach(eso_get_enabled_shipping_zones() as $shipping_zone_code => $shipping_zone_name ) :
            $shipping_zone = new Eso_Shipping_Zone($shipping_zone_code);
			$shipping_zone->render_edit_item();
		endforeach; ?>

        <div class="col-xl-4 col-lg-6 mb-default">
            <div class="whitebox uibox eso-box">
                <div class="eso-box__title">
                    <h3 class="eso-box__heading"><?php _e("Přidání nové zóny dopravy", "eso") ?></h3>
                </div>

                <form class="dynamic-form" id="shipping-zone-enable-form">
                    <input type="hidden" name="action" value="eso_admin_ajax">
                    <input type="hidden" name="eso_action" value="shipping_zone_enable">
                    <div class="form-group">
                        <label for="shipping-zone"><? _e("Země", "eso") ?></label>
                        <select name="shipping-zone" id="shipping-zone" class="form-control">
							<?php
							/* @var $shipping_zone Eso_Shipping_Zone */
							foreach(eso_get_all_shipping_zones( false ) as $shipping_zone_code => $shipping_zone_name) : ?>
                                <option value="<?php echo $shipping_zone_code ?>"><?php echo $shipping_zone_name ?></option>
							<?php endforeach; ?>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-lg btn-secondary btn-plus dynamic-form-submit"><?php _e("Přidat", "eso") ?></button>
                </form>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col">
            <h2><?php _e("Metody dopravy", "eso") ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="uibox whitebox">
                <div class="eso-box__title">
                    <h3 class="eso-box__heading"><?php _e("Přidání nové methody dopravy", "eso") ?></h3>
                </div>

                <form class="admin-ajax-form">
                    <input type="hidden" name="action" value="eso_admin_ajax" />
                    <input type="hidden" name="eso_action" value="create_delivery_method" />
                    <input type="hidden" name="eso_success_redirect" value="" />
                    <div class="form-group">
                        <label for="store-shipping-name" class="eso-box__label"><?php _e("Jméno", "eso") ?></label>
                        <input type="text" class="form-control" id="store-shipping-name" autocomplete="off" name="store-shipping-name"/>
                    </div>
                    <div class="form-group">
                        <label for="store-shipping-code" class="eso-box__label"><?php _e("Code (například 'czech_post')", "eso") ?></label>
                        <input type="text" class="form-control" id="store-shipping-code" autocomplete="off" name="store-shipping-code" />
                    </div>
                    <!-- Prices -->
                    <div class="form-group">
                        <label for="store-shipping-default-prices-czk" class="eso-box__label"><?php _e("Cena CZK", "eso") ?></label>
                        <input type="text" class="form-control" id="store-shipping-default-prices-czk" autocomplete="off" name="store-shipping-default-prices-czk" />
                    </div>
                    <div class="form-group">
                        <label for="store-shipping-default-prices-eur" class="eso-box__label"><?php _e("Cena EUR", "eso") ?></label>
                        <input type="text" class="form-control" id="store-shipping-default-prices-eur" autocomplete="off" name="store-shipping-default-prices-eur" />
                    </div>
                    <div class="form-group">
                        <label for="store-shipping-default-prices-usd" class="eso-box__label"><?php _e("Cena USD", "eso") ?></label>
                        <input type="text" class="form-control" id="store-shipping-default-prices-usd" autocomplete="off" name="store-shipping-default-prices-usd" />
                    </div>
                    <div class="form-group">
                        <label for="store-shipping-default-prices-gbr" class="eso-box__label"><?php _e("Cena GBR", "eso") ?></label>
                        <input type="text" class="form-control" id="store-shipping-default-prices-gbr" autocomplete="off" name="store-shipping-default-prices-gbr" />
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <button type="submit" class="btn btn-lg btn-primary dynamic-form-submit"><?php _e("Uložit", "eso") ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php }