<?php
function eso_plugin_settings_advanced() {
	eso_plugin_settings_header();

	$fields = new Eso_Admin_Fields();
	?>

    <div class="container-fluid">
        <div class="row mt-4">
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <h1>Výchozí nastavení</h1>
                    <p>Toto tlačítko vrátí nastavení e-shopu do výchozího stavu. Všechny produkty, objednávky a zákazníci
                        zůstanou neporušené.</p>
                    <p>Budou smazány:</p>
                    <ul>
                        <li>Nastavení napojení na API</li>
                        <li>Nedokončené objednávky zákazníků</li>
                        <li>Nastavení zón dopravy</li>
                    </ul>
                    <form class="admin-ajax-form">
                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                        <input type="hidden" name="eso_action" value="reset_to_factory_defaults"/>
                        <input type="hidden" name="eso_success_redirect" value="<?php echo admin_url("plugins.php"); ?>"/>
                        <button type="submit" class="btn btn-danger btn-lg mt-2"
                                onclick="return confirm('Skutečně se chcete vrátit do vychozího nastavení? Zálohu neděláme.')">
                            Smazat
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <h1>Vývojář</h1>
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#eso-debug-log"
                            aria-expanded="false">Zobrazit debug.log
                    </button>
                    <a href="<?php echo admin_url("options-permalink.php") ?>" class="btn btn-primary mt-2">Nastavení trvalých odkazů</a>
                    <a href="<?php echo admin_url("update-core.php") ?>" class="btn btn-primary mt-2">Zkontrolovat aktualizace</a>

                    <form class="admin-ajax-form--onchange mt-4">
                        <input type="hidden" name="action" value="eso_admin_ajax" />
                        <input type="hidden" name="eso_action" value="update_branch" />
                        <label for="branch" class="eso-label">Branch</label>
                        <select name="branch" id="branch" class="form-control">
                            <option value="master" <?php if(get_option("eso_branch") == "master") echo "selected" ?>>Stabilní</option>
                            <option value="beta" <?php if(get_option("eso_branch") == "beta") echo "selected" ?>>Beta</option>
                        </select>
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <h1>SEO</h1>
                    <form class="admin-ajax-form--onchange">
                        <input type="hidden" name="action" value="eso_admin_ajax">
                        <input type="hidden" name="eso_action" value="update_store_publicity">
                        <?php $fields->checkbox("blog_public", __("Dovolit indexaci webu vyhledáváčům", "eso"), get_option("blog_public")); ?>
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-xl-4 mb-default">
                <div class="eso-box inline">
                    <h1>Pomoc</h1>
                    <a href="https://bitbucket.org/marketsoul/e-soul-plugin/issues?status=new&status=open" target="_blank" class="btn btn-secondary mt-2">Bug tracker</a>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade mt-5" id="eso-debug-log">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <?php
                    $logFile = ini_get('error_log');
                    $logFile = str_replace('\\', DIRECTORY_SEPARATOR, $logFile);
                    $logFile = str_replace('/', DIRECTORY_SEPARATOR, $logFile);

                    if (isset($_POST['command']) && $_POST['command'] == 'CLEAR') {

                        file_put_contents($logFile, '');
                        $current_user = wp_get_current_user();
                        error_log('Log is erased (' . $current_user->user_login . ' - ' . $current_user->user_email . ')');
                    }

                    if (! empty($logFile)) {

                        echo '<pre class="log">';
                        $myfile = fopen($logFile, 'r') or die(__('Unable to open file!', 'eso'));



                        echo fread($myfile, filesize($logFile));
                        fclose($myfile);
                        echo '</pre>';

                        echo '<form method="post" action="" novalidate="novalidate"">';
                        echo '<input type="hidden" name="command" id="command" value="CLEAR">';
                        echo '<p class="submit">';
                        echo '<input type="submit" name="submit" id="submit" class="btn btn-danger" value="' . __('Smazat debug.log', 'eso') . '">';
                        echo '</p>';
                        echo '</form>';

                        echo '<button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
	<?php
}