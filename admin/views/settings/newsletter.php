<?php
function eso_plugin_settings_newsletter() {
	eso_plugin_settings_header();
	$admin_tables = new Eso_Admin_Tables();
	?>
	<div class="container container--left">
		<div class="row">
			<div class="col col-md-6 mt-4">
                <div class="eso-box">
	                <?php eso_the_docs_link( "newsletter" ) ?>
                    <h1><?php _e("Export emailů přihlášených k newsletteru", "eso") ?></h1>
                    <a href="<?php echo current_url() ?>&generateXls=true"
                       class="btn btn-secondary btn-xls btn-lg mt-2 mt-md-0">
		                <?php _e( "Export", "eso" ) ?></a>
                </div>
			</div>
		</div>
	</div>
	<?php
}