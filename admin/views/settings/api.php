<?php
function eso_plugin_settings_api() {
	eso_plugin_settings_header();
	?>
    <div class="container container--left">
        <div class="row">
            <div class="col">
				<?php
				foreach ( eso_get_all_module_categories() as $category_obj ) :
					$category = new Eso_Module_Category( $category_obj->id );
					if ( $category->has_modules() ) : ?>
                        <section class="eso-module-category">
                            <h2><?php echo $category->get_name() ?></h2>
							<?php
							/* @var $module Eso_Module */
							foreach ( $category->get_modules() as $module ) :
                                $module->render_item();
                            endforeach; ?>
                        </section>
					<?php endif; ?>
				<?php endforeach; ?>
            </div>
        </div>
    </div>
	<?php
}