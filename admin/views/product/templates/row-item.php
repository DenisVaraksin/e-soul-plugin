<?php
/**
 * @var $this Eso_Product
 * @see eso_render_available_related_products()
 */
?>
<tr data-toggle="add-related-product" data-id="<?php echo $this->get_id() ?>">
    <td>
		<?php if ( ! $this->get_featured_image() ) {
			echo eso_empty_thumbnail();
		} else {
			$this->the_featured_image( "table" );
		} ?>
    </td>
    <td>
		<?php echo $this->get_name() ?>
    </td>
</tr>
