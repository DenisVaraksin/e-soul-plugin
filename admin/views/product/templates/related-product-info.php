<?php
/**
 * @var $this Eso_Product
 * @var $parent_id
 */
?>
<div class="col-xl-2 col-lg-3 mb-default related-product-info" data-parent="<?php echo $parent_id ;?>" data-id="<?php echo $this->get_id() ?>">
    <div class='eso-remove'></div>
    <div class="whitebox">
		<?php if(!$this->get_featured_image()) {
			echo eso_empty_thumbnail();
		} else {
			$this->the_featured_image( "tiny" );
		} ?>
        <h4 class="mb-1"><?php echo $this->get_name() ?></h4>
        <span class="stock-status stock-status--<?php echo $this->get_stock_status_slug() ?>"><small><?php echo $this->get_stock_status_name() ?></small></span>
    </div>
</div>
