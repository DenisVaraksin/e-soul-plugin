<?php
/**
 * @var $this Eso_Product_Variant
 */
$fields = new Eso_Fields();
?>
<div class="col-xl-4 col-lg-6 mb-default product-variant" data-slug="<?php echo $this->get_slug() ?>">
    <div class="whitebox">
        <div class='eso-remove'></div>
        <h3 class="font-weight-bold"><?php echo $this->get_name() ?></h3>
		<?php foreach ( $this->get_attributes() as $attribute ) : ?>
            <?php $this->render_editable_attribute($attribute["slug"]); ?>
		<?php endforeach ?>

        <div id="add-variant-attribute" class="mt-3">
	        <?php $fields->form_group_input("attribute_name", null) ?>
            <button type="button" class="btn btn-light btn-plus"><?php _e("Přidat", "eso") ?></button>
        </div>
    </div>
</div>
