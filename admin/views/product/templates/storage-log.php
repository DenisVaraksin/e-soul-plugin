<?php
/**
 * @since 2019.7
 *
 * @var $this Eso_Product
 */
?>
<div class="modal fade" id="log-<?php echo $this->get_id() ?>" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3><?php _e( "Historie skladu", "eso" ) ?>
                    - <?php echo $this->get_name() ?></h3>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
				<?php
				if ( $this->get_log() ) { ?>
                    <table class="table table-responsive-xl table-striped">
                        <thead>
                        <tr>
                            <th scope="col"><?php _e( "Datum", "eso" ) ?></th>
                            <th scope="col"><?php _e( "Uživatel", "eso" ) ?></th>
                            <th scope="col"><?php _e( "Předcházející stav", "eso" ) ?></th>
                            <th scope="col"><?php _e( "Nový stav", "eso" ) ?></th>
                            <th scope="col"><?php _e( "Rozdíl", "eso" ) ?></th>
                        </tr>
                        </thead>
						<?php
						foreach ( $this->get_log() as $log ) : ?>
                            <tr>
                                <td>
									<?php
									$time = new DateTime( $log["time"] );
									echo $time->format( "d. m. Y H:i:s" ) ?>
                                </td>
                                <td>
									<?php echo $log["user"]; ?>
                                </td>
								<?php if ( isset( $log["message"] ) ) : ?>
                                    <td colspan="2"><?php echo $log["message"]; ?></td>
								<?php else : ?>
                                    <td>
										<?php echo $log["old_amount"] ?>
                                    </td>
                                    <td>
										<?php
										$old_amount = (int) $log["old_amount"];
										$diff       = (int) $log["storage_diff"];

                                        $new_amount = $old_amount + $diff;
                                        echo $new_amount;
										?>
                                    </td>
                                    <td>
										<?php if ( $diff < 0 ) : ?>
                                            <span class="text-danger"><?php echo $diff ?></span>
										<?php else : ?>
                                            <span class="text-success"><?php echo $diff ?></span>
										<?php endif; ?>
                                    </td>
								<?php endif; ?>
                            </tr>
						<?php endforeach; ?>
                    </table>
					<?php
				} else { ?>
                    <p><?php _e( "Zatím žádné změny.", "eso" ) ?></p>
				<?php } ?>
            </div>
        </div>
    </div>
</div>
