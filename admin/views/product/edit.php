<?php
function eso_plugin_products_single() {
	if ( isset( $_GET["id"] ) ) {
		$product = new Eso_Product( (int) $_GET["id"] );

		if ( empty( $product->get_name() ) ) {
			write_log( "Not the right ID when accessing product edit page" );

			return;
		}
	}
	$admin_fields = new Eso_Admin_Fields();
	?>
    <form class="admin-ajax-form admin-ajax-form--no-enter">
        <input type="hidden" name="action" value="eso_admin_ajax"/>
        <input type="hidden" name="eso_action" value="save_product"/>
        <input type="hidden" name="eso_success_callback" value="the_product_view_url"/>
        <input type="hidden" name="eso_callback_target" value="#product_view_link"/>

		<?php if ( isset( $product ) ) : ?>
            <input type="hidden" name="id" value="<?php echo $product->get_id() ?>"/>
		<?php else : ?>
            <input type="hidden" name="id" value="new_post"/>
            <input type="hidden" name="eso_success_redirect"
                   value="<?php echo admin_url( "admin.php?page=eso-products" ) ?>"/>
		<?php endif; ?>

        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-lg-6">
                    <h1 class="">
						<?php if ( isset( $product ) ) {
							echo $product->get_name();
						} else {
							_e( "Nový produkt", "eso" );
						} ?>
                    </h1>
                </div>
                <div class="col-lg-6 text-lg-right">
                    <span id="product_view_link" class="btn btn-link btn-sm"></span>
					<?php if ( isset( $product ) ) : ?>
                        <button type="submit"
                                class="btn btn-secondary btn-lg"> <?php _e( "Uložit", "eso" ) ?></button>
					<?php else : ?>
                        <button type="submit"
                                class="btn btn-secondary btn-lg btn-plus"> <?php _e( "Publikovat", "eso" ) ?></button>
					<?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="whitebox uibox">
                        <div class="row">
                            <div class="col-xl-6">
								<?php if ( isset( $product ) ) : ?>
                                    <div class="form-group row">
                                        <label for="p-ID" class="col-md-3 col-form-label"><?php _e( "ID" ) ?></label>
                                        <div class="col-md-9">
                                            <input type="text" readonly class="pl-0 form-control-plaintext" id="p-ID"
                                                   value="<?php echo $product->get_id() ?>">
                                        </div>
                                    </div>
								<?php endif; ?>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-3 col-form-label"><?php _e( "Název" ) ?></label>
                                    <div class="col-md-9">
                                        <input type="text" id="name" name="name" class="form-control"
                                               value="<?php if ( isset( $product ) )
											       echo $product->get_name() ?>" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ean_code"
                                           class="col-md-3 col-form-label"><?php _e( "EAN code" ) ?></label>
                                    <div class="col-md-9">
                                        <input type="text" id="ean_code" name="ean_code" class="form-control"
                                               value="<?php if ( isset( $product ) )
                                                   echo $product->get_ean_code() ?>"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="slug"
                                           class="col-md-3 col-form-label"><?php _e( "URL", "eso" ) ?></label>
                                    <div class="col-md-9">
                                        <input type="text" id="slug" name="slug" class="form-control"
                                               value="<?php if ( isset( $product ) )
											       echo $product->get_slug() ?>"
                                               placeholder="<?php _e( "Doplní se automaticky", "eso" ) ?>"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="product_tag"
                                           class="col-md-3 col-form-label"><?php _e( "Štítky", "eso" ) ?></label>
                                    <div class="col-md-9">
										<?php if ( isset( $product ) ) {
											$admin_fields->render_product_tags_input( $product->get_tags() );
										} else {
											$admin_fields->render_product_tags_input();
										} ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-xl-6">
                                <div class="form-group row">
                                    <label for="visibility"
                                           class="col-md-3 col-form-label"><?php _e( "Viditelnost", "eso" ) ?></label>
                                    <div class="col-md-9">
                                        <select id="visibility" name="visibility" class="form-control">
                                            <option value="publish" <?php if ( isset( $product ) ) {
												selected( $product->get_visibility(), 'publish' );
											} ?> ><?php _e( "Public" ) ?></option>
                                            <option value="private" <?php if ( isset( $product ) ) {
												selected( $product->get_visibility(), 'private' );
											} ?> ><?php _e( "Private" ) ?></option>
                                        </select>
                                    </div>
                                </div>
								<?php if ( isset( $product ) ) : ?>
                                    <div class="form-group row">
                                        <label for="published"
                                               class="col-md-3 col-form-label"><?php _e( "Published" ) ?></label>
                                        <div class="col-md-9">
                                            <input type="text" readonly class="pl-0 form-control-plaintext" id="p-ID"
                                                   value="<?php if ( isset( $product ) )
												       echo $product->get_date_created() ?>">
                                        </div>
                                    </div>
								<?php endif; ?>
                                <div class="form-group row">
                                    <label for="stock_status"
                                           class="col-md-3 col-form-label"><?php _e( "Stav", "eso" ) ?></label>
                                    <div class="col-md-9">
										<?php
										if ( isset( $product ) ) :
											$admin_fields->render_stock_status_select( $product->get_stock_status_id() );
										else :
											$admin_fields->render_stock_status_select();
										endif;
										?>
                                    </div>
                                </div>
								<?php if ( count( get_terms(
										[
											"taxonomy"   => "product_category",
											"hide_empty" => false
										] ) ) > 0 ) : ?>
                                    <div class="form-group row">
                                        <label for="product_category"
                                               class="col-md-3 col-form-label"><?php _e( "Kategorie", "eso" ) ?></label>
                                        <div class="col-md-9">
											<?php
											if ( isset( $product ) ) :
												$admin_fields->render_product_categories_input( $product->get_categories() );
											else :
												$admin_fields->render_product_categories_input();
											endif;
											?>
                                        </div>
                                    </div>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col pt-4">
                            <label class="eso-label w-100">
                                <span class="eso-label-text"><?php _e( "Krátký popis", "eso" ) ?></span>
                                <textarea name="perex" class="form-control"
                                          rows="4"><?php if ( isset( $product ) )
										echo $product->get_perex() ?></textarea>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h2><?php _e( "Nastavení ceny", "eso" ); ?></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="nav nav-pills eso-pills eso-pills-sm">
								<?php
								/* @var $currency Eso_Currency */
								foreach ( eso_get_enabled_currencies() as $currency ) : ?>
                                <a href="#currency-<?php echo $currency->get_code() ?>"
                                   class="<?php active( $currency->is_default() ) ?>"
                                   data-toggle="pill"><?php echo $currency->get_code() ?>
									<?php endforeach ?>
                                </a>
                            </div>
                            <div class="tab-content">
								<?php
								/* @var $currency Eso_Currency */
								foreach ( eso_get_enabled_currencies() as $currency ) :
									?>
                                    <div id="currency-<?php echo $currency->get_code() ?>"
                                         class="pt-4 pb-3 tab-pane fade in <?php if ( $currency->is_default() ) {
										     echo "show active";
									     } ?>">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                    <label class="eso-label col-md-6"
                                                           for="price_<?php echo $currency->get_code() ?>"><?php _e( "Cena s DPH", "eso" ) ?></label>
                                                    <div class="col-md-6">
                                                        <input type="number"
                                                               id="price_<?php echo $currency->get_code() ?>"
                                                               name="price[<?php echo $currency->get_code() ?>]"
                                                               class="form-control product-price-trigger product-price has-pencil"
                                                               value="<?php if ( isset( $product ) )
															       echo $product->get_price_before_discount( $currency ) ?>"
                                                               required/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-md-6"
                                                           for="tax_<?php echo $currency->get_code() ?>"><?php _e( "DPH", "eso" ) ?></label>
                                                    <div class="col-md-6">
                                                        <select id="tax_<?php echo $currency->get_code() ?>"
                                                                name="tax[<?php echo $currency->get_code() ?>]"
                                                                class="form-control product-price-trigger product-tax-rate">
															<?php
															/* @var $tax_rate Eso_Tax_Rate */
															foreach ( eso_get_tax_rates() as $tax_rate ) : ?>
                                                                <option value="<?php echo $tax_rate->get_id() ?>" <?php if ( isset( $product ) ) {
																	selected( $product->get_tax_rate_id( $currency ), $tax_rate->get_id() );
																} ?>><?php echo $tax_rate->get_value() ?>
                                                                    %
                                                                </option>
															<?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-md-6"
                                                           for="eso_price_without_tax_<?php echo $currency->get_code() ?>"><?php _e( "Cena bez DPH", "eso" ) ?></label>
                                                    <div class="col-md-6">
                                                    <span class="product-price-preview before-discount-without-tax"
                                                          id="price_without_tax_<?php echo $currency->get_code() ?>"><?php if ( isset( $product ) )
		                                                    echo $product->get_price_before_discount_without_tax( $currency ) ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                    <label class="col-form-label col-md-6"
                                                           for="discount_type"><?php _e( "Sleva", "eso" ) ?></label>
                                                    <div class="col-md-6">
                                                        <select id="discount_type"
                                                                name="discount_type[<?php echo $currency->get_code() ?>]"
                                                                class="form-control product-price-trigger product-discount-type">
                                                            <option value="%" <?php if ( isset( $product ) ) {
																selected( $product->get_discount_type( $currency ), "%" );
															} ?>><?php _e( "procenta" ) ?>
                                                                (%)
                                                            </option>
                                                            <option value="-" <?php if ( isset( $product ) ) {
																selected( $product->get_discount_type( $currency ), "-" );
															} ?>><?php _e( "částka" ) ?>
                                                                (<?php echo $currency->get_symbol() ?>)
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-md-6"
                                                           for="discount"><?php _e( "Hodnota", "eso" ) ?></label>
                                                    <div class="col-md-6">
                                                        <input type="number" id="discount"
                                                               name="discount[<?php echo $currency->get_code() ?>]"
                                                               class="form-control product-price-trigger product-discount has-pencil"
                                                               value="<?php if ( isset( $product ) )
															       echo $product->get_discount( $currency ) ?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-md-6"
                                                           for="price_after_discount_without_tax_<?php echo $currency->get_code() ?>"><?php _e( "Výsledná cena bez DPH", "eso" ) ?></label>
                                                    <div class="col-md-6">
                                                    <span class="product-price-preview after-discount-without-tax"
                                                          id="price_after_discount_without_tax_<?php echo $currency->get_code() ?>"><?php if ( isset( $product ) )
		                                                    echo $product->get_price_without_tax( $currency ) ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-md-6"
                                                           for="price_after_discount_<?php echo $currency->get_code() ?>"><?php _e( "Výsledná cena s DPH", "eso" ) ?></label>
                                                    <div class="col-md-6">
                                                    <span class="product-price-preview after-discount"
                                                          id="price_after_discount_<?php echo $currency->get_code() ?>"><?php if ( isset( $product ) )
		                                                    echo $product->get_price( $currency ) ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <hr>
					<?php if ( isset( $product ) ) : ?>
                        <div class="row">
                            <div class="col">
                                <h2 class="mt-2 mb-4"><?php _e( "Vlastnosti produktu", "eso" ) ?></h2>
                            </div>
                        </div>
                        <div class="row variants-wrapper">
							<?php
							if ( $product->get_variants() ) {
								/* @var $variant Eso_Product_Variant */
								foreach ( $product->get_variants() as $variant ) :
									$variant->render_edit_form();
								endforeach;
							}

							$product->admin_render_new_variant_form();
							?>
                        </div>
                        <hr>
                        <div class="row mb-2">
                            <div class="col-lg-8">
                                <h2 class="mt-2 mb-3"><?php _e( "Vývoj prodeje", "eso" ) ?></h2>
                                <div class="whitebox mb-default">
									<?php $admin_fields->render_chart_nav() ?>
                                    <div class="tab-content">
                                        <div id="overview">
                                            <canvas id="overview-graph" width="400" height="300"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row mb-2">
                            <div class="col">
                                <h2 class="mt-2"><?php _e( "Související produkty", "eso" ) ?></h2>
                            </div>
                        </div>
                        <div class="row mb-2" id="related-products-wrapper">
							<?php
							/* @var $related_product Eso_Product */
							foreach ( $product->get_related_products() as $related_product ) : ?>
								<?php $related_product->admin_render_related_product_info( $product->get_id() ) ?>
							<?php endforeach; ?>

                            <div class="col-xl-4 col-lg-6 mb-default available-related-products">
                                <div class="whitebox uibox d-none"></div>
                                <button id="add-related-product" class="btn btn-plus btn-light">
									<?php _e( "Přidat", "eso" ) ?>
                                </button>
                            </div>
                        </div>
                    <?php else : ?>
                    <div class="row">
                        <div class="col">
                            <small><?php _e("Pro vložení souvisejících produktů a variant produkt nejprve uložte.", "eso") ?></small>
                        </div>
                    </div>
					<?php endif; ?>
                </div>

                <div class="col-lg-3">
                    <div class="whitebox uibox">
                        <h2><?php _e( "Produktové fotografie", "eso" ) ?></h2>
                        <div id="eso-product-gallery">
							<?php
							if ( isset( $product ) ) {
								if ( $product->get_images() ) { ?>
									<?php
									$counter = 1;
									foreach ( $product->get_images() as $image_id ) { ?>
                                        <div id="eso-product-image-<?php echo $image_id; ?>"
                                             class="eso-product-image <?php if ( $counter == 1 ) {
											     echo "eso-product-image--main";
										     } ?>">
											<?php
											echo wp_get_attachment_image( $image_id, 'small', false, [
												'id'         => 'image_' . $image_id,
												'data-order' => $counter
											] );
											?>
                                            <div data-id="<?php echo $image_id ?>" class='eso-remove'></div>
                                            <input type="hidden" class="image_id" name="images[]"
                                                   value="<?php echo $image_id ?>">
                                        </div>
										<?php
										$counter ++;
									} ?>
								<?php }
							}; ?>
                        </div>
                        <input type='button' class="button button-primary button-large"
                               value="<?php esc_attr_e( 'Přidat foto', 'eso' ); ?>" id="eso-product-gallery-add"/>
                    </div>
                    <div class="whitebox mt-4">
                        <label class="col-form-label mt-n2" for="color"><?php _e("Barva", "eso") ?></label>
                        <input type="color" id="color" name="color" class="form-control"
                               value="<?php if ( isset( $product ) )
		                           echo $product->get_color() ?>" />
                        <label class="col-form-label mt-2 d-block"><?php _e("Symbol", "eso") ?></label>
                        <div id="product-symbol-image">
	                        <?php if(isset($product) && $product->get_symbol_image_id()) { ?>
                                <img src="<?php echo $product->get_symbol_image_url(); ?>" alt="product symbol" />
	                        <?php } ?>
                        </div>
                        <input type="hidden" name="symbol_image_id" value="<?php
                        if(isset($product) && $product->get_symbol_image_id()) {
                            echo $product->get_symbol_image_id();
                        } ?>" />
                        <button type="button" class="btn btn-outline-primary btn-sm" id="eso-product-symbol-add">
                            <?php _e("Vybrat symbol") ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
	<?php
}