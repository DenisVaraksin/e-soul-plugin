<?php
function eso_plugin_products_categories_page() {
	$fields       = new Eso_Fields();
	$admin_tables = new Eso_Admin_Tables();
	?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1><?php _e( "Kategorie produktů", "eso" ) ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="eso-box mb-default ajax--silent" id="product-category-table">
					<?php $admin_tables->render_product_category_table() ?>
                </div>
                <h1><?php _e( "Vytvořit novou kategorii", "eso" ) ?></h1>
                <div class="eso-box">
                    <form class="admin-ajax-form">
                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                        <input type="hidden" name="eso_action" value="insert_taxonomy_term"/>
                        <input type="hidden" name="eso_success_callback" value="render_product_category_table"/>
                        <input type="hidden" name="eso_callback_target" value="#product-category-table"/>
                        <input type="hidden" name="taxonomy" value="product_category"/>
						<?php $fields->form_group_input( "term_name", "Název kategorie", null ) ?>
                        <button type="submit" class="btn btn-primary btn-lg"><?php _e( "Vytvořit", "eso" ) ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php }