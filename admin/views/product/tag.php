<?php
function eso_plugin_products_tags_page() {
	$fields       = new Eso_Fields();
	$admin_tables = new Eso_Admin_Tables();
	?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1><?php _e( "Štítky produktů", "eso" ) ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="eso-box mb-default" id="product-tag-table">
					<?php if ( ! empty( eso_get_product_tags() ) ) :
						$admin_tables->render_product_tag_table();
					else :
						_e( "Zatím žádné štítky.", "eso" );
					endif; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <h1><?php _e( "Vytvořit nový štítek", "eso" ) ?></h1>
                <div class="eso-box">
                    <form class="admin-ajax-form">
                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                        <input type="hidden" name="eso_action" value="insert_taxonomy_term"/>
                        <input type="hidden" name="eso_success_callback" value="render_product_tag_table"/>
                        <input type="hidden" name="eso_callback_target" value="#product-tag-table"/>
                        <input type="hidden" name="taxonomy" value="product_tag"/>
						<?php $fields->form_group_input( "term_name", "Název", null ) ?>
                        <button type="submit" class="btn btn-primary btn-lg"><?php _e( "Vytvořit", "eso" ) ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
	<?php
}