<?php
function eso_plugin_products_storage() { ?>
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12">
                <h1><?php _e( "Stavy skladu", "eso" ) ?></h1>
            </div>
            <div class="col-12">
				<?php
				$products = new WP_Query( [
					"post_type"      => "esoul_product",
					"posts_per_page" => - 1,
				] );

				if ( $products->have_posts() ) { ?>
                    <form class="admin-ajax-form">
                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                        <input type="hidden" name="eso_action" value="edit_storage"/>
                        <input type="hidden" name="eso_success_redirect" value=""/>
                        <table class="table table-striped table-responsive-xl text-nowrap">
                            <thead>
                            <tr>
                                <th scope="col"
                                    class="manage-column column-photo sortable"><?php _e( "Foto", "eso" ) ?></th>
                                <th scope="col"
                                    class="manage-column column-name sortable"><?php _e( "Název", "eso" ) ?></th>
                                <th scope="col"
                                    class="manage-column column-stock-status sortable"><?php _e( "Stav", "eso" ) ?></th>
                                <th scope="col"
                                    class="manage-column column-count sortable text-right"><?php _e( "Sklad. množství", "eso" ) ?></th>
                                <th scope="col"
                                    class="manage-column column-actions sortable text-right"><?php _e( "Změna", "eso" ) ?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php while ( $products->have_posts() ) {
								$products->the_post();
								$product = new Eso_Product( get_the_ID() );
								?>
                                <tr>
                                    <td><?php
										if ( $product->get_featured_image_id() ) {
											$product->the_featured_image( "table" );
										} else {
											echo eso_empty_thumbnail();
										} ?></td>
                                    <td>
                                        <a href="<?php $product->the_edit_url() ?>"><?php echo $product->get_name() ?></a>
                                    </td>
                                    <td><?php $product->render_stock_status_name() ?></td>
                                    <td id="stock-amount-<?php echo $product->get_id() ?>"
                                        class="text-right"><?php echo $product->get_stock_amount() ?></td>
                                    <td class="float-right">
                                        <input type="number" name="quantity[<?php echo $product->get_id() ?>]" value="0"
                                               title="<?php _e( "Množství", "eso" ) ?>"
                                               class="form-control form-control--digit text-right"/>
                                    </td>
                                    <td>
                                        <button type="button" class="render-storage-log btn btn-text p-0"
                                                data-product="<?php echo $product->get_id() ?>"><?php _e( "Historie změn", "eso" ) ?></button>
                                    </td>
                                </tr>
							<?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right">
									<?php if ( eso_is_comgate_elogist_active() ) : ?>
                                        <button type="button"
                                                class="btn btn-lg btn-primary" data-toggle="modal"
                                                data-target="#storageorder-options">
											<?php _e( "Změnit stav skladu", "eso" ) ?>
                                        </button>
									<?php else : ?>
                                        <button type="submit"
                                                class="btn btn-lg btn-primary">
											<?php _e( "Změnit stav skladu", "eso" ) ?>
                                        </button>
									<?php endif; ?>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="modal fade" id="storageorder-options" tabindex="-1"
                             role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3><?php _e( "Vyberte prosím jednu z možností", "eso" ) ?>
                                        </h3>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="inline-card-wrapper">
                                            <div class="card w-100 mr-sm-3 card-btn card-btn-primary do-storageorder">
                                                <div class="card-body pl-1">
                                                    <h3 class="card-title"><?php _e( "Odeslat také do Comgate", "eso" ) ?></h3>
                                                    <p class="card-text"><?php _e( "Naskladnit v e-shopu i v Comgate", "eso" ) ?></p>
                                                </div>
                                            </div>
                                            <button type="submit" class="card w-100 card-btn card-btn-primary">
                                                <div class="card-body pl-1">
                                                    <h3 class="card-title"><?php _e( "Pouze e-shop", "eso" ) ?></h3>
                                                    <p class="card-text"><?php _e( "Naskladnit pouze v e-shopu", "eso" ) ?></p>
                                                </div>
                                            </button>
                                        </div>
                                        <div class="storageorder-options" style="display: none;">
                                            <label class="eso-label w-100">
                                                <span class="eso-label-text"><?php _e( "Cílový sklad", "eso" ) ?></span>
												<?php if ( eso_is_comgate_elogist_active() ) {
													$comgate_elogist = new Eso_Comgate_Elogist();
													$comgate_elogist->render_enabled_countries();
												}
												?>
                                            </label>
                                            <label class="eso-label w-100">
                                                <span class="eso-label-text"><?php _e( "Dodavatel", "eso" ) ?></span>
                                                <input type="text"
                                                       name="storageorder-supplier" class="form-control"/>
                                            </label>
                                            <label class="eso-label w-100">
                                                <span class="eso-label-text"><?php _e( "Poznámka", "eso" ) ?></span>
                                                <input type="text" name="storageorder-note" class="form-control"/>
                                            </label>
                                            <button type="submit" class="btn btn-lg btn-primary mt-2">
												<?php _e( "Naskladnit", "eso" ) ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
					<?php wp_reset_postdata();
				} ?>
            </div>

        </div>
    </div>
<?php }