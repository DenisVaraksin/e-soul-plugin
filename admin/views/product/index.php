<?php
function eso_products_page() {
	$fields       = new Eso_Fields();
	$admin_fields = new Eso_Admin_Fields();
	$admin_query  = new Eso_Admin_Query();
	?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <h1><?php _e( "Seznam produktů", "eso" ) ?></h1>
            </div>
            <div class="col-md-4 text-md-right">
                <a href="<?php echo admin_url( "admin.php?page=single-product" ) ?>"
                   class="btn btn-secondary btn-lg btn-plus">
					<?php _e( "Přidat produkt", "eso" ) ?>
                </a>
            </div>
        </div>
    </div>
    <form class="index-filter form-inline">
        <input type="hidden" name="page" value="eso-products"/>
		<?php
		( ! empty( $_GET["search_id"] ) ) ? $search_id = (int) $_GET["search_id"] : $search_id = null;

		$fields->form_group_input( "search_id", "ID", $search_id, $search_id, "number", [
			'class'       => 'form-control--digit',
			'placeholder' => '#',
			'min'         => 1
		] );

		( ! empty( $_GET["title"] ) ) ? $title = $_GET["title"] : $title = null;

		$fields->form_group_input( "title", "Název", $title, $title, "text" )
		?>
        <div class="form-group">
            <label for="stock_status" class="eso-box__label">
				<?php _e( "Stav", "eso" ) ?>
            </label>
			<?php
			if ( ! empty( $_GET["stock_status"] ) ) {
				$stock_status = (int) $_GET["stock_status"];
			} else {
				$stock_status = null;
			}

			$admin_fields->render_stock_status_select( $stock_status, false ); ?>
        </div>

        <div class="form-group">
			<?php
			( ! empty( $_GET["product_category"] ) ) ? $product_category = (int) $_GET["product_category"] : $product_category = null;

			$admin_fields->render_product_category_select( true, $product_category, true );
			?>
        </div>

        <div class="form-group">
		    <?php
		    ( ! empty( $_GET["product_tag"] ) ) ? $product_tag = $_GET["product_tag"] : $product_tag = null;

		    $admin_fields->render_product_tag_select($product_tag, true);
		    ?>
        </div>

        <button type="submit" class="btn btn-primary btn-lg">
			<?php echo eso_icon( "filter" ) . " " . __( "Filtrovat", "eso" ); ?>
        </button>

        <?php eso_index_filter_reset("eso-products"); ?>
    </form>

    <!--Table -->
	<?php
	$products = $admin_query->posts_index( "esoul_product" );

	if ( $products->have_posts() ) {
		$order = $admin_query->switch_ordering();
		?>
        <!-- Table header -->
        <div class="round-corners">
            <table class="table table-responsive-xl table-hovered table-striped text-nowrap">
                <thead>
                <tr>
                    <th scope="col">
                        <a href="<?php echo current_url() ?>&orderby=ID&order=<?php echo $order ?>">
							<?php _e( "ID", "eso" ) ?><?php echo eso_sort_icon(); ?>
                        </a>
                    </th>
                    <th scope="col">
						<?php _e( "Foto", "eso" ) ?>
                    </th>
                    <th scope="col">
                        <a href="<?php echo current_url() ?>&orderby=title&order=<?php echo $order ?>">
							<?php _e( "Název", "eso" ) ?><?php echo eso_sort_icon(); ?>
                        </a>
                    </th>
                    <th scope="col">
						<?php _e( "Stav", "eso" ) ?>
                    </th>
                    <th scope="col">
                        <a href="<?php echo current_url() ?>&orderby=meta_value_num&meta_key=stock_amount&order=<?php echo $order ?>">
						<?php _e( "Množství", "eso" ) ?><?php echo eso_sort_icon(); ?>
                        </a>
                    </th>
                    <th scope="col">
                        <a href="<?php echo current_url() ?>&orderby=product_category&order=<?php echo $order ?>">
							<?php _e( "Kategorie", "eso" ) ?><?php echo eso_sort_icon(); ?>
                        </a>
                    </th>
                    <th scope="col">
		                <?php _e( "Štítek", "eso" ) ?>
                    </th>
                    <th scope="col">
                        <a href="<?php echo current_url() ?>&orderby=meta_value_num&meta_key=price_<?php echo eso_get_active_currency_code() ?>&order=<?php echo $order ?>">
                        <?php _e( "Cena", "eso" ) ?><?php echo eso_sort_icon(); ?>
                        </a>
                    </th>
                    <th scope="col">
                        <a href="<?php echo current_url() ?>&orderby=meta_value_num&meta_key=turnover_<?php echo eso_get_active_currency_code() ?>&order=<?php echo $order ?>">
                        <?php _e( "Tržby", "eso" ) ?><?php echo eso_sort_icon(); ?>
                        </a>
                    </th>
                    <th scope="col">
						<?php _e( "Akce", "eso" ) ?>
                    </th>
                </tr>
                </thead>
                <!-- Table body -->
				<?php while ( $products->have_posts() ) {
					$products->the_post();
					$product = new Eso_Product( get_the_ID() );
					?>
                    <tr>
                        <td>#<?php echo $product->get_id() ?></td>
                        <td>
							<?php
							if ( $product->get_featured_image_id() ) {
								$product->the_featured_image( "table" );
							} else {
								echo eso_empty_thumbnail();
							} ?>
                        </td>
                        <td>
                            <a href="<?php $product->the_edit_url() ?>"><?php echo $product->get_name() ?>
                        </td>
                        <td class="stock-status--<?php echo $product->get_stock_status_slug() ?>"><?php echo $product->get_stock_status_name() ?></td>
                        <td><?php echo $product->get_stock_amount() ?></td>
                        <td><?php echo $product->get_category_list() ?></td>
                        <td><?php echo $product->get_tag_list() ?></td>
                        <td><?php echo $product->get_price( eso_get_active_currency(), true, true ) ?></td>
                        <td>
                            <form class="admin-ajax--onready">
                                <div class="admin-ajax-result"></div>
                                <input type="hidden" name="action" value="eso_admin_ajax"/>
                                <input type="hidden" name="eso_action" value="get_product_turnover"/>
                                <input type="hidden" name="product_id" value="<?php echo $product->get_id() ?>"/>
                                <input type="hidden" name="currency_code" value="<?php echo eso_get_active_currency_code() ?>"/>
                            </form>
                        </td>
                        <td>
                            <ul class="table-actions actions">
                                <li>
                                    <a href="<?php $product->the_edit_url() ?>" data-toggle="tooltip"
                                       title="<?php _e( "Upravit produkt", "eso" ) ?>"><?php echo eso_icon( "settings" ) ?></a>
                                </li>
                                <li>
                                    <a href="<?php $product->the_block_editor_url() ?>" data-toggle="tooltip"
                                       title="<?php _e( "Upravit obsah", "eso" ) ?>"><?php echo eso_icon( "edit-interface-sign" ) ?></a>
                                </li>
                                <li>
                                    <form class="admin-ajax-form"
                                          data-confirm="<?php _e( "Skutečně chcete tento produkt smazat?", "eso" ) ?>">
                                        <input type="hidden" name="action" value="eso_admin_ajax"/>
                                        <input type="hidden" name="eso_action" value="delete_product"/>
                                        <input type="hidden" name="product_id"
                                               value="<?php echo $product->get_id() ?>"/>
                                        <input type="hidden" name="eso_success_redirect"
                                               value="<?php echo current_url() ?>"/>
                                        <button type="submit" class="btn btn-link" data-toggle="tooltip"
                                                title="<?php _e( "Smazat produkt", "eso" ) ?>">
											<?php echo eso_icon( "trash" ) ?>
                                        </button>
                                    </form>
                                </li>

                            </ul>

                        </td>
                    </tr>

				<?php } ?>
            </table>
        </div>

		<?php
		$admin_query->pagination( $products->max_num_pages );

		wp_reset_postdata();
	} else {
		echo "<p>";
		if ( ! empty( $_GET["search_id"] ) || ! empty( $_GET["title"] ) || ! empty( $_GET["stock_status"] ) || ! empty( $_GET["product_category"] ) ) {
			_e( "Zadaným filtrům neodpovídají žádné produkty" );
			echo "<br><br><a class='btn btn-primary' href='" . admin_url( "admin.php?page=eso-products" ) . "'>" . __( "Resetovat parametry vyhledávání", "eso" ) . "</a>";
		} else {
			_e( "Zatím žádné produkty", "eso" );
		}
		echo "</p>";
	}
}

