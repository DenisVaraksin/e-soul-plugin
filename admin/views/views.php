<?php
require_once( ESO_DIR . '/admin/views/dashboard/index.php' );
require_once( ESO_DIR . '/admin/views/customer/edit.php' );
require_once( ESO_DIR . '/admin/views/customer/index.php' );
require_once( ESO_DIR . '/admin/views/customer/groups.php' );
require_once( ESO_DIR . '/admin/views/product/index.php' );
require_once( ESO_DIR . '/admin/views/product/edit.php' );
require_once( ESO_DIR . '/admin/views/product/tag.php' );
require_once( ESO_DIR . '/admin/views/product/stock-status.php' );
require_once( ESO_DIR . '/admin/views/product/category.php' );
require_once( ESO_DIR . '/admin/views/product/storage.php' );
require_once( ESO_DIR . '/admin/views/order/index.php' );
require_once( ESO_DIR . '/admin/views/order/edit.php' );
require_once( ESO_DIR . '/admin/views/order/status.php' );
require_once( ESO_DIR . '/admin/views/settings/menu.php' );
require_once( ESO_DIR . '/admin/views/settings/api.php' );
require_once( ESO_DIR . '/admin/views/settings/coupons.php' );
require_once( ESO_DIR . '/admin/views/settings/newsletter.php' );
require_once( ESO_DIR . '/admin/views/settings/coupon-edit.php' );
require_once( ESO_DIR . '/admin/views/settings/general.php' );
require_once( ESO_DIR . '/admin/views/settings/invoices.php' );
require_once( ESO_DIR . '/admin/views/settings/payments.php' );
require_once( ESO_DIR . '/admin/views/settings/shipping-method.php' );
require_once( ESO_DIR . '/admin/views/settings/advanced.php' );

require_once( ESO_DIR . '/admin/views/Eso_Admin_Fields.php' );
require_once( ESO_DIR . '/admin/views/Eso_Admin_Tables.php' );