<?php /* @var $this Eso_Module */ ?>
    <div class="eso-card d-sm-flex" data-toggle="collapse" data-target="#settings-<?php echo $this->get_code() ?>"
         aria-expanded="false" aria-controls="settings-<?php echo $this->get_code() ?>">
        <div class="eso-card__left">
        <span class="eso-card__image">
            <img
                    src="<?php echo ESO_URL . "assets/img/modules/" . $this->get_code() . ".png" ?>"
                    alt="<?php echo $this->get_code() ?>"/>
        </span>
            <span class="eso-card__label"><?php echo $this->get_name(); ?></span>
        </div>
        <span class="eso-card__label mt-1"><?php _e( "Stav: ", "eso" ); ?>
            <div id="<?php echo $this->get_code() ?>-status" class="d-inline-block">
                <?php
                if ( $this->is_enabled() ) {
	                echo "<span class='text-success'>" . __( "Aktivní", "eso" ) . "</span>";
                } else {
	                echo "<span class='text-danger'>" . __( "Neaktivní", "eso" ) . "</span>";
                } ?>
            </div>
        </span>
        <div class="eso-cart__right">
			<?php echo eso_icon( "down", "eso-card-trigger" ) ?>
        </div>
    </div>
<?php $this->render_settings() ?>