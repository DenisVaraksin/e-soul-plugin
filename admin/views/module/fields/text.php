<?php /* @var $field */ ?>
<label class="eso-label w-100 mt-2 mb-2">
	<span class="eso-label-text"><?php echo $field->name ?></span>
	<input type="text" name="fields[<?php echo $field->code ?>]"
	       value="<?php echo $field->value ?>"
	       class="form-control"
		<?php
		if ( $field->atts ) {
			$field_atts = maybe_unserialize( $field->atts );

			foreach ( $field_atts as $att_label => $att_value ) {
				echo $att_label . '="' . $att_value . '"';
			}
		}
		if ( $field->required == "1" ) {
			echo "required";
		} ?>>
</label>