<?php /* @var $field */ ?>
<label class="mt-2 mb-1"><?php echo $field->name ?></label>
<div class="custom-file mb-2 mt-0">
	<input type="file" class="custom-file-input" name="files[<?php echo $field->code ?>]" id="module-file-<?php echo $field->code ?>"
	       value="<?php echo $field->value ?>" onChange="getFileInputPlaceholder()"
		<?php
		if ( $field->atts ) {
			$field_atts = maybe_unserialize( $field->atts );

			foreach ( $field_atts as $att_label => $att_value ) {
				echo $att_label . '="' . $att_value . '"';
			}
		}
		if ( $field->required == "1" ) {
			echo "required";
		} ?>>
	<label class="custom-file-label" for="module-file-<?php echo $field->code ?>"><?php echo $field->name ?></label>
</div>