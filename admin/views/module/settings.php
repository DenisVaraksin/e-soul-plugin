<?php /* @var $this Eso_Module */ ?>
<div class="eso-card__settings collapse" id="settings-<?php echo $this->get_code() ?>">
    <div class="eso-box inline">
        <form class="admin-ajax-form" enctype="multipart/form-data">
            <input type="hidden" name="action" value="eso_admin_ajax"/>
            <input type="hidden" name="eso_action" value="module_update_settings"/>
            <input type="hidden" name="eso_success_callback" value="module_update_status" />
            <input type="hidden" name="eso_callback_target" value="#<?php echo $this->get_code() ?>-status" />
            <input type="hidden" name="code" value="<?php echo $this->get_code() ?>"/>
			<?php foreach ( $this->get_data() as $field ) {
			    $this->render_field($field);
			} ?>
            <label class="eso-label w-100 mt-2">
                <label class="switch">
                    <input type="checkbox" name="enabled" <?php checked( true, $this->is_enabled() ) ?>>
                    <span class="slider round"></span>
                </label>
                <span class="eso-label-text"><?php _e( "Aktivní", "eso" ) ?></span>
            </label>
            <button type="submit" class="btn btn-lg btn-primary mt-2"><?php _e( "Uložit", "eso" ) ?></button>
            <div class="text-right float-right mt-3">
		        <?php eso_the_docs_link( $this->get_code() ) ?>
            </div>
        </form>
    </div>
</div>
