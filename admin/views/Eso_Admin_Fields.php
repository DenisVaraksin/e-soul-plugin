<?php
if ( ! class_exists( 'Eso_Admin_Fields', false ) ) {
	/**
	 * @since 2019.6
	 *
	 * @property Eso_Fields fields
	 */
	class Eso_Admin_Fields {
		public function __construct() {
			$this->fields = new Eso_Fields();
		}

		/**
		 * @param null $current_value
		 * @param bool $required
		 */
		public function render_stock_status_select( $current_value = null, $required = true ) { ?>
            <select id="stock_status" name="stock_status" class="form-control"
                    title="<?php _e( "Stav skladu pro produkt", "eso" ) ?>">
				<?php
				if ( ! $required ) {
					echo "<option value=''></option>";
				}
				foreach ( eso_stock_status_terms() as $term ) : ?>
                    <option value="<?php echo $term->term_id ?>" <?php if ( ! empty( $current_value ) ) {
						selected( $current_value, $term->term_id );
					} ?>><?php echo $term->name ?></option>
				<?php endforeach; ?>
            </select>
		<?php }

		/**
		 * @param null $current_value
		 * @param bool $label
		 *
		 * @since 2020.3.2 allow multiple tags selection
		 *
		 */
		public function render_product_tags_input( $current_value = null, $label = false ) {
			if ( $label ) { ?>
                <label for="product_tag" class="eso-box__label"><?php _e( "Štítky", "eso" ) ?></label>
			<?php }
			?>
            <input type="hidden" id="all_product_tags" value="<?php
			/** @var Eso_Product_Tag $product_tag */
			foreach ( eso_get_product_tags() as $product_tag ) {
				echo $product_tag->get_name() . ",";
			}
			?>">
            <input id="product_tag" name="product_tag" value="<?php
			if ( $current_value ) {
				foreach ( $current_value as $term ) {
					echo $term->name . ",";
				}
			}
			?>" class="form-control"/>
		<?php }

		/**
		 * @param null $current_value
		 * @param bool $label
		 */
		public function render_product_tag_select( $current_value = null, $label = false ) {
			if ( empty( eso_get_product_tags() ) ) {
				return;
			}

			if ( $label ) { ?>
                <label for="product_tag" class="eso-box__label"><?php _e( "Štítek", "eso" ) ?></label>
			<?php }
			?>
            <select id="product_tag" name="product_tag" class="form-control"
                    title="<?php _e( "Štítek produktu", "eso" ) ?>">
                <option value=''></option>
				<?php
				/* @var $term Eso_Product_Tag */
				foreach ( eso_get_product_tags() as $term ) : ?>
                    <option value="<?php echo $term->get_slug() ?>" <?php if ( ! empty( $current_value ) ) {
						selected( $current_value, $term->get_slug() );
					} ?>><?php echo $term->get_name() ?></option>
				<?php endforeach; ?>
            </select>
		<?php }

		/**
		 * @param bool $hide_empty
		 * @param null $current_value
		 * @param string $label
		 *
		 * @return null
		 */
		public function render_product_category_select( $hide_empty = true, $current_value = null, $label = null ) {
			$terms = eso_get_product_categories( '0', [], $hide_empty );

			if ( count( $terms ) < 1 ) { ?>
                <select id="product_category" name="product_category" class="form-control"></select>
				<?php return null;
			}

			if ( $label ) { ?>
                <label for="product_category" class="eso-box__label"><?php _e( 'Kategorie', 'eso' ) ?></label>
			<?php }
			?>
            <select id="product_category" name="product_category" class="form-control"
                    title="<?php _e( 'Kategorie produktu', 'eso' ) ?>">
                <option value=''></option>
				<?php foreach ( $terms as $term ) : ?>
                    <option value="<?php echo $term->term_id ?>" <?php if ( ! empty( $current_value ) ) {
						selected( $current_value, $term->term_id );
					} ?>><?php echo $term->name ?></option>
					<?php $children = eso_get_product_categories( $term->term_id, [], false );
					if ( ! empty( $children ) ) {
						foreach ( $children as $child ) { ?>
                            <option value="<?php echo $child->term_id ?>" <?php if ( ! empty( $current_value ) ) {
								selected( $current_value, $child->term_id );
							} ?>>– <?php echo $child->name ?></option>

							<?php $grandchildren = eso_get_product_categories( $child->term_id, [], false );
							if ( ! empty( $grandchildren ) ) {
								foreach ( $grandchildren as $grandchild ) { ?>
                                    <option value="<?php echo $grandchild->term_id ?>" <?php if ( ! empty( $current_value ) ) {
										selected( $current_value, $grandchild->term_id );
									} ?>>–– <?php echo $grandchild->name ?></option>
								<?php }
							}
							?>
						<?php }
					}
					?>
				<?php endforeach; ?>
            </select>
		<?php }

		/**
		 * @param null $current_value
		 * @param bool $label
		 *
		 * @since 2020.3.5
		 */
		public function render_product_categories_input( $current_value = null, $label = false ) {
			if ( $label ) { ?>
                <label for="product_category" class="eso-box__label"><?php _e( "Kategorie", "eso" ) ?></label>
			<?php }
			?>
            <input type="hidden" id="all_product_categories" value="<?php
			foreach ( eso_get_product_categories(null, [], false, true) as $term ) {
				echo $term->name . ",";
			}
			?>">
            <input id="product_category" name="product_category" value="<?php
			if ( $current_value ) {
				foreach ( $current_value as $term ) {
					echo $term->name . ",";
				}
			}
			?>" class="form-control"/>
		<?php }

		/**
		 * @param null $current_value
		 * @param bool $required
		 */
		public function render_order_status_select( $current_value = null, $required = true ) { ?>
            <select name="order_status" class="form-control order-status-select"
                    title="<?php _e( "Stav objednávky", "eso" ) ?>">
				<?php
				if ( ! $required ) {
					echo "<option value=''></option>";
				}
				$terms = get_terms( [ 'taxonomy' => 'order_status', 'hide_empty' => false ] );
				foreach ( $terms as $term ) { ?>
                    <option value="<?php echo $term->term_id ?>" <?php if ( ! empty( $current_value ) ) {
						selected( $current_value, $term->term_id );
					} ?>
                    ><?php echo $term->name ?></option>
				<?php }
				?>
            </select>
		<?php }

		/**
		 * @param null $current_value
		 * @param bool $required
		 *
		 * @since 2020.2.7
		 * @TODO to implement
		 */
		public function render_coupon_select( $current_value = null, $required = true ) { ?>
            <select name="coupon" class="form-control coupon-select" title="<?php _e( "Slevový kód", "eso" ) ?>">
				<?php
				if ( ! $required ) {
					echo "<option value=''></option>";
				}
				$terms = get_terms( [ 'taxonomy' => 'order_status', 'hide_empty' => false ] );
				foreach ( $terms as $term ) { ?>
                    <option value="<?php echo $term->term_id ?>" <?php if ( ! empty( $current_value ) ) {
						selected( $current_value, $term->term_id );
					} ?>
                    ><?php echo $term->name ?></option>
				<?php }
				?>
            </select>
		<?php }

		/**
		 * @param bool $hide_empty
		 * @param null $current_value
		 * @param bool $required
		 */
		public function render_customer_group_select( $hide_empty = false, $current_value = null, $required = true, $name = "customer_group" ) { ?>
            <select name="<?php echo $name ?>" title="<?php _e( "Skupina", "eso" ) ?>">
				<?php
				if ( ! $required ) {
					echo "<option value=''>" . __( "Všechny", "eso" ) . "</option>";
				}
				$groups = eso_get_customer_groups( $hide_empty );
				/* @var $group Eso_Customer_Group */
				foreach ( $groups as $group ) { ?>
                    <option value="<?php echo $group->get_id() ?>" <?php if ( ! empty( $current_value ) ) {
						selected( $current_value, $group->get_id() );
					} ?>
                    ><?php echo $group->get_name() ?></option>
				<?php }
				?>
            </select>
		<?php }

		/**
		 * @since 2019.7
		 */
		public function render_chart_nav() { ?>
            <div class="nav nav-pills eso-pills eso-pills-sm graph-nav">
	<span class="active overview-trigger" data-target="this-week" data-from="<?php
	$day = new DateTime();
	try {
		$day->sub( new DateInterval( "P6D" ) );
		echo $day->format( "Y-m-d" );
	} catch ( Exception $e ) {
		write_log( $e );
	} ?>" data-till="<?php $today = new DateTime();
	echo $today->format( 'Y-m-d' ); ?>"
          data-toggle="pill"><?php _e( "Posledních 7 dní", "eso" ) ?>
	</span>
                <span class="overview-trigger" data-target="this-month"
                      data-from="<?php $day = new DateTime( "first day of this month" );
				      echo $day->format( "Y-m-d" ); ?>"
                      data-till="<?php $day = new DateTime();
				      echo $day->format( "Y-m-d" ); ?>"
                      data-toggle="pill"><?php _e( "Tento měsíc", "eso" ) ?>
    </span>
                <span class="overview-trigger" data-target="last-month"
                      data-from="<?php $day = new DateTime( "first day of last month" );
				      echo $day->format( "Y-m-d" ); ?>"
                      data-till="<?php $day = new DateTime( "last day of last month" );
				      echo $day->format( "Y-m-d" ); ?>"
                      data-toggle="pill"><?php _e( "Minulý měsíc", "eso" ) ?>
    </span>
                <span class="overview-trigger" data-target="this-year"
                      data-from="<?php echo date( "Y" ) ?>-01-01"
                      data-till="<?php echo date( "Y" ) ?>-12-31"
                      data-toggle="pill"><?php _e( "Tento rok", "eso" ) ?>
    </span>
                <span class="overview-trigger" data-target="last-year"
                      data-from="<?php $y = date( "Y" );
				      echo $y - 1; ?>-01-01"
                      data-till="<?php $y = date( "Y" );
				      echo $y - 1; ?>-12-31"
                      data-toggle="pill"><?php _e( "Minulý rok", "eso" ) ?>
    </span>
            </div>
		<?php }

		/**
		 * @since 2019.11
		 */

		public function render_chart_nav_small() { ?>
            <div class="nav nav-pills eso-pills eso-pills-sm graph-nav">
	<span class="active overview-trigger d-none" data-target="this-week" data-from="<?php
	$day = new DateTime();
	try {
		$day->sub( new DateInterval( "P6D" ) );
		echo $day->format( "Y-m-d" );
	} catch ( Exception $e ) {
		write_log( $e );
	} ?>" data-till="<?php $today = new DateTime();
	echo $today->format( 'Y-m-d' ); ?>"
          data-toggle="pill"><?php _e( "Posledních 7 dní", "eso" ) ?>
    </span>
            </div>
		<?php }

		/**
		 * @param $name
		 * @param $current_value
		 * @param $label
		 * @param int $value
		 */
		public function checkbox( $name, $label, $current_value, $value = 1 ) { ?>
            <label>
                <label class="switch">
                    <input type="checkbox" type="checkbox"
                           name="<?php echo $name ?>"
                           id="<?php echo $name ?>"
                           value="<?php echo $value ?>"
						<?php checked( $value, $current_value ) ?>>
                    <span class="slider round"></span>
                </label>
                <span class="eso-label-text"><?php echo $label ?></span>
            </label>
		<?php }

		/**
		 * @param $label
		 * @param $value
		 */
		public function box_row( $label, $value ) { ?>
            <div class="row">
                <div class="col-sm-4">
                    <span class="eso-box__label"><?php echo $label ?></span>
                </div>
                <div class="col-sm-8">
					<?php echo $value ?>
                </div>
            </div>
		<?php }

		/**
		 * @param $label
		 * @param $value
		 */
		public function box_row_info( $label, $value ) { ?>
            <div class="row">
                <div class="col-sm-7">
                    <span><?php echo $label ?></span>
                </div>
                <div class="col-sm-5">
                    <span class="eso-box__label"><?php echo $value ?></span>
                </div>
            </div>
		<?php }

		/**
		 * @param $name
		 * @param $label
		 * @param $current_value
		 * @param string $type
		 * @param array $attributes
		 */
		public function dynamic_row_input( $name, $label, $current_value, $type = "text", $attributes = [] ) { ?>
            <div class="row">
                <div class="col-sm-4">
                    <label for="<?php echo $name ?>"
                           class="eso-box__label"><?php _e( $label, "eso" ) ?></label>
                </div>

                <div class="col-sm-8">
                    <input id="<?php echo $name ?>" type="<?php echo $type ?>" readonly
                           name="<?php echo $name ?>"
                           value="<?php echo $current_value ?>"
						<?php $this->fields->render_attributes( $attributes ) ?>
                    />
                </div>
            </div>
		<?php }

		/**
		 * @param $name
		 * @param $label
		 * @param $type
		 * @param $current_value
		 * @param $attributes
		 */
		public function dynamic_row_select( $name, $label, $type, $current_value, $attributes = [] ) { ?>
            <div class="row">
                <div class="col-sm-4">
                    <label for="<?php echo $name ?>"
                           class="eso-box__label"><?php _e( $label, "eso" ) ?></label>
                </div>
                <div class="col-sm-8">
					<?php if ( $type == "all_countries" ) {
						$this->fields->render_country_select( $name, $label, true, $current_value, $attributes );
					} else if ( $type == "enabled_countries" ) {
						$this->fields->render_country_select( $name, $label, false, $current_value, $attributes );
					} ?>
                </div>
            </div>
		<?php }

	}
}