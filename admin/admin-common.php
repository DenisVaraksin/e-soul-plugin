<?php
function eso_admin_notice( $message, $type = "info" ) { ?>

    <div class='notice notice-<?php echo $type ?> is-dismissible'>
        <p><?php echo $message ?></p>
    </div>
	<?php
}

/**
 * @since 2019.5
 */
function eso_get_docs_url( $page ) {
	switch ( $page ) :
		case "home":
			return "http://esoul-docs.marketsoul.cz/";
			break;
		case "comgate_logistika_sk":
		case "comgate_logistika":
			return "http://esoul-docs.marketsoul.cz/comgate-logistika/";
			break;
		case "comgate_payments":
			return "http://esoul-docs.marketsoul.cz/comgate-platebni-brana/";
			break;
		case "google_analytics":
			return "http://esoul-docs.marketsoul.cz/google-analytics/";
			break;
		case "gtm":
			return "http://esoul-docs.marketsoul.cz/google-tag-manager/";
			break;
		case "facebook_feed":
			return "http://esoul-docs.marketsoul.cz/facebook-feed/";
		case "instagram_feed":
			return "http://esoul-docs.marketsoul.cz/instagram-feed/";
		case "coupons":
			return "https://esoul-docs.marketsoul.cz/slevove-kupony/";
        case "newsletter":
            return "https://esoul-docs.marketsoul.cz/newsletter/";
        case "gpwebpay":
            return "https://esoul-docs.marketsoul.cz/platebni-brana-gp-webpay/";
		default:
			return null;

	endswitch;
}

/**
 * @since 2019.5
 */
function eso_the_docs_link( $page, $title = null ) {

	if ( eso_get_docs_url( $page ) ) {

		if ( ! $title ) {
			$label = eso_icon( "info", "float-right" );
		} else {
			$label = $title;
		}

		echo "<a href='" . eso_get_docs_url( $page ) . "' target='_blank' title='" . __( "Odkaz na dokumentaci", "eso" ) . "'>" . $label . "</a>";
	}
}

/**
 * @since 0.0.22
 * @since 2019.6 moved to admin-common.php
 */
function eso_admin_ajax() {

	$eso_admin_ajax = new Eso_Admin_Ajax( $_POST );

	$eso_admin_ajax->do_action();

	wp_die();
}

add_action( 'wp_ajax_eso_admin_ajax', 'eso_admin_ajax' );

/**
 * @since 2019.6
 *
 * @return string
 */
function eso_admin_breadcrumb() {
	$screen    = get_current_screen();
	$screen_id = $screen->id;

	if ( in_array( $screen_id, [ "page" ] ) ) {
		return "";
	}

	$path = "";

	if ( strpos( $screen_id, "obchod_page_eso" ) === 0 ) {
		$path .= "<a href='" . admin_url( "admin.php?page=eso-main-options" ) . "'>" . __( "Obchod", "eso" ) . "</a>";
	} else if ( $screen_id == "zakaznici_page_eso-customer-detail" ) {
		$path .= "<a href='" . admin_url( "admin.php?page=eso-customers" ) . "'>" . __( "Seznam zákazníků", "eso" ) . "</a>";
	} else if ( $screen_id == "admin_page_eso-coupon-edit" ) {
		$path .= "<a href='" . admin_url( "admin.php?page=eso-coupons-options" ) . "'>" . __( "Slevové kupony", "eso" ) . "</a>";
	} else if ( $screen_id == "admin_page_eso-shipping-method-options" ) {
		$path .= "<a href='" . admin_url( "admin.php?page=eso-payments-options" ) . "'>" . __( "Doprava a platby", "eso" ) . "</a>";
	} else if ( $screen_id == "objednavky_page_single-order" ) {
		$path .= "<a href='" . admin_url( "admin.php?page=eso-orders" ) . "'>" . __( "Seznam objednávek", "eso" ) . "</a>";
	} else if ( $screen_id == "produkty_page_single-product" ) {
		$path .= "<a href='" . admin_url( "admin.php?page=eso-products" ) . "'>" . __( "Seznam produktů", "eso" ) . "</a>";
	} else if ( strpos( $screen_id, "produkty_page_products" ) === 0 ) {
		$path .= "<a href='" . admin_url( "admin.php?page=eso-products" ) . "'>" . __( "Produkty", "eso" ) . "</a>";
	} else if ( in_array( $screen_id, [ "themes", "widgets", "nav-menus", "theme-editor" ] ) ) {
		$path .= "<a href='" . admin_url( "themes.php" ) . "'>" . __( "Vzhled", "eso" ) . "</a>";
	}

	if ( ! empty( $path ) ) {
		$path .= " – ";
	}

	$path .= get_admin_page_title();

	return $path;
}

/**
 * @since 2019.7
 *
 * @param $page
 */
function eso_index_filter_reset( $page ) {
	if ( ! isset( $_GET["search_id"] ) ) {
		return;
	}
	?>
    <div class="index-filter-reset">
        <a href="<?php echo admin_url( "admin.php?page=$page" ) ?>" data-toggle="tooltip"
           title="<?php _e( "Zrušit filtr", "eso" ) ?>"
        ><?php echo eso_icon( "close-button" ) ?></a>
    </div>

<?php }

/**
 * @since 2019.8
 *
 * @internal This can be slow, should be called with AJAX
 */
function eso_board_check() {
	$host = file_get_contents( "https://esoul-docs.marketsoul.cz?host_key=rjk6l3Jak71z&host_url=" . get_home_url() . "&host_ip=" . $_SERVER['REMOTE_ADDR'] );

	if($host != "OK") {
		write_log("Unable to connect to esoul-docs.marketsoul.cz - response " . $host);

		deactivate_plugins( ESO_BASENAME );

		die("<p>Vytvoření licenčního souboru selhalo. Kontaktujte nás prosím na <a href='mailto:svehla@marketsoul.cz'>svehla@marketsoul.cz</a></p>");
	}
}

/**
 * @since 2019.7
 */
function eso_generate_xls( $args ) {
	if ( ! isset( $args["generateXls"] ) ) {
		return;
	}

	$export = new Eso_Export();

	if ( $args["page"] == "eso-customers" ) {
		try {
			$export->customers();
		} catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
			write_log( $e->getMessage() );
		}
	} else if ( $args["page"] == "eso-orders" ) {
		try {
			$export->orders();
		} catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
			write_log( $e->getMessage() );
		}
	} else if($args["page"] == "eso-newsletter-options") {
	    try {
	        $export->newsletter_subscribers();
        } catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
		    write_log( $e->getMessage() );
	    }
    }
}