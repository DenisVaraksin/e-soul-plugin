<?php
/**
 * @since 2019.7
 *
 * @return array
 */
function eso_get_order_statuses() {
	$terms = get_terms( [ 'taxonomy' => 'order_status', 'hide_empty' => false ] );

	$statuses = [];

	foreach ( $terms as $term ) {
		$status     = new Eso_Order_Status( $term->term_id );
		$statuses[] = $status;
	}

	return $statuses;
}