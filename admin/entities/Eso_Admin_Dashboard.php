<?php
if(!class_exists('Eso_Admin_Dashboard', false)) {
	class Eso_Admin_Dashboard {
		public function __construct() {

		}

		public function render_orders() {
			$orders = new WP_Query( [
				"posts_per_page" => 5,
				"post_type"      => "esoul_order"
			] );

			require_once( ESO_DIR . "/admin/views/dashboard/orders.php" );
		}

		public function render_products() {
			$products = new WP_Query( [
				"posts_per_page" => 5,
				"post_type"      => "esoul_product",
				"orderby"        => "meta_value_num",
				"order"          => "DESC",
				"meta_key"       => "turnover_CZK"
			] );

			require_once( ESO_DIR . "/admin/views/dashboard/products.php" );
		}

		public function render_customers() {
			$users = new WP_User_Query( array(
				'role'    => 'eso_customer',
				'number'  => 5,
				'orderby' => 'ID',
				'order'   => 'DESC'
			) );

			require_once( ESO_DIR . "/admin/views/dashboard/customers.php" );
		}


		public function render() {
			require_once( ESO_DIR . "/admin/views/dashboard/tables.php" );
		}

	}
}