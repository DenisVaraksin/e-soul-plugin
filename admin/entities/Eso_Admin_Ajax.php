<?php
if ( ! class_exists( 'Eso_Admin_Ajax', false ) ) {
	class Eso_Admin_Ajax {

		private $data;

		public function __construct( $data ) {
			$this->data = $data;
		}

		private function get_eso_action() {
			if ( isset( $this->data["do_success_callback"] ) ) {
				return $this->data["eso_success_callback"];
			}

			if ( isset( $this->data["eso_action"] ) ) {
				return $this->data["eso_action"];
			}

			write_log( "eso_action has to be defined and point to an existing function." );

			return null;
		}

		public function do_action() {
			try {
				$this->{$this->get_eso_action()}();
			} catch ( Exception $e ) {
				write_log( "Calling undefined function. " . $e->getMessage() );
			}

		}

		/**
		 * @since 0.0.22
		 */
		function shipping_zone_enable() {
			$shipping_zone = new Eso_Shipping_Zone( $this->data["shipping-zone"] );

			$shipping_zone->set_enabled();

			$shipping_zone->render_edit_item();

			return true;
		}

		/**
		 * @since 0.0.22
		 */
		function shipping_zone_remove() {
			$shipping_zone = new Eso_Shipping_Zone( $this->data["shipping-zone"] );

			$shipping_zone->set_enabled( false );

			return true;

		}

		/**
		 * Adds shipping method to selected Eso_Shipping_Zone
		 * @since 0.0.23
		 */
		function shipping_method_add() {
			$shipping_zone = new Eso_Shipping_Zone( $this->data["shipping_zone"] );

			$shipping_zone->add_shipping_method( $this->data["shipping_method"] );

			$shipping_method = new Eso_Shipping_Method( $this->data["shipping_method"] );

			$shipping_method->render_edit_row( $shipping_zone );
		}

		/**
		 * @since 0.0.30
		 */
		function shipping_method_remove() {
			$shipping_zone = new Eso_Shipping_Zone( $this->data["shipping_zone"] );

			$shipping_zone->remove_shipping_method( $this->data["shipping_method"] );
		}

		/**
		 * @since 0.0.30
		 */
		function shipping_zone_show_add_methods_options() {
			$shipping_zone = new Eso_Shipping_Zone( $this->data["shipping_zone"] );

			$shipping_zone->render_add_methods_options();
		}

		/**
		 * @since 0.0.31
		 */
		function shipping_method_update_prices() {
			$shipping_zone = new Eso_Shipping_Zone( $this->data["shipping_zone_code"] );

			$shipping_zone->update_shipping_method_prices( $this->data["shipping_method_code"], $this->data["prices"] );
		}

		/**
		 * @since 0.0.31
		 */
		function shipping_method_update_payment_methods() {
			$shipping_zone = new Eso_Shipping_Zone( $this->data["shipping_zone_code"] );

			$shipping_zone->update_payment_methods( $this->data["shipping_method_code"], $this->data["methods"] );
		}

		/**
		 * @since 0.0.18
		 */
		function order_billing_update() {
			$order = new Eso_Order( intval( $this->data["order_id"] ) );

			foreach ( $this->data["fields"] as $field_key => $field_value ) {
				$order->set_customer_field( $field_key, $field_value );
			}
		}

		/**
		 * @since 2019.7
		 */
		function get_order_status() {
			$order = new Eso_Order( $this->data["order_id"] );

			echo "<span class='order-status--" . $order->get_status_slug() . "'>" . $order->get_status_name() . "</span>";
		}

		/**
		 * @since 2019.4
		 *
		 * @since 2019.5 rewrite to use Eso_Order setters
		 */
		function edit_order_status() {
			$order_id = intval( $this->data["order_id"] );

			$order = new Eso_Order( $order_id );

			$order->set_status_by_id( $this->data["order_status"] );
		}

		/**
		 * @since 2019.5
		 *
		 * @since 2019.5 rewrite to use Eso_Order setters
		 */
		function edit_product_status() {
			$product_id = intval( $this->data["product_id"] );

			$product = new Eso_Product( $product_id );

			$product->set_stock_status_by_id( $this->data["stock_status"] );
		}

		/**
		 * @since 2019.6
		 */
		function save_product() {
			eso_save_product_data( $this->data );
		}

		/**
		 * @throws Exception
		 * @since 2019.7
		 */
		function get_product_sales_development() {
			$product = new Eso_Product( $this->data["product_id"] );

			echo json_encode( $product->get_overview_data( new DateTime( $this->data["from"] ), new DateTime( $this->data["till"] ) ) );
		}

		/**
		 * @since 2019.6
		 */
		function delete_product() {
			$product = new Eso_Product( $this->data["product_id"] );

			$product->delete();
		}

		/**
		 * @since 2019.6
		 */
		function the_product_view_url() {
			if ( $this->data["id"] == "new_post" ) {
				return;
			}

			$product = new Eso_Product( $this->data["id"] );

			echo "<a href='" . $product->get_view_url() . "' target='_blank'>" . __( "Zobrazit na webu" ) . "</a>";
		}

		/**
		 * @since 2019.7
		 */
		function render_available_related_products() {
			eso_render_available_related_products( $this->data["product_id"] );
		}

		/**
		 * @since 2019.7
		 */
		function add_related_product() {
			$product = new Eso_Product( $this->data["product_id"] );
			$product->add_related_product( $this->data["related_id"] );

			$related_product = new Eso_Product( $this->data["related_id"] );
			$related_product->admin_render_related_product_info( $this->data["product_id"] );
		}

		/**
		 * @since 2019.7
		 */
		function remove_related_product() {
			$product = new Eso_Product( $this->data["parent"] );
			$product->remove_related_product( $this->data["product_id"] );
		}

		/**
		 * @since 2019.7
		 */
		function add_product_variant() {
			$product         = new Eso_Product( $this->data["product_id"] );
			$product_variant = new Eso_Product_Variant( sanitize_title( $this->data["variant_name"] ), $product );
			$product_variant->add( $this->data["variant_name"], $this->data["variant_attributes"] );
			$product_variant->render_edit_form();
		}

		/**
		 * @since 2019.7
		 */
		function remove_product_variant() {
			$product         = new Eso_Product( $this->data["product_id"] );
			$product_variant = new Eso_Product_Variant( $this->data["variant_slug"], $product );
			$product_variant->remove();
		}

		/**
		 * @since 2019.7
		 */
		function add_product_variant_attribute() {
			$product         = new Eso_Product( $this->data["product_id"] );
			$product_variant = new Eso_Product_Variant( $this->data["variant_slug"], $product );
			$product_variant->add_attribute( $this->data["attribute_name"] );
			$product_variant->render_edit_form();
		}

		/**
		 * @since 2019.7
		 */
		function remove_product_variant_attribute() {
			$product         = new Eso_Product( $this->data["product_id"] );
			$product_variant = new Eso_Product_Variant( $this->data["variant_slug"], $product );
			$product_variant->remove_attribute( $this->data["attribute_slug"] );
		}

		/**
		 * @since 0.0.31
		 */
		function customer_fields_update() {

			$customer = new Eso_Customer( intval( $this->data["customer"] ) );

			foreach ( $this->data["fields"] as $field_key => $field_value ) {
				$customer->set_field( $field_key, $field_value );
			}
		}

		/**
		 * @since 2019.6
		 */
		function delete_customer() {
			$customer = new Eso_Customer( $this->data["customer_id"] );

			$customer->delete();
		}

		/**
		 * @since 2019.6
		 */
		function insert_customer_group() {
			eso_insert_customer_group( $this->data["group_name"] );
		}

		/**
		 * @since 2019.6
		 */
		function render_customer_groups_table() {
			$admin_tables = new Eso_Admin_Tables();
			$admin_tables->render_customer_groups_table();
		}

		/**
		 * @since 2019.6
		 */
		function rename_customer_group() {
			$group = new Eso_Customer_Group( $this->data["group_id"] );

			$group->set_name( $this->data["group_name"] );
			$group->set_slug( sanitize_title( $this->data["group_name"] ) );
		}

		/**
		 * @since 2019.6
		 */
		function update_customer_group() {
			$customer = new Eso_Customer( $this->data["customer_id"] );
			$customer->set_group( $this->data["customer_group"] );
		}

		/**
		 * @since 2019.6
		 */
		function delete_customer_group() {
			$group = new Eso_Customer_Group( $this->data["group_id"] );
			$group->delete();
		}

		/**
		 * @since 2019.7
		 */
		function insert_taxonomy_term() {
			wp_insert_term( $this->data["term_name"], $this->data["taxonomy"] );
		}

		/**
		 * @since 2019.7
		 */
		function delete_taxonomy_term() {
			wp_delete_term( $this->data["term_id"], $this->data["taxonomy"] );
		}

		/**
		 * @since 2019.7
		 */
		function render_product_category_table() {
			$admin_tables = new Eso_Admin_Tables();
			$admin_tables->render_product_category_table();
		}

		/**
		 * @since 2019.7
		 */
		function rename_product_category() {
			$category = new Eso_Product_Category( $this->data["category_id"] );
			$category->set_name( $this->data["category_name"] );
		}

		/**
		 * @since 2020.3.3
		 */
		function change_product_category_parent() {
			$category = new Eso_Product_Category($this->data["category_id"]);
			$parent_id = $this->data["category_parent"];

			$category->set_parent($parent_id);
		}

		/**
		 * @since 2020.3.3
		 */
		function change_taxonomy_priority() {
			update_term_meta($this->data["term_id"], 'priority', $this->data["priority"]);
		}

		/**
		 * @since 2019.7
		 */
		function render_stock_status_table() {
			$admin_tables = new Eso_Admin_Tables();
			$admin_tables->render_stock_status_table();
		}

		/**
		 * @since 2019.7
		 */
		function rename_stock_status() {
			$status = new Eso_Stock_Status( $this->data["term_id"] );
			$status->set_name( $this->data["term_name"] );
		}

		/**
		 * @since 2019.7
		 */
		function render_product_tag_table() {
			$admin_tables = new Eso_Admin_Tables();
			$admin_tables->render_product_tag_table();
		}

		/**
		 * @since 2019.7
		 */
		function rename_product_tag() {
			$status = new Eso_Product_Tag( $this->data["term_id"] );
			$status->set_name( $this->data["term_name"] );
		}

		/**
		 * @since 2019.8
		 */
		function rename_order_status() {
			$status = new Eso_Order_Status( $this->data["term_id"] );
			$status->set_name( $this->data["term_name"] );
		}

		/**
		 * @since 2019.7
		 */
		function render_order_status_table() {
			$admin_tables = new Eso_Admin_Tables();
			$admin_tables->render_order_status_table();
		}

		/**
		 * @see /admin/views/settings-general.php
		 * @since 0.0.23
		 * @since 2019.6 takes options as array and templating functions
		 * @see Eso_Admin_Fields
		 */
		function save_general_settings() {
			foreach ( $this->data["options"] as $option_name => $option_value ) {
				if ( $option_name == "eso_personal_collection_address" ) {
					update_option( $option_name, sanitize_textarea_field( $option_value ) );
				} else {
					update_option( $option_name, sanitize_text_field( $option_value ) );
				}

			}

			update_option( "eso_store_favicon", sanitize_text_field( $this->data["store-favicon"] ) );
			update_option( "eso_store_logo", sanitize_text_field( $this->data["store-logo"] ) );
		}

		/**
		 * @since 0.0.30
		 */
		function save_payment_transfer_settings() {
			update_option( "eso_store_account_number", sanitize_text_field( $this->data["store-account-number"] ) );
			update_option( "eso_store_iban", sanitize_text_field( $this->data["store-iban"] ) );
			update_option( "eso_store_bic", sanitize_text_field( $this->data["store-bic"] ) );
		}

		/**
		 * @since 2019.6
		 */
		function save_invoices_settings() {
			foreach ( $this->data["options"] as $option_name => $option_value ) {
				update_option( $option_name, sanitize_text_field( $option_value ) );
			}

			if ( isset( $this->data["eso_invoice_prefix_year"] ) ) {
				update_option( "eso_invoice_prefix_year", sanitize_text_field( $this->data["eso_invoice_prefix_year"] ) );
			} else {
				update_option( "eso_invoice_prefix_year", 0 );
			}

			if ( isset( $this->data["eso_billing_vat_payer"] ) ) {
				update_option( "eso_billing_vat_payer", sanitize_text_field( $this->data["eso_billing_vat_payer"] ) );
			} else {
				update_option( "eso_billing_vat_payer", 0 );
			}
		}

		/**
		 * @since 2019.6
		 */
		function send_invoice() {
			$order = new Eso_Order( $this->data["id"] );

			$email = new Eso_Email( $order->get_customer()->get_email(), "invoice", $order->get_id() );
			$send  = $email->send();

			if ( $send ) {
				$order->add_log( "Manuálně odeslána faktura na email zákazníka" );
			}
		}

		/**
		 * @since 2019.6
		 */
		function delete_order() {
			$order = new Eso_Order( $this->data["order_id"] );
			$order->delete();
		}

		/**
		 * @since 0.0.23
		 */
		function get_media_image() {
			if ( isset( $this->data['id'] ) ) {
				$image = wp_get_attachment_image( $this->data["id"], 'medium' );

				echo $image;
			} else {
				wp_send_json_error();
			}
		}

		/**
		 * @since 0.0.15
		 */
		function enabled_currencies_edit() {

			/* @var $currency Eso_Currency */
			foreach ( eso_get_all_currencies() as $currency ) {
				if ( in_array( $currency->get_code(), $this->data["eso_currency"] ) ) {
					$currency->set_enabled( true );
				} else {
					$currency->set_enabled( false );
				}
			}
		}

		/**
		 * @since 0.0.15
		 */
		function default_currency_edit() {
			$currency = new Eso_Currency( $this->data["eso_default_currency"] );
			$currency->set_as_default();
		}

		/**
		 * @since 0.0.17
		 */
		function frontend_debug_bar() {

			if ( current_user_can( 'edit_pages' ) ) {
				$debug_data = [
					"Session token" => eso_session_token()
				];
				print_r( $debug_data );
			}
		}

		/**
		 * @since 2019.4
		 */
		function show_frontend_dashboard() {
			$dashboard = new Eso_Dashboard();

			$dashboard->render();
		}

		/**
		 * @since 2019.5
		 */
		function module_update_settings() {
			$module = new Eso_Module( $this->data["code"] );

			if ( isset( $this->data["enabled"] ) ) {
				$module->set_enabled( true );
			} else {
				$module->set_enabled( false );
			}

			if ( ! isset( $this->data["fields"]["production"] ) ) {
				$module->set_data_field( "production", 0 );
			}

			if(isset($_FILES["files"])) {
				foreach ( $_FILES["files"]["tmp_name"] as $field_key => $field_value ) {
					$module->handle_file_upload( $field_key, $field_value );
				}
			}

			foreach ( $this->data["fields"] as $field_key => $field_value ) {
				$module->set_data_field( $field_key, $field_value );
			}
		}

		/**
		 * @since 2019.7
		 */
		function module_update_status() {

			$module = new Eso_Module( $this->data["code"] );

			if ( $module->is_enabled() ) {
				echo "<span class='text-success'>" . __( "Aktivní", "eso" ) . "</span>";
			} else {
				echo "<span class='text-danger'>" . __( "Neaktivní", "eso" ) . "</span>";
			}
		}

		/**
		 * @since 2019.5
		 */
		function reset_to_factory_defaults() {
			$options = new Eso_Options();

			$options->reset_to_factory_defaults();
		}

		/**
		 * @since 2019.5
		 */
		function render_comgate_delivery_info() {
			$order_id = intval( $this->data["order_id"] );

			$order = new Eso_Order( $order_id );

			$comgate_elogist = new Eso_Comgate_Delivery( $order );

			$order_status = $comgate_elogist->get_order_status();

			if ( empty( $order_status ) || ! isset( $order_status->status ) ) {
				return null;
			}

			if ( isset( $order_status->status ) ) {
				echo "Stav: " . $order_status->status . "<br>";
			}

			if ( isset( $order_status->carrierId ) ) {
				echo "Metoda dopravy: " . $order_status->carrierId . "<br>";
			}

			if ( isset( $order_status->branchId ) ) {
				$branchName = $order->get_zasilkovna_branch_name();

				if ( ! $branchName ) {
					$branchName = $order_status->branchId;
				}

				echo "Pobočka: " . $branchName . "<br>";
			}

			if ( isset( $order_status->service ) ) {
				echo "Služba: " . $order_status->service . "<br>";
			}

			if ( isset( $order_status->trackingNo ) ) {
				echo "Číslo zásilky: " . $order_status->trackingNo . "<br>";
			}

			if ( isset( $order_status->carrierWeb ) ) {
				echo "Web dopravce: " . $order_status->carrierWeb . "<br>";
			}

			if ( isset( $order_status->changed ) ) {
				$date = new Datetime( $order_status->changed );

				echo "Změněno: " . $date->format( "d. m. Y H:i:s" ) . "<br>";
			}

		}

		/**
		 * @since 2019.5
		 */
		function product_refresh_stock_amount() {
			$product = new Eso_Product( $this->data["product_id"] );

			echo $product->get_stock_amount();
		}

		/**
		 * @since 2019.5
		 */
		function edit_storage() {
			$postarr  = [
				"post_type"  => "esoul_storageorder",
				"post_title" => "StorageOrder"
			];
			$order_id = wp_insert_post( $postarr );

			$quantities = $this->data["quantity"];

			if ( isset( $this->data["storageorder-note"] ) ) {
				$note = $this->data["storageorder-note"];
			} else {
				$note = "";
			}

			update_post_meta( $order_id, "products", $quantities );

			$items = [];

			$counter = 0;
			foreach ( $quantities as $product_id => $quantity ) {
				$product = new Eso_Product( $product_id );
				if ( $quantity > 0 ) {
					$product->add_stock_amount( $quantity, true );

					$currency                       = new Eso_Currency( "CZK" );
					$items[ $counter ]['productId'] = $product_id;
					$items[ $counter ]['quantity']  = $quantity;
					$items[ $counter ]['unitValue'] = $product->get_price( $currency );
					$counter ++;
				} else if ( $quantity < 0 ) {
					$product->reduce_stock_amount( $quantity, true );
				}
			}

			if ( ! empty( $this->data["storageorder-supplier"] ) ) {
				update_post_meta( $order_id, "country", $this->data["storageorder-country"] );
				update_post_meta( $order_id, "supplier", $this->data["storageorder-supplier"] );
				update_post_meta( $order_id, "note", $note );

				$comgate_elogist = new Eso_Comgate_Elogist();

				$comgate_elogist->storage_order(
					$this->data["storageorder-country"],
					$order_id,
					$this->data["storageorder-supplier"],
					$items,
					$note );
			}
		}

		/**
		 * @since 2019.7
		 */
		function render_storage_log() {
			$product = new Eso_Product( $this->data["product"] );
			$product->admin_render_storage_log();
		}

		/**
		 * @since 2019.5
		 */
		function get_customer_turnover() {
			$currency = new Eso_Currency( $this->data["currency_code"] );
			$customer = new Eso_Customer( $this->data["customer_id"] );

			echo $customer->set_turnover( $currency, true );
		}

		/**
		 * @since 2019.5
		 */
		function get_product_turnover() {
			$currency = new Eso_Currency( $this->data["currency_code"] );
			$product  = new Eso_Product( $this->data["product_id"] );

			echo $product->set_turnover( $currency, true );
		}

		/**
		 * @since 2019.7
		 */
		function preview_product_prices() {
			$tax_rate                              = new Eso_Tax_Rate( $this->data["tax_rate"] );
			$prices                                = [];
			$price_without_tax                     = eso_get_price_without_tax( $this->data["price"], $tax_rate, true );
			$prices["before_discount_without_tax"] = $price_without_tax;
			$prices["after_discount_without_tax"]  = eso_get_price_after_discount( $price_without_tax, $this->data["discount_type"], $this->data["discount"], true );
			$prices["after_discount_with_tax"]     = eso_get_price_after_discount( $this->data["price"], $this->data["discount_type"], $this->data["discount"], true );

			echo json_encode( $prices );
		}

		/**
		 * @since 2019.6
		 */
		function the_quick_look() {
			switch ( $this->data["type"] ) {
				case "customer":
					$customer = new Eso_Customer( $this->data["id"] );
					$customer->render_quick_look();
					break;
				case "order":
					$order = new Eso_Order( $this->data["id"] );
					$order->render_quick_look();
					break;
			}
		}

		/**
		 * @since 2019.6
		 */
		function update_store_publicity() {
			if ( isset( $this->data["blog_public"] ) ) {
				update_option( "blog_public", 1 );
			} else {
				update_option( "blog_public", 0 );
			}
		}

		/**
		 * @since 2019.6
		 */
		function render_dashboard_summary() {
			$dashboard = new Eso_Dashboard();

			$dashboard->render_summary( $this->data["from"], $this->data["till"] );
		}

		/**
		 * @since 2019.6
		 */
		function reset_customer_password() {
			$login = new Eso_Login();
			$login->send_reset_password_notification( $this->data["customer_id"] );
		}

		/**
		 * @since 2019.7
		 */
		function insert_coupon() {
			$data = $this->data["coupon"];

			eso_insert_coupon( $data );
		}

		/**
		 * @since 2019.7
		 */
		function render_insert_coupon_form() {
			$admin_tables = new Eso_Admin_Tables();
			$admin_tables->render_insert_coupon_form();
		}

		/**
		 * @since 2019.7
		 */
		function edit_coupon() {
			$data = $this->data["coupon"];

			$coupon = new Eso_Coupon( $this->data["coupon_id"] );
			$coupon->update( $data );
		}

		/**
		 * @since 2019.7
		 */
		function delete_coupon() {
			$coupon = new Eso_Coupon( $this->data["coupon_id"] );
			$coupon->delete();
		}

		/**
		 * @since 2019.7
		 */
		function index_bulk_actions() {
			$admin_query = new Eso_Admin_Query();
			if ( empty( $this->data["items"] ) ) {
				return;
			}

			$admin_query->do_bulk_actions( $this->data["inputs"], $this->data["items"] );
		}

		/**
		 * @since 2019.8
		 */
		function board_check() {
			eso_board_check();
		}

		/**
		 * @since 2020.2.5
		 */
		function update_branch() {
			$allowed_branches = ["master", "beta"];

			if (in_array($this->data["branch"], $allowed_branches)) {
				update_option("eso_branch", $this->data["branch"]);
			}
		}

        /**
         * @since 2020.4
         */
        function create_delivery_method() {

            $name = sanitize_text_field($this->data["store-shipping-name"] );
            $code = (string) $this->data["store-shipping-code"] ;
//          $code = (string) $this->data["store-shipping-code"] ;
            $code = (str_replace('_', '', strtolower($code)));

            $default_prices_czk = (float) $this->data["store-shipping-default-prices-czk"] ;
            $default_prices_eur = (float) $this->data["store-shipping-default-prices-eur"] ;
            $default_prices_usd = (float) $this->data["store-shipping-default-prices-usd"] ;
            $default_prices_gbr = (float) $this->data["store-shipping-default-prices-gbr"] ;

            $custom_field = [
                "custom_field" => [
                    "name"            => $name,
                    "prices"          => [
                        "CZK" => $default_prices_czk,
                        "EUR" => $default_prices_eur,
                        "GBP" => $default_prices_usd,
                        "USD" => $default_prices_gbr
                    ],
                    "payment_methods" => [
                        "bank_transfer" => [
                            "prices" => []
                        ]
                    ]
                ]
            ];


            global $wpdb;

                if ( empty( $wpdb->get_col( "SELECT code FROM " . ESO_SHIPPING_METHOD_TABLE . " WHERE code = '" . $code . "'" ) ) ) {
                    $data = [
                        "code"           => $code,
                        "name"           => $name,
                        "message"        => "custom",
                        "default_prices" => serialize( $custom_field["custom_field"]["prices"] ),
                        "default_payment_methods" => serialize($custom_field["custom_field"]["payment_methods"])
                    ];

                    $wpdb->insert( ESO_SHIPPING_METHOD_TABLE, $data );
                }
        }

        /**
         * @since 2020.4
         */
        function load_xml() {
            if ( isset($_FILES['file']) ) {


                $uploads = wp_upload_dir();
                $target = $uploads['baseurl'] . '/' . basename($_FILES['file']['name']);

                // moving temporary file to WP uploads directory
                $myfile = & $_FILES['file'];
                $overrides = [ 'test_form' => false ];
                $movefile = wp_handle_upload( $myfile, $overrides );

                $filepath =  $movefile['url'];
                return $filepath;

            }
            else {
                return false;
            }
        }


        /**
         * @since 2020.4
         */
// handle the ajax request
        function read_xml(  ) {

            $uploads = wp_upload_dir();
            $url = $uploads['baseurl'] . '/dataeshop.xml';

            $xml = simplexml_load_file($url) or die("feed not loading");

            $i = 0;
            foreach($xml->Worksheet->Table->Row as $item) {
                if ($i != 0) {
                    if ($i == 3) {
                        break;
                    }
                //gathering data from XML
                $product_id = (int)strval($item->Cell[0]->Data);
                $product_category = strval($item->Cell[1]->Data);
                $product_tax = (int) strval($item->Cell[3]->Data);
                $product_name = strval($item->Cell[2]->Data);
                $product_price = (float)strval($item->Cell[5]->Data);
                $product_picture = strval($item->Cell[6]->Data);

                //posting new POST into database
                $post_author = get_current_user_id();
                $post_data = array(
                    'post_title' => sanitize_text_field($product_name),
                    'post_content' => '',
                    'post_status' => 'publish',
                    'post_author' => $post_author,
                    'post_category' => '',
                    'post_type' => 'esoul_product'
                );
                $eso_id = wp_insert_post($post_data);

                //instantiating new Eso Product
                $product = new Eso_Product($eso_id);

                update_post_meta($eso_id, "price_CZK", $product_price);
                update_post_meta($eso_id, 'stock_amount', 100);


                update_post_meta($eso_id, 'tax', ["CZK" => 2, "USD" => 2, "EUR" => 2, "GBP" => 2]);

                //setting TAG as an XML ids
                // creating new
                    $insert_data = wp_insert_term(
                        $product_id,  // новый термин
                        'product_tag', // таксономия
                        array(
                            'description' => '',
                            'slug' => '',
                            'parent' => 0
                        )
                    );
                    $tags = get_terms([
                        'taxonomy' => 'product_tag',
                        'hide_empty' => false,
                    ]);
                    //looping through existing tags and if values matches with XML entety - assigning a tag to the product
                    foreach ($tags as $tag) {
                        $xml_tag = (int) strval($tag->name);
                        if ($xml_tag == $product_id) {
                            wp_set_post_terms($eso_id, array($tag->term_id), 'product_tag', false);
                            break;
                        }
                    }

                //setting category with checking all other products if they have that category already
                $categories = get_terms([
                    'taxonomy' => 'product_category',
                    'hide_empty' => false,
                ]);
                foreach ($categories as $category) {
                    $xml_category = strval($category->name);
                    if ($xml_category == $product_category) {
                        wp_set_post_terms($eso_id, array($category->term_id), 'product_category', false);
                        break;
                    }
                }

                //setting pictures
                $pictures_url = $uploads['baseurl'] . '/image/' . $product_picture;
                $image_id = media_sideload_image($pictures_url, 0, null, "id");
                update_post_meta($eso_id, 'images', [$image_id]);
            }
            $i++;
            }
        }

	}
}
