<?php

if ( ! class_exists( 'Eso_Admin_Query', false ) ) {
	/**
	 * @since 2019.6
	 *
	 * Class Eso_Admin_Query
	 */
	class Eso_Admin_Query {

		public function __construct() {

		}

		/**
		 * @param $current_args
		 *
		 * @return array
		 */
		function default_args( $current_args ) {
			$args = [];

			if ( isset( $_GET["orderby"] ) ) {
				$orderby         = $_GET["orderby"];
				$args["orderby"] = $orderby;

				if ( isset( $_GET["meta_key"] ) ) {
					$args["meta_key"] = $_GET["meta_key"];
				}
			}

			if ( isset( $_GET["order"] ) ) {
				$order         = $_GET["order"];
				$args["order"] = $order;
			}

			( ! empty( $_GET["date_from"] ) ) ? $from = $_GET["date_from"] : $from = false;

			( ! empty( $_GET["date_to"] ) ) ? $to = $_GET["date_to"] : $to = false;

			if ( $from && $to ) {
				$args["date_query"] = [
					[
						"after"  => $from,
						"before" => $to
					]
				];
			} else if ( $from ) {
				$args["date_query"] = [
					[
						"after" => $from
					]
				];
			} else if ( $to ) {
				$args["date_query"] = [
					[
						"to" => $to
					]
				];
			}

			( ! empty( $_GET["turnover_from"] ) ) ? $turnover_from = $_GET["turnover_from"] : $turnover_from = false;


			if ( $turnover_from ) {
				$args["meta_query"][] = [
					"key"     => "turnover_CZK",
					"value"   => $turnover_from,
					"type"    => "NUMERIC",
					"compare" => ">="
				];
			}

			if ( empty( get_query_var( "paged" ) ) && isset( $_GET["paged"] ) ) {
				set_query_var( "paged", (int) $_GET["paged"] );
			} else if ( ! isset( $_GET["paged"] ) ) {
				set_query_var( "paged", 1 );
			}

			$paged         = get_query_var( 'paged' );
			$args["paged"] = $paged;

			return array_merge_recursive( $args, $current_args );
		}

		/**
		 * @return WP_User_Query
		 */
		function customer_index($number = 20) {
			( ! empty( $_GET["search_id"] ) ) ? $id = (int) $_GET["search_id"] : $id = false;
			( ! empty( $_GET["search"] ) ) ? $string = $_GET["search"] : $string = false;

			if ( $id && $string ) {
				$args["search"]         = (int) $id . " " . $string;
				$args["search_columns"] = [ "ID", "display_name", "user_email" ];
			} else if ( $id ) {
				$args["search"]           = (int) $id;
				$args["search_columns"][] = "ID";
			} else if ( $string ) {
				$args["search"]         = "*" . $string . "*";
				$args["search_columns"] = [ "display_name", "user_email" ];
			}

			$args["role"]   = "eso_customer";
			$args["number"] = $number;

			if ( ! empty( $_GET["customer_group"] ) ) {
				$args["meta_query"][] = [
					"key"   => "group",
					"value" => (int) $_GET["customer_group"]
				];
			}

			$args = $this->default_args( $args );

			return new WP_User_Query( $args );
		}

		/**
		 * @param $post_type
		 *
		 * @return WP_Query
		 */
		function posts_index( $post_type, $posts_per_page = 20 ) {
			$args = [];

			if ( ! empty( $_GET["search_id"] ) ) {
				$args["post__in"] = [ (int) $_GET["search_id"] ];
			}

			if ( ! empty( $_GET["order_status"] ) ) {
				$args["tax_query"] = [
					[
						"taxonomy" => "order_status",
						"terms"    => [ (int) $_GET["order_status"] ]
					]
				];
			}

			if ( ! empty( $_GET["title"] ) ) {
				$args["s"] = sanitize_text_field( $_GET["title"] );
			}

			( ! empty( $_GET["stock_status"] ) ) ? $stock_status = $_GET["stock_status"] : $stock_status = null;
			( ! empty( $_GET["product_category"] ) ) ? $product_category = $_GET["product_category"] : $product_category = null;
			( ! empty( $_GET["product_tag"] ) ) ? $product_tag = $_GET["product_tag"] : $product_tag = null;
			( ! empty( $_GET["total_from"] ) ) ? $total_from = $_GET["total_from"] : $total_from = null;
			( ! empty( $_GET["coupon_code"] ) ) ? $coupon_code = $_GET["coupon_code"] : $coupon_code = null;

			$args["tax_query"]["relation"] = "AND";

			if ( $stock_status ) {
				$args["tax_query"][] = [
					[
						"taxonomy" => "stock_status",
						"terms"    => [ (int) $stock_status ]
					]
				];
			}

			if ( $product_category ) {
				$args["tax_query"][] = [
					[
						"taxonomy" => "product_category",
						"terms"    => [ (int) $product_category ]
					]
				];
			}

			if ( $product_tag ) {
				$args["tax_query"][] = [
					[
						"taxonomy" => "product_tag",
						"terms"    => $product_tag,
						"field"    => "slug"
					]
				];
			}

			$args["meta_query"]["relation"] = "AND";

			if ( $total_from ) {
				$args["meta_query"][] = [
					"key"     => "total_CZK",
					"value"   => $total_from,
					"compare" => ">=",
					"type"    => "NUMERIC"
				];
			}

			if ( $coupon_code ) {
				$args["meta_query"][] = [
					"key"     => "options",
					"value"   => $coupon_code,
					"compare" => "LIKE"
				];
			}

			$args["post_type"]      = $post_type;
			$args["post_status"]    = "publish";
			$args["posts_per_page"] = $posts_per_page;

			$args = $this->default_args( $args );

			return new WP_Query( $args );
		}

		/**
		 * @return string
		 */
		function switch_ordering() {
			if ( isset( $_GET["order"] ) ) {
				if ( $_GET["order"] == "DESC" ) {
					return "ASC";
				}

				return "DESC";

			}

			return "ASC";
		}

		/**
		 * Used by custom index views
		 *
		 * @param $total
		 */
		public function pagination( $total ) {
			echo paginate_links( array(
				'base'         => str_replace( 999999999, '%#%', get_pagenum_link( 999999999, false ) ),
				'total'        => $total,
				'current'      => max( 1, get_query_var( 'paged' ) ),
				'format'       => 'paged=%#%',
				'show_all'     => false,
				'type'         => 'list',
				'prev_next'    => true,
				'prev_text'    => sprintf( '<i></i> %1$s', __( 'Novější', 'eso' ) ),
				'next_text'    => sprintf( '%1$s <i></i>', __( 'Starší', 'eso' ) ),
				'add_fragment' => '',
				'add_args'     => [],
			) );

			?>
		<?php }

		/**
		 * @since 2019.7
		 */
		public function do_bulk_actions($inputs, $items) {
			if(empty($items)) {
				return;
			}
			foreach($items as $item) {
				if(isset($inputs["customer-group"])) {
					$customer = new Eso_Customer($item);
					$customer->set_group($inputs["customer-group"]);
				}
			}
		}
	}
}
