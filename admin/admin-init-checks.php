<?php

function eso_check_if_theme_is_active() {
    $theme = wp_get_theme();

    //We need our custom theme to build on
    if( 'E-soul Theme' != $theme->name && 'E-soul Theme' != $theme->parent_theme && 'Frusack Theme' != $theme->name && 'E-soul Hello' != $theme->name  ) {
        eso_admin_notice(sprintf(__("Stáhněte a aktivujte kompatibilní šablonu, aby mohl plugin správně fungovat. %s %s"), "<br><br><a href='https://esoul-docs.marketsoul.cz/ke-stazeni/' class='button button-primary'>Kompatibilní šablony</a>", "&nbsp;&nbsp;<a href='" . admin_url('themes.php') . "' class=''>" . __('Spravovat šablony', 'eso') . "</a>" ), "error");
    }
}

add_action( 'admin_notices', 'eso_check_if_theme_is_active' );

/**
 * @since 2019.6
 */
function eso_change_post_objects() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->all_items = __("Seznam příspěvků", "eso");

	$labels = &$wp_post_types['page']->labels;
	$labels->all_items = __("Seznam stránek", "eso");
}

add_action( 'init', 'eso_change_post_objects' );

/**
 * Redirect from default dashboard.
 *
 * @since 2019.7
 *
 * @return void
 */
function eso_dashboard_redirect() {
	global $pagenow;

	if( $pagenow == 'index.php' ){
		wp_redirect( admin_url( 'admin.php?page=eso-dashboard' ), 301 );
		exit;
	}

}

add_action( 'admin_init', 'eso_dashboard_redirect' );

/**
 * We are disabling theme and plugin editor since we use git for updates and changes would be overridden
 *
 * @since 2018.1.9
 */
if( !defined('DISALLOW_FILE_EDIT') ){
	define( 'DISALLOW_FILE_EDIT', true );
}