(function ($) {

    $(function () {
        setTimeout(function () {
            const ctx = document.getElementById('overview-graph').getContext("2d");

            const gradientStroke = ctx.createLinearGradient(100, 0, 500, 0);
            gradientStroke.addColorStop(0, "#cce4f5");
            gradientStroke.addColorStop(1, "#6cc3ff");

            const chart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: "",
                    datasets: [{
                        label: "",
                        data: [],
                        backgroundColor: gradientStroke,
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }]
                    }
                }
            });

            let this_week = $("[data-target='this-week']");

            fetchData(this_week.data("from"), this_week.data("till"));
        }, 300);


        function fetchData(from, till) {
            console.log(from);
            console.log(till);

            $.ajax({
                url: php_vars["ajaxurl"],
                method: "POST",
                data: {
                    "action": "eso_ajax",
                    "eso_action": "get_overview_data",
                    "from": from,
                    "till": till,
                },
                success: function (result) {
                    const data = JSON.parse(result);

                    let overview_graph = $("#overview-graph");

                    overview_graph.prev().remove();
                    overview_graph.remove();
                    $('#overview').append('<canvas id="overview-graph"><canvas>');

                    const ctx = document.getElementById('overview-graph').getContext("2d");

                    const gradientStroke = ctx.createLinearGradient(100, 0, 500, 0);
                    gradientStroke.addColorStop(0, "#cce4f5");
                    gradientStroke.addColorStop(1, "#6cc3ff");

                    const chart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: data.labels,
                            datasets: [{
                                label: data.datasets.label,
                                data: data.datasets.data,
                                backgroundColor: gradientStroke,
                            }]
                        },
                        options: {
                            maintainAspectRatio: false,
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItems, data) {
                                        return tooltipItems.yLabel + ' Kč';
                                    }
                                }
                            },
                            legend: {
                                display: false
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        maxTicksLimit: 6,
                                        callback: function (label, index, labels) {
                                            return label + ' Kč';
                                        }
                                    },
                                    gridLines: {
                                        display: false
                                    }
                                }],
                                xAxes: [{
                                    gridLines: {
                                        display: false
                                    }
                                }]
                            }
                        }
                    });

                    $("#overview-graph").css("height", "240px");
                }
            });
        }

        $(".overview-trigger").click(function () {
            fetchData($(this).data("from"), $(this).data("till"));

            const summary = $("#dashboard-summary");
            const thisTrigger = $(this);
            const loadingsummary =$("#summary-range-header");

            if (summary.length) {
                $.ajax({
                    url: php_vars["ajaxurl"],
                    method: "POST",
                    data: {
                        "action": "eso_admin_ajax",
                        "eso_action": "render_dashboard_summary",
                        "from": $(this).data("from"),
                        "till": $(this).data("till")
                    },
                    beforeSend: function () {
                        makeLoading(loadingsummary)
                    },
                    error: function (result) {
                        console.log(result);
                    },
                    success: function (result) {
                        removeLoading(loadingsummary);
                        console.log(thisTrigger.text());

                        $("#summary-range").html(thisTrigger.text());
                        summary.html(result);
                    }
                })
            }
        });

        function makeLoading(element) {
            element.prop("disabled", true);
            element.append('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        }

        function removeLoading(element) {
            element.prop("disabled", false);
            element.find(".spinner-border").remove();
        }

    });
})(jQuery);