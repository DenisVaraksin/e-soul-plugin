(function ($) {

    $(function () {
        const product_id = $("#p-ID").val();

        $('#post-body-content input').attr('readonly', 'readonly').addClass("form-control-plaintext");

        $('#eso-product-gallery-add').click(function (e) {
            e.preventDefault();
            var image_frame;
            if (image_frame) {
                image_frame.open();
            }
            image_frame = wp.media({
                title: 'Vybrat fotografie',
                multiple: false,
                library: {
                    type: 'image'
                }
            });

            image_frame.on('close', function () {
                const selection = image_frame.state().get('selection');
                let gallery_ids = [];
                let my_index = 0;
                selection.each(function (attachment) {
                    gallery_ids[my_index] = attachment['id'];
                    my_index++;
                });

                refreshImage(gallery_ids.join(","));
            });

            image_frame.open();
        });

        function refreshImage(the_id) {
            const data = {
                "action": 'eso_admin_ajax',
                "eso_action": 'get_media_image',
                "id": the_id
            };

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: data,
                success: function (response) {
                    const product_gallery = $("#eso-product-gallery");
                    const gallery_item = "<div id='eso-product-image-" + the_id + "' class='eso-product-image'>" + response + "<div class='eso-remove'></div><input type='hidden' name='images[]' value='" + the_id + "'>" + "</div>";

                    product_gallery.append(gallery_item);

                    imageReorder();
                }
            });
        }

        $(document).on('click', '.eso-product-image .eso-remove', function () {
            $(this).parent().remove();
            imageReorder();
        });

        function imageReorder() {
            let image_item = $(".eso-product-image");

            image_item.removeClass("eso-product-image--main");
            image_item.first().addClass("eso-product-image--main");
        }

        $('#eso-product-symbol-add').click(function () {
            var image_frame;
            if (image_frame) {
                image_frame.open();
            }
            image_frame = wp.media({
                title: 'Vybrat/nahrát symbol',
                multiple: false,
                library: {
                    type: 'image'
                }
            });

            image_frame.on('close', function () {
                const selection = image_frame.state().get('selection');
                selection.each(function (attachment) {
                    refreshSymbol(attachment['id']);
                });
            });

            image_frame.open();
        });

        function refreshSymbol(attachment_id) {
            $("input[name='symbol_image_id']").val(attachment_id);

            $.getJSON(php_vars["media_url"] + attachment_id, function (data) {
                const productSymbolImage = $("#product-symbol-image")
                if (!$("#product-symbol-image img").length) {
                    productSymbolImage.append("<img />");
                }

                productSymbolImage.find("img").attr("src", data.media_details.sizes.medium.source_url);
            });
        }


        $(".product-price-trigger").change(function (e) {
            const pane = $(this).closest(".tab-pane");
            const price = pane.find(".product-price").val();
            const tax_rate = pane.find(".product-tax-rate").val();
            const discount_type = pane.find(".product-discount-type").val();
            const discount = pane.find(".product-discount").val();

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "preview_product_prices",
                    "price": price,
                    "tax_rate": tax_rate,
                    "discount_type": discount_type,
                    "discount": discount
                },
                beforeSend: function () {
                    makeLoading(pane.find(".product-price-preview"));
                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    removeLoading(pane.find(".product-price-preview"));

                    result = JSON.parse(result);

                    pane.find(".before-discount-without-tax").text(result.before_discount_without_tax);
                    pane.find(".after-discount-without-tax").text(result.after_discount_without_tax);
                    pane.find(".after-discount").text(result.after_discount_with_tax);
                }
            })
        });

        /**
         * @since 2019.7
         */
        $("#add-related-product").click(function () {
            const button = $(this);
            const box = $(this).closest("div").find(".whitebox");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "render_available_related_products",
                    "product_id": product_id
                },
                beforeSend: function () {
                    makeLoading(button);
                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    // removeLoading(button);
                    box.removeClass("d-none");
                    button.remove();
                    box.html(result);
                }
            })
        });

        /**
         * @since 2019.7
         */
        $(document).on("click", '[data-toggle="add-related-product"]', function () {
            const wrapper = $("#related-products-wrapper");
            const thisItem = $(this);
            const related_id = thisItem.data("id");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "add_related_product",
                    "product_id": product_id,
                    "related_id": related_id
                },
                beforeSend: function () {
                    makeLoading(wrapper);
                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    thisItem.remove();
                    removeLoading(wrapper);

                    wrapper.prepend(result);
                }
            });
        });

        /**
         * @since 2019.7
         */
        $(document).on("click", ".related-product-info .eso-remove", function () {
            const box = $(this).closest(".related-product-info");
            const related_product_id = box.data("id");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "remove_related_product",
                    "parent": product_id,
                    "product_id": related_product_id,
                },
                beforeSend: function () {
                    makeLoading(box);
                },
                success: function () {
                    box.remove();
                }
            });
        });

        /**
         * @since 2019.7
         */
        $("#add-variant-form button").click(function (e) {
            e.preventDefault();

            const thisForm = $(this).closest("#add-variant-form");
            const wrapper = thisForm.closest(".variants-wrapper");
            const variant_name = thisForm.find("input[name='variant_name']").val();
            const variant_attributes = thisForm.find("input[name='variant_attributes']").val();

            if (variant_name.length && variant_attributes.length) {
                $.ajax({
                    url: ajaxurl,
                    method: "POST",
                    data: {
                        "action": "eso_admin_ajax",
                        "eso_action": "add_product_variant",
                        "product_id": product_id,
                        "variant_name": variant_name,
                        "variant_attributes": variant_attributes
                    },
                    beforeSend: function () {
                        makeLoading(wrapper);
                    },
                    error: function (result) {
                        console.log(result);
                    },
                    success: function (result) {
                        removeLoading(wrapper);
                        wrapper.prepend(result);
                        thisForm.find("input[type='text']").val("");
                    }
                });
            } else {
                alert("Vyplňte prosím obě hodnoty");
            }
        });

        /**
         * @since 2019.7
         */
        $(document).on("click", ".product-variant .eso-remove", function () {
            const product_variant = $(this).closest(".product-variant");
            const variant_slug = product_variant.data("slug");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "remove_product_variant",
                    "variant_slug": variant_slug,
                    "product_id": product_id,
                },
                beforeSend: function () {
                    makeLoading(product_variant);
                },
                success: function () {
                    product_variant.remove();
                }
            });
        });

        $(document).on("click", ".product-variant .remove-tag", function () {
            const product_variant = $(this).closest(".product-variant");
            const thisTag = $(this).closest(".editable-tag");

            const variant_slug = product_variant.data("slug");
            const attribute_slug = thisTag.data("slug");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "remove_product_variant_attribute",
                    "variant_slug": variant_slug,
                    "product_id": product_id,
                    "attribute_slug": attribute_slug
                },
                beforeSend: function () {
                    makeLoading(thisTag);
                },
                error: function () {

                },
                success: function () {
                    removeLoading(thisTag);
                    thisTag.remove();
                }
            })
        });

        $(document).on("click", "#add-variant-attribute button", function () {
            const product_variant = $(this).closest(".product-variant");
            const attribute_name = $(this).closest("#add-variant-attribute").find("input[type='text']").val();
            const variant_slug = product_variant.data("slug");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "add_product_variant_attribute",
                    "product_id": product_id,
                    "variant_slug": variant_slug,
                    "attribute_name": attribute_name
                },
                beforeSend: function () {
                    makeLoading(product_variant);

                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    removeLoading(product_variant);

                    product_variant.replaceWith(result);
                }
            });
        });

        const ctx = document.getElementById('overview-graph').getContext("2d");

        const gradientStroke = ctx.createLinearGradient(100, 0, 500, 0);
        gradientStroke.addColorStop(0, "#cce4f5");
        gradientStroke.addColorStop(1, "#6cc3ff");

        const chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: "",
                datasets: [{
                    label: "",
                    data: [],
                    backgroundColor: gradientStroke,
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });

        let this_week = $("[data-target='this-week']");

        fetchData(this_week.data("from"), this_week.data("till"));

        function fetchData(from, till) {
            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "get_product_sales_development",
                    "from": from,
                    "till": till,
                    "product_id": product_id
                },
                success: function (result) {
                    const data = JSON.parse(result);

                    $('#overview-graph').remove();
                    $('#overview').append('<canvas id="overview-graph"><canvas>');

                    const ctx = document.getElementById('overview-graph').getContext("2d");

                    const gradientStroke = ctx.createLinearGradient(100, 0, 500, 0);
                    gradientStroke.addColorStop(0, "#cce4f5");
                    gradientStroke.addColorStop(1, "#6cc3ff");

                    const chart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: data.labels,
                            datasets: [{
                                label: data.datasets.label,
                                data: data.datasets.data,
                                backgroundColor: gradientStroke,
                            }]
                        },
                        options: {
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItems, data) {
                                        return tooltipItems.yLabel + ' Kč';
                                    }
                                }
                            },
                            legend: {
                                display: false
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        maxTicksLimit: 9,
                                        callback: function (label, index, labels) {
                                            return label + ' Kč';
                                        }
                                    },
                                    gridLines: {
                                        display: false
                                    }
                                }],
                                xAxes: [{
                                    gridLines: {
                                        display: false
                                    }
                                }]
                            }
                        }
                    });


                }
            });
        }

        $(".overview-trigger").click(function () {
            fetchData($(this).data("from"), $(this).data("till"));

            const summary = $("#dashboard-summary");
            const thisTrigger = $(this);

            if (summary.length) {
                $.ajax({
                    url: php_vars["ajaxurl"],
                    method: "POST",
                    data: {
                        "action": "eso_admin_ajax",
                        "eso_action": "render_dashboard_summary",
                        "from": $(this).data("from"),
                        "till": $(this).data("till")
                    },
                    beforeSend: function () {
                        makeLoading(summary)
                    },
                    error: function (result) {
                        console.log(result);
                    },
                    success: function (result) {
                        removeLoading(summary);

                        $("#summary-range").html(thisTrigger.text());
                        summary.html(result);
                    }
                })
            }
        });

        /**
         * @since 2020.3.2
         */
        let all_product_tags_string = $("#all_product_tags").val();
        let all_product_tags = all_product_tags_string.split(',');

        $("#product_tag").tagator({
            autocomplete: all_product_tags,
            showAllOptionsOnFocus: true
        });

        /**
         * @since 2020.3.5
         */
        let all_product_categories_string = $("#all_product_categories").val();
        let all_product_categories = all_product_categories_string.split(',');

        $("#product_category").tagator({
            autocomplete: all_product_categories,
            showAllOptionsOnFocus: true,
            allowAutocompleteOnly: true
        });

        function makeLoading(element) {
            element.prop("disabled", true);
            element.prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        }

        function removeLoading(element) {
            element.prop("disabled", false);
            element.find(".spinner-border").remove();
        }

    });

})(jQuery);