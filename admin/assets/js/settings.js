(function($){

    $(function() {

        const status = $("#wp-admin-bar-status");

        $('.btn-gallery-trigger').click(function (e) {
            e.preventDefault();
            const galleryTrigger = $(this);

            let accept = galleryTrigger.data("accept");

            if(accept === undefined) {
                accept = "image";
            }

            var image_frame;
            if (image_frame) {
                image_frame.open();
            }
            image_frame = wp.media({
                title: 'Vybrat fotografie',
                multiple: false,
                library: {
                    type: accept
                }
            });

            image_frame.on('close', function () {
                const selection = image_frame.state().get('selection');
                let gallery_ids = [];
                let my_index = 0;
                selection.each(function (attachment) {
                    gallery_ids[my_index] = attachment['id'];
                    my_index++;
                });

                refreshImage(gallery_ids.join(","), galleryTrigger.attr("id"));
            });

            image_frame.open();
        });

        function refreshImage(the_id, wrapper_id) {
            const btnTrigger = $("#" + wrapper_id);
            const input = $("#" + wrapper_id + "-value");

            const data = {
                "action": 'eso_admin_ajax',
                "eso_action": 'get_media_image',
                "id": the_id
            };

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: data,
                success: function(response) {
                    btnTrigger.append(response);
                    btnTrigger.find(".eso-icon").hide();
                    btnTrigger.find(".eso-add").hide();
                    btnTrigger.next().show();

                    input.attr("value", the_id);
                }
            });
        }

        $(document).on('click', '.eso-remove', function () {
            const target_id = $(this).data("target");
            const target = $(target_id);

            $(this).hide();

            $(target_id + "-value").attr("value", "");

            target.find(".eso-add").show();
            target.find(".eso-icon").show();
            target.find(".attachment-medium").hide();
        });

        $("#shipping-zone-enable-form").submit(function (e) {
            e.preventDefault();

            const dynamicForm = $(this);
            const dynamicFormSubmit = $(this).find(".dynamic-form-submit");
            const selectInput = $("#shipping-zone");
            const shippingZone = selectInput.val();

            $.ajax({
                "url": ajaxurl,
                "method": "POST",
                "data": dynamicForm.serialize(),
                beforeSend: function () {
                    makeLoading(dynamicFormSubmit);
                },
                error: function () {},
                success: function (result) {

                    location.reload();
                }
            });
        });

        $(".shipping-zone-remove").click(function (e) {
            e.preventDefault();

            const col = $(this).closest(".shipping-zone-edit-col");
            const dynamicFormSubmit = $(this);

            $.ajax({
                "url": ajaxurl,
                "method": "POST",
                "data": {
                    "action": "eso_admin_ajax",
                    "eso_action": "shipping_zone_remove",
                    "shipping-zone": dynamicFormSubmit.data("shipping-zone")
                },
                beforeSend: function () {
                    makeLoading(dynamicFormSubmit);
                },
                error: function () {},
                success: function (result) {
                    console.log(result);
                    removeLoading(dynamicFormSubmit);
                    col.remove();
                }
            });
        });

        $(".shipping-method-add").click(function () {
            const dynamicButton = $(this);
            const shippingMethod = dynamicButton.data("shipping-method");
            const shippingZone = dynamicButton.data("shipping-zone");
            const rows = dynamicButton.closest(".eso-box__rows");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "shipping_method_add",
                    "shipping_method": shippingMethod,
                    "shipping_zone": shippingZone
                },
                beforeSend: function () {
                    makeLoading(dynamicButton);
                },
                error: function () {},
                success: function (result) {
                    dynamicButton.remove();
                    rows.prepend(result);
                    removeLoading(dynamicButton);
                }
            });
        });

        $(".shipping-method-remove").click(function(e) {
            e.preventDefault();

            const dynamicButton = $(this);
            const row = $(this).closest(".eso-box__row");
            const shippingMethod = dynamicButton.data("shipping-method");
            const shippingZone = dynamicButton.closest(".eso-box__rows").data("shipping-zone");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "shipping_method_remove",
                    "shipping_method": shippingMethod,
                    "shipping_zone": shippingZone
                },
                beforeSend: function() {
                    makeLoading(dynamicButton);
                },
                error: function() {

                },
                success: function() {
                    removeLoading(dynamicButton);
                    row.remove();
                }
            });
        });

        function makeLoading(element) {
            element.prop("disabled", true);
            element.prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        }

        function removeLoading(element) {
            element.prop("disabled", false);
            element.find(".spinner-border").remove();
        }

    });

})(jQuery);

/**
 * Show bootstrap custom file input placeholder when file is selected
 * @since 2020.1.5
 */
function getFileInputPlaceholder() {
    // var value = jQuery(this).val().replace('C:\\fakepath\\', '').trim();
    // jQuery(this).next(".custom-file-label").html(value);
}