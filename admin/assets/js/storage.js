(function ($) {

    $(function () {

        $(".render-storage-log").click(function (e) {
            e.preventDefault();

            const product_id = $(this).data("product");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "render_storage_log",
                    "product": product_id
                },
                beforeSend: function () {

                },
                error: function (result) {
                    console.log(result.responseText);
                },
                success: function (result) {
                    console.log(result);
                    $("body").append(result);
                    $("#log-" + product_id).modal();
                }
            });
        });

        $(".do-storageorder").click(function () {
            $("input[name='storageorder-supplier']").attr("required", "required");

            $(this).closest(".inline-card-wrapper").hide();
            $(".storageorder-options").show();
        });

        function makeLoading(element) {
            element.prop("disabled", true);
            element.prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        }

        function removeLoading(element) {
            element.prop("disabled", false);
            element.find(".spinner-border").remove();
        }

    });

})(jQuery);