(function ($) {

    $(function () {

         $(".wp-admin.edit-php").find(".page-title-action").wrap("<div style='text-align: right;'></div>").css("opacity", 1);

        const status = $("#wp-admin-bar-status");

        $(document).on("keydown", ".admin-ajax-form--no-enter :input:not(textarea)", function (event) {
            if (event.key == "Enter") {
                event.preventDefault();
            }
        });

        $(document).on("submit", ".admin-ajax-form", function (e) {
            e.preventDefault();

            processAdminAjaxForm($(this));
        });

        $(document).on("change", ".admin-ajax-form--onchange", function () {
            processAdminAjaxForm($(this));
        });

        function processAdminAjaxForm(thisForm) {
            if (thisForm.attr("data-confirm")) {
                let can_continue = confirm(thisForm.data("confirm"));

                if (can_continue === false) {
                    return false;
                }
            }

            const submitButton = thisForm.find("button[type='submit']");
            const successRedirect = thisForm.find("input[name='eso_success_redirect']");
            const successCallback = thisForm.find("input[name='eso_success_callback']");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                processData: false,
                contentType: false,
                data: new FormData(thisForm[0]),
                beforeSend: function () {
                    status.html(php_vars["_saving"]).addClass("text-warning");
                    makeLoading(submitButton);
                },
                complete: function () {
                },
                error: function (result) {
                    status.html(php_vars["_error"]).removeClass("text-warning").addClass("text-danger");
                },
                success: function (result) {
                    status.html(php_vars["_saved"]).removeClass("text-warning").removeClass("text-danger").addClass("text-success");
                    if (successCallback.length) {
                        processAdminAjaxFormSuccessCallback(thisForm);
                    }

                    if (successRedirect.length) {
                        window.location.replace(successRedirect.val());
                    } else {
                        removeLoading(submitButton);
                    }
                }
            });
        }

        function processAdminAjaxFormSuccessCallback(thisForm) {
            const formData = thisForm.serialize() + "&do_success_callback";
            const callbackTarget = $(thisForm.find("input[name='eso_callback_target']").val());

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: formData,
                beforeSend: function () {
                    makeLoading(callbackTarget);
                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    callbackTarget.html(result);
                    removeLoading(callbackTarget);
                }
            });
        }

        $(".admin-ajax--onready").each(function () {
            processAdminAjax($(this));
        });

        function processAdminAjax(thisForm) {
            const ajaxResult = thisForm.find(".admin-ajax-result");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: thisForm.serialize(),
                beforeSend: function () {
                    makeLoading(ajaxResult);
                },
                complete: function () {
                },
                error: function (result) {
                    console.log(result);
                },
                success: function (result) {
                    removeLoading(ajaxResult);
                    ajaxResult.html(result);
                }
            });
        }

        $(".dynamic-edit").click(function () {

            const formDynamic = $(this).closest(".form-dynamic");

            if (formDynamic.hasClass("editable")) {
                formDynamic.find("input, select").attr("readonly", true);
                formDynamic.find("button[type='submit']").addClass("invisible");
                formDynamic.removeClass("editable");
            } else {
                formDynamic.find("input, select").attr("readonly", false);
                formDynamic.find("button[type='submit']").removeClass("invisible");
                formDynamic.addClass("editable");
            }
        });

        $('[data-toggle="tooltip"]').tooltip({
            placement: 'bottom',
            container: 'body'
        });

        const quick_look = $('[data-toggle="quick-look"]');
        /**
         * @since 2019.6
         */
        quick_look.on("hover", function () {
            if ($(this).parent().find(".quick-look").length) {
                $(this).parent().find(".quick-look").remove();
            }

            $(this).after("<div class='quick-look'></div>");

            const modal = $(this).parent().find(".quick-look");

            $.ajax({
                url: ajaxurl,
                method: "POST",
                data: {
                    "action": "eso_admin_ajax",
                    "eso_action": "the_quick_look",
                    "type": $(this).data("type"),
                    "id": $(this).data("id"),
                },
                beforeSend: function () {
                    makeLoading(modal);
                },
                success: function (result) {
                    modal.html(result);
                    removeLoading(modal);
                }
            });
        });

        /**
         * @since 2019.7
         */
        quick_look.on("mouseleave", function () {
            $(this).parent().find(".quick-look").remove()
        });

        $(document).on("click", ".quick-look-close", function () {
            $(this).closest(".quick-look").remove();
        });

        // Since we dont use currency switcher it seems this can be removed anytime
        // $(".currency-switch").click(function () {
        //     var currency = $(this).data("currency");
        //
        //     $.ajax({
        //         url: php_vars["ajaxurl"],
        //         method: "POST",
        //         data: {
        //             "action": "eso_ajax",
        //             "eso_action": "currency_switch",
        //             "eso-currency-code": currency,
        //         },
        //         error: function () {
        //             $(".currency-switcher-list").append(php_vars["_error"]);
        //         },
        //         success: function (result) {
        //             location.reload();
        //         }
        //     })
        // });

        $(".index-filter button[type='submit'], .index-filter-reset").click(function () {
            $(this).prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        });

        $("#bulk-select").click(function () {
            if ($(this).is(":checked")) {
                $("input[name='items[]']").prop("checked", true);
            } else {
                $("input[name='items[]']").prop("checked", false);
            }
        });

        /**
         * @since 2020.1.21
         */
        let typeDate = $('[type="date"]');
        if ( typeDate.prop('type') !== 'date' ) {
            let placeholder = typeDate.attr("placeholder");
            typeDate.attr("placeholder", placeholder + " (YY-MM-DD)");
        }

        function makeLoading(element) {
            element.prop("disabled", true);
            element.prepend('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
        }

        function removeLoading(element) {
            element.prop("disabled", false);
            element.find(".spinner-border").remove();
        }

    });

})
(jQuery);

