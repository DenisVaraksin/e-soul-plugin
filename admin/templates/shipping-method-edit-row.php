<?php
/* @var $this Eso_Shipping_Method */
/* @var $shipping_zone Eso_Shipping_Zone */
?>
<div class="eso-box__row">
	<div class="eso-box__row-title">
		<?php echo $this->get_name(); ?>
	</div>
	<div class="eso-box__row-actions">
        <a href="admin.php?page=eso-shipping-method-options&shipping-method=<?php echo $this->get_code() ?>&shipping-zone=<?php echo $shipping_zone->get_code() ?>" class="shipping-method-edit" data-shipping-method="<?php echo $this->get_code() ?>"><?php echo eso_icon( "edit-interface-sign", "eso-box__icon" ) ?></a>
        <a href="" class="shipping-method-remove" data-shipping-method="<?php echo $this->get_code() ?>"><?php echo eso_icon( "trash", "eso-box__icon" ) ?></a>
	</div>
</div>