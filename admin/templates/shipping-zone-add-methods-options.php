<?php /* @var $this Eso_Shipping_Zone */ ?>
<div class="eso-box__row--add">
    <div class="eso-box__row-title">
        <button class="add-shipping-method btn btn-light btn-plus" type="button" data-toggle="collapse"
                data-target="#insertable-shipping-methods-<?php echo $this->get_code() ?>"
                aria-expanded="false"
                aria-controls="insertable-shipping-methods-<?php echo $this->get_code() ?>"><?php _e( "Přidat metodu dopravy", "eso" ) ?></button>
    </div>
    <div class="eso-box__list collapse pt-3" id="insertable-shipping-methods-<?php echo $this->get_code() ?>">
        <h5><?php _e( "Vyberte z metod dopravy", "eso" ) ?></h5>
		<?php
		/* @var $shipping_method Eso_Shipping_Method */
		foreach ( $this->get_insertable_shipping_methods() as $shipping_method ) : ?>
            <button class="btn btn-secondary btn-plus shipping-method-add d-block mb-2 text-left"
                    data-shipping-method="<?php echo $shipping_method->get_code() ?>"
                    data-shipping-zone="<?php echo $this->get_code() ?>">
	            <?php if($shipping_method->get_icon()) :
		            $shipping_method->render_icon();
	            endif; ?>
                <?php echo $shipping_method->get_name() ?>
            </button>
		<?php endforeach; ?>
    </div>
</div>