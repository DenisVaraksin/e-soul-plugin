<?php /* @var $this Eso_Shipping_Zone */ ?>
<div class="col-xl-4 col-lg-6 shipping-zone-edit-col mb-default">
    <div class="whitebox uibox eso-box eso-box--shipping-zone">
        <div class="eso-box__title">
            <h3 class="eso-box__heading"><?php echo $this->get_name(); ?></h3>
            <button class="shipping-zone-remove float-right btn btn-link"
                    data-shipping-zone="<?php echo $this->get_code() ?>"><?php echo eso_icon( "trash", "eso-box__icon" ) ?></button>
        </div>
        <h4><?php _e( "Metody dopravy", "eso" ) ?></h4>
        <div class="eso-box__rows" data-shipping-zone="<?php echo $this->get_code() ?>">
			<?php
			if ( ! empty( $this->get_shipping_methods() ) ) {
				/* @var $shipping_method Eso_Shipping_Method */
				foreach ( $this->get_shipping_methods() as $shipping_method_code => $shipping_method_prices ) :
					$shipping_method = new Eso_Shipping_Method( $shipping_method_code );
					$shipping_method->render_edit_row( $this );
					?>
				<?php endforeach;
			}
			if ( $this->has_insertable_shipping_methods() ) :
				$this->render_add_methods_options();
			endif; ?>
        </div>
    </div>
</div>