<?php
add_action( 'admin_menu', 'eso_custom_menu' );

function eso_custom_menu() {

	/**
	 * Dashboard
	 */
	add_menu_page( __("Dashboard", "eso"), __("Dashboard", "eso"), 'install_plugins', 'eso-dashboard', 'eso_plugin_dashboard_page', ESO_URL . "/assets/img/logo.svg", 1 );
	add_menu_page( __("Dashboard", "eso"), __("Dashboard", "eso"), 'install_plugins', 'eso-dashboard', 'eso_plugin_dashboard_page', '', 2 );

	/**
	 * Obchod
	 */
	add_menu_page( __( "Obchod", "eso" ), __( "Obchod", "eso" ), 'install_plugins', "eso-main-options", 'eso_plugin_settings_general', ESO_URL . 'assets/img/icons/menu/store.svg', 45 );
	add_submenu_page( "eso-main-options", __( "Napojení API", "eso" ), __( "Napojení API", "eso" ), 'install_plugins', 'eso-api-options', 'eso_plugin_settings_api' );
	add_submenu_page( "eso-main-options", __( "Doprava a platby", "eso" ), __( "Doprava a platby", "eso" ), 'install_plugins', 'eso-payments-options', 'eso_plugin_settings_payments' );
	add_submenu_page( "eso-main-options", __( "Faktury", "eso" ), __( "Faktury", "eso" ), 'install_plugins', 'eso-invoices-options', 'eso_plugin_settings_invoices' );
	add_submenu_page( "eso-main-options", __( "Slevové kupony", "eso" ), __( "Slevové kupony", "eso" ), 'install_plugins', 'eso-coupons-options', 'eso_plugin_settings_coupons' );
	add_submenu_page( "eso-main-options", __( "Newsletter", "eso" ), __( "Newsletter", "eso" ), 'install_plugins', 'eso-newsletter-options', 'eso_plugin_settings_newsletter' );
	add_submenu_page( "eso-main-options", __( "Pokročilé", "eso" ), __( "Pokročilé", "eso" ), 'install_plugins', 'eso-advanced-options', 'eso_plugin_settings_advanced' );

	add_submenu_page( 'eso-payments-options', __( "Nastavení metody platby", "eso" ), __( "Nastavení metody platby", "eso" ), 'install_plugins', 'eso-shipping-method-options', 'eso_plugin_settings_shipping_method' );
	add_submenu_page( 'eso-coupon-edit', __( "Upravit slevový kupon", "eso" ), __( "Upravit slevový kupon", "eso" ), 'install_plugins', 'eso-coupon-edit', 'eso_plugin_coupon_edit_page' );


	/**
	 * Customers
	 */
	add_menu_page( __( "Zákazníci", "eso" ), __( "Zákazníci", "eso" ), 'install_plugins', 'eso-customers', 'eso_customer_index', ESO_URL . 'assets/img/icons/menu/customers.svg', 45 );
	add_submenu_page( 'eso-customers', __( "Seznam zákazníků", "eso" ), __( "Seznam zákazníků", "eso" ), 'install_plugins', 'eso-customers', 'eso_customer_index' );
	add_submenu_page( 'eso-customers', __( "Skupiny", "eso" ), __( "Skupiny", "eso" ), 'install_plugins', 'eso-customer-groups', 'eso_customer_groups_page' );

	if(isset($_GET["customer-id"])) {
		$customer = new Eso_Customer( (int) $_GET["customer-id"]);
		$title = $customer->get_full_name();
	} else {
		$title = __("Detail zákazníka", "eso");
	}

	add_submenu_page( 'eso-customers', $title, $title, 'install_plugins', 'eso-customer-detail', 'eso_customer_detail_page' );

	/**
	 * Products
	 */
	add_menu_page( __( "Produkty", "eso" ), __( "Produkty", "eso" ), 'install_plugins', 'eso-products', 'eso_products_page', ESO_URL . 'assets/img/icons/menu/products.svg', 25 );
	add_submenu_page( "eso-products", __( "Seznam produktů", "eso" ), __( "Seznam produktů", "eso" ), 'install_plugins', 'eso-products', 'eso_products_page' );

	if(isset($_GET["page"]) && $_GET["page"] == "single-product" && isset($_GET["id"])) {
		$product = new Eso_Product( (int) $_GET["id"]);
		$title = $product->get_name();
	} else {
		$title = __("Nový produkt", "eso");
	}
	add_submenu_page("eso-products", $title, __("Nový produkt", "eso"), "install_plugins", "single-product", "eso_plugin_products_single");
	add_submenu_page("eso-products", __("Sklad", "eso"), __("Sklad", "eso"), "install_plugins", "products-storage-options", "eso_plugin_products_storage");
	add_submenu_page("eso-products", __("Štítky", "eso"), __("Štítky", "eso"), "install_plugins", "products-tags", "eso_plugin_products_tags_page");
	add_submenu_page("eso-products", __("Kategorie", "eso"), __("Kategorie", "eso"), "install_plugins", "products-categories", "eso_plugin_products_categories_page");
	add_submenu_page("eso-products", __("Stavy", "eso"), __("Stavy", "eso"), "install_plugins", "products-stock-statuses", "eso_plugin_products_stock_statuses_page");

	/**
	 * Objednávky
	 */
	add_menu_page( __( "Objednávky", "eso" ), __( "Objednávky", "eso" ), 'install_plugins', 'eso-orders', 'eso_orders_page', ESO_URL . 'assets/img/icons/menu/orders.svg', 25 );
	add_submenu_page("eso-orders", __("Seznam objednávek", "eso"), __("Seznam objednávek", "eso"), "install_plugins", "eso-orders", "eso_orders_page");

	if(isset($_GET["page"]) && $_GET["page"] == "single-order" && isset($_GET["id"])) {
		$order = new Eso_Order( (int) $_GET["id"]);
		$title = $order->get_title();
	} else {
		$title = __("Detail objednávky", "eso");
	}
	add_submenu_page("eso-orders", $title, $title, "install_plugins", "single-order", "eso_plugin_orders_single");
	add_submenu_page("eso-orders", __("Stavy objednávek", "eso"), __("Stavy", "eso"), "install_plugins", "orders-status", "eso_plugin_orders_statuses_page");

}