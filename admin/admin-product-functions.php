<?php
//This can be deleted 2019.8
//function eso_product_entity() {
//	add_meta_box(
//		'eso_product_main_box',
//		'Produkt',
//		'eso_product_main_box',
//		'esoul_product',
//		'normal',
//		'high'
//	);
//
//	add_meta_box(
//		'eso_product_sidebox',
//		'Produktové fotografie',
//		'eso_product_sidebox',
//		'esoul_product',
//		'side',
//		'high'
//	);
//
//}
//
//add_filter( 'postbox_classes_esoul_product_eso_product_main_box', 'eso_add_metabox_classes' );
//add_filter( 'postbox_classes_esoul_product_eso_product_sidebox', 'eso_add_metabox_classes' );
//
//function eso_add_metabox_classes( $classes ) {
//	array_push( $classes, 'postbox-esoul' );
//
//	return $classes;
//}

function eso_product_save_button( $post ) {
	if ( $post->post_type == "esoul_product" ) :
		?>
        <div class="eso-post-actions">
			<?php
			$screen = get_current_screen();

			if ( $screen->action == "add" ) {
				$button_text = __( 'Publish' );
			} else {
				$button_text = __( 'Update' ); ?>

                <a href="<?php echo get_the_permalink() ?>?preview=true" target="_blank"
                   class="button button-large"><?php _e( "Zobrazit na webu", "eso" ) ?></a>
				<?php
			}

			?>
            <input type="submit" name="save" class="button button-primary button-large"
                   value="<?php echo $button_text; ?>"/>
        </div>
	<?php endif;
}

add_action( 'edit_form_top', 'eso_product_save_button' );

/**
 * @param $data
 *
 * @since 2019.6 We are not saving prices as an array but with price_CURRENCY format
 * @since 2020.2.1 Add color and symbol_id fields, remove content field
 */
function eso_save_product_data( $data ) {
	if ( ! current_user_can( "install_plugins" ) ) {
		write_log( "Unauthorized access" );

		return;
	}

	if ( $data["id"] == "new_post" ) {
		$postarr = [
			"post_title"   => $data["name"],
			"post_content" => "",
			"post_type"    => "esoul_product",
			"post_status"  => "publish"
		];

		$post_id = wp_insert_post( $postarr );

		$product = new Eso_Product( $post_id );

		$product->add_stock_amount( 20 );

	} else {
		$post_id = (int) $data["id"];
	}

	if ( isset( $data['images'] ) ) {
		$product_meta['images'] = $data['images'];
	} else {
		$product_meta['images'] = false;
	}

	foreach ( $data['price'] as $symbol => $price ) {
		$product_meta[ 'price_' . $symbol ] = $price;
	}

	$product_meta['symbol_image_id'] = $data['symbol_image_id'];
	$product_meta['color']           = $data['color'];
    $product_meta['ean_code']        = $data['ean_code'];
	$product_meta['tax']             = $data['tax'];
	$product_meta['discount_type']   = $data['discount_type'];
	$product_meta['discount']        = $data['discount'];
	$product_meta['perex']           = esc_textarea( $data['perex'] );

	foreach ( $product_meta as $key => $value ) :
		if ( get_post_meta( $post_id, $key, false ) ) {
			update_post_meta( $post_id, $key, $value );
		} else {
			add_post_meta( $post_id, $key, $value );
		}
		if ( ! $value ) {
			delete_post_meta( $post_id, $key );
		}
	endforeach;

	//Product visibility, product title
	$product_visibility = sanitize_text_field( $_POST['visibility'] );
	$product_title      = sanitize_text_field( $_POST['name'] );
	$product_slug       = sanitize_text_field( $_POST['slug'] );

	remove_action( 'save_post_esoul_product', 'eso_save_product_data', 1 );

	wp_update_post( [
		'ID'          => $post_id,
		'post_status' => $product_visibility,
		'post_title'  => $product_title,
		'post_name'   => $product_slug
	] );

	add_action( 'save_post_esoul_product', 'eso_save_product_data', 1, 2 );

	//Save taxonomies
	$stock_status = (int) $_POST['stock_status'];

	wp_set_post_terms( $post_id, $stock_status, 'stock_status', false );

//	test_log(sanitize_text_field( $_POST["product_category"] ));
	//categories
	if ( ! empty( $_POST["product_category"] ) ) {
	    $categoriesNames = explode(",", $_POST["product_category"]);
	    $categories = [];
	    foreach($categoriesNames as $categoryName) {
            $category = get_term_by("name", $categoryName, 'product_category');

            if($category) {
                $categories[] = $category->term_id;
            }
        }

	    if(!empty($categories)) {
		    wp_set_post_terms( $post_id, $categories, 'product_category', false );
        }
	} else {
		wp_set_post_terms( $post_id, null, 'product_category', false );
	}

	if ( ! empty( $_POST["product_tag"] ) ) {
		wp_set_post_terms( $post_id, sanitize_text_field( $_POST["product_tag"] ), 'product_tag', false );
	} else {
		wp_set_post_terms( $post_id, null, 'product_tag', false );
	}
}

add_action( 'save_post_esoul_product', 'eso_save_product_data', 1, 2 );

/**
 * @param $post_id
 *
 * @since 2019.5
 */
function eso_product_save_post_action( $post_id ) {
	if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
		return;
	}

	if ( eso_is_comgate_elogist_active() ) {
		$comgate_elogist = new Eso_Comgate_Elogist();
		$comgate_elogist->update_product( $post_id );
	}
}

add_action( "save_post_esoul_product", "eso_product_save_post_action" );

/**
 * @param $product_id
 *
 * @since 2019.7
 * @TODO move to Eso_Product
 */
function eso_render_available_related_products( $product_id ) {
	$product = new Eso_Product( $product_id );

	if ( is_array( $product->get_related_products_array() ) ) {
		$not_in = array_merge( $product->get_related_products_array(), [ $product_id ] );
	} else {
		$not_in = [ $product_id ];
	}

	$args = [
		"post_type"      => "esoul_product",
		"posts_per_page" => - 1,
		"post__not_in"   => $not_in
	];

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) { ?>
        <table class="table table-striped text-nowrap">
			<?php
			while ( $query->have_posts() ) {
				$query->the_post();
				$product = new Eso_Product( get_the_ID() );
				$product->admin_render_row_item();
			}
			?>
        </table>
		<?php
		wp_reset_postdata();
	}
}



