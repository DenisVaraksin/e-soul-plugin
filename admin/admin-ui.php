<?php
function eso_admin_styles_and_scripts() {
	$page    = get_current_screen();
	$page_id = $page->id;

	if ( $page_id == "customize" ) {
		return;
	}


	wp_enqueue_style( 'eso-admin', ESO_URL . 'admin/assets/css/admin.css', array(), ESO_VERSION, 'all' );

	//Reset styles
	wp_enqueue_style( 'eso-reset', ESO_URL . 'admin/assets/css/reset/reset.css', array(), ESO_VERSION, 'all' );
	if(is_plugin_active('polylang/polylang.php')) {
		wp_enqueue_style( 'eso-polylang', ESO_URL . 'admin/assets/css/reset/polylang.css', array(), ESO_VERSION, 'all' );
	}

	wp_enqueue_style( 'eso-components', ESO_URL . 'admin/assets/css/components.css', array(), ESO_VERSION, 'all' );
	wp_enqueue_style( 'eso-components-sm', ESO_URL . 'admin/assets/css/components-sm.css', array(), ESO_VERSION, '(min-width: 768px)' );

	if ( $GLOBALS['pagenow'] != 'wp-login.php' ) {
		wp_enqueue_media();

		wp_enqueue_script( 'popper', "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js", array( 'jquery' ), ESO_VERSION, true );

		wp_enqueue_script( 'bsjs', "//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js", array( 'jquery' ), ESO_VERSION, true );

		wp_enqueue_script( 'eso-admin-js', ESO_URL . 'admin/assets/js/admin.min.js', array( 'jquery' ), ESO_VERSION, true );

		if ( get_post_type() == "esoul_product" || $page_id == "produkty_page_single-product" || $page_id == "toplevel_page_eso-products" ) {
			wp_enqueue_script( 'eso-product-js', ESO_URL . 'admin/assets/js/product.min.js', array( 'jquery' ), ESO_VERSION, true );

			wp_enqueue_style( 'eso-product-css', ESO_URL . 'admin/assets/css/product.css', array(), ESO_VERSION, 'all' );
		}

		if ( $page_id == "produkty_page_single-product" || $page_id == "products_page_single-product" ) {

			wp_enqueue_script( 'charts-js', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js', array( 'jquery' ), ESO_VERSION, true );
			wp_enqueue_style( 'charts-css', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css', array(), ESO_VERSION, 'all' );

			//Tagator for multiple tags select
			wp_enqueue_style( 'tagator', ESO_URL . 'admin/assets/css/packages/fm.tagator.jquery.css', array( ), ESO_VERSION, 'all' );
			wp_enqueue_script( 'tagator', ESO_URL . 'admin/assets/js/packages/fm.tagator.jquery.min.js', array( 'jquery' ), ESO_VERSION, true );

			wp_enqueue_script( 'eso-single-product-js', ESO_URL . 'admin/assets/js/single-product.min.js', array( 'jquery' ), ESO_VERSION, true );
		}

		if ( $page_id == "produkty_page_products-storage-options" ) {
			wp_enqueue_script( 'eso-storage-js', ESO_URL . 'admin/assets/js/storage.min.js', array( 'jquery' ), ESO_VERSION, true );
		}

		if ( $page_id == "obchod_page_eso-api-options" ) {
			wp_enqueue_script( 'eso-settings-api', ESO_URL . 'admin/assets/js/settings-api.min.js', array( 'jquery' ), ESO_VERSION, true );
		}

		if ( isset( $GLOBALS["_GET"]["page"] ) ) {
			wp_enqueue_script( 'eso-settings-js', ESO_URL . 'admin/assets/js/settings.min.js', array( 'jquery' ), ESO_VERSION, true );
		}

		$data = array(
			'_done'           => __( "Dokončeno", "eso" ),
			'_saved'          => __( "Uloženo", "eso" ),
			'_saving'         => __( "Ukládám...", "eso" ),
			'_error'          => __( "Chyba ukládání", "eso" ),
			'site_url'        => get_site_url(),
			'ajaxurl'         => admin_url( 'admin-ajax.php' ),
			'current_page_id' => get_the_ID(),
			'media_url'        => get_home_url() . "/wp-json/wp/v2/media/"
		);

		if ( $page_id == "toplevel_page_eso-dashboard" ) {
			wp_enqueue_script( 'charts-js', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js', array( 'jquery' ), ESO_VERSION, true );
			wp_enqueue_style( 'charts-css', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css', array(), ESO_VERSION, 'all' );

			wp_enqueue_script( 'eso-dashboard-js', ESO_URL . 'admin/assets/js/dashboard.min.js', array( 'jquery' ), ESO_VERSION, true );
		}

		wp_localize_script( 'eso-admin-js', 'php_vars', $data );

		wp_enqueue_style( 'font', "//fonts.googleapis.com/css?family=Muli:300,400,600,700,800&subset=latin-ext" );

		if ( get_post_type() == "esoul_product" || $page_id == "objednavky_page_single-order" || $page_id == "toplevel_page_eso-orders" ) {
			wp_enqueue_script( 'esoul-single-order', ESO_URL . 'assets/js/order/single.min.js', array( 'jquery' ), ESO_VERSION, true );
		}
	}
}

if ( ! is_customize_preview() ) {
	add_action( 'admin_enqueue_scripts', 'eso_admin_styles_and_scripts' );
}

// removes admin color scheme options
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

//Removes the leftover 'Visual Editor', 'Keyboard Shortcuts' and 'Toolbar' options.
add_action( 'admin_head', function () {

	ob_start( function ( $subject ) {

		$subject = preg_replace( '#<h[0-9]>' . __( "Personal Options" ) . '</h[0-9]>.+?/table>#s', '', $subject, 1 );

		return $subject;
	} );
} );

//add_action( 'admin_footer', function () {
//	ob_end_flush();
//} );

/**
 * @since 2019.8
 */
function eso_custom_admin_head() { ?>
    <script type='text/javascript'>
        window.smartlook || (function (d) {
            var o = smartlook = function () {
                o.api.push(arguments)
            }, h = d.getElementsByTagName('head')[0];
            var c = d.createElement('script');
            o.api = new Array();
            c.async = true;
            c.type = 'text/javascript';
            c.charset = 'utf-8';
            c.src = 'https://rec.smartlook.com/recorder.js';
            h.appendChild(c);
        })(document);
        smartlook('init', '9ea70896fce6e3ee185d7e76d97d82ae902ecaba');
    </script>
<?php }

add_action( 'admin_head', 'eso_custom_admin_head' );

function eso_create_nav_menu( $menu_name, $menu_items, $menu_location ) {
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	if ( ! $menu_exists ) {
		$menu_id = wp_create_nav_menu( $menu_name );

		foreach ( $menu_items as $menu_item ) {
			wp_update_nav_menu_item( $menu_id, 0, $menu_item );
		}

		$locations = get_theme_mod( 'nav_menu_locations' );

		$locations[ $menu_location ] = $menu_id;
		set_theme_mod( 'nav_menu_locations', $locations );
	}
}

add_filter( 'get_user_option_screen_layout_esoul_order', function () {
	return 1;
} );