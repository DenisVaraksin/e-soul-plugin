<?php
/**
 * @param $wp_admin_bar
 *
 * @since 0.0.23
 */
// add content to the WP Toolbar
function eso_customize_wpadminbar( $wp_admin_bar ) {
	//First remove default nodes
	$wp_admin_bar->remove_node( 'wp-logo' );
	$wp_admin_bar->remove_node( 'site-name' );
	$wp_admin_bar->remove_node( 'comments' );
	$wp_admin_bar->remove_node( 'new-content' );
	$wp_admin_bar->remove_node( 'archive' );
	$wp_admin_bar->remove_node( 'my-account' );

	//Create eSoul ones
	$args = [
		'id'    => 'breadcrumb',
		'title' => eso_admin_breadcrumb(),
		"meta"  => [
			"class" => ""
		]
	];
	$wp_admin_bar->add_node( $args );

	$args = [
		"id"     => "logout",
		"title"  => __( "Odhlásit se", "eso" ),
		"href"   => wp_logout_url(),
		"parent" => "current-user",
		"meta"   => [
			"class" => "float-right"
		]
	];
	$wp_admin_bar->add_node( $args );

	$current_user = wp_get_current_user();
	$args         = [
		'id'    => 'current-user',
		'title' => $current_user->display_name,
		'href'  => '#',
		"meta"  => [
			"class" => "float-right"
		]
	];
	$wp_admin_bar->add_node( $args );

	$args = [
		'id'    => 'current-user-label',
		'title' => __( "Přihlášený uživatel: ", "eso" ),
		'href'  => '',
		"meta"  => [
			"class" => "float-right"
		]
	];
	$wp_admin_bar->add_node( $args );

	$args = [
		'id'    => 'visit-store',
		'title' => eso_icon( "house-outline" ) . __( "Zobrazit e-shop", "eso" ),
		'href'  => home_url(),
		"meta"  => [
			"class" => "float-right"
		]
	];
	$wp_admin_bar->add_node( $args );

	$screen = get_current_screen();

	if ( $screen->id == "toplevel_page_eso-products" ) {
		$args = [
			'id'    => 'language-switcher',
			'title' => eso_render_currency_switcher(),
			"meta"  => [
				"class" => "float-right"
			]
		];
		$wp_admin_bar->add_node( $args );
	}

	$args = [
		'id'    => 'status',
		'title' => '',
		'href'  => '',
		'meta'  => [
			'class' => 'status-label float-right'
		]
	];
	$wp_admin_bar->add_node( $args );

}

add_action( 'admin_bar_menu', 'eso_customize_wpadminbar', 999 );
